<?php
  $u = new User();
  $c = Page::getCurrentPage();
?>
<!doctype html>
<html lang="nl">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  <?php Loader::element('header_required') ?>
  <link href="<?php echo $view->getThemePath() ?>/assets/css/style.css" type="text/css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,700" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="<?php echo $view->getThemePath() ?>/assets/js/app.js"></script>
  
</head>
<body<?php if ($u->isLoggedIn()):?> style="margin-top: 48px;"<?php endif; ?>>

  <div class="page"><!-- Start Page -->
    <div class="page__wrap"><!-- Start Page Wrap -->

      <!-- Header -->
      
      <header class="header header--fixed background__black--transparent">
        <div class="grid__container">
          <div class="grid__row">
            <div class="grid__col--4 col-centered">
              <div class="header__logo">
              <?php
                  $a = new GlobalArea('Logo');
                  $a->display($c);
                ?>
              </div>
            </div>
            <div class="header__nav">
              <div class="grid__col--7 col-centered">
                <nav class="header__nav--items" <?php if ($c->isEditMode()): ?>style="width: 100%;"<?php endif; ?>>
                <?php
                  $a = new GlobalArea('Navigation');
                  $a->display($c);
                ?>
                </nav>
                <a class="header__navicon float--right" href="#">
                  <div class="header__navicon--bars"></div>
                </a>
              </div>
              <div class="grid__col--2 social--btns">
                <?php
                  $a = new GlobalArea('Social media');
                  $a->display($c);
                ?>
              </div>
            </div>
          </div>
        </div>
      </header>
      
      <section class="hero--slider">
        <?php
          $a = new Area('Hero slider');
          $a->display($c);
        ?>
      </section>

