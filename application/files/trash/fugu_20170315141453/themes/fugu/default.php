<?php
  Loader::packageElement('header', 'fugu');
  /*$image = $c->getAttribute('page_image');
  if ($image) {
    $image_src = $image->getRelativePath();
    $image_title = $image->getTitle();
  } else {
    $image_src = $view->getThemePath() . '/assets/images/placeholder.png';
  }*/
?>

  <section class="introduction">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--7 col-centered">
          <?php
            $a = new Area('Introduction block');
            $a->display($c);
          ?>
        </div>
      </div>
    </div>
  </section>

  <div class="section__body">
    <section class="section">
      <div class="grid__container">
        <div class="grid__row">
          <div class="grid__col--6 grid__col--sm--12 section__events--img">
            <?php
              $a = new Area('Events images');
              $a->display($c);
            ?>
          </div>
          <div class="grid__col--4 grid__col--sm--12 grid__shift--1 section__events--txt">
            <?php
              $a = new Area('Events text');
              $a->display($c);
            ?>
          </div>
        </div>
      </div>
    </section>

    <section class="section">
      <div class="grid__container">
        <div class="grid__row">
          <div class="grid__col--5 grid__col--sm--12 section__productions--txt">
            <?php
              $a = new Area('Producties text');
              $a->display($c);
            ?>
          </div>
          <div class="grid__col--7 grid__col--sm--12 section__productions--img">
            <?php
              $a = new Area('Producties image');
              $a->display($c);
            ?>
          </div>
        </div>
      </div>
    </section>

    <section class="section">
      <div class="grid__container">
        <div class="grid__row">
          <div class="grid__col--12 section__updates--slider">
            <?php
              $a = new Area('Updates slider');
              $a->display($c);
            ?>
          </div>
        </div>
      </div>
    </section>

    <section class="section">
      <div class="grid__container">
        <div class="grid__row">
          <div class="grid__col--6 grid__col--sm--12 section__friends--img">
            <?php
              $a = new Area('Friends image');
              $a->display($c);
            ?>
          </div>
          <div class="grid__col--6 grid__col--sm--12 section__friends-txt">
            <?php
              $a = new Area('Friends text');
              $a->display($c);
            ?>
          </div>
        </div>
      </div>
    </section>

    <section class="section">
      <div class="grid__container">
        <div class="grid__row">
          <div class="grid__col--6 grid__col--sm--12 section__about--img">
            <?php
              $a = new Area('About image');
              $a->display($c);
            ?>
          </div>
          <div class="grid__col--6 grid__col--sm--12 section__about-txt">
            <?php
              $a = new Area('About text');
              $a->display($c);
            ?>
          </div>
        </div>
      </div>
    </section>
  </div>

<?php Loader::packageElement('footer', 'fugu'); ?>
