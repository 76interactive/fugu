-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 16 mrt 2017 om 12:33
-- Serverversie: 10.1.19-MariaDB
-- PHP-versie: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Fugu`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AreaLayoutColumns`
--

CREATE TABLE `AreaLayoutColumns` (
  `arLayoutColumnID` int(10) UNSIGNED NOT NULL,
  `arLayoutID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arLayoutColumnIndex` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arLayoutColumnDisplayID` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AreaLayoutCustomColumns`
--

CREATE TABLE `AreaLayoutCustomColumns` (
  `arLayoutColumnID` int(10) UNSIGNED NOT NULL,
  `arLayoutColumnWidth` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AreaLayoutPresets`
--

CREATE TABLE `AreaLayoutPresets` (
  `arLayoutPresetID` int(10) UNSIGNED NOT NULL,
  `arLayoutID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arLayoutPresetName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AreaLayouts`
--

CREATE TABLE `AreaLayouts` (
  `arLayoutID` int(10) UNSIGNED NOT NULL,
  `arLayoutSpacing` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arLayoutIsCustom` tinyint(1) NOT NULL DEFAULT '0',
  `arLayoutIsPreset` tinyint(1) NOT NULL DEFAULT '0',
  `arLayoutMaxColumns` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arLayoutUsesThemeGridFramework` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AreaLayoutsUsingPresets`
--

CREATE TABLE `AreaLayoutsUsingPresets` (
  `arLayoutID` int(10) UNSIGNED NOT NULL,
  `preset` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AreaLayoutThemeGridColumns`
--

CREATE TABLE `AreaLayoutThemeGridColumns` (
  `arLayoutColumnID` int(10) UNSIGNED NOT NULL,
  `arLayoutColumnSpan` int(10) UNSIGNED DEFAULT '0',
  `arLayoutColumnOffset` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AreaPermissionAssignments`
--

CREATE TABLE `AreaPermissionAssignments` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AreaPermissionBlockTypeAccessList`
--

CREATE TABLE `AreaPermissionBlockTypeAccessList` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AreaPermissionBlockTypeAccessListCustom`
--

CREATE TABLE `AreaPermissionBlockTypeAccessListCustom` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `btID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Areas`
--

CREATE TABLE `Areas` (
  `arID` int(10) UNSIGNED NOT NULL,
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arOverrideCollectionPermissions` tinyint(1) NOT NULL DEFAULT '0',
  `arInheritPermissionsFromAreaOnCID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arIsGlobal` tinyint(1) NOT NULL DEFAULT '0',
  `arParentID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Areas`
--

INSERT INTO `Areas` (`arID`, `cID`, `arHandle`, `arOverrideCollectionPermissions`, `arInheritPermissionsFromAreaOnCID`, `arIsGlobal`, `arParentID`) VALUES
(1, 127, 'Main', 0, 0, 0, 0),
(2, 128, 'Primary', 0, 0, 0, 0),
(3, 128, 'Secondary 1', 0, 0, 0, 0),
(4, 128, 'Secondary 2', 0, 0, 0, 0),
(5, 128, 'Secondary 3', 0, 0, 0, 0),
(6, 128, 'Secondary 4', 0, 0, 0, 0),
(7, 128, 'Secondary 5', 0, 0, 0, 0),
(8, 145, 'Main', 0, 0, 0, 0),
(9, 146, 'Main', 0, 0, 0, 0),
(10, 1, 'Header Site Title', 0, 0, 1, 0),
(11, 147, 'Main', 0, 0, 0, 0),
(12, 1, 'Header Navigation', 0, 0, 1, 0),
(13, 1, 'Main', 0, 0, 0, 0),
(14, 1, 'Page Footer', 0, 0, 0, 0),
(15, 148, 'Main', 0, 0, 0, 0),
(16, 1, 'Footer Legal', 0, 0, 1, 0),
(17, 149, 'Main', 0, 0, 0, 0),
(18, 1, 'Footer Navigation', 0, 0, 1, 0),
(19, 150, 'Main', 0, 0, 0, 0),
(20, 1, 'Footer Contact', 0, 0, 1, 0),
(21, 142, 'Header Site Title', 0, 0, 1, 0),
(22, 142, 'Header Navigation', 0, 0, 1, 0),
(23, 142, 'Footer Legal', 0, 0, 1, 0),
(24, 142, 'Footer Navigation', 0, 0, 1, 0),
(25, 142, 'Footer Contact', 0, 0, 1, 0),
(26, 151, 'Main', 0, 0, 0, 0),
(27, 1, 'Header Search', 0, 0, 1, 0),
(28, 152, 'Main', 0, 0, 0, 0),
(29, 1, 'Footer Site Title', 0, 0, 1, 0),
(30, 153, 'Main', 0, 0, 0, 0),
(31, 1, 'Footer Social', 0, 0, 1, 0),
(32, 145, 'Header Site Title', 0, 0, 1, 0),
(33, 145, 'Header Navigation', 0, 0, 1, 0),
(34, 145, 'Page Footer', 0, 0, 0, 0),
(35, 145, 'Footer Legal', 0, 0, 1, 0),
(36, 145, 'Footer Navigation', 0, 0, 1, 0),
(37, 145, 'Footer Contact', 0, 0, 1, 0),
(38, 154, 'Main', 0, 0, 0, 0),
(39, 1, 'Navigation', 0, 0, 1, 0),
(40, 1, 'Hero', 0, 0, 0, 0),
(41, 1, 'Typography', 0, 0, 0, 0),
(42, 142, 'Main', 0, 0, 0, 0),
(43, 1, 'Logo', 0, 0, 1, 0),
(44, 155, 'Main', 0, 0, 0, 0),
(45, 162, 'Main', 0, 0, 0, 0),
(46, 1, 'Social media', 0, 0, 1, 0),
(47, 1, 'Hero slider', 0, 0, 0, 0),
(48, 1, 'Introduction block', 0, 0, 0, 0),
(49, 1, 'Events images', 0, 0, 0, 0),
(50, 1, 'Events text', 0, 0, 0, 0),
(51, 1, 'Producties text', 0, 0, 0, 0),
(52, 1, 'Producties image', 0, 0, 0, 0),
(53, 1, 'Updates slider', 0, 0, 0, 0),
(54, 1, 'Friends image', 0, 0, 0, 0),
(55, 1, 'Friends text', 0, 0, 0, 0),
(56, 1, 'About image', 0, 0, 0, 0),
(57, 1, 'About text', 0, 0, 0, 0),
(58, 1, 'Introduction', 0, 0, 0, 0),
(59, 1, 'Events slider', 0, 0, 0, 0),
(60, 1, 'Productions text', 0, 0, 0, 0),
(61, 1, 'Productions img', 0, 0, 0, 0),
(62, 1, 'Friends img', 0, 0, 0, 0),
(63, 1, 'About img', 0, 0, 0, 0),
(64, 1, 'Producties img', 0, 0, 0, 0),
(65, 1, 'Footer header', 0, 0, 0, 0),
(66, 1, 'Footer text', 0, 0, 0, 0),
(67, 1, 'Footer form', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atAddress`
--

CREATE TABLE `atAddress` (
  `avID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_province` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atAddressCustomCountries`
--

CREATE TABLE `atAddressCustomCountries` (
  `atAddressCustomCountryID` int(10) UNSIGNED NOT NULL,
  `akID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `country` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atAddressSettings`
--

CREATE TABLE `atAddressSettings` (
  `akID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `akHasCustomCountries` tinyint(1) NOT NULL DEFAULT '0',
  `akDefaultCountry` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atBoolean`
--

CREATE TABLE `atBoolean` (
  `avID` int(10) UNSIGNED NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `atBoolean`
--

INSERT INTO `atBoolean` (`avID`, `value`) VALUES
(9, 1),
(16, 1),
(18, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(42, 1),
(43, 1),
(47, 1),
(51, 1),
(99, 1),
(107, 1),
(108, 1),
(109, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atBooleanSettings`
--

CREATE TABLE `atBooleanSettings` (
  `akID` int(10) UNSIGNED NOT NULL,
  `akCheckedByDefault` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `atBooleanSettings`
--

INSERT INTO `atBooleanSettings` (`akID`, `akCheckedByDefault`) VALUES
(5, 0),
(6, 0),
(9, 0),
(10, 0),
(11, 0),
(12, 1),
(13, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atDateTime`
--

CREATE TABLE `atDateTime` (
  `avID` int(10) UNSIGNED NOT NULL,
  `value` datetime DEFAULT '1000-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atDateTimeSettings`
--

CREATE TABLE `atDateTimeSettings` (
  `akID` int(10) UNSIGNED NOT NULL,
  `akDateDisplayMode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atDefault`
--

CREATE TABLE `atDefault` (
  `avID` int(10) UNSIGNED NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `atDefault`
--

INSERT INTO `atDefault` (`avID`, `value`) VALUES
(1, 'fa fa-th-large'),
(2, 'pages, add page, delete page, copy, move, alias'),
(3, 'pages, add page, delete page, copy, move, alias'),
(4, 'pages, add page, delete page, copy, move, alias, bulk'),
(5, 'find page, search page, search, find, pages, sitemap'),
(6, 'add file, delete file, copy, move, alias, resize, crop, rename, images, title, attribute'),
(7, 'file, file attributes, title, attribute, description, rename'),
(8, 'files, category, categories'),
(10, 'new file set'),
(11, 'users, groups, people, find, delete user, remove user, change password, password'),
(12, 'find, search, people, delete user, remove user, change password, password'),
(13, 'user, group, people, permissions, expire, badges'),
(14, 'user attributes, user data, gather data, registration data'),
(15, 'new user, create'),
(17, 'new user group, new group, group, create'),
(19, 'group set'),
(20, 'community, points, karma'),
(21, 'action, community actions'),
(22, 'forms, log, error, email, mysql, exception, survey'),
(23, 'forms, questions, response, data'),
(24, 'questions, quiz, response'),
(25, 'forms, log, error, email, mysql, exception, survey, history'),
(26, 'new theme, theme, active theme, change theme, template, css'),
(27, 'page types'),
(36, 'page attributes, custom'),
(37, 'single, page, custom, application'),
(38, 'atom, rss, feed, syndication'),
(39, 'icon-bullhorn'),
(40, 'add workflow, remove workflow'),
(41, 'stacks, reusable content, scrapbook, copy, paste, paste block, copy block, site name, logo'),
(44, 'edit stacks, view stacks, all stacks'),
(45, 'block, refresh, custom'),
(46, 'add-on, addon, add on, package, app, ecommerce, discussions, forums, themes, templates, blocks'),
(48, 'add-on, addon, ecommerce, install, discussions, forums, themes, templates, blocks'),
(49, 'update, upgrade'),
(50, 'concrete5.org, my account, marketplace'),
(52, 'buy theme, new theme, marketplace, template'),
(53, 'buy addon, buy add on, buy add-on, purchase addon, purchase add on, purchase add-on, find addon, new addon, marketplace'),
(54, 'dashboard, configuration'),
(55, 'website name, title'),
(56, 'accessibility, easy mode'),
(57, 'sharing, facebook, twitter'),
(58, 'logo, favicon, iphone, icon, bookmark'),
(59, 'tinymce, content block, fonts, editor, content, overlay'),
(60, 'translate, translation, internationalization, multilingual'),
(61, 'timezone, profile, locale'),
(62, 'multilingual, localization, internationalization, i18n'),
(63, 'vanity, pretty url, redirection, hostname, canonical, seo, pageview, view'),
(64, 'bulk, seo, change keywords, engine, optimization, search'),
(65, 'traffic, statistics, google analytics, quant, pageviews, hits'),
(66, 'pretty, slug'),
(67, 'configure search, site search, search option'),
(68, 'file options, file manager, upload, modify'),
(69, 'security, files, media, extension, manager, upload'),
(70, 'images, picture, responsive, retina'),
(71, 'uploading, upload, images, image, resizing, manager'),
(72, 'security, alternate storage, hide files'),
(73, 'cache option, change cache, override, turn on cache, turn off cache, no cache, page cache, caching'),
(74, 'cache option, turn off cache, no cache, page cache, caching'),
(75, 'index search, reindex search, build sitemap, sitemap.xml, clear old versions, page versions, remove old'),
(76, 'queries, database, mysql'),
(77, 'editors, hide site, offline, private, public, access'),
(78, 'security, actions, administrator, admin, package, marketplace, search'),
(79, 'security, lock ip, lock out, block ip, address, restrict, access'),
(80, 'security, registration'),
(81, 'antispam, block spam, security'),
(82, 'lock site, under construction, hide, hidden'),
(83, 'profile, login, redirect, specific, dashboard, administrators'),
(84, 'member profile, member page, community, forums, social, avatar'),
(85, 'signup, new user, community, public registration, public, registration'),
(86, 'auth, authentication, types, oauth, facebook, login, registration'),
(87, 'smtp, mail settings'),
(88, 'email server, mail settings, mail configuration, external, internal'),
(89, 'test smtp, test mail'),
(90, 'email server, mail settings, mail configuration, private message, message system, import, email, message'),
(91, 'conversations'),
(92, 'conversations'),
(93, 'conversations ratings, ratings, community, community points'),
(94, 'conversations bad words, banned words, banned, bad words, bad, words, list'),
(95, 'attribute configuration'),
(96, 'attributes, sets'),
(97, 'attributes, types'),
(98, 'topics, tags, taxonomy'),
(100, 'overrides, system info, debug, support, help'),
(101, 'errors, exceptions, develop, support, help'),
(102, 'email, logging, logs, smtp, pop, errors, mysql, log'),
(103, 'network, proxy server'),
(104, 'database, entities, doctrine, orm'),
(105, 'export, backup, database, sql, mysql, encryption, restore'),
(106, 'upgrade, new version, update'),
(110, 'fa fa-edit'),
(111, 'fa fa-trash-o'),
(112, 'fa fa-th'),
(113, 'fa fa-briefcase');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atFile`
--

CREATE TABLE `atFile` (
  `avID` int(10) UNSIGNED NOT NULL,
  `fID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atNumber`
--

CREATE TABLE `atNumber` (
  `avID` int(10) UNSIGNED NOT NULL,
  `value` decimal(14,4) DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `atNumber`
--

INSERT INTO `atNumber` (`avID`, `value`) VALUES
(114, '4724.0000'),
(115, '1181.0000'),
(116, '1920.0000'),
(117, '1235.0000'),
(118, '500.0000'),
(119, '400.0000'),
(120, '1000.0000'),
(121, '800.0000'),
(122, '1000.0000'),
(123, '800.0000'),
(124, '1024.0000'),
(125, '576.0000'),
(126, '1000.0000'),
(127, '800.0000'),
(128, '1000.0000'),
(129, '800.0000'),
(130, '1000.0000'),
(131, '800.0000'),
(132, '1920.0000'),
(133, '1080.0000');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atSelectedTopics`
--

CREATE TABLE `atSelectedTopics` (
  `avID` int(10) UNSIGNED NOT NULL,
  `TopicNodeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atSelectOptions`
--

CREATE TABLE `atSelectOptions` (
  `ID` int(10) UNSIGNED NOT NULL,
  `akID` int(10) UNSIGNED DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayOrder` int(10) UNSIGNED DEFAULT NULL,
  `isEndUserAdded` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atSelectOptionsSelected`
--

CREATE TABLE `atSelectOptionsSelected` (
  `avID` int(10) UNSIGNED NOT NULL,
  `atSelectOptionID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atSelectSettings`
--

CREATE TABLE `atSelectSettings` (
  `akID` int(10) UNSIGNED NOT NULL,
  `akSelectAllowMultipleValues` tinyint(1) NOT NULL DEFAULT '0',
  `akSelectOptionDisplayOrder` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'display_asc',
  `akSelectAllowOtherValues` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `atSelectSettings`
--

INSERT INTO `atSelectSettings` (`akID`, `akSelectAllowMultipleValues`, `akSelectOptionDisplayOrder`, `akSelectAllowOtherValues`) VALUES
(8, 1, 'display_asc', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atSocialLinks`
--

CREATE TABLE `atSocialLinks` (
  `avsID` int(10) UNSIGNED NOT NULL,
  `avID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serviceInfo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atTextareaSettings`
--

CREATE TABLE `atTextareaSettings` (
  `akID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `akTextareaDisplayMode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `akTextareaDisplayModeCustomOptions` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `atTextareaSettings`
--

INSERT INTO `atTextareaSettings` (`akID`, `akTextareaDisplayMode`, `akTextareaDisplayModeCustomOptions`) VALUES
(2, '', ''),
(3, '', ''),
(4, '', ''),
(7, '', '');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `atTopicSettings`
--

CREATE TABLE `atTopicSettings` (
  `akID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `akTopicParentNodeID` int(11) DEFAULT NULL,
  `akTopicTreeID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AttributeKeyCategories`
--

CREATE TABLE `AttributeKeyCategories` (
  `akCategoryID` int(10) UNSIGNED NOT NULL,
  `akCategoryHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akCategoryAllowSets` smallint(6) NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `AttributeKeyCategories`
--

INSERT INTO `AttributeKeyCategories` (`akCategoryID`, `akCategoryHandle`, `akCategoryAllowSets`, `pkgID`) VALUES
(1, 'collection', 1, NULL),
(2, 'user', 1, NULL),
(3, 'file', 1, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AttributeKeys`
--

CREATE TABLE `AttributeKeys` (
  `akID` int(10) UNSIGNED NOT NULL,
  `akHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akIsSearchable` tinyint(1) NOT NULL DEFAULT '0',
  `akIsSearchableIndexed` tinyint(1) NOT NULL DEFAULT '0',
  `akIsAutoCreated` tinyint(1) NOT NULL DEFAULT '0',
  `akIsInternal` tinyint(1) NOT NULL DEFAULT '0',
  `akIsColumnHeader` tinyint(1) NOT NULL DEFAULT '0',
  `akIsEditable` tinyint(1) NOT NULL DEFAULT '0',
  `atID` int(10) UNSIGNED DEFAULT NULL,
  `akCategoryID` int(10) UNSIGNED DEFAULT NULL,
  `pkgID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `AttributeKeys`
--

INSERT INTO `AttributeKeys` (`akID`, `akHandle`, `akName`, `akIsSearchable`, `akIsSearchableIndexed`, `akIsAutoCreated`, `akIsInternal`, `akIsColumnHeader`, `akIsEditable`, `atID`, `akCategoryID`, `pkgID`) VALUES
(1, 'meta_title', 'Meta Title', 1, 1, 1, 0, 0, 1, 1, 1, 0),
(2, 'meta_description', 'Meta Description', 1, 1, 1, 0, 0, 1, 2, 1, 0),
(3, 'meta_keywords', 'Meta Keywords', 1, 1, 1, 0, 0, 1, 2, 1, 0),
(4, 'icon_dashboard', 'Dashboard Icon', 0, 0, 1, 1, 0, 1, 2, 1, 0),
(5, 'exclude_nav', 'Exclude From Nav', 1, 1, 1, 0, 0, 1, 3, 1, 0),
(6, 'exclude_page_list', 'Exclude From Page List', 1, 1, 1, 0, 0, 1, 3, 1, 0),
(7, 'header_extra_content', 'Header Extra Content', 1, 1, 1, 0, 0, 1, 2, 1, 0),
(8, 'tags', 'Tags', 1, 1, 1, 0, 0, 1, 8, 1, 0),
(9, 'is_featured', 'Is Featured', 1, 0, 1, 0, 0, 1, 3, 1, 0),
(10, 'exclude_search_index', 'Exclude From Search Index', 1, 1, 1, 0, 0, 1, 3, 1, 0),
(11, 'exclude_sitemapxml', 'Exclude From sitemap.xml', 1, 1, 1, 0, 0, 1, 3, 1, 0),
(12, 'profile_private_messages_enabled', 'I would like to receive private messages.', 1, 0, 0, 0, 0, 1, 3, 2, 0),
(13, 'profile_private_messages_notification_enabled', 'Send me email notifications when I receive a private message.', 1, 0, 0, 0, 0, 1, 3, 2, 0),
(14, 'width', 'Width', 1, 1, 1, 0, 0, 1, 6, 3, 0),
(15, 'height', 'Height', 1, 1, 1, 0, 0, 1, 6, 3, 0),
(16, 'account_profile_links', 'Personal Links', 0, 0, 0, 0, 0, 1, 11, 2, 0),
(17, 'duration', 'Duration', 1, 1, 1, 0, 0, 1, 6, 3, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AttributeSetKeys`
--

CREATE TABLE `AttributeSetKeys` (
  `akID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `asID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `displayOrder` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `AttributeSetKeys`
--

INSERT INTO `AttributeSetKeys` (`akID`, `asID`, `displayOrder`) VALUES
(1, 1, 1),
(2, 1, 2),
(7, 1, 3),
(11, 1, 4),
(9, 2, 1),
(5, 2, 2),
(6, 2, 3),
(10, 2, 4),
(8, 2, 5);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AttributeSets`
--

CREATE TABLE `AttributeSets` (
  `asID` int(10) UNSIGNED NOT NULL,
  `asName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akCategoryID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `asIsLocked` tinyint(1) NOT NULL DEFAULT '1',
  `asDisplayOrder` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `AttributeSets`
--

INSERT INTO `AttributeSets` (`asID`, `asName`, `asHandle`, `akCategoryID`, `pkgID`, `asIsLocked`, `asDisplayOrder`) VALUES
(1, 'SEO', 'seo', 1, 0, 0, 0),
(2, 'Navigation and Indexing', 'navigation', 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AttributeTypeCategories`
--

CREATE TABLE `AttributeTypeCategories` (
  `atID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `akCategoryID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `AttributeTypeCategories`
--

INSERT INTO `AttributeTypeCategories` (`atID`, `akCategoryID`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 2),
(10, 1),
(10, 2),
(10, 3),
(11, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AttributeTypes`
--

CREATE TABLE `AttributeTypes` (
  `atID` int(10) UNSIGNED NOT NULL,
  `atHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `atName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `AttributeTypes`
--

INSERT INTO `AttributeTypes` (`atID`, `atHandle`, `atName`, `pkgID`) VALUES
(1, 'text', 'Text', 0),
(2, 'textarea', 'Text Area', 0),
(3, 'boolean', 'Checkbox', 0),
(4, 'date_time', 'Date/Time', 0),
(5, 'image_file', 'Image/File', 0),
(6, 'number', 'Number', 0),
(7, 'rating', 'Rating', 0),
(8, 'select', 'Select', 0),
(9, 'address', 'Address', 0),
(10, 'topics', 'Topics', 0),
(11, 'social_links', 'Social Links', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AttributeValues`
--

CREATE TABLE `AttributeValues` (
  `avID` int(10) UNSIGNED NOT NULL,
  `akID` int(10) UNSIGNED DEFAULT NULL,
  `avDateAdded` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `uID` int(10) UNSIGNED DEFAULT NULL,
  `atID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `AttributeValues`
--

INSERT INTO `AttributeValues` (`avID`, `akID`, `avDateAdded`, `uID`, `atID`) VALUES
(1, 4, '2017-03-14 09:30:49', 1, 2),
(2, 3, '2017-03-14 09:30:49', 1, 2),
(3, 3, '2017-03-14 09:30:49', 1, 2),
(4, 3, '2017-03-14 09:30:50', 1, 2),
(5, 3, '2017-03-14 09:30:50', 1, 2),
(6, 3, '2017-03-14 09:30:50', 1, 2),
(7, 3, '2017-03-14 09:30:50', 1, 2),
(8, 3, '2017-03-14 09:30:50', 1, 2),
(9, 5, '2017-03-14 09:30:50', 1, 3),
(10, 3, '2017-03-14 09:30:50', 1, 2),
(11, 3, '2017-03-14 09:30:50', 1, 2),
(12, 3, '2017-03-14 09:30:50', 1, 2),
(13, 3, '2017-03-14 09:30:50', 1, 2),
(14, 3, '2017-03-14 09:30:50', 1, 2),
(15, 3, '2017-03-14 09:30:50', 1, 2),
(16, 5, '2017-03-14 09:30:50', 1, 3),
(17, 3, '2017-03-14 09:30:50', 1, 2),
(18, 5, '2017-03-14 09:30:50', 1, 3),
(19, 3, '2017-03-14 09:30:50', 1, 2),
(20, 3, '2017-03-14 09:30:50', 1, 2),
(21, 3, '2017-03-14 09:30:50', 1, 2),
(22, 3, '2017-03-14 09:30:50', 1, 2),
(23, 3, '2017-03-14 09:30:50', 1, 2),
(24, 3, '2017-03-14 09:30:50', 1, 2),
(25, 3, '2017-03-14 09:30:50', 1, 2),
(26, 3, '2017-03-14 09:30:50', 1, 2),
(27, 3, '2017-03-14 09:30:50', 1, 2),
(28, 5, '2017-03-14 09:30:50', 1, 3),
(29, 5, '2017-03-14 09:30:50', 1, 3),
(30, 5, '2017-03-14 09:30:50', 1, 3),
(31, 5, '2017-03-14 09:30:50', 1, 3),
(32, 5, '2017-03-14 09:30:50', 1, 3),
(33, 5, '2017-03-14 09:30:50', 1, 3),
(34, 5, '2017-03-14 09:30:50', 1, 3),
(35, 5, '2017-03-14 09:30:50', 1, 3),
(36, 3, '2017-03-14 09:30:50', 1, 2),
(37, 3, '2017-03-14 09:30:50', 1, 2),
(38, 3, '2017-03-14 09:30:50', 1, 2),
(39, 4, '2017-03-14 09:30:50', 1, 2),
(40, 3, '2017-03-14 09:30:50', 1, 2),
(41, 3, '2017-03-14 09:30:50', 1, 2),
(42, 5, '2017-03-14 09:30:50', 1, 3),
(43, 10, '2017-03-14 09:30:50', 1, 3),
(44, 3, '2017-03-14 09:30:50', 1, 2),
(45, 3, '2017-03-14 09:30:50', 1, 2),
(46, 3, '2017-03-14 09:30:51', 1, 2),
(47, 5, '2017-03-14 09:30:51', 1, 3),
(48, 3, '2017-03-14 09:30:51', 1, 2),
(49, 3, '2017-03-14 09:30:51', 1, 2),
(50, 3, '2017-03-14 09:30:51', 1, 2),
(51, 5, '2017-03-14 09:30:51', 1, 3),
(52, 3, '2017-03-14 09:30:51', 1, 2),
(53, 3, '2017-03-14 09:30:51', 1, 2),
(54, 3, '2017-03-14 09:30:51', 1, 2),
(55, 3, '2017-03-14 09:30:51', 1, 2),
(56, 3, '2017-03-14 09:30:51', 1, 2),
(57, 3, '2017-03-14 09:30:51', 1, 2),
(58, 3, '2017-03-14 09:30:51', 1, 2),
(59, 3, '2017-03-14 09:30:51', 1, 2),
(60, 3, '2017-03-14 09:30:51', 1, 2),
(61, 3, '2017-03-14 09:30:51', 1, 2),
(62, 3, '2017-03-14 09:30:51', 1, 2),
(63, 3, '2017-03-14 09:30:51', 1, 2),
(64, 3, '2017-03-14 09:30:51', 1, 2),
(65, 3, '2017-03-14 09:30:51', 1, 2),
(66, 3, '2017-03-14 09:30:51', 1, 2),
(67, 3, '2017-03-14 09:30:51', 1, 2),
(68, 3, '2017-03-14 09:30:51', 1, 2),
(69, 3, '2017-03-14 09:30:51', 1, 2),
(70, 3, '2017-03-14 09:30:51', 1, 2),
(71, 3, '2017-03-14 09:30:51', 1, 2),
(72, 3, '2017-03-14 09:30:51', 1, 2),
(73, 3, '2017-03-14 09:30:51', 1, 2),
(74, 3, '2017-03-14 09:30:51', 1, 2),
(75, 3, '2017-03-14 09:30:51', 1, 2),
(76, 3, '2017-03-14 09:30:51', 1, 2),
(77, 3, '2017-03-14 09:30:51', 1, 2),
(78, 3, '2017-03-14 09:30:51', 1, 2),
(79, 3, '2017-03-14 09:30:51', 1, 2),
(80, 3, '2017-03-14 09:30:51', 1, 2),
(81, 3, '2017-03-14 09:30:51', 1, 2),
(82, 3, '2017-03-14 09:30:51', 1, 2),
(83, 3, '2017-03-14 09:30:51', 1, 2),
(84, 3, '2017-03-14 09:30:51', 1, 2),
(85, 3, '2017-03-14 09:30:51', 1, 2),
(86, 3, '2017-03-14 09:30:51', 1, 2),
(87, 3, '2017-03-14 09:30:51', 1, 2),
(88, 3, '2017-03-14 09:30:51', 1, 2),
(89, 3, '2017-03-14 09:30:51', 1, 2),
(90, 3, '2017-03-14 09:30:52', 1, 2),
(91, 3, '2017-03-14 09:30:52', 1, 2),
(92, 3, '2017-03-14 09:30:52', 1, 2),
(93, 3, '2017-03-14 09:30:52', 1, 2),
(94, 3, '2017-03-14 09:30:52', 1, 2),
(95, 3, '2017-03-14 09:30:52', 1, 2),
(96, 3, '2017-03-14 09:30:52', 1, 2),
(97, 3, '2017-03-14 09:30:52', 1, 2),
(98, 3, '2017-03-14 09:30:52', 1, 2),
(99, 10, '2017-03-14 09:30:52', 1, 3),
(100, 3, '2017-03-14 09:30:52', 1, 2),
(101, 3, '2017-03-14 09:30:52', 1, 2),
(102, 3, '2017-03-14 09:30:52', 1, 2),
(103, 3, '2017-03-14 09:30:52', 1, 2),
(104, 3, '2017-03-14 09:30:52', 1, 2),
(105, 3, '2017-03-14 09:30:52', 1, 2),
(106, 3, '2017-03-14 09:30:52', 1, 2),
(107, 5, '2017-03-14 09:30:52', 1, 3),
(108, 5, '2017-03-14 09:30:52', 1, 3),
(109, 10, '2017-03-14 09:30:52', 1, 3),
(110, 4, '2017-03-14 09:30:53', 1, 2),
(111, 4, '2017-03-14 09:30:53', 1, 2),
(112, 4, '2017-03-14 09:30:53', 1, 2),
(113, 4, '2017-03-14 09:30:53', 1, 2),
(114, 14, '2017-03-14 14:42:31', 1, 6),
(115, 15, '2017-03-14 14:42:31', 1, 6),
(116, 14, '2017-03-14 17:21:13', 1, 6),
(117, 15, '2017-03-14 17:21:13', 1, 6),
(118, 14, '2017-03-16 09:50:04', 1, 6),
(119, 15, '2017-03-16 09:50:04', 1, 6),
(120, 14, '2017-03-16 09:51:26', 1, 6),
(121, 15, '2017-03-16 09:51:26', 1, 6),
(122, 14, '2017-03-16 09:52:51', 1, 6),
(123, 15, '2017-03-16 09:52:51', 1, 6),
(124, 14, '2017-03-16 09:54:13', 1, 6),
(125, 15, '2017-03-16 09:54:13', 1, 6),
(126, 14, '2017-03-16 09:54:53', 1, 6),
(127, 15, '2017-03-16 09:54:53', 1, 6),
(128, 14, '2017-03-16 09:55:39', 1, 6),
(129, 15, '2017-03-16 09:55:39', 1, 6),
(130, 14, '2017-03-16 09:56:15', 1, 6),
(131, 15, '2017-03-16 09:56:15', 1, 6),
(132, 14, '2017-03-16 09:57:33', 1, 6),
(133, 15, '2017-03-16 09:57:33', 1, 6);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `AuthenticationTypes`
--

CREATE TABLE `AuthenticationTypes` (
  `authTypeID` int(10) UNSIGNED NOT NULL,
  `authTypeHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authTypeName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authTypeIsEnabled` tinyint(1) NOT NULL,
  `authTypeDisplayOrder` int(10) UNSIGNED DEFAULT NULL,
  `pkgID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `AuthenticationTypes`
--

INSERT INTO `AuthenticationTypes` (`authTypeID`, `authTypeHandle`, `authTypeName`, `authTypeIsEnabled`, `authTypeDisplayOrder`, `pkgID`) VALUES
(1, 'concrete', 'Standard', 1, 0, 0),
(2, 'community', 'concrete5.org', 0, 0, 0),
(3, 'facebook', 'Facebook', 0, 0, 0),
(4, 'twitter', 'Twitter', 0, 0, 0),
(5, 'google', 'Google', 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `authTypeConcreteCookieMap`
--

CREATE TABLE `authTypeConcreteCookieMap` (
  `ID` int(10) UNSIGNED NOT NULL,
  `token` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uID` int(11) DEFAULT NULL,
  `validThrough` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `authTypeConcreteCookieMap`
--

INSERT INTO `authTypeConcreteCookieMap` (`ID`, `token`, `uID`, `validThrough`) VALUES
(1, '5b140a95d10eb7e3ac1710ed44dc51a8', 1, 1490863268);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `BannedWords`
--

CREATE TABLE `BannedWords` (
  `bwID` int(10) UNSIGNED NOT NULL,
  `bannedWord` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `BannedWords`
--

INSERT INTO `BannedWords` (`bwID`, `bannedWord`) VALUES
(1, 'fuck'),
(2, 'shit'),
(3, 'bitch'),
(4, 'ass');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `BasicWorkflowPermissionAssignments`
--

CREATE TABLE `BasicWorkflowPermissionAssignments` (
  `wfID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `BasicWorkflowProgressData`
--

CREATE TABLE `BasicWorkflowProgressData` (
  `wpID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uIDStarted` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uIDCompleted` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `wpDateCompleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `BlockFeatureAssignments`
--

CREATE TABLE `BlockFeatureAssignments` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `bID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `faID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `BlockFeatureAssignments`
--

INSERT INTO `BlockFeatureAssignments` (`cID`, `cvID`, `bID`, `faID`) VALUES
(1, 6, 13, 2),
(1, 7, 13, 2),
(1, 8, 13, 2),
(1, 9, 13, 2),
(1, 10, 13, 2),
(1, 11, 13, 2),
(1, 12, 13, 2),
(1, 13, 13, 2),
(1, 14, 13, 2),
(1, 15, 13, 2),
(1, 16, 13, 2),
(1, 17, 13, 2),
(1, 18, 13, 2),
(1, 19, 13, 2),
(1, 20, 13, 2),
(1, 21, 13, 2),
(1, 22, 13, 2),
(1, 23, 13, 2),
(1, 24, 13, 2),
(1, 25, 13, 2),
(1, 26, 13, 2),
(1, 27, 13, 2),
(1, 28, 13, 2),
(1, 29, 13, 2),
(1, 29, 36, 7),
(1, 29, 39, 8),
(1, 29, 41, 9),
(1, 30, 13, 2),
(1, 30, 36, 7),
(1, 30, 39, 8),
(1, 30, 41, 9),
(1, 31, 13, 2),
(1, 31, 36, 7),
(1, 31, 39, 8),
(1, 31, 41, 9),
(1, 32, 13, 2),
(1, 32, 36, 7),
(1, 32, 39, 8),
(1, 32, 41, 9),
(1, 33, 13, 2),
(1, 33, 36, 7),
(1, 33, 39, 8),
(1, 33, 41, 9),
(1, 34, 13, 2),
(1, 34, 36, 7),
(1, 34, 39, 8),
(1, 34, 41, 9),
(1, 35, 13, 2),
(1, 35, 36, 7),
(1, 35, 39, 8),
(1, 35, 41, 9),
(1, 36, 13, 2),
(1, 36, 36, 7),
(1, 36, 39, 8),
(1, 36, 41, 9),
(1, 37, 13, 2),
(1, 37, 36, 7),
(1, 37, 39, 8),
(1, 37, 41, 9),
(1, 38, 13, 2),
(1, 38, 36, 7),
(1, 38, 39, 8),
(1, 38, 41, 9),
(1, 39, 13, 2),
(1, 39, 36, 7),
(1, 39, 39, 8),
(1, 39, 41, 9),
(1, 40, 13, 2),
(1, 40, 36, 7),
(1, 40, 39, 8),
(1, 40, 41, 9),
(1, 41, 13, 2),
(1, 41, 36, 7),
(1, 41, 39, 8),
(1, 41, 41, 9),
(1, 42, 13, 2),
(1, 42, 36, 7),
(1, 42, 39, 8),
(1, 42, 41, 9),
(1, 43, 13, 2),
(1, 43, 36, 7),
(1, 43, 39, 8),
(1, 43, 41, 9),
(1, 44, 13, 2),
(1, 44, 36, 7),
(1, 44, 39, 8),
(1, 44, 41, 9),
(1, 45, 13, 2),
(1, 45, 36, 7),
(1, 45, 39, 8),
(1, 45, 41, 9),
(1, 46, 13, 2),
(1, 46, 36, 7),
(1, 46, 39, 8),
(1, 46, 41, 9),
(1, 47, 13, 2),
(1, 47, 36, 7),
(1, 47, 39, 8),
(1, 47, 41, 9),
(1, 48, 13, 2),
(1, 48, 36, 7),
(1, 48, 39, 8),
(1, 48, 41, 9),
(1, 49, 13, 2),
(1, 49, 36, 7),
(1, 49, 39, 8),
(1, 49, 41, 9),
(1, 50, 13, 2),
(1, 50, 36, 7),
(1, 50, 39, 8),
(1, 50, 41, 9),
(1, 51, 13, 2),
(1, 51, 36, 7),
(1, 51, 39, 8),
(1, 51, 41, 9),
(1, 52, 13, 2),
(1, 52, 36, 7),
(1, 52, 39, 8),
(1, 52, 41, 9),
(1, 53, 13, 2),
(1, 53, 36, 7),
(1, 53, 39, 8),
(1, 53, 41, 9),
(1, 54, 13, 2),
(1, 54, 36, 7),
(1, 54, 39, 8),
(1, 54, 41, 9),
(1, 54, 63, 11),
(1, 54, 64, 12),
(1, 55, 13, 2),
(1, 55, 36, 7),
(1, 55, 39, 8),
(1, 55, 41, 9),
(1, 55, 63, 11),
(1, 55, 64, 12),
(1, 55, 65, 13),
(1, 56, 13, 2),
(1, 56, 36, 7),
(1, 56, 39, 8),
(1, 56, 41, 9),
(1, 56, 63, 11),
(1, 56, 64, 12),
(1, 56, 65, 13),
(1, 57, 13, 2),
(1, 57, 36, 7),
(1, 57, 39, 8),
(1, 57, 41, 9),
(1, 57, 63, 11),
(1, 57, 64, 12),
(1, 57, 65, 13),
(1, 58, 13, 2),
(1, 58, 36, 7),
(1, 58, 39, 8),
(1, 58, 41, 9),
(1, 58, 63, 11),
(1, 58, 64, 12),
(1, 58, 65, 13),
(1, 59, 13, 2),
(1, 59, 36, 7),
(1, 59, 39, 8),
(1, 59, 41, 9),
(1, 59, 63, 11),
(1, 59, 64, 12),
(1, 59, 65, 13),
(1, 60, 13, 2),
(1, 60, 36, 7),
(1, 60, 39, 8),
(1, 60, 41, 9),
(1, 60, 63, 11),
(1, 60, 64, 12),
(1, 60, 65, 13),
(1, 61, 13, 2),
(1, 61, 36, 7),
(1, 61, 39, 8),
(1, 61, 41, 9),
(1, 61, 63, 11),
(1, 61, 64, 12),
(1, 61, 65, 13),
(1, 62, 13, 2),
(1, 62, 36, 7),
(1, 62, 39, 8),
(1, 62, 41, 9),
(1, 62, 63, 11),
(1, 62, 64, 12),
(1, 62, 65, 13),
(1, 63, 13, 2),
(1, 63, 36, 7),
(1, 63, 39, 8),
(1, 63, 41, 9),
(1, 63, 63, 11),
(1, 63, 64, 12),
(1, 63, 65, 13),
(1, 64, 13, 2),
(1, 64, 36, 7),
(1, 64, 39, 8),
(1, 64, 41, 9),
(1, 64, 63, 11),
(1, 64, 64, 12),
(1, 64, 65, 13),
(1, 65, 13, 2),
(1, 65, 36, 7),
(1, 65, 39, 8),
(1, 65, 41, 9),
(1, 65, 63, 11),
(1, 65, 64, 12),
(1, 65, 65, 13),
(1, 66, 13, 2),
(1, 66, 36, 7),
(1, 66, 39, 8),
(1, 66, 41, 9),
(1, 66, 63, 11),
(1, 66, 64, 12),
(1, 66, 65, 13),
(1, 67, 13, 2),
(1, 67, 36, 7),
(1, 67, 39, 8),
(1, 67, 41, 9),
(1, 67, 80, 14),
(1, 67, 81, 15),
(1, 67, 83, 16),
(1, 68, 13, 2),
(1, 68, 36, 7),
(1, 68, 39, 8),
(1, 68, 41, 9),
(1, 68, 80, 14),
(1, 68, 81, 15),
(1, 68, 83, 16),
(1, 69, 13, 2),
(1, 69, 36, 7),
(1, 69, 39, 8),
(1, 69, 41, 9),
(1, 69, 80, 14),
(1, 69, 81, 15),
(1, 69, 83, 16),
(1, 70, 13, 2),
(1, 70, 36, 7),
(1, 70, 39, 8),
(1, 70, 41, 9),
(1, 70, 80, 14),
(1, 70, 81, 15),
(1, 70, 83, 16),
(1, 71, 13, 2),
(1, 71, 36, 7),
(1, 71, 39, 8),
(1, 71, 41, 9),
(1, 71, 80, 14),
(1, 71, 81, 15),
(1, 71, 83, 16),
(1, 72, 13, 2),
(1, 72, 36, 7),
(1, 72, 39, 8),
(1, 72, 41, 9),
(1, 72, 80, 14),
(1, 72, 81, 15),
(1, 72, 83, 16),
(1, 73, 13, 2),
(1, 73, 36, 7),
(1, 73, 39, 8),
(1, 73, 41, 9),
(1, 73, 80, 14),
(1, 73, 81, 15),
(1, 73, 83, 16),
(1, 74, 13, 2),
(1, 74, 36, 7),
(1, 74, 39, 8),
(1, 74, 41, 9),
(1, 74, 80, 14),
(1, 74, 81, 15),
(1, 74, 83, 16),
(146, 2, 11, 1),
(155, 2, 14, 3),
(155, 3, 15, 4),
(155, 4, 16, 5),
(155, 5, 16, 5),
(155, 6, 17, 6),
(155, 7, 58, 10);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `BlockPermissionAssignments`
--

CREATE TABLE `BlockPermissionAssignments` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `bID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `BlockRelations`
--

CREATE TABLE `BlockRelations` (
  `brID` int(10) UNSIGNED NOT NULL,
  `bID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `originalBID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `relationType` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `BlockRelations`
--

INSERT INTO `BlockRelations` (`brID`, `bID`, `originalBID`, `relationType`) VALUES
(1, 15, 14, 'DUPLICATE'),
(2, 16, 15, 'DUPLICATE'),
(3, 17, 16, 'DUPLICATE'),
(4, 20, 19, 'DUPLICATE'),
(5, 21, 20, 'DUPLICATE'),
(6, 22, 21, 'DUPLICATE'),
(7, 23, 22, 'DUPLICATE'),
(8, 24, 23, 'DUPLICATE'),
(9, 25, 24, 'DUPLICATE'),
(10, 27, 26, 'DUPLICATE'),
(11, 28, 27, 'DUPLICATE'),
(12, 29, 28, 'DUPLICATE'),
(13, 30, 29, 'DUPLICATE'),
(14, 31, 30, 'DUPLICATE'),
(15, 32, 31, 'DUPLICATE'),
(16, 43, 33, 'DUPLICATE'),
(17, 44, 43, 'DUPLICATE'),
(18, 46, 44, 'DUPLICATE'),
(19, 47, 45, 'DUPLICATE'),
(20, 48, 46, 'DUPLICATE'),
(21, 49, 47, 'DUPLICATE'),
(22, 50, 48, 'DUPLICATE'),
(23, 51, 49, 'DUPLICATE'),
(24, 52, 50, 'DUPLICATE'),
(25, 53, 51, 'DUPLICATE'),
(26, 54, 52, 'DUPLICATE'),
(27, 55, 34, 'DUPLICATE'),
(28, 57, 56, 'DUPLICATE'),
(29, 58, 17, 'DUPLICATE'),
(30, 60, 57, 'DUPLICATE'),
(31, 61, 60, 'DUPLICATE'),
(32, 66, 59, 'DUPLICATE'),
(33, 67, 66, 'DUPLICATE'),
(34, 68, 67, 'DUPLICATE'),
(35, 69, 61, 'DUPLICATE'),
(36, 70, 62, 'DUPLICATE'),
(37, 71, 35, 'DUPLICATE'),
(38, 72, 55, 'DUPLICATE'),
(39, 73, 40, 'DUPLICATE'),
(40, 74, 42, 'DUPLICATE'),
(41, 76, 75, 'DUPLICATE'),
(42, 77, 76, 'DUPLICATE'),
(43, 78, 77, 'DUPLICATE'),
(44, 79, 73, 'DUPLICATE'),
(45, 80, 64, 'DUPLICATE'),
(46, 81, 63, 'DUPLICATE'),
(47, 82, 78, 'DUPLICATE'),
(48, 83, 65, 'DUPLICATE'),
(49, 84, 70, 'DUPLICATE'),
(50, 85, 69, 'DUPLICATE'),
(51, 86, 84, 'DUPLICATE'),
(52, 87, 82, 'DUPLICATE'),
(53, 91, 90, 'DUPLICATE'),
(54, 92, 91, 'DUPLICATE'),
(55, 93, 92, 'DUPLICATE');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Blocks`
--

CREATE TABLE `Blocks` (
  `bID` int(10) UNSIGNED NOT NULL,
  `bName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bDateAdded` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `bDateModified` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `bFilename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bIsActive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `btID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uID` int(10) UNSIGNED DEFAULT NULL,
  `btCachedBlockRecord` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Blocks`
--

INSERT INTO `Blocks` (`bID`, `bName`, `bDateAdded`, `bDateModified`, `bFilename`, `bIsActive`, `btID`, `uID`, `btCachedBlockRecord`) VALUES
(1, '', '2017-03-14 09:30:52', '2017-03-14 09:30:52', NULL, '0', 12, 1, NULL),
(2, '', '2017-03-14 09:30:52', '2017-03-14 09:30:52', NULL, '0', 9, 1, NULL),
(3, '', '2017-03-14 09:30:52', '2017-03-14 09:30:52', NULL, '0', 10, 1, NULL),
(4, '', '2017-03-14 09:30:52', '2017-03-14 09:30:52', NULL, '0', 8, 1, NULL),
(5, '', '2017-03-14 09:30:52', '2017-03-14 09:30:52', NULL, '0', 8, 1, NULL),
(6, '', '2017-03-14 09:30:52', '2017-03-14 09:30:52', NULL, '0', 7, 1, NULL),
(7, '', '2017-03-14 09:30:52', '2017-03-14 09:30:52', NULL, '0', 6, 1, NULL),
(8, '', '2017-03-14 09:30:52', '2017-03-14 09:30:52', NULL, '0', 8, 1, NULL),
(9, '', '2017-03-14 09:30:55', '2017-03-14 09:30:55', NULL, '0', 2, 1, NULL),
(10, '', '2017-03-14 14:42:11', '2017-03-14 14:42:11', NULL, '0', 11, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjExOntzOjM6ImJJRCI7czoyOiIxMCI7czo5OiIAKgBfdGFibGUiO3M6MTI6ImJ0TmF2aWdhdGlvbiI7czo3OiJvcmRlckJ5IjtzOjExOiJkaXNwbGF5X2FzYyI7czoxMjoiZGlzcGxheVBhZ2VzIjtzOjM6InRvcCI7czoxNToiZGlzcGxheVBhZ2VzQ0lEIjtzOjE6IjAiO3M6MjM6ImRpc3BsYXlQYWdlc0luY2x1ZGVTZWxmIjtzOjE6IjAiO3M6MTU6ImRpc3BsYXlTdWJQYWdlcyI7czo0OiJub25lIjtzOjIwOiJkaXNwbGF5U3ViUGFnZUxldmVscyI7czo2OiJlbm91Z2giO3M6MjM6ImRpc3BsYXlTdWJQYWdlTGV2ZWxzTnVtIjtzOjE6IjAiO3M6MjM6ImRpc3BsYXlVbmF2YWlsYWJsZVBhZ2VzIjtzOjE6IjAiO3M6MTg6ImRpc3BsYXlTeXN0ZW1QYWdlcyI7czoxOiIwIjt9'),
(11, '', '2017-03-14 14:42:42', '2017-03-14 14:42:42', NULL, '0', 27, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjEwOntzOjM6ImJJRCI7czoyOiIxMSI7czo5OiIAKgBfdGFibGUiO3M6MTQ6ImJ0Q29udGVudEltYWdlIjtzOjM6ImZJRCI7czoxOiIxIjtzOjEwOiJmT25zdGF0ZUlEIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7czo5OiJtYXhIZWlnaHQiO3M6MToiMCI7czoxMjoiZXh0ZXJuYWxMaW5rIjtzOjA6IiI7czoxNToiaW50ZXJuYWxMaW5rQ0lEIjtzOjE6IjAiO3M6NzoiYWx0VGV4dCI7czo0OiJMb2dvIjtzOjU6InRpdGxlIjtzOjQ6IkxvZ28iO30='),
(12, '', '2017-03-14 14:48:21', '2017-03-14 14:48:21', NULL, '0', 11, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjExOntzOjM6ImJJRCI7czoyOiIxMiI7czo5OiIAKgBfdGFibGUiO3M6MTI6ImJ0TmF2aWdhdGlvbiI7czo3OiJvcmRlckJ5IjtzOjExOiJkaXNwbGF5X2FzYyI7czoxMjoiZGlzcGxheVBhZ2VzIjtzOjM6InRvcCI7czoxNToiZGlzcGxheVBhZ2VzQ0lEIjtzOjE6IjAiO3M6MjM6ImRpc3BsYXlQYWdlc0luY2x1ZGVTZWxmIjtzOjE6IjAiO3M6MTU6ImRpc3BsYXlTdWJQYWdlcyI7czo0OiJub25lIjtzOjIwOiJkaXNwbGF5U3ViUGFnZUxldmVscyI7czo2OiJlbm91Z2giO3M6MjM6ImRpc3BsYXlTdWJQYWdlTGV2ZWxzTnVtIjtzOjE6IjAiO3M6MjM6ImRpc3BsYXlVbmF2YWlsYWJsZVBhZ2VzIjtzOjE6IjAiO3M6MTg6ImRpc3BsYXlTeXN0ZW1QYWdlcyI7czoxOiIwIjt9'),
(13, '', '2017-03-14 14:54:36', '2017-03-14 14:54:36', NULL, '0', 27, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjEwOntzOjM6ImJJRCI7czoyOiIxMyI7czo5OiIAKgBfdGFibGUiO3M6MTQ6ImJ0Q29udGVudEltYWdlIjtzOjM6ImZJRCI7czoxOiIxIjtzOjEwOiJmT25zdGF0ZUlEIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7czo5OiJtYXhIZWlnaHQiO3M6MToiMCI7czoxMjoiZXh0ZXJuYWxMaW5rIjtzOjA6IiI7czoxNToiaW50ZXJuYWxMaW5rQ0lEIjtzOjE6IjAiO3M6NzoiYWx0VGV4dCI7czo0OiJMb2dvIjtzOjU6InRpdGxlIjtzOjQ6IkxvZ28iO30='),
(14, '', '2017-03-14 14:58:17', '2017-03-14 14:58:17', NULL, '0', 27, 1, NULL),
(15, '', '2017-03-14 15:08:29', '2017-03-14 15:08:29', NULL, '1', 27, 1, NULL),
(16, '', '2017-03-14 15:08:38', '2017-03-14 15:10:54', 'logo', '1', 27, 1, NULL),
(17, '', '2017-03-14 15:12:05', '2017-03-14 15:12:05', '', '1', 27, 1, NULL),
(18, '', '2017-03-14 15:21:42', '2017-03-14 15:21:42', NULL, '0', 21, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjU6e3M6MzoiYklEIjtzOjI6IjE4IjtzOjk6IgAqAF90YWJsZSI7czoxMzoiYnRTb2NpYWxMaW5rcyI7czoxNDoiYnRTb2NpYWxMaW5rSUQiO3M6MToiMSI7czo0OiJzbElEIjtzOjE6IjEiO3M6MTI6ImRpc3BsYXlPcmRlciI7czoxOiIwIjt9'),
(19, '', '2017-03-14 17:21:34', '2017-03-14 17:21:34', NULL, '0', 33, 1, NULL),
(20, '', '2017-03-14 17:22:05', '2017-03-15 09:10:45', 'hero-slider.php', '1', 33, 1, NULL),
(21, '', '2017-03-15 09:11:35', '2017-03-15 09:11:53', 'hero-slider.php', '1', 33, 1, NULL),
(22, '', '2017-03-15 09:13:28', '2017-03-15 09:14:25', 'hero-slider.php', '1', 33, 1, NULL),
(23, '', '2017-03-15 09:14:45', '2017-03-15 09:15:44', 'hero-slider.php', '1', 33, 1, NULL),
(24, '', '2017-03-15 09:57:15', '2017-03-15 09:57:16', 'hero-slider.php', '1', 33, 1, NULL),
(25, '', '2017-03-15 10:03:24', '2017-03-15 10:03:24', 'hero-slider.php', '1', 33, 1, NULL),
(26, '', '2017-03-15 10:10:38', '2017-03-15 10:10:38', NULL, '0', 12, 1, NULL),
(27, '', '2017-03-15 10:24:24', '2017-03-15 10:24:24', NULL, '1', 12, 1, NULL),
(28, '', '2017-03-15 10:32:38', '2017-03-15 10:32:38', NULL, '1', 12, 1, NULL),
(29, '', '2017-03-15 10:36:19', '2017-03-15 10:36:19', NULL, '1', 12, 1, NULL),
(30, '', '2017-03-15 10:37:07', '2017-03-15 10:37:07', NULL, '1', 12, 1, NULL),
(31, '', '2017-03-15 10:43:21', '2017-03-15 10:43:21', NULL, '1', 12, 1, NULL),
(32, '', '2017-03-15 11:27:32', '2017-03-15 11:27:33', NULL, '1', 12, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjM6e3M6MzoiYklEIjtzOjI6IjMyIjtzOjk6IgAqAF90YWJsZSI7czoxNDoiYnRDb250ZW50TG9jYWwiO3M6NzoiY29udGVudCI7czo1MzM6IjxoMT5XZWxsLXByZXBhcmVkIDxicj5ldmVudHMgJiBwcm9kdWN0aW9uczwvaDE+PHAgc3R5bGU9ImNvbHVtbi1jb3VudDogMjsiPkVlbiBldmVuZW1lbnQgb2YgcHJvZHVjdGllIGlzIHdhdCBvbnMgYmV0cmVmdCBuZXQgem9hbHMgaGV0IHplZXIgbXl0aGlzY2hlIGdlcmVjaHQgRnVndS4gRmFudGFzdGlzY2ggYWxzIGhldCBnZW1hYWt0IHdvcmR0IGRvb3IgaWVtYW5kIGRpZSBlciB2ZXJzdGFuZCB2YW4gaGVlZnQgbWFhciBmYXRhYWwgaW4gZGUgaGFuZGVuIHZhbiBpZW1hbmQgZGllIG5pZXQgd2VldCB3YXQgaGlqIGRvZXQuIEZyZXNoIEZ1Z3UgY29tYmluZWVydCB2ZXJmcmlzc2VuZGUgc2tpbGxzLCBrZW5uaXMgZW4gZGUgZXJ2YXJpbmcgZGllIG5vZGlnIGlzIG9tIGV2ZW5lbWVudGVuIG9mIHByb2R1Y3RpZXMgb3AgZWVuIHRvcCBuaXZlYXUgdGUgc2VydmVyZW4uDQo8L3A+PHAgc3R5bGU9InRleHQtYWxpZ246IGNlbnRlcjsiPjxhIGhyZWY9IiMiIGNsYXNzPSJidG4gYnRuX19sZWVzLS1tZWVyIj5MZWVzIG1lZXI8L2E+DQo8L3A+Ijt9'),
(33, '', '2017-03-15 11:50:10', '2017-03-15 11:59:03', NULL, '0', 33, 1, NULL),
(34, '', '2017-03-15 11:51:05', '2017-03-15 11:51:05', NULL, '0', 12, 1, NULL),
(35, '', '2017-03-15 11:51:53', '2017-03-15 11:51:53', NULL, '0', 12, 1, NULL),
(36, '', '2017-03-15 11:52:04', '2017-03-15 11:58:51', NULL, '0', 27, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjEwOntzOjM6ImJJRCI7czoyOiIzNiI7czo5OiIAKgBfdGFibGUiO3M6MTQ6ImJ0Q29udGVudEltYWdlIjtzOjM6ImZJRCI7czoxOiIxIjtzOjEwOiJmT25zdGF0ZUlEIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7czo5OiJtYXhIZWlnaHQiO3M6MToiMCI7czoxMjoiZXh0ZXJuYWxMaW5rIjtzOjA6IiI7czoxNToiaW50ZXJuYWxMaW5rQ0lEIjtzOjE6IjAiO3M6NzoiYWx0VGV4dCI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjt9'),
(38, '', '2017-03-15 11:54:28', '2017-03-15 11:54:28', NULL, '0', 30, 1, NULL),
(39, '', '2017-03-15 11:54:40', '2017-03-15 11:58:43', NULL, '0', 27, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjEwOntzOjM6ImJJRCI7czoyOiIzOSI7czo5OiIAKgBfdGFibGUiO3M6MTQ6ImJ0Q29udGVudEltYWdlIjtzOjM6ImZJRCI7czoxOiIxIjtzOjEwOiJmT25zdGF0ZUlEIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7czo5OiJtYXhIZWlnaHQiO3M6MToiMCI7czoxMjoiZXh0ZXJuYWxMaW5rIjtzOjA6IiI7czoxNToiaW50ZXJuYWxMaW5rQ0lEIjtzOjE6IjAiO3M6NzoiYWx0VGV4dCI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjt9'),
(40, '', '2017-03-15 11:57:19', '2017-03-15 11:57:19', NULL, '0', 12, 1, NULL),
(41, '', '2017-03-15 11:57:33', '2017-03-15 11:57:33', NULL, '0', 27, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjEwOntzOjM6ImJJRCI7czoyOiI0MSI7czo5OiIAKgBfdGFibGUiO3M6MTQ6ImJ0Q29udGVudEltYWdlIjtzOjM6ImZJRCI7czoxOiIxIjtzOjEwOiJmT25zdGF0ZUlEIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7czo5OiJtYXhIZWlnaHQiO3M6MToiMCI7czoxMjoiZXh0ZXJuYWxMaW5rIjtzOjA6IiI7czoxNToiaW50ZXJuYWxMaW5rQ0lEIjtzOjE6IjAiO3M6NzoiYWx0VGV4dCI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjt9'),
(42, '', '2017-03-15 11:58:33', '2017-03-15 11:58:33', NULL, '0', 12, 1, NULL),
(43, '', '2017-03-15 12:05:01', '2017-03-15 12:09:43', 'events-slider.php', '1', 33, 1, NULL),
(44, '', '2017-03-15 12:15:05', '2017-03-15 12:15:49', 'events-slider.php', '1', 33, 1, NULL),
(45, '', '2017-03-15 12:17:02', '2017-03-15 12:19:31', 'hero-slider.php', '0', 33, 1, NULL),
(46, '', '2017-03-15 12:17:16', '2017-03-15 12:17:16', '', '1', 33, 1, NULL),
(47, '', '2017-03-15 12:21:14', '2017-03-15 12:22:10', 'hero-slider.php', '1', 33, 1, NULL),
(48, '', '2017-03-15 12:27:02', '2017-03-15 12:27:02', 'events-slider.php', '1', 33, 1, NULL),
(49, '', '2017-03-15 12:30:24', '2017-03-15 12:31:12', 'hero-slider.php', '1', 33, 1, NULL),
(50, '', '2017-03-15 12:31:19', '2017-03-15 12:31:19', 'hero-slider.php', '1', 33, 1, NULL),
(51, '', '2017-03-15 12:33:05', '2017-03-15 12:46:23', '', '1', 33, 1, NULL),
(52, '', '2017-03-15 12:33:12', '2017-03-15 12:33:12', '', '1', 33, 1, NULL),
(53, '', '2017-03-15 12:47:04', '2017-03-15 12:47:04', 'hero-slider.php', '1', 33, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjg6e3M6MzoiYklEIjtzOjI6IjUzIjtzOjk6IgAqAF90YWJsZSI7czoxMzoiYnRJbWFnZVNsaWRlciI7czoxNDoibmF2aWdhdGlvblR5cGUiO3M6MToiMCI7czo3OiJ0aW1lb3V0IjtzOjQ6IjQwMDAiO3M6NToic3BlZWQiO3M6MzoiNTAwIjtzOjk6Im5vQW5pbWF0ZSI7czoxOiIwIjtzOjU6InBhdXNlIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7fQ=='),
(54, '', '2017-03-15 12:48:58', '2017-03-15 12:49:18', '', '1', 33, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjg6e3M6MzoiYklEIjtzOjI6IjU0IjtzOjk6IgAqAF90YWJsZSI7czoxMzoiYnRJbWFnZVNsaWRlciI7czoxNDoibmF2aWdhdGlvblR5cGUiO3M6MToiMCI7czo3OiJ0aW1lb3V0IjtzOjQ6IjQwMDAiO3M6NToic3BlZWQiO3M6MzoiNTAwIjtzOjk6Im5vQW5pbWF0ZSI7czoxOiIwIjtzOjU6InBhdXNlIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7fQ=='),
(55, '', '2017-03-15 13:54:43', '2017-03-15 13:55:02', NULL, '1', 12, 1, NULL),
(56, '', '2017-03-15 14:37:34', '2017-03-15 14:39:52', 'hero.php', '0', 33, 1, NULL),
(57, '', '2017-03-15 14:41:52', '2017-03-15 14:51:48', '', '1', 33, 1, NULL),
(58, '', '2017-03-15 14:54:48', '2017-03-15 14:54:48', '', '1', 27, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjEwOntzOjM6ImJJRCI7czoyOiI1OCI7czo5OiIAKgBfdGFibGUiO3M6MTQ6ImJ0Q29udGVudEltYWdlIjtzOjM6ImZJRCI7czoxOiIxIjtzOjEwOiJmT25zdGF0ZUlEIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7czo5OiJtYXhIZWlnaHQiO3M6MToiMCI7czoxMjoiZXh0ZXJuYWxMaW5rIjtzOjA6IiI7czoxNToiaW50ZXJuYWxMaW5rQ0lEIjtzOjE6IjEiO3M6NzoiYWx0VGV4dCI7czo0OiJMb2dvIjtzOjU6InRpdGxlIjtzOjQ6IkxvZ28iO30='),
(59, '', '2017-03-15 14:55:09', '2017-03-15 14:55:09', NULL, '0', 12, 1, NULL),
(60, '', '2017-03-15 15:16:18', '2017-03-15 15:17:22', '', '1', 33, 1, NULL),
(61, '', '2017-03-15 15:18:54', '2017-03-15 15:18:54', 'hero-slider.php', '1', 33, 1, NULL),
(62, '', '2017-03-15 15:35:45', '2017-03-15 15:35:45', NULL, '0', 33, 1, NULL),
(63, '', '2017-03-15 15:36:10', '2017-03-15 15:36:10', NULL, '0', 27, 1, NULL),
(64, '', '2017-03-15 15:36:19', '2017-03-15 15:36:19', NULL, '0', 27, 1, NULL),
(65, '', '2017-03-15 15:36:43', '2017-03-15 15:36:43', NULL, '0', 27, 1, NULL),
(66, '', '2017-03-15 15:37:47', '2017-03-15 15:37:47', NULL, '1', 12, 1, NULL),
(67, '', '2017-03-15 15:43:05', '2017-03-15 15:43:05', NULL, '1', 12, 1, NULL),
(68, '', '2017-03-15 15:46:18', '2017-03-15 15:46:18', NULL, '1', 12, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjM6e3M6MzoiYklEIjtzOjI6IjY4IjtzOjk6IgAqAF90YWJsZSI7czoxNDoiYnRDb250ZW50TG9jYWwiO3M6NzoiY29udGVudCI7czo0MTg6IjxoMT5XZWxsLXByZXBhcmVkIDxicj5ldmVudHMgJiBwcm9kdWN0aW9uczwvaDE+PHA+RWVuIGV2ZW5lbWVudCBvZiBwcm9kdWN0aWUgaXMgd2F0IG9ucyBiZXRyZWZ0IG5ldCB6b2FscyBoZXQgemVlciBteXRoaXNjaGUgZ2VyZWNodCBGdWd1LiBGYW50YXN0aXNjaCBhbHMgaGV0IGdlbWFha3Qgd29yZHQgZG9vciBpZW1hbmQgZGllIGVyIHZlcnN0YW5kIHZhbiBoZWVmdCBtYWFyIGZhdGFhbCBpbiBkZSBoYW5kZW4gdmFuIGllbWFuZCBkaWUgbmlldCB3ZWV0IHdhdCBoaWogZG9ldC4gRnJlc2ggRnVndSBjb21iaW5lZXJ0IHZlcmZyaXNzZW5kZSBza2lsbHMsIGtlbm5pcyBlbiBkZSBlcnZhcmluZyBkaWUgbm9kaWcgaXMgb20gZXZlbmVtZW50ZW4gb2YgcHJvZHVjdGllcyBvcCBlZW4gdG9wIG5pdmVhdSB0ZSBzZXJ2ZXJlbi4NCjwvcD4iO30='),
(69, '', '2017-03-15 15:51:26', '2017-03-15 15:51:26', 'hero-slider.php', '1', 33, 1, NULL),
(70, '', '2017-03-15 16:57:50', '2017-03-15 16:57:50', NULL, '1', 33, 1, NULL),
(71, '', '2017-03-16 09:01:54', '2017-03-16 09:01:54', NULL, '1', 12, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjM6e3M6MzoiYklEIjtzOjI6IjcxIjtzOjk6IgAqAF90YWJsZSI7czoxNDoiYnRDb250ZW50TG9jYWwiO3M6NzoiY29udGVudCI7czo0NDM6IjxoMj5Qcm9kdWN0aW9uczwvaDI+DQo8aDM+UFJPSkVDVFM8L2gzPg0KPHA+TmV0IHpvYWxzIGJpaiBvbnplIGV2ZW50cyBoYW50ZXJlbiB3aWogZGV6ZWxmZGUgd2Vya3dpanplIHZvb3IgaGV0IHByb2R1Y2VyZW4gdmFuIGNvbW1lcmNpYWxzLCBiZWRyaWpmc2ZpbG1zIGVuIGFuZGVyZSBhdWRpb3Zpc3VlbGUgcHJvZHVjdGllcy4gT256ZSBqYXJlbmxhbmdlIGVydmFyaW5nIGJpaiBkaXZlcnNlIG9tcm9lcGVuL3plbmRlcnMgZW4gY29tbWVyY2nDq2xlIHByb2R1Y2VudGVuIHpvcmdlbiBlcnZvb3IgZGF0IHV3IHByb2R1Y3RpZSBiaW5uZW4gZGUgcGxhbm5pbmcgZW4gYWZnZXNwcm9rZW4gYnVkZ2V0IGFmZ2Vyb25kIHdvcmR0LiBLbGVpbmUgb2YgZ3Jvb3QgRnJlc2ggRnVndSB6b3JndCBkYXQgYWwgaGFhciBwcm9kdWN0aWVzIOKAmHdlbGwtcHJlcGFyZWTigJkgemlqbi48L3A+Ijt9'),
(72, '', '2017-03-16 09:02:03', '2017-03-16 09:02:03', NULL, '1', 12, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjM6e3M6MzoiYklEIjtzOjI6IjcyIjtzOjk6IgAqAF90YWJsZSI7czoxNDoiYnRDb250ZW50TG9jYWwiO3M6NzoiY29udGVudCI7czo1MTE6IjxoMj5FdmVudHM8L2gyPg0KPGgzPlBST0pFQ1RTPC9oMz4NCjxwPjxzdHJvbmc+RWVuIGV2ZW50LCBwcm9kdWN0IGxhbmNlcmluZyBvZiBtZXJrIGFjdGl2YXRpZSBGcmVzaCBGdWd1IGhhbnRlZXJ0IGFsdGlqZCBkZSByZWdlbCDigJhJZiB5b3UgZmFpbCB0byBwcmVwYXJlLCB0aGVuIHByZXBhcmUgdG8gZmFpbOKAmS48L3N0cm9uZz4NCjwvcD4NCjxwPlZvb3Igb256ZSBvcGRyYWNodGdldmVycyB2ZXJ6b3JnZW4gd2lqIGhldCBoZWxlIHByb2NlcyB2b29yIGhldCBwZXJmZWN0ZSBldmVudC4gVmFuYWYgZGUgb250d2lra2VsaW5nIHZhbiBoZXQgY3JlYXRpZXZlIGNvbmNlcHQgdG90IGVuIG1ldCBkZSB2ZXJ6b3JnaW5nIHZhbiBlZW4gZ29vZGllYmFnIG5hIGFmbG9vcC4gVm9vciBlbGtlIHN0YXAgaGFudGVyZW4gd2lqIHNsaW1tZSBjaGVja2xpc3RzIG9tIGVydm9vciB0ZSB6b3JnZW4gZGF0IHV3IGV2ZW5lbWVudCB0b3QgaW4gZGUga2xlaW5zdGUgZGV0YWlscyBnZXJlZ2VsZCBpcy4NCjwvcD4iO30='),
(73, '', '2017-03-16 09:02:16', '2017-03-16 09:02:16', NULL, '1', 12, 1, NULL),
(74, '', '2017-03-16 09:02:43', '2017-03-16 09:02:43', NULL, '1', 12, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjM6e3M6MzoiYklEIjtzOjI6Ijc0IjtzOjk6IgAqAF90YWJsZSI7czoxNDoiYnRDb250ZW50TG9jYWwiO3M6NzoiY29udGVudCI7czo0MTc6IjxoMj5PbnplIGZ1Z3UgbWFzdGVyPC9oMj4NCjxoMz5PdmVyIG9uczwvaDM+DQo8cD5FZW4gZXZlbmVtZW50IG9mIHByb2R1Y3RpZSBpcyB3YXQgb25zIGJldHJlZnQgbmV0IHpvYWxzIGhldCB6ZWVyIG15dGhpc2NoZSBnZXJlY2h0IEZ1Z3UuIEZhbnRhc3Rpc2NoIGFscyBoZXQgZ2VtYWFrdCB3b3JkdCBkb29yIGllbWFuZCBkaWUgZXIgdmVyc3RhbmQgdmFuIGhlZWZ0IG1hYXIgZmF0YWFsIGluIGRlIGhhbmRlbiB2YW4gaWVtYW5kIGRpZSBuaWV0IHdlZXQgd2F0IGhpaiBkb2V0LiBGcmVzaCBGdWd1IGNvbWJpbmVlcnQgdmVyZnJpc3NlbmRlIHNraWxscywga2VubmlzIGVuIGRlIGVydmFyaW5nIGRpZSBub2RpZyBpcyBvbSBldmVuZW1lbnRlbiBvZiBwcm9kdWN0aWVzIG9wIGVlbiB0b3Agbml2ZWF1IHRlIHNlcnZlcmVuLg0KPC9wPiI7fQ=='),
(75, '', '2017-03-16 09:24:16', '2017-03-16 09:29:19', 'updates-slider', '0', 33, 1, NULL),
(76, '', '2017-03-16 09:29:43', '2017-03-16 09:29:43', 'updates-slider', '1', 33, 1, NULL),
(77, '', '2017-03-16 09:41:39', '2017-03-16 09:41:39', 'updates-slider', '1', 33, 1, NULL),
(78, '', '2017-03-16 09:42:52', '2017-03-16 09:42:52', 'updates-slider', '1', 33, 1, NULL),
(79, '', '2017-03-16 09:46:34', '2017-03-16 09:46:34', NULL, '1', 12, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjM6e3M6MzoiYklEIjtzOjI6Ijc5IjtzOjk6IgAqAF90YWJsZSI7czoxNDoiYnRDb250ZW50TG9jYWwiO3M6NzoiY29udGVudCI7czozMjY6IjxoMj5Mb3JlbSBpcHN1bSBkb2xvciBzYXRlcjwvaDI+DQo8aDM+ZGUgZmlqbnByb2V2ZXJzPGJyPjwvaDM+DQo8cD5Mb3JlbSBpcHN1bSBkb2xvciBzYXRlciBpbiB0ZXJyYSBtZXQgY29tcHRlciB2ZXJpdGFzIHNldCBpbiBkZWxpbyBtZW50cmFzLjwvcD4gDQo8cD5Mb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc3VyIGFkaXBpc2ljaW5nIGVsaXQsIHNlZCBkbyBlaXVzbW9kIHZlbmlhbSwgcXVpcyBub3N0cnVkIGV4ZXJjaXRhdGlvbiB1bGxhbWNvIGluIHZvbHVwIHZlbGl0IGVzc2UgY2lsbHVtIGRvbG9yZSBldSBmdWdpYXQgbnVsbGEgcGFyaWF0dXIuDQo8L3A+Ijt9'),
(80, '', '2017-03-16 09:50:09', '2017-03-16 09:51:30', NULL, '1', 27, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjEwOntzOjM6ImJJRCI7czoyOiI4MCI7czo5OiIAKgBfdGFibGUiO3M6MTQ6ImJ0Q29udGVudEltYWdlIjtzOjM6ImZJRCI7czoxOiI0IjtzOjEwOiJmT25zdGF0ZUlEIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7czo5OiJtYXhIZWlnaHQiO3M6MToiMCI7czoxMjoiZXh0ZXJuYWxMaW5rIjtzOjA6IiI7czoxNToiaW50ZXJuYWxMaW5rQ0lEIjtzOjE6IjAiO3M6NzoiYWx0VGV4dCI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjt9'),
(81, '', '2017-03-16 09:52:55', '2017-03-16 09:52:55', NULL, '1', 27, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjEwOntzOjM6ImJJRCI7czoyOiI4MSI7czo5OiIAKgBfdGFibGUiO3M6MTQ6ImJ0Q29udGVudEltYWdlIjtzOjM6ImZJRCI7czoxOiI1IjtzOjEwOiJmT25zdGF0ZUlEIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7czo5OiJtYXhIZWlnaHQiO3M6MToiMCI7czoxMjoiZXh0ZXJuYWxMaW5rIjtzOjA6IiI7czoxNToiaW50ZXJuYWxMaW5rQ0lEIjtzOjE6IjAiO3M6NzoiYWx0VGV4dCI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjt9'),
(82, '', '2017-03-16 09:54:17', '2017-03-16 09:54:17', 'updates-slider', '1', 33, 1, NULL),
(83, '', '2017-03-16 09:54:57', '2017-03-16 09:54:57', NULL, '1', 27, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjEwOntzOjM6ImJJRCI7czoyOiI4MyI7czo5OiIAKgBfdGFibGUiO3M6MTQ6ImJ0Q29udGVudEltYWdlIjtzOjM6ImZJRCI7czoxOiI3IjtzOjEwOiJmT25zdGF0ZUlEIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7czo5OiJtYXhIZWlnaHQiO3M6MToiMCI7czoxMjoiZXh0ZXJuYWxMaW5rIjtzOjA6IiI7czoxNToiaW50ZXJuYWxMaW5rQ0lEIjtzOjE6IjAiO3M6NzoiYWx0VGV4dCI7czowOiIiO3M6NToidGl0bGUiO3M6MDoiIjt9'),
(84, '', '2017-03-16 09:56:19', '2017-03-16 09:56:19', NULL, '1', 33, 1, NULL),
(85, '', '2017-03-16 09:57:43', '2017-03-16 09:57:43', 'hero-slider.php', '1', 33, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjg6e3M6MzoiYklEIjtzOjI6Ijg1IjtzOjk6IgAqAF90YWJsZSI7czoxMzoiYnRJbWFnZVNsaWRlciI7czoxNDoibmF2aWdhdGlvblR5cGUiO3M6MToiMCI7czo3OiJ0aW1lb3V0IjtzOjQ6IjQwMDAiO3M6NToic3BlZWQiO3M6MzoiNTAwIjtzOjk6Im5vQW5pbWF0ZSI7czoxOiIwIjtzOjU6InBhdXNlIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7fQ=='),
(86, '', '2017-03-16 09:58:06', '2017-03-16 09:58:06', NULL, '1', 33, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjg6e3M6MzoiYklEIjtzOjI6Ijg2IjtzOjk6IgAqAF90YWJsZSI7czoxMzoiYnRJbWFnZVNsaWRlciI7czoxNDoibmF2aWdhdGlvblR5cGUiO3M6MToiMCI7czo3OiJ0aW1lb3V0IjtzOjQ6IjQwMDAiO3M6NToic3BlZWQiO3M6MzoiNTAwIjtzOjk6Im5vQW5pbWF0ZSI7czoxOiIwIjtzOjU6InBhdXNlIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7fQ=='),
(87, '', '2017-03-16 09:59:39', '2017-03-16 09:59:39', 'updates-slider', '1', 33, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjg6e3M6MzoiYklEIjtzOjI6Ijg3IjtzOjk6IgAqAF90YWJsZSI7czoxMzoiYnRJbWFnZVNsaWRlciI7czoxNDoibmF2aWdhdGlvblR5cGUiO3M6MToiMCI7czo3OiJ0aW1lb3V0IjtzOjQ6IjQwMDAiO3M6NToic3BlZWQiO3M6MzoiNTAwIjtzOjk6Im5vQW5pbWF0ZSI7czoxOiIwIjtzOjU6InBhdXNlIjtzOjE6IjAiO3M6ODoibWF4V2lkdGgiO3M6MToiMCI7fQ=='),
(88, '', '2017-03-16 10:06:04', '2017-03-16 10:06:04', NULL, '0', 12, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjM6e3M6MzoiYklEIjtzOjI6Ijg4IjtzOjk6IgAqAF90YWJsZSI7czoxNDoiYnRDb250ZW50TG9jYWwiO3M6NzoiY29udGVudCI7czo1MDoiPGgyPkxvcmVtIGlwc3VtIGRvbG9yIHNhdGVyPC9oMj48aDM+Q29udGFjdA0KPC9oMz4iO30='),
(89, '', '2017-03-16 10:06:24', '2017-03-16 10:06:24', NULL, '0', 12, 1, 'TzozMjoiQ29uY3JldGVcQ29yZVxMZWdhY3lcQmxvY2tSZWNvcmQiOjM6e3M6MzoiYklEIjtzOjI6Ijg5IjtzOjk6IgAqAF90YWJsZSI7czoxNDoiYnRDb250ZW50TG9jYWwiO3M6NzoiY29udGVudCI7czozMzc6IjxwPkxvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zdXIgYWRpcGlzaWNpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkdW50IHV0IGxhYm9yZSBldCBkb2xvcmUgbWFnbmEgYWxpcXVhLg0KPC9wPg0KPHA+VXQgZW5pbSBhaW5pbSB2ZW5pYW0sIHF1aXMgbm9zdHJ1ZCBleGVyY2l0YXRpb24gdWxsYW1jbyBsYWJvcmlzIG5pc2kgdXQgYWxpcXVpcCBleCBlYSBjb21tb2RvIGNvbnNldC4gRHVpcyBhdXRlIGlydXJlIGRvbG9yIGluIHJlcHJlaGVuZGVyaXQgaW4gdm9sdXAgdmVsaXQgZXNzZSBjaWxsdW0gZG9sb3JlIGV1IGZ1Z2lhdCBudWxsYSBwYXJpYXR1ci4NCjwvcD4iO30='),
(90, '', '2017-03-16 10:08:36', '2017-03-16 10:22:33', 'footer-form', '0', 17, 1, NULL),
(91, '', '2017-03-16 10:32:53', '2017-03-16 10:32:53', '', '1', 17, 1, NULL),
(92, '', '2017-03-16 11:57:27', '2017-03-16 11:57:27', '', '1', 17, 1, NULL),
(93, '', '2017-03-16 11:57:57', '2017-03-16 11:57:57', '', '1', 17, 1, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `BlockTypePermissionBlockTypeAccessList`
--

CREATE TABLE `BlockTypePermissionBlockTypeAccessList` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `BlockTypePermissionBlockTypeAccessListCustom`
--

CREATE TABLE `BlockTypePermissionBlockTypeAccessListCustom` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `btID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `BlockTypes`
--

CREATE TABLE `BlockTypes` (
  `btID` int(10) UNSIGNED NOT NULL,
  `btHandle` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `btName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `btDescription` text COLLATE utf8_unicode_ci,
  `btCopyWhenPropagate` tinyint(1) NOT NULL DEFAULT '0',
  `btIncludeAll` tinyint(1) NOT NULL DEFAULT '0',
  `btIsInternal` tinyint(1) NOT NULL DEFAULT '0',
  `btSupportsInlineAdd` tinyint(1) NOT NULL DEFAULT '0',
  `btSupportsInlineEdit` tinyint(1) NOT NULL DEFAULT '0',
  `btIgnorePageThemeGridFrameworkContainer` tinyint(1) NOT NULL DEFAULT '0',
  `btDisplayOrder` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `btInterfaceWidth` int(10) UNSIGNED NOT NULL DEFAULT '400',
  `btInterfaceHeight` int(10) UNSIGNED NOT NULL DEFAULT '400',
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `BlockTypes`
--

INSERT INTO `BlockTypes` (`btID`, `btHandle`, `btName`, `btDescription`, `btCopyWhenPropagate`, `btIncludeAll`, `btIsInternal`, `btSupportsInlineAdd`, `btSupportsInlineEdit`, `btIgnorePageThemeGridFrameworkContainer`, `btDisplayOrder`, `btInterfaceWidth`, `btInterfaceHeight`, `pkgID`) VALUES
(1, 'core_area_layout', 'Area Layout', 'Proxy block for area layouts.', 0, 0, 1, 1, 1, 0, 0, 400, 400, 0),
(2, 'core_page_type_composer_control_output', 'Composer Control', 'Proxy block for blocks that need to be output through composer.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(3, 'core_scrapbook_display', 'Scrapbook Display', 'Proxy block for blocks pasted through the scrapbook.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(4, 'core_stack_display', 'Stack Display', 'Proxy block for stacks added through the UI.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(5, 'core_conversation', 'Conversation', 'Displays conversations on a page.', 1, 0, 0, 0, 0, 0, 0, 400, 400, 0),
(6, 'dashboard_featured_addon', 'Dashboard Featured Add-On', 'Features an add-on from concrete5.org.', 0, 0, 1, 0, 0, 0, 0, 300, 100, 0),
(7, 'dashboard_featured_theme', 'Dashboard Featured Theme', 'Features a theme from concrete5.org.', 0, 0, 1, 0, 0, 0, 0, 300, 100, 0),
(8, 'dashboard_newsflow_latest', 'Dashboard Newsflow Latest', 'Grabs the latest newsflow data from concrete5.org.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(9, 'dashboard_app_status', 'Dashboard App Status', 'Displays update and welcome back information on your dashboard.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(10, 'dashboard_site_activity', 'Dashboard Site Activity', 'Displays a summary of website activity.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(11, 'autonav', 'Auto-Nav', 'Creates navigation trees and sitemaps.', 0, 0, 0, 0, 0, 0, 0, 800, 350, 0),
(12, 'content', 'Content', 'HTML/WYSIWYG Editor Content.', 0, 0, 0, 1, 1, 0, 0, 600, 465, 0),
(13, 'date_navigation', 'Date Navigation', 'Displays a list of months to filter a page list by.', 0, 0, 0, 0, 0, 0, 0, 400, 450, 0),
(14, 'external_form', 'External Form', 'Include external forms in the filesystem and place them on pages.', 0, 0, 0, 0, 0, 0, 0, 370, 175, 0),
(15, 'file', 'File', 'Link to files stored in the asset library.', 0, 0, 0, 0, 0, 0, 0, 300, 320, 0),
(16, 'page_attribute_display', 'Page Attribute Display', 'Displays the value of a page attribute for the current page.', 0, 0, 0, 0, 0, 0, 0, 500, 365, 0),
(17, 'form', 'Form', 'Build simple forms and surveys.', 1, 0, 0, 0, 0, 0, 0, 420, 430, 0),
(18, 'page_title', 'Page Title', 'Displays a Page''s Title', 0, 0, 0, 0, 0, 0, 0, 400, 400, 0),
(19, 'feature', 'Feature', 'Displays an icon, a title, and a short paragraph description.', 0, 0, 0, 0, 0, 0, 0, 400, 520, 0),
(20, 'topic_list', 'Topic List', 'Displays a list of your site''s topics, allowing you to click on them to filter a page list.', 0, 0, 0, 0, 0, 0, 0, 400, 400, 0),
(21, 'social_links', 'Social Links', 'Allows users to add social icons to their website', 0, 0, 0, 0, 0, 0, 0, 400, 400, 0),
(22, 'testimonial', 'Testimonial', 'Displays a quote or paragraph next to biographical information and a person''s picture.', 0, 0, 0, 0, 0, 0, 0, 450, 560, 0),
(23, 'share_this_page', 'Share This Page', 'Allows users to share this page with social networks.', 0, 0, 0, 0, 0, 0, 0, 400, 400, 0),
(24, 'google_map', 'Google Map', 'Enter an address and a Google Map of that location will be placed in your page.', 0, 0, 0, 0, 0, 0, 0, 400, 460, 0),
(25, 'html', 'HTML', 'For adding HTML by hand.', 0, 0, 0, 0, 0, 1, 0, 600, 500, 0),
(26, 'horizontal_rule', 'Horizontal Rule', 'Adds a thin hairline horizontal divider to the page.', 0, 0, 0, 0, 0, 1, 0, 400, 400, 0),
(27, 'image', 'Image', 'Adds images and onstates from the library to pages.', 0, 0, 0, 0, 0, 0, 0, 400, 550, 0),
(28, 'faq', 'FAQ', 'Frequently Asked Questions Block', 0, 0, 0, 0, 0, 0, 0, 600, 465, 0),
(29, 'next_previous', 'Next & Previous Nav', 'Navigate through sibling pages.', 0, 0, 0, 0, 0, 0, 0, 430, 400, 0),
(30, 'page_list', 'Page List', 'List pages based on type, area.', 0, 0, 0, 0, 0, 0, 0, 800, 350, 0),
(31, 'rss_displayer', 'RSS Displayer', 'Fetch, parse and display the contents of an RSS or Atom feed.', 0, 0, 0, 0, 0, 0, 0, 400, 550, 0),
(32, 'search', 'Search', 'Add a search box to your site.', 0, 0, 0, 0, 0, 0, 0, 400, 420, 0),
(33, 'image_slider', 'Image Slider', 'Display your images and captions in an attractive slideshow format.', 0, 0, 0, 0, 0, 1, 0, 600, 550, 0),
(34, 'survey', 'Survey', 'Provide a simple survey, along with results in a pie chart format.', 0, 0, 0, 0, 0, 0, 0, 420, 400, 0),
(35, 'switch_language', 'Switch Language', 'Adds a front-end language switcher to your website.', 0, 0, 0, 0, 0, 0, 0, 500, 150, 0),
(36, 'tags', 'Tags', 'List pages based on type, area.', 0, 0, 0, 0, 0, 0, 0, 450, 439, 0),
(37, 'video', 'Video Player', 'Embeds uploaded video into a web page. Supports WebM, Ogg, and Quicktime/MPEG4 formats.', 0, 0, 0, 0, 0, 0, 0, 320, 270, 0),
(38, 'youtube', 'YouTube Video', 'Embeds a YouTube Video in your web page.', 0, 0, 0, 0, 0, 0, 0, 400, 490, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `BlockTypeSetBlockTypes`
--

CREATE TABLE `BlockTypeSetBlockTypes` (
  `btID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `btsID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `displayOrder` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `BlockTypeSetBlockTypes`
--

INSERT INTO `BlockTypeSetBlockTypes` (`btID`, `btsID`, `displayOrder`) VALUES
(12, 1, 0),
(25, 1, 1),
(27, 1, 2),
(15, 1, 3),
(26, 1, 4),
(19, 1, 5),
(11, 2, 0),
(18, 2, 1),
(28, 2, 2),
(30, 2, 3),
(29, 2, 4),
(13, 2, 5),
(36, 2, 6),
(20, 2, 7),
(31, 2, 8),
(35, 2, 9),
(17, 3, 0),
(32, 3, 1),
(14, 3, 2),
(34, 4, 0),
(5, 4, 1),
(21, 4, 2),
(22, 4, 3),
(23, 4, 4),
(16, 5, 0),
(33, 5, 1),
(37, 5, 2),
(38, 5, 3),
(24, 5, 4);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `BlockTypeSets`
--

CREATE TABLE `BlockTypeSets` (
  `btsID` int(10) UNSIGNED NOT NULL,
  `btsName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `btsHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `btsDisplayOrder` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `BlockTypeSets`
--

INSERT INTO `BlockTypeSets` (`btsID`, `btsName`, `btsHandle`, `pkgID`, `btsDisplayOrder`) VALUES
(1, 'Basic', 'basic', 0, 0),
(2, 'Navigation', 'navigation', 0, 0),
(3, 'Forms', 'form', 0, 0),
(4, 'Social Networking', 'social', 0, 0),
(5, 'Multimedia', 'multimedia', 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btContentFile`
--

CREATE TABLE `btContentFile` (
  `bID` int(10) UNSIGNED NOT NULL,
  `fID` int(10) UNSIGNED DEFAULT NULL,
  `fileLinkText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filePassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forceDownload` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btContentImage`
--

CREATE TABLE `btContentImage` (
  `bID` int(10) UNSIGNED NOT NULL,
  `fID` int(10) UNSIGNED DEFAULT '0',
  `fOnstateID` int(10) UNSIGNED DEFAULT '0',
  `maxWidth` int(10) UNSIGNED DEFAULT '0',
  `maxHeight` int(10) UNSIGNED DEFAULT '0',
  `externalLink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internalLinkCID` int(10) UNSIGNED DEFAULT '0',
  `altText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `btContentImage`
--

INSERT INTO `btContentImage` (`bID`, `fID`, `fOnstateID`, `maxWidth`, `maxHeight`, `externalLink`, `internalLinkCID`, `altText`, `title`) VALUES
(11, 1, 0, 0, 0, '', 0, 'Logo', 'Logo'),
(13, 1, 0, 0, 0, '', 0, 'Logo', 'Logo'),
(14, 1, 0, 0, 0, '', 0, 'Logo', 'Logo'),
(15, 1, 0, 0, 0, '/', 0, 'Logo', 'Logo'),
(16, 1, 0, 0, 0, '', 1, 'Logo', 'Logo'),
(17, 1, 0, 0, 0, '', 1, 'Logo', 'Logo'),
(36, 1, 0, 0, 0, '', 0, '', ''),
(39, 1, 0, 0, 0, '', 0, '', ''),
(41, 1, 0, 0, 0, '', 0, '', ''),
(58, 1, 0, 0, 0, '', 1, 'Logo', 'Logo'),
(63, 1, 0, 0, 0, '', 0, '', ''),
(64, 1, 0, 0, 0, '', 0, '', ''),
(65, 1, 0, 0, 0, '', 0, '', ''),
(80, 4, 0, 0, 0, '', 0, '', ''),
(81, 5, 0, 0, 0, '', 0, '', ''),
(83, 7, 0, 0, 0, '', 0, '', '');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btContentLocal`
--

CREATE TABLE `btContentLocal` (
  `bID` int(10) UNSIGNED NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `btContentLocal`
--

INSERT INTO `btContentLocal` (`bID`, `content`) VALUES
(1, '<div style="padding: 40px; text-align: center">\n<iframe width="853" height="480" src="//www.youtube.com/embed/VB-R71zk06U" frameborder="0" allowfullscreen></iframe>\n                                    </div>'),
(26, '<h1>Well-prepared events & productions</h1><p><span class="author-JsvNp1VykQbC i">Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu.</span></p><p><span class="author-JsvNp1VykQbC i">Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet.</span></p><p>Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.</p>'),
(27, '<h1>Well-prepared <br>events & productions</h1><p>Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu.\r\n</p><p>Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet.\r\n</p><p>Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.\r\n</p>'),
(28, '<h1>Well-prepared <br>events & productions</h1><p style="column-count: 2;">Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu. Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet. Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.\r\n</p>'),
(29, '<h1>Well-prepared <br>events & productions</h1><p style="column-count: 2;">Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu. Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet. Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.\r\n</p><p><a href="#">Lees meer</a></p>'),
(30, '<h1>Well-prepared <br>events & productions</h1>\r\n<p style="column-count: 2;">Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu. Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet. Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.\r\n</p>\r\n<a href="#" class="btn btn__lees-meer">Lees meer</a>\r\n'),
(31, '<h1>Well-prepared <br>events & productions</h1>\r\n<p style="column-count: 2;">Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu. Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet. Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.\r\n</p>\r\n<p><a href="#" class="btn btn__lees--meer">Lees meer</a>\r\n</p>'),
(32, '<h1>Well-prepared <br>events & productions</h1><p style="column-count: 2;">Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu. Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet. Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.\r\n</p><p style="text-align: center;"><a href="#" class="btn btn__lees--meer">Lees meer</a>\r\n</p>'),
(34, '<h2>Events</h2><h3>PROJECTS</h3><p><i>Een event, product lancering of merk activatie Fresh Fugu hanteert altijd de regel ‘If you fail to prepare, then prepare to fail’. Voor onze opdrachtgevers verzorgen wij het hele proces voor het perfecte event. Vanaf de ontwikkeling van het creatieve concept tot en met de verzorging van een goodiebag na afloop. Voor elke stap hanteren wij slimme checklists om ervoor te zorgen dat uw evenement tot in de kleinste details geregeld is.</i></p>'),
(35, '<h2>Productions</h2><h3>PROJECTS</h3><p><i>Net zoals bij onze events hanteren wij dezelfde werkwijze voor het produceren van commercials, bedrijfsfilms en andere audiovisuele producties. Onze jarenlange ervaring bij diverse omroepen/zenders en commerciële producenten zorgen ervoor dat uw productie binnen de planning en afgesproken budget afgerond wordt. Kleine of groot Fresh Fugu zorgt dat al haar producties ‘well-prepared’ zijn.</i><span class="redactor-invisible-space"><br></span></p>'),
(40, '<h2>Lorem ipsum dolor sater</h2>\r\n<h3>de fijnproevers<br></h3>\r\n<p>Lorem ipsum dolor sater in terra met compter veritas set in delio mentras.\r\n</p>\r\n<p>Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod veniam, quis nostrud exercitation ullamco in volup velit esse cillum dolore eu fugiat nulla pariatur.\r\n</p>'),
(42, '<h2>Onze fugu master</h2><h3>Over ons</h3><p><span class="author-JsvNp1VykQbC i">Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu.</span></p><p><span class="author-JsvNp1VykQbC i">Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet.</span></p><p>Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.</p>'),
(55, '<h2>Events</h2><h3>PROJECTS</h3><p><strong>Een event, product lancering of merk activatie Fresh Fugu hanteert altijd de regel ‘If you fail to prepare, then prepare to fail’.</strong>\r\n</p><p>Voor onze opdrachtgevers verzorgen wij het hele proces voor het perfecte event. Vanaf de ontwikkeling van het creatieve concept tot en met de verzorging van een goodiebag na afloop. Voor elke stap hanteren wij slimme checklists om ervoor te zorgen dat uw evenement tot in de kleinste details geregeld is.\r\n</p>'),
(59, ''),
(66, '<h1>Well-prepared events & productions</h1><p><span class="author-JsvNp1VykQbC i">Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu.</span></p><p><span class="author-JsvNp1VykQbC i">Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet.</span></p><p><span class="author-JsvNp1VykQbC i">Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.</span></p>'),
(67, '<h1>Well-prepared events & productions</h1>\r\n<p>Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu. Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet. Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.</p>'),
(68, '<h1>Well-prepared <br>events & productions</h1><p>Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu. Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet. Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.\r\n</p>'),
(71, '<h2>Productions</h2>\r\n<h3>PROJECTS</h3>\r\n<p>Net zoals bij onze events hanteren wij dezelfde werkwijze voor het produceren van commercials, bedrijfsfilms en andere audiovisuele producties. Onze jarenlange ervaring bij diverse omroepen/zenders en commerciële producenten zorgen ervoor dat uw productie binnen de planning en afgesproken budget afgerond wordt. Kleine of groot Fresh Fugu zorgt dat al haar producties ‘well-prepared’ zijn.</p>'),
(72, '<h2>Events</h2>\r\n<h3>PROJECTS</h3>\r\n<p><strong>Een event, product lancering of merk activatie Fresh Fugu hanteert altijd de regel ‘If you fail to prepare, then prepare to fail’.</strong>\r\n</p>\r\n<p>Voor onze opdrachtgevers verzorgen wij het hele proces voor het perfecte event. Vanaf de ontwikkeling van het creatieve concept tot en met de verzorging van een goodiebag na afloop. Voor elke stap hanteren wij slimme checklists om ervoor te zorgen dat uw evenement tot in de kleinste details geregeld is.\r\n</p>'),
(73, '<h2>Lorem ipsum dolor sater</h2>\r\n<h3>de fijnproevers<br></h3>\r\n<p>Lorem ipsum dolor sater in terra met compter veritas set in delio mentras. Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod veniam, quis nostrud exercitation ullamco in volup velit esse cillum dolore eu fugiat nulla pariatur.\r\n</p>'),
(74, '<h2>Onze fugu master</h2>\r\n<h3>Over ons</h3>\r\n<p>Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu. Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet. Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.\r\n</p>'),
(79, '<h2>Lorem ipsum dolor sater</h2>\r\n<h3>de fijnproevers<br></h3>\r\n<p>Lorem ipsum dolor sater in terra met compter veritas set in delio mentras.</p> \r\n<p>Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod veniam, quis nostrud exercitation ullamco in volup velit esse cillum dolore eu fugiat nulla pariatur.\r\n</p>'),
(88, '<h2>Lorem ipsum dolor sater</h2><h3>Contact\r\n</h3>'),
(89, '<p>Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua.\r\n</p>\r\n<p>Ut enim ainim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehenderit in volup velit esse cillum dolore eu fugiat nulla pariatur.\r\n</p>');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btCoreAreaLayout`
--

CREATE TABLE `btCoreAreaLayout` (
  `bID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arLayoutID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btCoreConversation`
--

CREATE TABLE `btCoreConversation` (
  `bID` int(10) UNSIGNED NOT NULL,
  `cnvID` int(11) DEFAULT NULL,
  `enablePosting` int(11) DEFAULT '1',
  `paginate` tinyint(1) NOT NULL DEFAULT '1',
  `itemsPerPage` smallint(5) UNSIGNED NOT NULL DEFAULT '50',
  `displayMode` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'threaded',
  `orderBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'date_desc',
  `enableOrdering` tinyint(1) NOT NULL DEFAULT '1',
  `enableCommentRating` tinyint(1) NOT NULL DEFAULT '1',
  `displayPostingForm` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'top',
  `addMessageLabel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dateFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'default',
  `customDateFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btCorePageTypeComposerControlOutput`
--

CREATE TABLE `btCorePageTypeComposerControlOutput` (
  `bID` int(10) UNSIGNED NOT NULL,
  `ptComposerOutputControlID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `btCorePageTypeComposerControlOutput`
--

INSERT INTO `btCorePageTypeComposerControlOutput` (`bID`, `ptComposerOutputControlID`) VALUES
(9, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btCoreScrapbookDisplay`
--

CREATE TABLE `btCoreScrapbookDisplay` (
  `bID` int(10) UNSIGNED NOT NULL,
  `bOriginalID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btCoreStackDisplay`
--

CREATE TABLE `btCoreStackDisplay` (
  `bID` int(10) UNSIGNED NOT NULL,
  `stID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btDashboardNewsflowLatest`
--

CREATE TABLE `btDashboardNewsflowLatest` (
  `bID` int(10) UNSIGNED NOT NULL,
  `slot` varchar(1) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `btDashboardNewsflowLatest`
--

INSERT INTO `btDashboardNewsflowLatest` (`bID`, `slot`) VALUES
(4, 'A'),
(5, 'B'),
(8, 'C');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btDateNavigation`
--

CREATE TABLE `btDateNavigation` (
  `bID` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `filterByParent` tinyint(1) DEFAULT '0',
  `redirectToResults` tinyint(1) DEFAULT '0',
  `cParentID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cTargetID` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'this field is where the links will direct you',
  `ptID` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btExternalForm`
--

CREATE TABLE `btExternalForm` (
  `bID` int(10) UNSIGNED NOT NULL,
  `filename` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btFaq`
--

CREATE TABLE `btFaq` (
  `bID` int(10) UNSIGNED NOT NULL,
  `blockTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btFaqEntries`
--

CREATE TABLE `btFaqEntries` (
  `id` int(10) UNSIGNED NOT NULL,
  `bID` int(10) UNSIGNED DEFAULT NULL,
  `linkTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btFeature`
--

CREATE TABLE `btFeature` (
  `bID` int(10) UNSIGNED NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paragraph` text COLLATE utf8_unicode_ci,
  `externalLink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internalLinkCID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btForm`
--

CREATE TABLE `btForm` (
  `bID` int(10) UNSIGNED NOT NULL,
  `questionSetId` int(10) UNSIGNED DEFAULT '0',
  `surveyName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submitText` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Submit',
  `thankyouMsg` text COLLATE utf8_unicode_ci,
  `notifyMeOnSubmission` tinyint(1) NOT NULL DEFAULT '0',
  `recipientEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayCaptcha` int(11) DEFAULT '1',
  `redirectCID` int(11) DEFAULT '0',
  `addFilesToSet` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `btForm`
--

INSERT INTO `btForm` (`bID`, `questionSetId`, `surveyName`, `submitText`, `thankyouMsg`, `notifyMeOnSubmission`, `recipientEmail`, `displayCaptcha`, `redirectCID`, `addFilesToSet`) VALUES
(90, 1489655214, 'Contact', 'Verzenden', 'Bedankt voor uw bericht!', 0, '', 0, 0, 0),
(91, 1489655214, 'Contact', 'Verzenden', 'Bedankt voor uw bericht!', 0, '', 0, 0, 0),
(92, 1489655214, 'Contact', 'Verzenden', 'Bedankt voor uw bericht!', 0, '', 0, 0, 0),
(93, 1489655214, 'Contact', 'Verzenden', 'Bedankt voor uw bericht!', 0, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btFormAnswers`
--

CREATE TABLE `btFormAnswers` (
  `aID` int(10) UNSIGNED NOT NULL,
  `asID` int(10) UNSIGNED DEFAULT '0',
  `msqID` int(10) UNSIGNED DEFAULT '0',
  `answer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `answerLong` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btFormAnswerSet`
--

CREATE TABLE `btFormAnswerSet` (
  `asID` int(10) UNSIGNED NOT NULL,
  `questionSetId` int(10) UNSIGNED DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btFormQuestions`
--

CREATE TABLE `btFormQuestions` (
  `qID` int(10) UNSIGNED NOT NULL,
  `msqID` int(10) UNSIGNED DEFAULT '0',
  `bID` int(10) UNSIGNED DEFAULT '0',
  `questionSetId` int(10) UNSIGNED DEFAULT '0',
  `question` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inputType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8_unicode_ci,
  `position` int(10) UNSIGNED DEFAULT '1000',
  `width` int(10) UNSIGNED DEFAULT '50',
  `height` int(10) UNSIGNED DEFAULT '3',
  `defaultDate` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `required` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `btFormQuestions`
--

INSERT INTO `btFormQuestions` (`qID`, `msqID`, `bID`, `questionSetId`, `question`, `inputType`, `options`, `position`, `width`, `height`, `defaultDate`, `required`) VALUES
(1, 1, 90, 1489655214, 'Uw voornaam...', 'field', '', 1000, 50, 3, '', 1),
(2, 2, 90, 1489655214, 'Uw achternaam...', 'field', '', 1000, 50, 3, '', 1),
(3, 3, 90, 1489655214, 'Uw e-mailadres...', 'email', 'a:1:{s:22:"send_notification_from";i:0;}', 1000, 0, 0, '', 1),
(4, 4, 90, 1489655214, 'Uw telefoonnummer...', 'telephone', '', 1000, 0, 0, '', 1),
(5, 5, 90, 1489655214, 'Uw bericht...', 'text', '', 1000, 50, 3, '', 1),
(6, 1, 91, 1489655214, 'Uw voornaam...', 'field', '', 1000, 50, 3, '', 1),
(7, 2, 91, 1489655214, 'Uw achternaam...', 'field', '', 1000, 50, 3, '', 1),
(8, 3, 91, 1489655214, 'Uw e-mailadres...', 'email', 'a:1:{s:22:"send_notification_from";i:0;}', 1000, 0, 0, '', 1),
(9, 4, 91, 1489655214, 'Uw telefoonnummer...', 'telephone', '', 1000, 0, 0, '', 1),
(10, 5, 91, 1489655214, 'Uw bericht...', 'text', '', 1000, 50, 3, '', 1),
(11, 1, 92, 1489655214, 'Uw voornaam...', 'field', '', 1000, 50, 3, '', 1),
(12, 2, 92, 1489655214, 'Uw achternaam...', 'field', '', 1000, 50, 3, '', 1),
(13, 3, 92, 1489655214, 'Uw e-mailadres...', 'email', 'a:1:{s:22:"send_notification_from";i:0;}', 1000, 0, 0, '', 1),
(14, 4, 92, 1489655214, 'Uw telefoonnummer...', 'telephone', '', 1000, 0, 0, '', 1),
(15, 5, 92, 1489655214, 'Uw bericht...', 'text', '', 1000, 50, 3, '', 1),
(16, 5, 93, 1489655214, 'Uw bericht...', 'text', '', 1000, 50, 1, '', 1),
(17, 1, 93, 1489655214, 'Uw voornaam...', 'field', '', 1000, 50, 3, '', 1),
(18, 2, 93, 1489655214, 'Uw achternaam...', 'field', '', 1000, 50, 3, '', 1),
(19, 3, 93, 1489655214, 'Uw e-mailadres...', 'email', 'a:1:{s:22:"send_notification_from";i:0;}', 1000, 0, 0, '', 1),
(20, 4, 93, 1489655214, 'Uw telefoonnummer...', 'telephone', '', 1000, 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btGoogleMap`
--

CREATE TABLE `btGoogleMap` (
  `bID` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `zoom` smallint(6) DEFAULT NULL,
  `width` varchar(8) COLLATE utf8_unicode_ci DEFAULT '100%',
  `height` varchar(8) COLLATE utf8_unicode_ci DEFAULT '400px',
  `scrollwheel` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btImageSlider`
--

CREATE TABLE `btImageSlider` (
  `bID` int(10) UNSIGNED NOT NULL,
  `navigationType` int(10) UNSIGNED DEFAULT '0',
  `timeout` int(10) UNSIGNED DEFAULT NULL,
  `speed` int(10) UNSIGNED DEFAULT NULL,
  `noAnimate` int(10) UNSIGNED DEFAULT NULL,
  `pause` int(10) UNSIGNED DEFAULT NULL,
  `maxWidth` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `btImageSlider`
--

INSERT INTO `btImageSlider` (`bID`, `navigationType`, `timeout`, `speed`, `noAnimate`, `pause`, `maxWidth`) VALUES
(19, 0, 4000, 500, 0, 0, 0),
(20, 0, 4000, 500, 0, 1, 0),
(21, 0, 4000, 500, 0, 1, 0),
(22, 0, 4000, 500, 0, 1, 0),
(23, 0, 4000, 500, 0, 1, 0),
(24, 0, 4000, 500, 0, 1, 0),
(25, 0, 4000, 500, 0, 1, 0),
(33, 0, 4000, 500, 0, 0, 0),
(43, 0, 4000, 500, 0, 0, 0),
(44, 0, 4000, 500, 0, 0, 0),
(45, 0, 4000, 500, 0, 0, 0),
(46, 0, 4000, 500, 0, 0, 0),
(47, 0, 4000, 500, 0, 0, 0),
(48, 0, 4000, 500, 0, 0, 0),
(49, 0, 4000, 500, 0, 0, 0),
(50, 0, 4000, 500, 0, 0, 0),
(51, 0, 4000, 500, 0, 0, 0),
(52, 0, 4000, 500, 0, 0, 0),
(53, 0, 4000, 500, 0, 0, 0),
(54, 0, 4000, 500, 0, 0, 0),
(56, 0, 4000, 500, 0, 0, 0),
(57, 0, 4000, 500, 0, 0, 0),
(60, 0, 4000, 500, 0, 0, 0),
(61, 0, 4000, 500, 0, 0, 0),
(62, 0, 4000, 500, 0, 0, 0),
(69, 0, 4000, 500, 0, 0, 0),
(70, 0, 4000, 500, 0, 0, 0),
(75, 0, 4000, 500, 0, 0, 0),
(76, 0, 4000, 500, 0, 0, 0),
(77, 0, 4000, 500, 0, 0, 0),
(78, 0, 4000, 500, 0, 0, 0),
(82, 0, 4000, 500, 0, 0, 0),
(84, 0, 4000, 500, 0, 0, 0),
(85, 0, 4000, 500, 0, 0, 0),
(86, 0, 4000, 500, 0, 0, 0),
(87, 0, 4000, 500, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btImageSliderEntries`
--

CREATE TABLE `btImageSliderEntries` (
  `id` int(10) UNSIGNED NOT NULL,
  `bID` int(10) UNSIGNED DEFAULT NULL,
  `cID` int(10) UNSIGNED DEFAULT '0',
  `fID` int(10) UNSIGNED DEFAULT '0',
  `linkURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internalLinkCID` int(10) UNSIGNED DEFAULT '0',
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `sortOrder` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `btImageSliderEntries`
--

INSERT INTO `btImageSliderEntries` (`id`, `bID`, `cID`, `fID`, `linkURL`, `internalLinkCID`, `title`, `description`, `sortOrder`) VALUES
(1, 19, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(3, 20, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(4, 20, 0, 2, '', 0, 'Slide 2', '', 1),
(5, 21, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(6, 21, 0, 2, '', 0, 'Slide 2', '', 1),
(7, 22, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(8, 22, 0, 2, '', 0, 'Slide 2', '', 1),
(9, 23, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(10, 23, 0, 2, '', 0, 'Slide 2', '', 1),
(13, 24, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(15, 25, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(16, 25, 0, 2, '', 0, 'Slide 2 titel', '', 1),
(19, 33, 0, 1, '', 0, '', '', 0),
(21, 43, 0, 1, '', 0, '', '', 0),
(22, 43, 0, 1, '', 0, '', '', 1),
(23, 44, 0, 1, '', 0, '', '', 0),
(24, 44, 0, 1, '', 0, '', '', 1),
(25, 45, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(26, 45, 0, 1, '', 0, 'Slide 2', '', 1),
(27, 46, 0, 1, '', 0, '', '', 0),
(28, 46, 0, 1, '', 0, '', '', 1),
(29, 47, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(30, 47, 0, 1, '', 0, 'Slide 2', '', 1),
(31, 48, 0, 1, '', 0, '', '', 0),
(32, 48, 0, 1, '', 0, '', '', 1),
(33, 49, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(34, 49, 0, 1, '', 0, 'Slide 2', '', 1),
(35, 50, 0, 1, '', 0, '', '', 0),
(36, 50, 0, 1, '', 0, '', '', 1),
(37, 51, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(38, 51, 0, 1, '', 0, 'Slide 2', '', 1),
(39, 52, 0, 1, '', 0, '', '', 0),
(40, 52, 0, 1, '', 0, '', '', 1),
(41, 53, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(42, 53, 0, 1, '', 0, 'Slide 2', '', 1),
(43, 54, 0, 1, '', 0, '', '', 0),
(44, 54, 0, 1, '', 0, '', '', 1),
(45, 56, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(46, 56, 0, 1, '', 0, 'Slide 2', '', 1),
(49, 57, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(50, 60, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(51, 61, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(52, 62, 0, 1, '', 0, '', '', 0),
(54, 69, 0, 2, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(55, 69, 0, 1, '', 0, 'Slide 2', '', 1),
(57, 70, 0, 1, '', 0, '', '', 0),
(58, 70, 0, 2, '', 0, '', '', 1),
(59, 75, 0, 1, '', 0, 'Omniversum introduceert omnivrsum', '<h3>Updates 03/03/2017</h3><p>Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur.</p>', 0),
(60, 76, 0, 1, '', 0, 'Omniversum introduceert omnivrsum', '<h3>Updates 03/03/2017</h3><p>Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur.</p>', 0),
(62, 77, 0, 1, '', 0, '', '<h2>Omniversum introduceert omnivrsum<br></h2><h3>Updates 03/03/2017</h3><p>Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur.</p>', 0),
(64, 78, 0, 1, '', 0, '', '<h2>Omniversum introduceert omni<span style="color: #e30613">vr</span>sum<br></h2>\r\n<h3>Updates <span style="color: #e30613">03/03/2017</span></h3>\r\n<p>Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur.\r\n</p>', 0),
(66, 82, 0, 6, '', 0, '', '<h2>Omniversum introduceert omni<span style="color: #e30613">vr</span>sum<br></h2><h3>Updates <span style="color: #e30613">03/03/2017</span></h3><p>Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur.</p>', 0),
(69, 84, 0, 8, '', 0, '', '', 0),
(70, 84, 0, 2, '', 0, '', '', 1),
(73, 85, 0, 10, '', 0, 'If you fail to prepare, then prepare to fail.', '', 0),
(74, 85, 0, 2, '', 0, 'Slide 2', '', 1),
(77, 86, 0, 8, '', 0, '', '', 0),
(78, 86, 0, 9, '', 0, '', '', 1),
(80, 87, 0, 6, '', 0, '', '<h2>Omniversum introduceert omni<span style="color: #e30613">vr</span>sum<br></h2><h3>Updates <span style="color: #e30613">03/03/2017</span></h3><p>Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur.\r\n</p>', 0),
(81, 87, 0, 6, '', 0, '', '<h2>Omniversum introduceert omni<span style="color: #e30613">vr</span>sum<br></h2>\r\n<h3>Updates <span style="color: #e30613">03/03/2017</span></h3>\r\n<p>Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur.\r\n</p>', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btNavigation`
--

CREATE TABLE `btNavigation` (
  `bID` int(10) UNSIGNED NOT NULL,
  `orderBy` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'alpha_asc',
  `displayPages` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'top' COMMENT 'was enum(''top'',''current'',''above'',''below'',''custom'')',
  `displayPagesCID` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `displayPagesIncludeSelf` tinyint(1) NOT NULL DEFAULT '0',
  `displaySubPages` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'none' COMMENT 'was enum(''none'',''all'',''relevant'',''relevant_breadcrumb'')',
  `displaySubPageLevels` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'none' COMMENT 'was enum(''all'',''none'',''enough'',''enough_plus1'',''custom'')',
  `displaySubPageLevelsNum` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `displayUnavailablePages` tinyint(1) NOT NULL DEFAULT '0',
  `displaySystemPages` smallint(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `btNavigation`
--

INSERT INTO `btNavigation` (`bID`, `orderBy`, `displayPages`, `displayPagesCID`, `displayPagesIncludeSelf`, `displaySubPages`, `displaySubPageLevels`, `displaySubPageLevelsNum`, `displayUnavailablePages`, `displaySystemPages`) VALUES
(10, 'display_asc', 'top', 0, 0, 'none', 'enough', 0, 0, 0),
(12, 'display_asc', 'top', 0, 0, 'none', 'enough', 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btNextPrevious`
--

CREATE TABLE `btNextPrevious` (
  `bID` int(10) UNSIGNED NOT NULL,
  `nextLabel` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `previousLabel` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentLabel` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loopSequence` int(11) DEFAULT '1',
  `excludeSystemPages` int(11) DEFAULT '1',
  `orderBy` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'display_asc'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btPageAttributeDisplay`
--

CREATE TABLE `btPageAttributeDisplay` (
  `bID` int(10) UNSIGNED NOT NULL,
  `attributeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attributeTitleText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayTag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateFormat` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'div',
  `thumbnailHeight` int(10) UNSIGNED DEFAULT NULL,
  `thumbnailWidth` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btPageList`
--

CREATE TABLE `btPageList` (
  `bID` int(10) UNSIGNED NOT NULL,
  `num` smallint(5) UNSIGNED NOT NULL,
  `orderBy` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Was enum, display_asc'',''display_desc'',''chrono_asc'',''chrono_desc'',''alpha_asc'',''alpha_desc'',''score_asc'',''score_desc''',
  `cParentID` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `cThis` tinyint(1) NOT NULL DEFAULT '0',
  `useButtonForLink` tinyint(1) NOT NULL DEFAULT '0',
  `buttonLinkText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pageListTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filterByRelated` tinyint(1) NOT NULL DEFAULT '0',
  `filterByCustomTopic` tinyint(1) NOT NULL DEFAULT '0',
  `filterDateOption` varchar(25) COLLATE utf8_unicode_ci DEFAULT 'all' COMMENT '(''all'',''today'',''past'',''future'',''between'')',
  `filterDateDays` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `filterDateStart` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filterDateEnd` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relatedTopicAttributeKeyHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customTopicAttributeKeyHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customTopicTreeNodeID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `includeName` tinyint(1) NOT NULL DEFAULT '1',
  `includeDescription` tinyint(1) NOT NULL DEFAULT '1',
  `includeDate` tinyint(1) NOT NULL DEFAULT '0',
  `includeAllDescendents` tinyint(1) NOT NULL DEFAULT '0',
  `paginate` tinyint(1) NOT NULL DEFAULT '0',
  `displayAliases` tinyint(1) NOT NULL DEFAULT '1',
  `ignorePermissions` tinyint(1) NOT NULL DEFAULT '0',
  `enableExternalFiltering` tinyint(1) NOT NULL DEFAULT '0',
  `ptID` smallint(5) UNSIGNED DEFAULT NULL,
  `pfID` int(11) DEFAULT '0',
  `truncateSummaries` int(11) DEFAULT '0',
  `displayFeaturedOnly` tinyint(1) DEFAULT '0',
  `noResultsMessage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayThumbnail` tinyint(1) DEFAULT '0',
  `truncateChars` int(11) DEFAULT '128'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `btPageList`
--

INSERT INTO `btPageList` (`bID`, `num`, `orderBy`, `cParentID`, `cThis`, `useButtonForLink`, `buttonLinkText`, `pageListTitle`, `filterByRelated`, `filterByCustomTopic`, `filterDateOption`, `filterDateDays`, `filterDateStart`, `filterDateEnd`, `relatedTopicAttributeKeyHandle`, `customTopicAttributeKeyHandle`, `customTopicTreeNodeID`, `includeName`, `includeDescription`, `includeDate`, `includeAllDescendents`, `paginate`, `displayAliases`, `ignorePermissions`, `enableExternalFiltering`, `ptID`, `pfID`, `truncateSummaries`, `displayFeaturedOnly`, `noResultsMessage`, `displayThumbnail`, `truncateChars`) VALUES
(38, 0, 'display_asc', 0, 0, 0, '', '', 0, 0, '', 0, NULL, NULL, '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btPageTitle`
--

CREATE TABLE `btPageTitle` (
  `bID` int(10) UNSIGNED NOT NULL,
  `useCustomTitle` int(10) UNSIGNED DEFAULT '0',
  `titleText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `formatting` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btRssDisplay`
--

CREATE TABLE `btRssDisplay` (
  `bID` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateFormat` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itemsToDisplay` int(10) UNSIGNED DEFAULT '5',
  `showSummary` tinyint(1) NOT NULL DEFAULT '1',
  `launchInNewWindow` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btSearch`
--

CREATE TABLE `btSearch` (
  `bID` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buttonText` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `baseSearchPath` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postTo_cID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resultsURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btShareThisPage`
--

CREATE TABLE `btShareThisPage` (
  `btShareThisPageID` int(10) UNSIGNED NOT NULL,
  `bID` int(10) UNSIGNED DEFAULT '0',
  `service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayOrder` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btSocialLinks`
--

CREATE TABLE `btSocialLinks` (
  `btSocialLinkID` int(10) UNSIGNED NOT NULL,
  `bID` int(10) UNSIGNED DEFAULT '0',
  `slID` int(10) UNSIGNED DEFAULT '0',
  `displayOrder` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `btSocialLinks`
--

INSERT INTO `btSocialLinks` (`btSocialLinkID`, `bID`, `slID`, `displayOrder`) VALUES
(1, 18, 1, 0),
(2, 18, 2, 1),
(3, 18, 3, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btSurvey`
--

CREATE TABLE `btSurvey` (
  `bID` int(10) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `requiresRegistration` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btSurveyOptions`
--

CREATE TABLE `btSurveyOptions` (
  `optionID` int(10) UNSIGNED NOT NULL,
  `bID` int(11) DEFAULT NULL,
  `optionName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayOrder` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btSurveyResults`
--

CREATE TABLE `btSurveyResults` (
  `resultID` int(10) UNSIGNED NOT NULL,
  `optionID` int(10) UNSIGNED DEFAULT '0',
  `uID` int(10) UNSIGNED DEFAULT '0',
  `bID` int(11) DEFAULT NULL,
  `cID` int(11) DEFAULT NULL,
  `ipAddress` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btSwitchLanguage`
--

CREATE TABLE `btSwitchLanguage` (
  `bID` int(10) UNSIGNED NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btTags`
--

CREATE TABLE `btTags` (
  `bID` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetCID` int(11) DEFAULT NULL,
  `displayMode` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'page',
  `cloudCount` int(11) DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btTestimonial`
--

CREATE TABLE `btTestimonial` (
  `bID` int(10) UNSIGNED NOT NULL,
  `fID` int(10) UNSIGNED DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paragraph` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btTopicList`
--

CREATE TABLE `btTopicList` (
  `bID` int(10) UNSIGNED NOT NULL,
  `mode` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S' COMMENT 'S = Search, P = Page',
  `topicAttributeKeyHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `topicTreeID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cParentID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btVideo`
--

CREATE TABLE `btVideo` (
  `bID` int(10) UNSIGNED NOT NULL,
  `webmfID` int(10) UNSIGNED DEFAULT '0',
  `oggfID` int(10) UNSIGNED DEFAULT '0',
  `posterfID` int(10) UNSIGNED DEFAULT '0',
  `mp4fID` int(10) UNSIGNED DEFAULT '0',
  `width` int(10) UNSIGNED DEFAULT NULL,
  `height` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `btYouTube`
--

CREATE TABLE `btYouTube` (
  `bID` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `videoURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vHeight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vWidth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sizing` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autoplay` tinyint(1) NOT NULL DEFAULT '0',
  `color` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controls` int(10) UNSIGNED DEFAULT NULL,
  `iv_load_policy` int(10) UNSIGNED DEFAULT NULL,
  `loopEnd` tinyint(1) NOT NULL DEFAULT '0',
  `modestbranding` tinyint(1) NOT NULL DEFAULT '0',
  `rel` tinyint(1) NOT NULL DEFAULT '0',
  `showinfo` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `CollectionAttributeValues`
--

CREATE TABLE `CollectionAttributeValues` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `akID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `avID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `CollectionAttributeValues`
--

INSERT INTO `CollectionAttributeValues` (`cID`, `cvID`, `akID`, `avID`) VALUES
(2, 1, 4, 1),
(3, 1, 3, 2),
(4, 1, 3, 3),
(5, 1, 3, 4),
(6, 1, 3, 5),
(8, 1, 3, 6),
(9, 1, 3, 7),
(10, 1, 3, 8),
(11, 1, 5, 9),
(11, 1, 3, 10),
(12, 1, 3, 11),
(13, 1, 3, 12),
(14, 1, 3, 13),
(15, 1, 3, 14),
(16, 1, 3, 15),
(16, 1, 5, 16),
(17, 1, 3, 17),
(17, 1, 5, 18),
(19, 1, 3, 19),
(20, 1, 3, 20),
(22, 1, 3, 21),
(23, 1, 3, 22),
(24, 1, 3, 23),
(25, 1, 3, 24),
(26, 1, 3, 25),
(28, 1, 3, 26),
(29, 1, 3, 27),
(29, 1, 5, 28),
(31, 1, 5, 29),
(32, 1, 5, 30),
(33, 1, 5, 31),
(34, 1, 5, 32),
(35, 1, 5, 33),
(36, 1, 5, 34),
(38, 1, 5, 35),
(39, 1, 3, 36),
(40, 1, 3, 37),
(41, 1, 3, 38),
(43, 1, 4, 39),
(44, 1, 3, 40),
(48, 1, 3, 41),
(50, 1, 5, 42),
(50, 1, 10, 43),
(50, 1, 3, 44),
(51, 1, 3, 45),
(52, 1, 3, 46),
(53, 1, 5, 47),
(54, 1, 3, 48),
(55, 1, 3, 49),
(56, 1, 3, 50),
(56, 1, 5, 51),
(57, 1, 3, 52),
(58, 1, 3, 53),
(59, 1, 3, 54),
(61, 1, 3, 55),
(62, 1, 3, 56),
(63, 1, 3, 57),
(64, 1, 3, 58),
(65, 1, 3, 59),
(66, 1, 3, 60),
(67, 1, 3, 61),
(68, 1, 3, 62),
(74, 1, 3, 63),
(75, 1, 3, 64),
(76, 1, 3, 65),
(77, 1, 3, 66),
(78, 1, 3, 67),
(80, 1, 3, 68),
(81, 1, 3, 69),
(82, 1, 3, 70),
(83, 1, 3, 71),
(84, 1, 3, 72),
(86, 1, 3, 73),
(87, 1, 3, 74),
(88, 1, 3, 75),
(89, 1, 3, 76),
(91, 1, 3, 77),
(92, 1, 3, 78),
(95, 1, 3, 79),
(96, 1, 3, 80),
(97, 1, 3, 81),
(98, 1, 3, 82),
(100, 1, 3, 83),
(101, 1, 3, 84),
(102, 1, 3, 85),
(103, 1, 3, 86),
(104, 1, 3, 87),
(105, 1, 3, 88),
(106, 1, 3, 89),
(107, 1, 3, 90),
(108, 1, 3, 91),
(109, 1, 3, 92),
(110, 1, 3, 93),
(111, 1, 3, 94),
(113, 1, 3, 95),
(114, 1, 3, 96),
(115, 1, 3, 97),
(116, 1, 3, 98),
(118, 1, 10, 99),
(119, 1, 3, 100),
(120, 1, 3, 101),
(121, 1, 3, 102),
(122, 1, 3, 103),
(123, 1, 3, 104),
(124, 1, 3, 105),
(126, 1, 3, 106),
(127, 1, 5, 107),
(128, 1, 5, 108),
(128, 1, 10, 109),
(129, 1, 4, 110),
(130, 1, 4, 111),
(131, 1, 4, 112),
(134, 1, 4, 113);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Collections`
--

CREATE TABLE `Collections` (
  `cID` int(10) UNSIGNED NOT NULL,
  `cDateAdded` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `cDateModified` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `cHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Collections`
--

INSERT INTO `Collections` (`cID`, `cDateAdded`, `cDateModified`, `cHandle`) VALUES
(1, '2017-03-14 09:29:49', '2017-03-16 11:58:00', 'home'),
(2, '2017-03-14 09:30:41', '2017-03-14 09:30:42', 'dashboard'),
(3, '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'sitemap'),
(4, '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'full'),
(5, '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'explore'),
(6, '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'search'),
(7, '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'files'),
(8, '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'search'),
(9, '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'attributes'),
(10, '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'sets'),
(11, '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'add_set'),
(12, '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'users'),
(13, '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'search'),
(14, '2017-03-14 09:30:42', '2017-03-14 09:30:43', 'groups'),
(15, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'attributes'),
(16, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'add'),
(17, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'add_group'),
(18, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'bulkupdate'),
(19, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'group_sets'),
(20, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'points'),
(21, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'assign'),
(22, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'actions'),
(23, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'reports'),
(24, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'forms'),
(25, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'surveys'),
(26, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'logs'),
(27, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'pages'),
(28, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'themes'),
(29, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'inspect'),
(30, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'types'),
(31, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'organize'),
(32, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'add'),
(33, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'form'),
(34, '2017-03-14 09:30:43', '2017-03-14 09:30:44', 'output'),
(35, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'attributes'),
(36, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'permissions'),
(37, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'templates'),
(38, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'add'),
(39, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'attributes'),
(40, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'single'),
(41, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'feeds'),
(42, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'conversations'),
(43, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'messages'),
(44, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'workflow'),
(45, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'me'),
(46, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'workflows'),
(47, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'blocks'),
(48, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'stacks'),
(49, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'permissions'),
(50, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'list'),
(51, '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'types'),
(52, '2017-03-14 09:30:44', '2017-03-14 09:30:45', 'extend'),
(53, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'news'),
(54, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'install'),
(55, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'update'),
(56, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'connect'),
(57, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'themes'),
(58, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'addons'),
(59, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'system'),
(60, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'basics'),
(61, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'name'),
(62, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'accessibility'),
(63, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'social'),
(64, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'icons'),
(65, '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'editor'),
(66, '2017-03-14 09:30:45', '2017-03-14 09:30:46', 'multilingual'),
(67, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'timezone'),
(68, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'multilingual'),
(69, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'setup'),
(70, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'copy'),
(71, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'page_report'),
(72, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'translate_interface'),
(73, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'seo'),
(74, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'urls'),
(75, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'bulk'),
(76, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'codes'),
(77, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'excluded'),
(78, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'searchindex'),
(79, '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'files'),
(80, '2017-03-14 09:30:46', '2017-03-14 09:30:47', 'permissions'),
(81, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'filetypes'),
(82, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'thumbnails'),
(83, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'image_uploading'),
(84, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'storage'),
(85, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'optimization'),
(86, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'cache'),
(87, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'clearcache'),
(88, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'jobs'),
(89, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'query_log'),
(90, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'permissions'),
(91, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'site'),
(92, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'tasks'),
(93, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'users'),
(94, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'advanced'),
(95, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'blacklist'),
(96, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'captcha'),
(97, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'antispam'),
(98, '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'maintenance'),
(99, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'registration'),
(100, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'postlogin'),
(101, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'profiles'),
(102, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'open'),
(103, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'authentication'),
(104, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'mail'),
(105, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'method'),
(106, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'test'),
(107, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'importers'),
(108, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'conversations'),
(109, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'settings'),
(110, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'points'),
(111, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'bannedwords'),
(112, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'permissions'),
(113, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'attributes'),
(114, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'sets'),
(115, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'types'),
(116, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'topics'),
(117, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'add'),
(118, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'environment'),
(119, '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'info'),
(120, '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'debug'),
(121, '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'logging'),
(122, '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'proxy'),
(123, '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'entities'),
(124, '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'backup'),
(125, '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'backup'),
(126, '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'update'),
(127, '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'welcome'),
(128, '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'home'),
(129, '2017-03-14 09:30:53', '2017-03-14 09:30:53', '!drafts'),
(130, '2017-03-14 09:30:53', '2017-03-14 09:30:53', '!trash'),
(131, '2017-03-14 09:30:53', '2017-03-14 09:30:53', '!stacks'),
(132, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'login'),
(133, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'register'),
(134, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'account'),
(135, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'edit_profile'),
(136, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'avatar'),
(137, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'messages'),
(138, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'inbox'),
(139, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'members'),
(140, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'profile'),
(141, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'directory'),
(142, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'page_not_found'),
(143, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'page_forbidden'),
(144, '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'download_file'),
(145, '2017-03-14 09:30:55', '2017-03-14 09:30:55', NULL),
(146, '2017-03-14 09:41:33', '2017-03-14 14:42:45', 'header-site-title'),
(147, '2017-03-14 09:41:33', '2017-03-14 14:42:45', 'header-navigation'),
(148, '2017-03-14 09:41:33', '2017-03-14 09:41:33', 'footer-legal'),
(149, '2017-03-14 09:41:33', '2017-03-14 09:41:33', 'footer-navigation'),
(150, '2017-03-14 09:41:33', '2017-03-14 09:41:33', 'footer-contact'),
(151, '2017-03-14 14:41:38', '2017-03-14 14:41:38', 'header-search'),
(152, '2017-03-14 14:41:39', '2017-03-14 14:41:39', 'footer-site-title'),
(153, '2017-03-14 14:41:39', '2017-03-14 14:41:39', 'footer-social'),
(154, '2017-03-14 14:47:21', '2017-03-14 14:48:25', 'navigation'),
(155, '2017-03-14 14:57:57', '2017-03-15 14:55:25', 'logo'),
(156, '2017-03-14 15:02:19', '2017-03-14 15:02:19', NULL),
(157, '2017-03-14 15:16:35', '2017-03-14 15:16:35', 'projects'),
(158, '2017-03-14 15:17:16', '2017-03-14 15:17:16', 'updates'),
(159, '2017-03-14 15:17:32', '2017-03-14 15:17:32', 'fijnproevers'),
(160, '2017-03-14 15:17:42', '2017-03-14 15:17:42', 'over-ons'),
(161, '2017-03-14 15:17:50', '2017-03-14 15:17:50', 'contact'),
(162, '2017-03-14 15:19:45', '2017-03-14 15:21:46', 'social-media');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `CollectionSearchIndexAttributes`
--

CREATE TABLE `CollectionSearchIndexAttributes` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ak_meta_title` longtext COLLATE utf8_unicode_ci,
  `ak_meta_description` longtext COLLATE utf8_unicode_ci,
  `ak_meta_keywords` longtext COLLATE utf8_unicode_ci,
  `ak_icon_dashboard` longtext COLLATE utf8_unicode_ci,
  `ak_exclude_nav` tinyint(1) DEFAULT '0',
  `ak_exclude_page_list` tinyint(1) DEFAULT '0',
  `ak_header_extra_content` longtext COLLATE utf8_unicode_ci,
  `ak_tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ak_is_featured` tinyint(1) DEFAULT '0',
  `ak_exclude_search_index` tinyint(1) DEFAULT '0',
  `ak_exclude_sitemapxml` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `CollectionSearchIndexAttributes`
--

INSERT INTO `CollectionSearchIndexAttributes` (`cID`, `ak_meta_title`, `ak_meta_description`, `ak_meta_keywords`, `ak_icon_dashboard`, `ak_exclude_nav`, `ak_exclude_page_list`, `ak_header_extra_content`, `ak_tags`, `ak_is_featured`, `ak_exclude_search_index`, `ak_exclude_sitemapxml`) VALUES
(1, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(2, NULL, NULL, NULL, 'fa fa-th-large', 0, 0, NULL, NULL, 0, 0, 0),
(3, NULL, NULL, 'pages, add page, delete page, copy, move, alias', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(4, NULL, NULL, 'pages, add page, delete page, copy, move, alias', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(5, NULL, NULL, 'pages, add page, delete page, copy, move, alias, bulk', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(6, NULL, NULL, 'find page, search page, search, find, pages, sitemap', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(8, NULL, NULL, 'add file, delete file, copy, move, alias, resize, crop, rename, images, title, attribute', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(9, NULL, NULL, 'file, file attributes, title, attribute, description, rename', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(10, NULL, NULL, 'files, category, categories', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(11, NULL, NULL, 'new file set', NULL, 1, 0, NULL, NULL, 0, 0, 0),
(12, NULL, NULL, 'users, groups, people, find, delete user, remove user, change password, password', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(13, NULL, NULL, 'find, search, people, delete user, remove user, change password, password', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(14, NULL, NULL, 'user, group, people, permissions, expire, badges', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(15, NULL, NULL, 'user attributes, user data, gather data, registration data', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(16, NULL, NULL, 'new user, create', NULL, 1, 0, NULL, NULL, 0, 0, 0),
(17, NULL, NULL, 'new user group, new group, group, create', NULL, 1, 0, NULL, NULL, 0, 0, 0),
(19, NULL, NULL, 'group set', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(20, NULL, NULL, 'community, points, karma', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(22, NULL, NULL, 'action, community actions', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(23, NULL, NULL, 'forms, log, error, email, mysql, exception, survey', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(24, NULL, NULL, 'forms, questions, response, data', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(25, NULL, NULL, 'questions, quiz, response', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(26, NULL, NULL, 'forms, log, error, email, mysql, exception, survey, history', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(28, NULL, NULL, 'new theme, theme, active theme, change theme, template, css', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(29, NULL, NULL, 'page types', NULL, 1, 0, NULL, NULL, 0, 0, 0),
(31, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(32, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(33, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(34, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(35, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(36, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(38, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(39, NULL, NULL, 'page attributes, custom', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(40, NULL, NULL, 'single, page, custom, application', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(41, NULL, NULL, 'atom, rss, feed, syndication', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(43, NULL, NULL, NULL, 'icon-bullhorn', 0, 0, NULL, NULL, 0, 0, 0),
(44, NULL, NULL, 'add workflow, remove workflow', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(48, NULL, NULL, 'stacks, reusable content, scrapbook, copy, paste, paste block, copy block, site name, logo', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(50, NULL, NULL, 'edit stacks, view stacks, all stacks', NULL, 1, 0, NULL, NULL, 0, 1, 0),
(51, NULL, NULL, 'block, refresh, custom', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(52, NULL, NULL, 'add-on, addon, add on, package, app, ecommerce, discussions, forums, themes, templates, blocks', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(53, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(54, NULL, NULL, 'add-on, addon, ecommerce, install, discussions, forums, themes, templates, blocks', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(55, NULL, NULL, 'update, upgrade', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(56, NULL, NULL, 'concrete5.org, my account, marketplace', NULL, 1, 0, NULL, NULL, 0, 0, 0),
(57, NULL, NULL, 'buy theme, new theme, marketplace, template', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(58, NULL, NULL, 'buy addon, buy add on, buy add-on, purchase addon, purchase add on, purchase add-on, find addon, new addon, marketplace', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(59, NULL, NULL, 'dashboard, configuration', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(61, NULL, NULL, 'website name, title', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(62, NULL, NULL, 'accessibility, easy mode', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(63, NULL, NULL, 'sharing, facebook, twitter', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(64, NULL, NULL, 'logo, favicon, iphone, icon, bookmark', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(65, NULL, NULL, 'tinymce, content block, fonts, editor, content, overlay', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(66, NULL, NULL, 'translate, translation, internationalization, multilingual', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(67, NULL, NULL, 'timezone, profile, locale', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(68, NULL, NULL, 'multilingual, localization, internationalization, i18n', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(74, NULL, NULL, 'vanity, pretty url, redirection, hostname, canonical, seo, pageview, view', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(75, NULL, NULL, 'bulk, seo, change keywords, engine, optimization, search', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(76, NULL, NULL, 'traffic, statistics, google analytics, quant, pageviews, hits', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(77, NULL, NULL, 'pretty, slug', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(78, NULL, NULL, 'configure search, site search, search option', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(80, NULL, NULL, 'file options, file manager, upload, modify', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(81, NULL, NULL, 'security, files, media, extension, manager, upload', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(82, NULL, NULL, 'images, picture, responsive, retina', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(83, NULL, NULL, 'uploading, upload, images, image, resizing, manager', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(84, NULL, NULL, 'security, alternate storage, hide files', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(86, NULL, NULL, 'cache option, change cache, override, turn on cache, turn off cache, no cache, page cache, caching', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(87, NULL, NULL, 'cache option, turn off cache, no cache, page cache, caching', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(88, NULL, NULL, 'index search, reindex search, build sitemap, sitemap.xml, clear old versions, page versions, remove old', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(89, NULL, NULL, 'queries, database, mysql', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(91, NULL, NULL, 'editors, hide site, offline, private, public, access', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(92, NULL, NULL, 'security, actions, administrator, admin, package, marketplace, search', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(95, NULL, NULL, 'security, lock ip, lock out, block ip, address, restrict, access', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(96, NULL, NULL, 'security, registration', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(97, NULL, NULL, 'antispam, block spam, security', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(98, NULL, NULL, 'lock site, under construction, hide, hidden', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(100, NULL, NULL, 'profile, login, redirect, specific, dashboard, administrators', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(101, NULL, NULL, 'member profile, member page, community, forums, social, avatar', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(102, NULL, NULL, 'signup, new user, community, public registration, public, registration', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(103, NULL, NULL, 'auth, authentication, types, oauth, facebook, login, registration', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(104, NULL, NULL, 'smtp, mail settings', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(105, NULL, NULL, 'email server, mail settings, mail configuration, external, internal', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(106, NULL, NULL, 'test smtp, test mail', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(107, NULL, NULL, 'email server, mail settings, mail configuration, private message, message system, import, email, message', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(108, NULL, NULL, 'conversations', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(109, NULL, NULL, 'conversations', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(110, NULL, NULL, 'conversations ratings, ratings, community, community points', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(111, NULL, NULL, 'conversations bad words, banned words, banned, bad words, bad, words, list', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(113, NULL, NULL, 'attribute configuration', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(114, NULL, NULL, 'attributes, sets', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(115, NULL, NULL, 'attributes, types', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(116, NULL, NULL, 'topics, tags, taxonomy', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(118, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 1, 0),
(119, NULL, NULL, 'overrides, system info, debug, support, help', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(120, NULL, NULL, 'errors, exceptions, develop, support, help', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(121, NULL, NULL, 'email, logging, logs, smtp, pop, errors, mysql, log', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(122, NULL, NULL, 'network, proxy server', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(123, NULL, NULL, 'database, entities, doctrine, orm', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(124, NULL, NULL, 'export, backup, database, sql, mysql, encryption, restore', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(126, NULL, NULL, 'upgrade, new version, update', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(127, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(128, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 1, 0),
(129, NULL, NULL, NULL, 'fa fa-edit', 0, 0, NULL, NULL, 0, 0, 0),
(130, NULL, NULL, NULL, 'fa fa-trash-o', 0, 0, NULL, NULL, 0, 0, 0),
(131, NULL, NULL, NULL, 'fa fa-th', 0, 0, NULL, NULL, 0, 0, 0),
(134, NULL, NULL, NULL, 'fa fa-briefcase', 0, 0, NULL, NULL, 0, 0, 0),
(146, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(147, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(154, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(155, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(162, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `CollectionVersionAreaStyles`
--

CREATE TABLE `CollectionVersionAreaStyles` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `issID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `CollectionVersionBlocks`
--

CREATE TABLE `CollectionVersionBlocks` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `bID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cbDisplayOrder` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `isOriginal` tinyint(1) NOT NULL DEFAULT '0',
  `cbOverrideAreaPermissions` tinyint(1) NOT NULL DEFAULT '0',
  `cbIncludeAll` tinyint(1) NOT NULL DEFAULT '0',
  `cbOverrideBlockTypeCacheSettings` tinyint(1) NOT NULL DEFAULT '0',
  `cbOverrideBlockTypeContainerSettings` tinyint(1) NOT NULL DEFAULT '0',
  `cbEnableBlockContainer` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `CollectionVersionBlocks`
--

INSERT INTO `CollectionVersionBlocks` (`cID`, `cvID`, `bID`, `arHandle`, `cbDisplayOrder`, `isOriginal`, `cbOverrideAreaPermissions`, `cbIncludeAll`, `cbOverrideBlockTypeCacheSettings`, `cbOverrideBlockTypeContainerSettings`, `cbEnableBlockContainer`) VALUES
(1, 6, 13, 'Logo', 0, 1, 0, 0, 0, 0, 0),
(1, 7, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 8, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 9, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 10, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 11, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 11, 19, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 12, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 12, 20, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 13, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 13, 20, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 14, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 14, 21, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 15, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 15, 21, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 16, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 16, 22, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 17, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 17, 22, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 18, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 18, 23, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 19, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 19, 23, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 20, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 20, 24, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 21, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 21, 25, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 22, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 22, 25, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 22, 26, 'Introduction block', 0, 1, 0, 0, 0, 0, 0),
(1, 23, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 23, 25, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 23, 27, 'Introduction block', 0, 1, 0, 0, 0, 0, 0),
(1, 24, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 24, 25, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 24, 28, 'Introduction block', 0, 1, 0, 0, 0, 0, 0),
(1, 25, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 25, 25, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 25, 29, 'Introduction block', 0, 1, 0, 0, 0, 0, 0),
(1, 26, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 26, 25, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 26, 30, 'Introduction block', 0, 1, 0, 0, 0, 0, 0),
(1, 27, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 27, 25, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 27, 31, 'Introduction block', 0, 1, 0, 0, 0, 0, 0),
(1, 28, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 28, 25, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 28, 32, 'Introduction block', 0, 1, 0, 0, 0, 0, 0),
(1, 29, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 29, 25, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 29, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 29, 33, 'Events images', 0, 1, 0, 0, 0, 0, 0),
(1, 29, 34, 'Events text', 0, 1, 0, 0, 0, 0, 0),
(1, 29, 35, 'Producties text', 0, 1, 0, 0, 0, 0, 0),
(1, 29, 36, 'Producties image', 0, 1, 0, 0, 0, 0, 0),
(1, 29, 38, 'Updates slider', 0, 1, 0, 0, 0, 0, 0),
(1, 29, 39, 'Friends image', 0, 1, 0, 0, 0, 0, 0),
(1, 29, 40, 'Friends text', 0, 1, 0, 0, 0, 0, 0),
(1, 29, 41, 'About image', 0, 1, 0, 0, 0, 0, 0),
(1, 29, 42, 'About text', 0, 1, 0, 0, 0, 0, 0),
(1, 30, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 30, 25, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 30, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 30, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 30, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 30, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 30, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 30, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 30, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 30, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 30, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 30, 43, 'Events images', 0, 1, 0, 0, 0, 0, 0),
(1, 31, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 31, 25, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 31, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 31, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 31, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 31, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 31, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 31, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 31, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 31, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 31, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 31, 43, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 32, 43, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 33, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 33, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 33, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 33, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 33, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 33, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 33, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 33, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 33, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 33, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 33, 44, 'Events images', 0, 1, 0, 0, 0, 0, 0),
(1, 34, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 34, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 34, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 34, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 34, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 34, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 34, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 34, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 34, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 34, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 34, 44, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 35, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 35, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 35, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 35, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 35, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 35, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 35, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 35, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 35, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 35, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 35, 45, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 35, 46, 'Events images', 0, 1, 0, 0, 0, 0, 0),
(1, 36, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 36, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 36, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 36, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 36, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 36, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 36, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 36, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 36, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 36, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 36, 45, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 36, 46, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 46, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 37, 47, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 38, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 38, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 38, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 38, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 38, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 38, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 38, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 38, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 38, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 38, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 38, 47, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 38, 48, 'Events images', 0, 1, 0, 0, 0, 0, 0),
(1, 39, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 39, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 39, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 39, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 39, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 39, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 39, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 39, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 39, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 39, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 39, 48, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 39, 49, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 40, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 40, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 40, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 40, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 40, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 40, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 40, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 40, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 40, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 40, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 40, 49, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 40, 50, 'Events images', 0, 1, 0, 0, 0, 0, 0),
(1, 41, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 41, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 41, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 41, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 41, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 41, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 41, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 41, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 41, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 41, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 41, 51, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 41, 52, 'Events images', 0, 1, 0, 0, 0, 0, 0),
(1, 42, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 51, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 42, 52, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 52, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 43, 53, 'Hero slider', 0, 1, 0, 0, 0, 0, 0),
(1, 44, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 44, 54, 'Events images', 0, 1, 0, 0, 0, 0, 0),
(1, 45, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 34, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 45, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 46, 55, 'Events text', 0, 1, 0, 0, 0, 0, 0),
(1, 47, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 47, 56, 'Hero', 0, 1, 0, 0, 0, 0, 0),
(1, 48, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 48, 57, 'Hero', 0, 1, 0, 0, 0, 0, 0),
(1, 49, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 49, 57, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 57, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 50, 59, 'Introduction', 0, 1, 0, 0, 0, 0, 0),
(1, 51, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 59, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 51, 60, 'Hero', 0, 1, 0, 0, 0, 0, 0),
(1, 52, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 59, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 52, 60, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 59, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 53, 61, 'Hero', 0, 1, 0, 0, 0, 0, 0),
(1, 54, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 59, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 61, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 54, 62, 'Events slider', 0, 1, 0, 0, 0, 0, 0),
(1, 54, 63, 'Friends img', 0, 1, 0, 0, 0, 0, 0),
(1, 54, 64, 'About img', 0, 1, 0, 0, 0, 0, 0),
(1, 55, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 59, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 61, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 62, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 55, 65, 'Producties img', 0, 1, 0, 0, 0, 0, 0),
(1, 56, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 61, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 62, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 65, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 56, 66, 'Introduction', 0, 1, 0, 0, 0, 0, 0),
(1, 57, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 61, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 62, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 65, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 57, 67, 'Introduction', 0, 1, 0, 0, 0, 0, 0),
(1, 58, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 61, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 62, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 65, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 58, 68, 'Introduction', 0, 1, 0, 0, 0, 0, 0),
(1, 59, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 62, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 65, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 59, 69, 'Hero', 0, 1, 0, 0, 0, 0, 0),
(1, 60, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 35, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 40, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 42, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 55, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 65, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 69, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 60, 70, 'Events slider', 0, 1, 0, 0, 0, 0, 0),
(1, 61, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 38, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 65, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 69, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 70, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 61, 71, 'Producties text', 0, 1, 0, 0, 0, 0, 0),
(1, 61, 72, 'Events text', 0, 1, 0, 0, 0, 0, 0),
(1, 61, 73, 'Friends text', 0, 1, 0, 0, 0, 0, 0),
(1, 61, 74, 'About text', 0, 1, 0, 0, 0, 0, 0),
(1, 62, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 65, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 69, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 70, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 73, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 62, 75, 'Updates slider', 0, 1, 0, 0, 0, 0, 0),
(1, 63, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 65, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 69, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 70, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 73, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 63, 76, 'Updates slider', 0, 1, 0, 0, 0, 0, 0),
(1, 64, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 65, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 69, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 70, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 73, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 64, 77, 'Updates slider', 0, 1, 0, 0, 0, 0, 0),
(1, 65, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 65, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 69, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 70, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 73, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 65, 78, 'Updates slider', 0, 1, 0, 0, 0, 0, 0),
(1, 66, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 63, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 64, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 65, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 69, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 70, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 78, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 66, 79, 'Friends text', 0, 1, 0, 0, 0, 0, 0),
(1, 67, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 79, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 67, 80, 'About img', 0, 1, 0, 0, 0, 0, 0),
(1, 67, 81, 'Friends img', 0, 1, 0, 0, 0, 0, 0),
(1, 67, 82, 'Updates slider', 0, 1, 0, 0, 0, 0, 0),
(1, 67, 83, 'Producties img', 0, 1, 0, 0, 0, 0, 0),
(1, 67, 84, 'Events slider', 0, 1, 0, 0, 0, 0, 0),
(1, 67, 85, 'Hero', 0, 1, 0, 0, 0, 0, 0),
(1, 68, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 79, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 80, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 81, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 82, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 83, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 85, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 68, 86, 'Events slider', 0, 1, 0, 0, 0, 0, 0),
(1, 69, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 79, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 80, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 81, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 83, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 85, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 86, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 69, 87, 'Updates slider', 0, 1, 0, 0, 0, 0, 0),
(1, 70, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 79, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 80, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 81, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 83, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 85, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 86, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 87, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 70, 88, 'Footer header', 0, 1, 0, 0, 0, 0, 0),
(1, 70, 89, 'Footer text', 0, 1, 0, 0, 0, 0, 0),
(1, 70, 90, 'Footer form', 0, 1, 0, 0, 0, 0, 0),
(1, 71, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 79, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 80, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 81, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 83, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 85, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 86, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 87, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 88, 'Footer header', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 89, 'Footer text', 0, 0, 0, 0, 0, 0, 0),
(1, 71, 90, 'Footer form', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 79, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 80, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 81, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 83, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 85, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 86, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 87, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 88, 'Footer header', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 89, 'Footer text', 0, 0, 0, 0, 0, 0, 0),
(1, 72, 91, 'Footer form', 0, 1, 0, 0, 0, 0, 0),
(1, 73, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 79, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 80, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 81, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 83, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 85, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 86, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 87, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 88, 'Footer header', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 89, 'Footer text', 0, 0, 0, 0, 0, 0, 0),
(1, 73, 92, 'Footer form', 0, 1, 0, 0, 0, 0, 0),
(1, 74, 13, 'Logo', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 32, 'Introduction block', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 36, 'Producties image', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 39, 'Friends image', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 41, 'About image', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 53, 'Hero slider', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 54, 'Events images', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 68, 'Introduction', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 71, 'Producties text', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 72, 'Events text', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 74, 'About text', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 79, 'Friends text', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 80, 'About img', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 81, 'Friends img', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 83, 'Producties img', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 85, 'Hero', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 86, 'Events slider', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 87, 'Updates slider', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 88, 'Footer header', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 89, 'Footer text', 0, 0, 0, 0, 0, 0, 0),
(1, 74, 93, 'Footer form', 0, 1, 0, 0, 0, 0, 0),
(127, 1, 1, 'Main', 0, 1, 0, 0, 0, 0, 0),
(128, 1, 2, 'Primary', 0, 1, 0, 0, 0, 0, 0),
(128, 1, 3, 'Primary', 1, 1, 0, 0, 0, 0, 0),
(128, 1, 4, 'Secondary 1', 0, 1, 0, 0, 0, 0, 0),
(128, 1, 5, 'Secondary 2', 0, 1, 0, 0, 0, 0, 0),
(128, 1, 6, 'Secondary 3', 0, 1, 0, 0, 0, 0, 0),
(128, 1, 7, 'Secondary 4', 0, 1, 0, 0, 0, 0, 0),
(128, 1, 8, 'Secondary 5', 0, 1, 0, 0, 0, 0, 0),
(145, 1, 9, 'Main', 0, 1, 0, 0, 0, 0, 0),
(146, 2, 11, 'Main', 0, 1, 0, 0, 0, 0, 0),
(147, 2, 10, 'Main', 0, 1, 0, 0, 0, 0, 0),
(154, 2, 12, 'Main', 0, 1, 0, 0, 0, 0, 0),
(155, 2, 14, 'Main', 0, 1, 0, 0, 0, 0, 0),
(155, 3, 15, 'Main', 0, 1, 0, 0, 0, 0, 0),
(155, 4, 16, 'Main', 0, 1, 0, 0, 0, 0, 0),
(155, 5, 16, 'Main', 0, 0, 0, 0, 0, 0, 0),
(155, 6, 17, 'Main', 0, 1, 0, 0, 0, 0, 0),
(155, 7, 58, 'Main', 0, 1, 0, 0, 0, 0, 0),
(162, 2, 18, 'Main', 0, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `CollectionVersionBlocksCacheSettings`
--

CREATE TABLE `CollectionVersionBlocksCacheSettings` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `bID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btCacheBlockOutput` tinyint(1) NOT NULL DEFAULT '0',
  `btCacheBlockOutputOnPost` tinyint(1) NOT NULL DEFAULT '0',
  `btCacheBlockOutputForRegisteredUsers` tinyint(1) NOT NULL DEFAULT '0',
  `btCacheBlockOutputLifetime` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `CollectionVersionBlocksOutputCache`
--

CREATE TABLE `CollectionVersionBlocksOutputCache` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `bID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btCachedBlockOutput` longtext COLLATE utf8_unicode_ci,
  `btCachedBlockOutputExpires` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `CollectionVersionBlocksOutputCache`
--

INSERT INTO `CollectionVersionBlocksOutputCache` (`cID`, `cvID`, `bID`, `arHandle`, `btCachedBlockOutput`, `btCachedBlockOutputExpires`) VALUES
(1, 72, 18, 'Social media', '\n<div id="ccm-block-social-links18" class="ccm-block-social-links">\n    <ul class="list-inline">\n            <li><a target="_blank" href="www.facebook.com"><i class="fa fa-facebook"></i></a></li>\n            <li><a target="_blank" href="www.instagram.com"><i class="fa fa-instagram"></i></a></li>\n            <li><a target="_blank" href="www.linkedin.com"><i class="fa fa-linkedin-square"></i></a></li>\n        </ul>\n</div>', 1647427973),
(1, 72, 58, 'Logo', '<a href="http://fugu.dev/"><img src="/application/files/2314/8949/8950/FreshFugu_Logo.png" alt="Logo" width="4724" height="1181" class="ccm-image-block img-responsive bID-58" title="Logo"></a>\n', 1647427973),
(1, 72, 80, 'About img', '<img src="/application/files/5614/8965/4286/foto-about.jpg" alt="" width="1000" height="800" class="ccm-image-block img-responsive bID-80">\n', 1647427973),
(1, 72, 81, 'Friends img', '<img src="/application/files/5514/8965/4371/fijnproevers.jpg" alt="" width="1000" height="800" class="ccm-image-block img-responsive bID-81">\n', 1647427973),
(1, 72, 83, 'Producties img', '<img src="/application/files/9314/8965/4493/production.jpg" alt="" width="1000" height="800" class="ccm-image-block img-responsive bID-83">\n', 1647427973),
(1, 73, 18, 'Social media', '\n<div id="ccm-block-social-links18" class="ccm-block-social-links">\n    <ul class="list-inline">\n            <li><a target="_blank" href="www.facebook.com"><i class="fa fa-facebook"></i></a></li>\n            <li><a target="_blank" href="www.instagram.com"><i class="fa fa-instagram"></i></a></li>\n            <li><a target="_blank" href="www.linkedin.com"><i class="fa fa-linkedin-square"></i></a></li>\n        </ul>\n</div>', 1647428250),
(1, 73, 58, 'Logo', '<a href="http://fugu.dev/"><img src="/application/files/2314/8949/8950/FreshFugu_Logo.png" alt="Logo" width="4724" height="1181" class="ccm-image-block img-responsive bID-58" title="Logo"></a>\n', 1647428250),
(1, 73, 80, 'About img', '<img src="/application/files/5614/8965/4286/foto-about.jpg" alt="" width="1000" height="800" class="ccm-image-block img-responsive bID-80">\n', 1647428250),
(1, 73, 81, 'Friends img', '<img src="/application/files/5514/8965/4371/fijnproevers.jpg" alt="" width="1000" height="800" class="ccm-image-block img-responsive bID-81">\n', 1647428250),
(1, 73, 83, 'Producties img', '<img src="/application/files/9314/8965/4493/production.jpg" alt="" width="1000" height="800" class="ccm-image-block img-responsive bID-83">\n', 1647428250),
(1, 74, 18, 'Social media', '\n<div id="ccm-block-social-links18" class="ccm-block-social-links">\n    <ul class="list-inline">\n            <li><a target="_blank" href="www.facebook.com"><i class="fa fa-facebook"></i></a></li>\n            <li><a target="_blank" href="www.instagram.com"><i class="fa fa-instagram"></i></a></li>\n            <li><a target="_blank" href="www.linkedin.com"><i class="fa fa-linkedin-square"></i></a></li>\n        </ul>\n</div>', 1647428280),
(1, 74, 58, 'Logo', '<a href="http://fugu.dev/"><img src="/application/files/2314/8949/8950/FreshFugu_Logo.png" alt="Logo" width="4724" height="1181" class="ccm-image-block img-responsive bID-58" title="Logo"></a>\n', 1647428280),
(1, 74, 80, 'About img', '<img src="/application/files/5614/8965/4286/foto-about.jpg" alt="" width="1000" height="800" class="ccm-image-block img-responsive bID-80">\n', 1647428280),
(1, 74, 81, 'Friends img', '<img src="/application/files/5514/8965/4371/fijnproevers.jpg" alt="" width="1000" height="800" class="ccm-image-block img-responsive bID-81">\n', 1647428280),
(1, 74, 83, 'Producties img', '<img src="/application/files/9314/8965/4493/production.jpg" alt="" width="1000" height="800" class="ccm-image-block img-responsive bID-83">\n', 1647428280);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `CollectionVersionBlockStyles`
--

CREATE TABLE `CollectionVersionBlockStyles` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `bID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `issID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `CollectionVersionFeatureAssignments`
--

CREATE TABLE `CollectionVersionFeatureAssignments` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `faID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `CollectionVersionFeatureAssignments`
--

INSERT INTO `CollectionVersionFeatureAssignments` (`cID`, `cvID`, `faID`) VALUES
(1, 6, 2),
(1, 7, 2),
(1, 8, 2),
(1, 9, 2),
(1, 10, 2),
(1, 11, 2),
(1, 12, 2),
(1, 13, 2),
(1, 14, 2),
(1, 15, 2),
(1, 16, 2),
(1, 17, 2),
(1, 18, 2),
(1, 19, 2),
(1, 20, 2),
(1, 21, 2),
(1, 22, 2),
(1, 23, 2),
(1, 24, 2),
(1, 25, 2),
(1, 26, 2),
(1, 27, 2),
(1, 28, 2),
(1, 29, 2),
(1, 29, 7),
(1, 29, 8),
(1, 29, 9),
(1, 30, 2),
(1, 30, 7),
(1, 30, 8),
(1, 30, 9),
(1, 31, 2),
(1, 31, 7),
(1, 31, 8),
(1, 31, 9),
(1, 32, 2),
(1, 32, 7),
(1, 32, 8),
(1, 32, 9),
(1, 33, 2),
(1, 33, 7),
(1, 33, 8),
(1, 33, 9),
(1, 34, 2),
(1, 34, 7),
(1, 34, 8),
(1, 34, 9),
(1, 35, 2),
(1, 35, 7),
(1, 35, 8),
(1, 35, 9),
(1, 36, 2),
(1, 36, 7),
(1, 36, 8),
(1, 36, 9),
(1, 37, 2),
(1, 37, 7),
(1, 37, 8),
(1, 37, 9),
(1, 38, 2),
(1, 38, 7),
(1, 38, 8),
(1, 38, 9),
(1, 39, 2),
(1, 39, 7),
(1, 39, 8),
(1, 39, 9),
(1, 40, 2),
(1, 40, 7),
(1, 40, 8),
(1, 40, 9),
(1, 41, 2),
(1, 41, 7),
(1, 41, 8),
(1, 41, 9),
(1, 42, 2),
(1, 42, 7),
(1, 42, 8),
(1, 42, 9),
(1, 43, 2),
(1, 43, 7),
(1, 43, 8),
(1, 43, 9),
(1, 44, 2),
(1, 44, 7),
(1, 44, 8),
(1, 44, 9),
(1, 45, 2),
(1, 45, 7),
(1, 45, 8),
(1, 45, 9),
(1, 46, 2),
(1, 46, 7),
(1, 46, 8),
(1, 46, 9),
(1, 47, 2),
(1, 47, 7),
(1, 47, 8),
(1, 47, 9),
(1, 48, 2),
(1, 48, 7),
(1, 48, 8),
(1, 48, 9),
(1, 49, 2),
(1, 49, 7),
(1, 49, 8),
(1, 49, 9),
(1, 50, 2),
(1, 50, 7),
(1, 50, 8),
(1, 50, 9),
(1, 51, 2),
(1, 51, 7),
(1, 51, 8),
(1, 51, 9),
(1, 52, 2),
(1, 52, 7),
(1, 52, 8),
(1, 52, 9),
(1, 53, 2),
(1, 53, 7),
(1, 53, 8),
(1, 53, 9),
(1, 54, 2),
(1, 54, 7),
(1, 54, 8),
(1, 54, 9),
(1, 54, 11),
(1, 54, 12),
(1, 55, 2),
(1, 55, 7),
(1, 55, 8),
(1, 55, 9),
(1, 55, 11),
(1, 55, 12),
(1, 55, 13),
(1, 56, 2),
(1, 56, 7),
(1, 56, 8),
(1, 56, 9),
(1, 56, 11),
(1, 56, 12),
(1, 56, 13),
(1, 57, 2),
(1, 57, 7),
(1, 57, 8),
(1, 57, 9),
(1, 57, 11),
(1, 57, 12),
(1, 57, 13),
(1, 58, 2),
(1, 58, 7),
(1, 58, 8),
(1, 58, 9),
(1, 58, 11),
(1, 58, 12),
(1, 58, 13),
(1, 59, 2),
(1, 59, 7),
(1, 59, 8),
(1, 59, 9),
(1, 59, 11),
(1, 59, 12),
(1, 59, 13),
(1, 60, 2),
(1, 60, 7),
(1, 60, 8),
(1, 60, 9),
(1, 60, 11),
(1, 60, 12),
(1, 60, 13),
(1, 61, 2),
(1, 61, 7),
(1, 61, 8),
(1, 61, 9),
(1, 61, 11),
(1, 61, 12),
(1, 61, 13),
(1, 62, 2),
(1, 62, 7),
(1, 62, 8),
(1, 62, 9),
(1, 62, 11),
(1, 62, 12),
(1, 62, 13),
(1, 63, 2),
(1, 63, 7),
(1, 63, 8),
(1, 63, 9),
(1, 63, 11),
(1, 63, 12),
(1, 63, 13),
(1, 64, 2),
(1, 64, 7),
(1, 64, 8),
(1, 64, 9),
(1, 64, 11),
(1, 64, 12),
(1, 64, 13),
(1, 65, 2),
(1, 65, 7),
(1, 65, 8),
(1, 65, 9),
(1, 65, 11),
(1, 65, 12),
(1, 65, 13),
(1, 66, 2),
(1, 66, 7),
(1, 66, 8),
(1, 66, 9),
(1, 66, 11),
(1, 66, 12),
(1, 66, 13),
(1, 67, 2),
(1, 67, 7),
(1, 67, 8),
(1, 67, 9),
(1, 67, 14),
(1, 67, 15),
(1, 67, 16),
(1, 68, 2),
(1, 68, 7),
(1, 68, 8),
(1, 68, 9),
(1, 68, 14),
(1, 68, 15),
(1, 68, 16),
(1, 69, 2),
(1, 69, 7),
(1, 69, 8),
(1, 69, 9),
(1, 69, 14),
(1, 69, 15),
(1, 69, 16),
(1, 70, 2),
(1, 70, 7),
(1, 70, 8),
(1, 70, 9),
(1, 70, 14),
(1, 70, 15),
(1, 70, 16),
(1, 71, 2),
(1, 71, 7),
(1, 71, 8),
(1, 71, 9),
(1, 71, 14),
(1, 71, 15),
(1, 71, 16),
(1, 72, 2),
(1, 72, 7),
(1, 72, 8),
(1, 72, 9),
(1, 72, 14),
(1, 72, 15),
(1, 72, 16),
(1, 73, 2),
(1, 73, 7),
(1, 73, 8),
(1, 73, 9),
(1, 73, 14),
(1, 73, 15),
(1, 73, 16),
(1, 74, 2),
(1, 74, 7),
(1, 74, 8),
(1, 74, 9),
(1, 74, 14),
(1, 74, 15),
(1, 74, 16),
(146, 2, 1),
(155, 2, 3),
(155, 3, 4),
(155, 4, 5),
(155, 5, 5),
(155, 6, 6),
(155, 7, 10);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `CollectionVersionRelatedEdits`
--

CREATE TABLE `CollectionVersionRelatedEdits` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cRelationID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvRelationID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `CollectionVersions`
--

CREATE TABLE `CollectionVersions` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `cvName` text COLLATE utf8_unicode_ci,
  `cvHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cvDescription` text COLLATE utf8_unicode_ci,
  `cvDatePublic` datetime DEFAULT NULL,
  `cvDateCreated` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `cvComments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cvIsApproved` tinyint(1) NOT NULL DEFAULT '0',
  `cvIsNew` tinyint(1) NOT NULL DEFAULT '0',
  `cvAuthorUID` int(10) UNSIGNED DEFAULT NULL,
  `cvApproverUID` int(10) UNSIGNED DEFAULT NULL,
  `pThemeID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pTemplateID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvActivateDatetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `CollectionVersions`
--

INSERT INTO `CollectionVersions` (`cID`, `cvID`, `cvName`, `cvHandle`, `cvDescription`, `cvDatePublic`, `cvDateCreated`, `cvComments`, `cvIsApproved`, `cvIsNew`, `cvAuthorUID`, `cvApproverUID`, `pThemeID`, `pTemplateID`, `cvActivateDatetime`) VALUES
(1, 1, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 09:29:49', 'Initial Version', 0, 0, 1, NULL, 3, 5, NULL),
(1, 2, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 14:42:11', 'Version 2', 0, 0, 1, 1, 3, 5, NULL),
(1, 3, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 14:44:57', 'New Version 3', 0, 0, 1, 1, 3, 5, NULL),
(1, 4, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 14:45:56', 'New Version 4', 0, 0, 1, 1, 3, 6, NULL),
(1, 5, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 14:48:21', 'Version 5', 0, 0, 1, 1, 3, 6, NULL),
(1, 6, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 14:54:36', 'Version 6', 0, 0, 1, 1, 3, 6, NULL),
(1, 7, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 14:58:17', 'Version 7', 0, 0, 1, 1, 3, 6, NULL),
(1, 8, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 15:08:29', 'Version 8', 0, 0, 1, 1, 3, 6, NULL),
(1, 9, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 15:10:54', 'Version 9', 0, 0, 1, 1, 3, 6, NULL),
(1, 10, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 15:21:42', 'Version 10', 0, 0, 1, 1, 3, 6, NULL),
(1, 11, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 17:21:34', 'Version 11', 0, 0, 1, 1, 3, 6, NULL),
(1, 12, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-14 17:22:05', 'Version 12', 0, 0, 1, 1, 3, 6, NULL),
(1, 13, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 09:10:44', 'Version 13', 0, 0, 1, 1, 3, 6, NULL),
(1, 14, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 09:11:35', 'Version 14', 0, 0, 1, 1, 3, 6, NULL),
(1, 15, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 09:11:53', 'Version 15', 0, 0, 1, 1, 3, 6, NULL),
(1, 16, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 09:13:28', 'Version 16', 0, 0, 1, 1, 3, 6, NULL),
(1, 17, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 09:14:25', 'Version 17', 0, 0, 1, 1, 3, 6, NULL),
(1, 18, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 09:14:45', 'Version 18', 0, 0, 1, 1, 3, 6, NULL),
(1, 19, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 09:15:44', 'Version 19', 0, 0, 1, 1, 3, 6, NULL),
(1, 20, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 09:57:15', 'Version 20', 0, 0, 1, 1, 3, 6, NULL),
(1, 21, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 10:03:24', 'Version 21', 0, 0, 1, 1, 3, 6, NULL),
(1, 22, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 10:10:38', 'Version 22', 0, 0, 1, 1, 3, 6, NULL),
(1, 23, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 10:24:24', 'Version 23', 0, 0, 1, 1, 3, 6, NULL),
(1, 24, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 10:32:38', 'Version 24', 0, 0, 1, 1, 3, 6, NULL),
(1, 25, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 10:36:19', 'Version 25', 0, 0, 1, 1, 3, 6, NULL),
(1, 26, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 10:37:07', 'Version 26', 0, 0, 1, 1, 3, 6, NULL),
(1, 27, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 10:43:21', 'Version 27', 0, 0, 1, 1, 3, 6, NULL),
(1, 28, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 11:27:32', 'Version 28', 0, 0, 1, 1, 3, 6, NULL),
(1, 29, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 11:50:10', 'Version 29', 0, 0, 1, 1, 3, 6, NULL),
(1, 30, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:05:01', 'Version 30', 0, 0, 1, 1, 3, 6, NULL),
(1, 31, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:09:43', 'Version 31', 0, 0, 1, 1, 3, 6, NULL),
(1, 32, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:12:53', 'Version 32', 0, 0, 1, 1, 3, 6, NULL),
(1, 33, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:15:05', 'Version 33', 0, 0, 1, 1, 3, 6, NULL),
(1, 34, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:15:49', 'Version 34', 0, 0, 1, 1, 3, 6, NULL),
(1, 35, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:17:02', 'Version 35', 0, 0, 1, 1, 3, 6, NULL),
(1, 36, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:19:31', 'Version 36', 0, 0, 1, 1, 3, 6, NULL),
(1, 37, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:21:14', 'Version 37', 0, 0, 1, 1, 3, 6, NULL),
(1, 38, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:27:02', 'Version 38', 0, 0, 1, 1, 3, 6, NULL),
(1, 39, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:30:24', 'Version 39', 0, 0, 1, 1, 3, 6, NULL),
(1, 40, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:31:12', 'Version 40', 0, 0, 1, 1, 3, 6, NULL),
(1, 41, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:33:05', 'Version 41', 0, 0, 1, 1, 3, 6, NULL),
(1, 42, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:46:22', 'Version 42', 0, 0, 1, 1, 3, 6, NULL),
(1, 43, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:47:04', 'Version 43', 0, 0, 1, 1, 3, 6, NULL),
(1, 44, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:48:58', 'Version 44', 0, 0, 1, 1, 3, 6, NULL),
(1, 45, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 12:49:18', 'Version 45', 0, 0, 1, 1, 3, 6, NULL),
(1, 46, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 13:54:43', 'Version 46', 0, 0, 1, 1, 3, 6, NULL),
(1, 47, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 14:37:34', 'Version 47', 0, 0, 1, 1, 3, 6, NULL),
(1, 48, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 14:41:52', 'Version 48', 0, 0, 1, 1, 3, 6, NULL),
(1, 49, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 14:51:48', 'Version 49', 0, 0, 1, 1, 3, 6, NULL),
(1, 50, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 14:54:48', 'Version 50', 0, 0, 1, 1, 3, 6, NULL),
(1, 51, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 15:16:18', 'Version 51', 0, 0, 1, 1, 3, 6, NULL),
(1, 52, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 15:17:22', 'Version 52', 0, 0, 1, 1, 3, 6, NULL),
(1, 53, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 15:18:54', 'Version 53', 0, 0, 1, 1, 3, 6, NULL),
(1, 54, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 15:35:44', 'Version 54', 0, 0, 1, 1, 3, 6, NULL),
(1, 55, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 15:36:43', 'Version 55', 0, 0, 1, 1, 3, 6, NULL),
(1, 56, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 15:37:47', 'Version 56', 0, 0, 1, 1, 3, 6, NULL),
(1, 57, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 15:43:05', 'Version 57', 0, 0, 1, 1, 3, 6, NULL),
(1, 58, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 15:46:18', 'Version 58', 0, 0, 1, 1, 3, 6, NULL),
(1, 59, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 15:51:26', 'Version 59', 0, 0, 1, 1, 3, 6, NULL),
(1, 60, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-15 16:57:50', 'Version 60', 0, 0, 1, 1, 3, 6, NULL),
(1, 61, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 09:01:54', 'Version 61', 0, 0, 1, 1, 3, 6, NULL),
(1, 62, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 09:22:48', 'Version 62', 0, 0, 1, 1, 3, 6, NULL),
(1, 63, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 09:29:18', 'Version 63', 0, 0, 1, 1, 3, 6, NULL),
(1, 64, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 09:41:39', 'Version 64', 0, 0, 1, 1, 3, 6, NULL),
(1, 65, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 09:42:52', 'Version 65', 0, 0, 1, 1, 3, 6, NULL),
(1, 66, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 09:46:34', 'Version 66', 0, 0, 1, 1, 3, 6, NULL),
(1, 67, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 09:50:09', 'Version 67', 0, 0, 1, 1, 3, 6, NULL),
(1, 68, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 09:58:06', 'Version 68', 0, 0, 1, 1, 3, 6, NULL),
(1, 69, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 09:59:39', 'Version 69', 0, 0, 1, 1, 3, 6, NULL),
(1, 70, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 10:06:04', 'Version 70', 0, 0, 1, 1, 3, 6, NULL),
(1, 71, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 10:22:33', 'Version 71', 0, 0, 1, 1, 3, 6, NULL),
(1, 72, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 10:32:53', 'Version 72', 0, 0, 1, 1, 3, 6, NULL),
(1, 73, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 11:57:26', 'Version 73', 0, 0, 1, 1, 3, 6, NULL),
(1, 74, 'Home', 'home', '', '2017-03-14 09:29:49', '2017-03-16 11:57:56', 'Version 74', 1, 0, 1, 1, 3, 6, NULL),
(2, 1, 'Dashboard', 'dashboard', '', '2017-03-14 09:30:41', '2017-03-14 09:30:41', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(3, 1, 'Sitemap', 'sitemap', 'Whole world at a glance.', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(4, 1, 'Full Sitemap', 'full', '', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(5, 1, 'Flat View', 'explore', '', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(6, 1, 'Page Search', 'search', '', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(7, 1, 'Files', 'files', 'All documents and images.', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(8, 1, 'File Manager', 'search', '', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(9, 1, 'Attributes', 'attributes', '', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(10, 1, 'File Sets', 'sets', '', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(11, 1, 'Add File Set', 'add_set', '', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(12, 1, 'Members', 'users', 'Add and manage the user accounts and groups on your website.', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(13, 1, 'Search Users', 'search', '', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(14, 1, 'User Groups', 'groups', '', '2017-03-14 09:30:42', '2017-03-14 09:30:42', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(15, 1, 'Attributes', 'attributes', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(16, 1, 'Add User', 'add', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(17, 1, 'Add Group', 'add_group', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(18, 1, 'Move Multiple Groups', 'bulkupdate', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(19, 1, 'Group Sets', 'group_sets', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(20, 1, 'Community Points', 'points', NULL, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(21, 1, 'Assign Points', 'assign', NULL, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(22, 1, 'Actions', 'actions', NULL, '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(23, 1, 'Reports', 'reports', 'Get data from forms and logs.', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(24, 1, 'Form Results', 'forms', 'Get submission data.', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(25, 1, 'Surveys', 'surveys', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(26, 1, 'Logs', 'logs', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(27, 1, 'Pages & Themes', 'pages', 'Reskin your site.', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(28, 1, 'Themes', 'themes', 'Reskin your site.', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(29, 1, 'Inspect', 'inspect', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(30, 1, 'Page Types', 'types', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(31, 1, 'Organize Page Type Order', 'organize', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(32, 1, 'Add Page Type', 'add', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(33, 1, 'Compose Form', 'form', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(34, 1, 'Defaults and Output', 'output', '', '2017-03-14 09:30:43', '2017-03-14 09:30:43', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(35, 1, 'Page Type Attributes', 'attributes', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(36, 1, 'Page Type Permissions', 'permissions', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(37, 1, 'Page Templates', 'templates', 'Form factors for pages in your site.', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(38, 1, 'Add Page Template', 'add', 'Add page templates to your site.', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(39, 1, 'Attributes', 'attributes', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(40, 1, 'Single Pages', 'single', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(41, 1, 'RSS Feeds', 'feeds', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(42, 1, 'Conversations', 'conversations', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(43, 1, 'Messages', 'messages', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(44, 1, 'Workflow', 'workflow', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(45, 1, 'Waiting for Me', 'me', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(46, 1, 'Workflow List', 'workflows', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(47, 1, 'Stacks & Blocks', 'blocks', 'Manage sitewide content and administer block types.', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(48, 1, 'Stacks', 'stacks', 'Share content across your site.', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(49, 1, 'Block & Stack Permissions', 'permissions', 'Control who can add blocks and stacks on your site.', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(50, 1, 'Stack List', 'list', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(51, 1, 'Block Types', 'types', 'Manage the installed block types in your site.', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(52, 1, 'Extend concrete5', 'extend', '', '2017-03-14 09:30:44', '2017-03-14 09:30:44', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(53, 1, 'Dashboard', 'news', '', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(54, 1, 'Add Functionality', 'install', 'Install add-ons & themes.', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(55, 1, 'Update Add-Ons', 'update', 'Update your installed packages.', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(56, 1, 'Connect to the Community', 'connect', 'Connect to the concrete5 community.', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(57, 1, 'Get More Themes', 'themes', 'Download themes from concrete5.org.', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(58, 1, 'Get More Add-Ons', 'addons', 'Download add-ons from concrete5.org.', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(59, 1, 'System & Settings', 'system', 'Secure and setup your site.', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(60, 1, 'Basics', 'basics', 'Basic information about your website.', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(61, 1, 'Site Name', 'name', '', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(62, 1, 'Accessibility', 'accessibility', '', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(63, 1, 'Social Links', 'social', '', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(64, 1, 'Bookmark Icons', 'icons', 'Bookmark icon and mobile home screen icon setup.', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(65, 1, 'Rich Text Editor', 'editor', '', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(66, 1, 'Languages', 'multilingual', '', '2017-03-14 09:30:45', '2017-03-14 09:30:45', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(67, 1, 'Time Zone', 'timezone', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(68, 1, 'Multilingual', 'multilingual', 'Run your site in multiple languages.', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(69, 1, 'Multilingual Setup', 'setup', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(70, 1, 'Copy Languages', 'copy', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(71, 1, 'Page Report', 'page_report', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(72, 1, 'Translate Site Interface', 'translate_interface', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(73, 1, 'SEO & Statistics', 'seo', 'Enable pretty URLs and tracking codes.', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(74, 1, 'URLs and Redirection', 'urls', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(75, 1, 'Bulk SEO Updater', 'bulk', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(76, 1, 'Tracking Codes', 'codes', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(77, 1, 'Excluded URL Word List', 'excluded', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(78, 1, 'Search Index', 'searchindex', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(79, 1, 'Files', 'files', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(80, 1, 'File Manager Permissions', 'permissions', '', '2017-03-14 09:30:46', '2017-03-14 09:30:46', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(81, 1, 'Allowed File Types', 'filetypes', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(82, 1, 'Thumbnails', 'thumbnails', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(83, 1, 'Image Uploading', 'image_uploading', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(84, 1, 'File Storage Locations', 'storage', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(85, 1, 'Optimization', 'optimization', 'Keep your site running well.', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(86, 1, 'Cache & Speed Settings', 'cache', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(87, 1, 'Clear Cache', 'clearcache', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(88, 1, 'Automated Jobs', 'jobs', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(89, 1, 'Database Query Log', 'query_log', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(90, 1, 'Permissions & Access', 'permissions', 'Control who sees and edits your site.', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(91, 1, 'Site Access', 'site', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(92, 1, 'Task Permissions', 'tasks', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(93, 1, 'User Permissions', 'users', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(94, 1, 'Advanced Permissions', 'advanced', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(95, 1, 'IP Blacklist', 'blacklist', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(96, 1, 'Captcha Setup', 'captcha', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(97, 1, 'Spam Control', 'antispam', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(98, 1, 'Maintenance Mode', 'maintenance', '', '2017-03-14 09:30:47', '2017-03-14 09:30:47', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(99, 1, 'Login & Registration', 'registration', 'Change login behaviors and setup public profiles.', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(100, 1, 'Login Destination', 'postlogin', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(101, 1, 'Public Profiles', 'profiles', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(102, 1, 'Public Registration', 'open', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(103, 1, 'Authentication Types', 'authentication', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(104, 1, 'Email', 'mail', 'Control how your site send and processes mail.', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(105, 1, 'SMTP Method', 'method', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(106, 1, 'Test Mail Settings', 'test', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(107, 1, 'Email Importers', 'importers', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(108, 1, 'Conversations', 'conversations', 'Manage your conversations settings', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(109, 1, 'Settings', 'settings', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(110, 1, 'Community Points', 'points', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(111, 1, 'Banned Words', 'bannedwords', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(112, 1, 'Conversation Permissions', 'permissions', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(113, 1, 'Attributes', 'attributes', 'Setup attributes for pages, users, files and more.', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(114, 1, 'Sets', 'sets', 'Group attributes into sets for easier organization', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(115, 1, 'Types', 'types', 'Choose which attribute types are available for different items.', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(116, 1, 'Topics', 'topics', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(117, 1, 'Add Topic Tree', 'add', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(118, 1, 'Environment', 'environment', 'Advanced settings for web developers.', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(119, 1, 'Environment Information', 'info', '', '2017-03-14 09:30:48', '2017-03-14 09:30:48', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(120, 1, 'Debug Settings', 'debug', '', '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(121, 1, 'Logging Settings', 'logging', '', '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(122, 1, 'Proxy Server', 'proxy', '', '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(123, 1, 'Database Entities', 'entities', '', '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(124, 1, 'Backup & Restore', 'backup', 'Backup or restore your website.', '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(125, 1, 'Backup Database', 'backup', '', '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(126, 1, 'Update concrete5', 'update', '', '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(127, 1, 'Welcome to concrete5', 'welcome', 'Learn about how to use concrete5, how to develop for concrete5, and get general help.', '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'Initial Version', 1, 0, 1, NULL, 3, 4, NULL),
(128, 1, 'Customize Dashboard Home', 'home', '', '2017-03-14 09:30:49', '2017-03-14 09:30:49', 'Initial Version', 1, 0, 1, NULL, 3, 2, NULL),
(129, 1, 'Drafts', '!drafts', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(130, 1, 'Trash', '!trash', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(131, 1, 'Stacks', '!stacks', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(132, 1, 'Login', 'login', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(133, 1, 'Register', 'register', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(134, 1, 'My Account', 'account', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(135, 1, 'Edit Profile', 'edit_profile', 'Edit your user profile and change password.', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(136, 1, 'Profile Picture', 'avatar', 'Specify a new image attached to posts or edits.', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(137, 1, 'Messages', 'messages', 'Inbox for site-specific messages.', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(138, 1, 'Inbox', 'inbox', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(139, 1, 'Members', 'members', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(140, 1, 'View Profile', 'profile', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(141, 1, 'Directory', 'directory', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(142, 1, 'Page Not Found', 'page_not_found', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(143, 1, 'Page Forbidden', 'page_forbidden', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(144, 1, 'Download File', 'download_file', '', '2017-03-14 09:30:53', '2017-03-14 09:30:53', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(145, 1, '', NULL, NULL, '2017-03-14 09:30:55', '2017-03-14 09:30:55', 'Initial Version', 1, 0, NULL, NULL, 0, 5, NULL),
(146, 1, 'Header Site Title', 'header-site-title', NULL, '2017-03-14 09:41:33', '2017-03-14 09:41:33', 'Initial Version', 0, 0, 1, NULL, 3, 0, NULL),
(146, 2, 'Header Site Title', 'header-site-title', NULL, '2017-03-14 09:41:33', '2017-03-14 14:42:42', 'New Version 2', 1, 0, 1, 1, 3, 0, NULL),
(147, 1, 'Header Navigation', 'header-navigation', NULL, '2017-03-14 09:41:33', '2017-03-14 09:41:33', 'Initial Version', 0, 0, 1, NULL, 3, 0, NULL),
(147, 2, 'Header Navigation', 'header-navigation', NULL, '2017-03-14 09:41:33', '2017-03-14 14:42:11', 'New Version 2', 1, 0, 1, 1, 3, 0, NULL),
(148, 1, 'Footer Legal', 'footer-legal', NULL, '2017-03-14 09:41:33', '2017-03-14 09:41:33', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(149, 1, 'Footer Navigation', 'footer-navigation', NULL, '2017-03-14 09:41:33', '2017-03-14 09:41:33', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(150, 1, 'Footer Contact', 'footer-contact', NULL, '2017-03-14 09:41:33', '2017-03-14 09:41:33', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(151, 1, 'Header Search', 'header-search', NULL, '2017-03-14 14:41:38', '2017-03-14 14:41:38', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(152, 1, 'Footer Site Title', 'footer-site-title', NULL, '2017-03-14 14:41:39', '2017-03-14 14:41:39', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(153, 1, 'Footer Social', 'footer-social', NULL, '2017-03-14 14:41:39', '2017-03-14 14:41:39', 'Initial Version', 1, 0, 1, NULL, 3, 0, NULL),
(154, 1, 'Navigation', 'navigation', NULL, '2017-03-14 14:47:21', '2017-03-14 14:47:21', 'Initial Version', 0, 0, 1, NULL, 3, 0, NULL),
(154, 2, 'Navigation', 'navigation', NULL, '2017-03-14 14:47:21', '2017-03-14 14:48:21', 'New Version 2', 1, 0, 1, 1, 3, 0, NULL),
(155, 1, 'Logo', 'logo', NULL, '2017-03-14 14:57:57', '2017-03-14 14:57:57', 'Initial Version', 0, 0, 1, NULL, 3, 0, NULL),
(155, 2, 'Logo', 'logo', NULL, '2017-03-14 14:57:57', '2017-03-14 14:58:17', 'New Version 2', 0, 0, 1, 1, 3, 0, NULL),
(155, 3, 'Logo', 'logo', NULL, '2017-03-14 14:57:57', '2017-03-14 15:08:29', 'New Version 3', 0, 0, 1, 1, 3, 0, NULL),
(155, 4, 'Logo', 'logo', NULL, '2017-03-14 14:57:57', '2017-03-14 15:08:38', 'New Version 4', 0, 0, 1, 1, 3, 0, NULL),
(155, 5, 'Logo', 'logo', NULL, '2017-03-14 14:57:57', '2017-03-14 15:10:54', 'New Version 5', 0, 0, 1, 1, 3, 0, NULL),
(155, 6, 'Logo', 'logo', NULL, '2017-03-14 14:57:57', '2017-03-14 15:12:05', 'New Version 6', 0, 0, 1, 1, 3, 0, NULL),
(155, 7, 'Logo', 'logo', NULL, '2017-03-14 14:57:57', '2017-03-15 14:54:48', 'New Version 7', 1, 0, 1, 1, 3, 0, NULL),
(156, 1, '', NULL, NULL, '2017-03-14 15:02:19', '2017-03-14 15:02:19', 'Initial Version', 1, 0, NULL, NULL, 0, 6, NULL),
(157, 1, 'Projects', 'projects', NULL, '2017-03-14 15:16:35', '2017-03-14 15:16:35', 'Initial Version', 1, 0, NULL, NULL, 3, 0, NULL),
(158, 1, 'Updates', 'updates', NULL, '2017-03-14 15:17:16', '2017-03-14 15:17:16', 'Initial Version', 1, 0, NULL, NULL, 3, 0, NULL),
(159, 1, 'Fijnproevers', 'fijnproevers', NULL, '2017-03-14 15:17:32', '2017-03-14 15:17:32', 'Initial Version', 1, 0, NULL, NULL, 3, 0, NULL),
(160, 1, 'Over ons', 'over-ons', NULL, '2017-03-14 15:17:42', '2017-03-14 15:17:42', 'Initial Version', 1, 0, NULL, NULL, 3, 0, NULL),
(161, 1, 'Contact', 'contact', NULL, '2017-03-14 15:17:50', '2017-03-14 15:17:50', 'Initial Version', 1, 0, NULL, NULL, 3, 0, NULL),
(162, 1, 'Social media', 'social-media', NULL, '2017-03-14 15:19:45', '2017-03-14 15:19:45', 'Initial Version', 0, 0, 1, NULL, 3, 0, NULL),
(162, 2, 'Social media', 'social-media', NULL, '2017-03-14 15:19:45', '2017-03-14 15:21:42', 'New Version 2', 1, 0, 1, 1, 3, 0, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `CollectionVersionThemeCustomStyles`
--

CREATE TABLE `CollectionVersionThemeCustomStyles` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cvID` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `pThemeID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `scvlID` int(10) UNSIGNED DEFAULT '0',
  `preset` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sccRecordID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Config`
--

CREATE TABLE `Config` (
  `configNamespace` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `configGroup` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configItem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configValue` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Config`
--

INSERT INTO `Config` (`configNamespace`, `configGroup`, `configItem`, `configValue`) VALUES
('', 'concrete', 'security.token.encryption', '7myHLpVwEQsox84GwQ8wwm3XPXtUA0eBMEAuHjr4ur3J16c7STcjQMgCnK8GbH7s'),
('', 'concrete', 'security.token.jobs', 'NRnl9OsYl3wFHtBLpg2SpPa8yuw457yDlkjbsE0GJ0Dglema6qLEUZ3CQIAlTfP1'),
('', 'concrete', 'security.token.validation', '1Nheb13WpJuomeMvc9wXzRvJ0PXpOzgQIHbcGxal6Vax4ACXj8Dq2BIVgKIA6W5q');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConfigStore`
--

CREATE TABLE `ConfigStore` (
  `cfKey` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cfValue` longtext COLLATE utf8_unicode_ci,
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `ConfigStore`
--

INSERT INTO `ConfigStore` (`cfKey`, `timestamp`, `cfValue`, `uID`, `pkgID`) VALUES
('MAIN_HELP_LAST_VIEWED', '2017-03-14 08:41:33', '1489480893', 1, 0),
('NEWSFLOW_LAST_VIEWED', '2017-03-14 08:30:55', '1489585197', 1, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationDiscussions`
--

CREATE TABLE `ConversationDiscussions` (
  `cnvDiscussionID` int(10) UNSIGNED NOT NULL,
  `cnvDiscussionDateCreated` datetime NOT NULL,
  `cID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationEditors`
--

CREATE TABLE `ConversationEditors` (
  `cnvEditorID` int(10) UNSIGNED NOT NULL,
  `cnvEditorHandle` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvEditorName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvEditorIsActive` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `ConversationEditors`
--

INSERT INTO `ConversationEditors` (`cnvEditorID`, `cnvEditorHandle`, `cnvEditorName`, `cnvEditorIsActive`, `pkgID`) VALUES
(1, 'plain_text', 'Plain Text', 1, 0),
(2, 'markdown', 'Markdown', 0, 0),
(3, 'redactor', 'Redactor', 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationFeatureDetailAssignments`
--

CREATE TABLE `ConversationFeatureDetailAssignments` (
  `faID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cnvID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationFlaggedMessages`
--

CREATE TABLE `ConversationFlaggedMessages` (
  `cnvMessageID` int(10) UNSIGNED NOT NULL,
  `cnvMessageFlagTypeID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationFlaggedMessageTypes`
--

CREATE TABLE `ConversationFlaggedMessageTypes` (
  `cnvMessageFlagTypeID` int(10) UNSIGNED NOT NULL,
  `cnvMessageFlagTypeHandle` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `ConversationFlaggedMessageTypes`
--

INSERT INTO `ConversationFlaggedMessageTypes` (`cnvMessageFlagTypeID`, `cnvMessageFlagTypeHandle`) VALUES
(1, 'spam');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationMessageAttachments`
--

CREATE TABLE `ConversationMessageAttachments` (
  `cnvMessageAttachmentID` int(10) UNSIGNED NOT NULL,
  `cnvMessageID` int(11) DEFAULT NULL,
  `fID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationMessageRatings`
--

CREATE TABLE `ConversationMessageRatings` (
  `cnvMessageRatingID` int(10) UNSIGNED NOT NULL,
  `cnvMessageID` int(10) UNSIGNED DEFAULT NULL,
  `cnvRatingTypeID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cnvMessageRatingIP` tinyblob,
  `timestamp` datetime DEFAULT NULL,
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationMessages`
--

CREATE TABLE `ConversationMessages` (
  `cnvMessageID` int(10) UNSIGNED NOT NULL,
  `cnvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cnvEditorID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cnvMessageAuthorName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvMessageAuthorEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvMessageAuthorWebsite` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvMessageSubmitIP` tinyblob,
  `cnvMessageSubmitUserAgent` longtext COLLATE utf8_unicode_ci,
  `cnvMessageLevel` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cnvMessageParentID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cnvMessageDateCreated` datetime DEFAULT NULL,
  `cnvMessageSubject` text COLLATE utf8_unicode_ci,
  `cnvMessageBody` text COLLATE utf8_unicode_ci,
  `cnvIsMessageDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `cnvIsMessageApproved` tinyint(1) NOT NULL DEFAULT '0',
  `cnvMessageTotalRatingScore` bigint(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationPermissionAddMessageAccessList`
--

CREATE TABLE `ConversationPermissionAddMessageAccessList` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationPermissionAssignments`
--

CREATE TABLE `ConversationPermissionAssignments` (
  `cnvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `ConversationPermissionAssignments`
--

INSERT INTO `ConversationPermissionAssignments` (`cnvID`, `pkID`, `paID`) VALUES
(0, 66, 86),
(0, 67, 87),
(0, 68, 91),
(0, 69, 89),
(0, 70, 88),
(0, 71, 90),
(0, 72, 92),
(0, 73, 93);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationRatingTypes`
--

CREATE TABLE `ConversationRatingTypes` (
  `cnvRatingTypeID` int(10) UNSIGNED NOT NULL,
  `cnvRatingTypeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvRatingTypeName` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvRatingTypeCommunityPoints` int(11) DEFAULT NULL,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `ConversationRatingTypes`
--

INSERT INTO `ConversationRatingTypes` (`cnvRatingTypeID`, `cnvRatingTypeHandle`, `cnvRatingTypeName`, `cnvRatingTypeCommunityPoints`, `pkgID`) VALUES
(1, 'up_vote', 'Up Vote', 1, 0),
(2, 'down_vote', 'Down Vote', 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Conversations`
--

CREATE TABLE `Conversations` (
  `cnvID` int(10) UNSIGNED NOT NULL,
  `cID` int(10) UNSIGNED DEFAULT '0',
  `cnvDateCreated` datetime NOT NULL,
  `cnvDateLastMessage` datetime DEFAULT NULL,
  `cnvParentMessageID` int(10) UNSIGNED DEFAULT '0',
  `cnvAttachmentsEnabled` tinyint(1) NOT NULL DEFAULT '1',
  `cnvMessagesTotal` int(10) UNSIGNED DEFAULT '0',
  `cnvOverrideGlobalPermissions` tinyint(1) DEFAULT '0',
  `cnvAttachmentOverridesEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `cnvMaxFilesGuest` int(11) DEFAULT '0',
  `cnvMaxFilesRegistered` int(11) DEFAULT '0',
  `cnvMaxFileSizeGuest` int(11) DEFAULT '0',
  `cnvMaxFileSizeRegistered` int(11) DEFAULT '0',
  `cnvFileExtensions` text COLLATE utf8_unicode_ci,
  `cnvNotificationOverridesEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `cnvEnableSubscription` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ConversationSubscriptions`
--

CREATE TABLE `ConversationSubscriptions` (
  `cnvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(1) COLLATE utf8_unicode_ci DEFAULT 'S'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `ConversationSubscriptions`
--

INSERT INTO `ConversationSubscriptions` (`cnvID`, `uID`, `type`) VALUES
(0, 1, 'S');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `DownloadStatistics`
--

CREATE TABLE `DownloadStatistics` (
  `dsID` int(10) UNSIGNED NOT NULL,
  `fID` int(10) UNSIGNED NOT NULL,
  `fvID` int(10) UNSIGNED NOT NULL,
  `uID` int(10) UNSIGNED NOT NULL,
  `rcID` int(10) UNSIGNED NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FeatureAssignments`
--

CREATE TABLE `FeatureAssignments` (
  `faID` int(10) UNSIGNED NOT NULL,
  `feID` int(10) UNSIGNED DEFAULT NULL,
  `fcID` int(10) UNSIGNED DEFAULT NULL,
  `fdObject` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `FeatureAssignments`
--

INSERT INTO `FeatureAssignments` (`faID`, `feID`, `fcID`, `fdObject`) VALUES
(1, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(2, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(3, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(4, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(5, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(6, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(7, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:58:"/application/files/8014/8950/8472/pexels-photo-202276.jpeg";s:8:"\0*\0width";d:1920;s:9:"\0*\0height";d:1235;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(8, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:58:"/application/files/8014/8950/8472/pexels-photo-202276.jpeg";s:8:"\0*\0width";d:1920;s:9:"\0*\0height";d:1235;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(9, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(10, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(11, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(12, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(13, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(14, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(15, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(16, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:52:"/application/files/2314/8949/8950/FreshFugu_Logo.png";s:8:"\0*\0width";d:4724;s:9:"\0*\0height";d:1181;s:7:"\0*\0item";N;s:5:"error";s:0:"";}');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FeatureCategories`
--

CREATE TABLE `FeatureCategories` (
  `fcID` int(10) UNSIGNED NOT NULL,
  `fcHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `FeatureCategories`
--

INSERT INTO `FeatureCategories` (`fcID`, `fcHandle`, `pkgID`) VALUES
(1, 'collection_version', 0),
(2, 'gathering_item', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Features`
--

CREATE TABLE `Features` (
  `feID` int(10) UNSIGNED NOT NULL,
  `feHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feScore` int(11) NOT NULL DEFAULT '1',
  `feHasCustomClass` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Features`
--

INSERT INTO `Features` (`feID`, `feHandle`, `feScore`, `feHasCustomClass`, `pkgID`) VALUES
(1, 'title', 1, 0, 0),
(2, 'link', 1, 0, 0),
(3, 'author', 1, 0, 0),
(4, 'date_time', 1, 0, 0),
(5, 'image', 500, 1, 0),
(6, 'conversation', 10, 1, 0),
(7, 'description', 1, 0, 0),
(8, 'featured', 1000, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileAttributeValues`
--

CREATE TABLE `FileAttributeValues` (
  `fID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `akID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `avID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `FileAttributeValues`
--

INSERT INTO `FileAttributeValues` (`fID`, `fvID`, `akID`, `avID`) VALUES
(1, 1, 14, 114),
(1, 1, 15, 115),
(2, 1, 14, 116),
(2, 1, 15, 117),
(4, 1, 14, 120),
(4, 1, 15, 121),
(5, 1, 14, 122),
(5, 1, 15, 123),
(6, 1, 14, 124),
(6, 1, 15, 125),
(7, 1, 14, 126),
(7, 1, 15, 127),
(8, 1, 14, 128),
(8, 1, 15, 129),
(9, 1, 14, 130),
(9, 1, 15, 131),
(10, 1, 14, 132),
(10, 1, 15, 133);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileImageThumbnailPaths`
--

CREATE TABLE `FileImageThumbnailPaths` (
  `fileID` int(10) UNSIGNED NOT NULL,
  `fileVersionID` int(10) UNSIGNED NOT NULL,
  `thumbnailTypeHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `storageLocationID` int(10) UNSIGNED NOT NULL,
  `path` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileImageThumbnailTypes`
--

CREATE TABLE `FileImageThumbnailTypes` (
  `ftTypeID` int(10) UNSIGNED NOT NULL,
  `ftTypeHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ftTypeName` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ftTypeWidth` int(11) NOT NULL DEFAULT '0',
  `ftTypeHeight` int(11) DEFAULT NULL,
  `ftTypeIsRequired` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `FileImageThumbnailTypes`
--

INSERT INTO `FileImageThumbnailTypes` (`ftTypeID`, `ftTypeHandle`, `ftTypeName`, `ftTypeWidth`, `ftTypeHeight`, `ftTypeIsRequired`) VALUES
(1, 'file_manager_listing', 'File Manager Thumbnails', 60, 60, 1),
(2, 'file_manager_detail', 'File Manager Detail Thumbnails', 400, NULL, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FilePermissionAssignments`
--

CREATE TABLE `FilePermissionAssignments` (
  `fID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FilePermissionFileTypes`
--

CREATE TABLE `FilePermissionFileTypes` (
  `extension` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `fsID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Files`
--

CREATE TABLE `Files` (
  `fID` int(10) UNSIGNED NOT NULL,
  `fDateAdded` datetime DEFAULT NULL,
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fslID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ocID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fOverrideSetPermissions` tinyint(1) NOT NULL DEFAULT '0',
  `fPassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Files`
--

INSERT INTO `Files` (`fID`, `fDateAdded`, `uID`, `fslID`, `ocID`, `fOverrideSetPermissions`, `fPassword`) VALUES
(1, '2017-03-14 14:42:30', 1, 1, 0, 0, NULL),
(2, '2017-03-14 17:21:12', 1, 1, 0, 0, NULL),
(4, '2017-03-16 09:51:26', 1, 1, 0, 0, NULL),
(5, '2017-03-16 09:52:51', 1, 1, 0, 0, NULL),
(6, '2017-03-16 09:54:13', 1, 1, 0, 0, NULL),
(7, '2017-03-16 09:54:53', 1, 1, 0, 0, NULL),
(8, '2017-03-16 09:55:39', 1, 1, 0, 0, NULL),
(9, '2017-03-16 09:56:15', 1, 1, 0, 0, NULL),
(10, '2017-03-16 09:57:33', 1, 1, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileSearchIndexAttributes`
--

CREATE TABLE `FileSearchIndexAttributes` (
  `fID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ak_width` decimal(14,4) DEFAULT '0.0000',
  `ak_height` decimal(14,4) DEFAULT '0.0000',
  `ak_duration` decimal(14,4) DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `FileSearchIndexAttributes`
--

INSERT INTO `FileSearchIndexAttributes` (`fID`, `ak_width`, `ak_height`, `ak_duration`) VALUES
(1, '4724.0000', '1181.0000', '0.0000'),
(2, '1920.0000', '1235.0000', '0.0000'),
(4, '1000.0000', '800.0000', '0.0000'),
(5, '1000.0000', '800.0000', '0.0000'),
(6, '1024.0000', '576.0000', '0.0000'),
(7, '1000.0000', '800.0000', '0.0000'),
(8, '1000.0000', '800.0000', '0.0000'),
(9, '1000.0000', '800.0000', '0.0000'),
(10, '1920.0000', '1080.0000', '0.0000');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileSetFiles`
--

CREATE TABLE `FileSetFiles` (
  `fsfID` int(10) UNSIGNED NOT NULL,
  `fID` int(10) UNSIGNED NOT NULL,
  `fsID` int(10) UNSIGNED NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsDisplayOrder` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileSetPermissionAssignments`
--

CREATE TABLE `FileSetPermissionAssignments` (
  `fsID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `FileSetPermissionAssignments`
--

INSERT INTO `FileSetPermissionAssignments` (`fsID`, `pkID`, `paID`) VALUES
(0, 39, 33),
(0, 40, 34),
(0, 41, 35),
(0, 42, 36),
(0, 43, 37),
(0, 44, 38),
(0, 45, 40),
(0, 46, 39),
(0, 47, 41);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileSetPermissionFileTypeAccessList`
--

CREATE TABLE `FileSetPermissionFileTypeAccessList` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileSetPermissionFileTypeAccessListCustom`
--

CREATE TABLE `FileSetPermissionFileTypeAccessListCustom` (
  `extension` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileSets`
--

CREATE TABLE `FileSets` (
  `fsID` int(10) UNSIGNED NOT NULL,
  `fsName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fsType` smallint(6) NOT NULL,
  `fsOverrideGlobalPermissions` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileSetSavedSearches`
--

CREATE TABLE `FileSetSavedSearches` (
  `fsID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fsSearchRequest` text COLLATE utf8_unicode_ci,
  `fsResultColumns` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileStorageLocations`
--

CREATE TABLE `FileStorageLocations` (
  `fslID` int(10) UNSIGNED NOT NULL,
  `fslName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fslConfiguration` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fslIsDefault` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `FileStorageLocations`
--

INSERT INTO `FileStorageLocations` (`fslID`, `fslName`, `fslConfiguration`, `fslIsDefault`) VALUES
(1, 'Default', 'O:69:"Concrete\\Core\\File\\StorageLocation\\Configuration\\DefaultConfiguration":1:{s:10:"\0*\0default";b:1;}', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileStorageLocationTypes`
--

CREATE TABLE `FileStorageLocationTypes` (
  `fslTypeID` int(10) UNSIGNED NOT NULL,
  `fslTypeHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fslTypeName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `FileStorageLocationTypes`
--

INSERT INTO `FileStorageLocationTypes` (`fslTypeID`, `fslTypeHandle`, `fslTypeName`, `pkgID`) VALUES
(1, 'default', 'Default', 0),
(2, 'local', 'Local', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileVersionLog`
--

CREATE TABLE `FileVersionLog` (
  `fvlID` int(10) UNSIGNED NOT NULL,
  `fID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fvUpdateTypeID` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `fvUpdateTypeAttributeID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `FileVersionLog`
--

INSERT INTO `FileVersionLog` (`fvlID`, `fID`, `fvID`, `fvUpdateTypeID`, `fvUpdateTypeAttributeID`) VALUES
(1, 1, 1, 5, 14),
(2, 1, 1, 5, 15),
(3, 2, 1, 5, 14),
(4, 2, 1, 5, 15),
(7, 4, 1, 5, 14),
(8, 4, 1, 5, 15),
(9, 5, 1, 5, 14),
(10, 5, 1, 5, 15),
(11, 6, 1, 5, 14),
(12, 6, 1, 5, 15),
(13, 7, 1, 5, 14),
(14, 7, 1, 5, 15),
(15, 8, 1, 5, 14),
(16, 8, 1, 5, 15),
(17, 9, 1, 5, 14),
(18, 9, 1, 5, 15),
(19, 10, 1, 5, 14),
(20, 10, 1, 5, 15);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `FileVersions`
--

CREATE TABLE `FileVersions` (
  `fID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fvID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fvFilename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fvPrefix` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fvGenericType` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `fvSize` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fvTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fvDescription` text COLLATE utf8_unicode_ci,
  `fvTags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fvIsApproved` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `fvDateAdded` datetime DEFAULT NULL,
  `fvApproverUID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fvAuthorUID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fvActivateDatetime` datetime DEFAULT NULL,
  `fvHasListingThumbnail` tinyint(1) NOT NULL DEFAULT '0',
  `fvHasDetailThumbnail` tinyint(1) NOT NULL DEFAULT '0',
  `fvExtension` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fvType` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `FileVersions`
--

INSERT INTO `FileVersions` (`fID`, `fvID`, `fvFilename`, `fvPrefix`, `fvGenericType`, `fvSize`, `fvTitle`, `fvDescription`, `fvTags`, `fvIsApproved`, `fvDateAdded`, `fvApproverUID`, `fvAuthorUID`, `fvActivateDatetime`, `fvHasListingThumbnail`, `fvHasDetailThumbnail`, `fvExtension`, `fvType`) VALUES
(1, 1, 'FreshFugu_Logo.png', '231489498950', 0, 92493, 'FreshFugu_Logo.png', '', '', 1, '2017-03-14 14:42:30', 1, 1, '2017-03-14 14:42:30', 1, 1, 'png', 1),
(2, 1, 'pexels-photo-202276.jpeg', '801489508472', 0, 473918, 'pexels-photo-202276.jpeg', '', '', 1, '2017-03-14 17:21:13', 1, 1, '2017-03-14 17:21:13', 1, 1, 'jpeg', 1),
(4, 1, 'foto-about.jpg', '561489654286', 0, 31323, 'foto-about.jpg', '', '', 1, '2017-03-16 09:51:26', 1, 1, '2017-03-16 09:51:26', 1, 1, 'jpg', 1),
(5, 1, 'fijnproevers.jpg', '551489654371', 0, 452068, 'fijnproevers.jpg', '', '', 1, '2017-03-16 09:52:51', 1, 1, '2017-03-16 09:52:51', 1, 1, 'jpg', 1),
(6, 1, 'Omniversum-2-Het-Omniversum-in-Den-Haag.jpg', '251489654453', 0, 149944, 'Omniversum-2-Het-Omniversum-in-Den-Haag.jpg', '', '', 1, '2017-03-16 09:54:13', 1, 1, '2017-03-16 09:54:13', 1, 1, 'jpg', 1),
(7, 1, 'production.jpg', '931489654493', 0, 438130, 'production.jpg', '', '', 1, '2017-03-16 09:54:53', 1, 1, '2017-03-16 09:54:53', 1, 1, 'jpg', 1),
(8, 1, 'slide1.jpg', '171489654539', 0, 463202, 'slide1.jpg', '', '', 1, '2017-03-16 09:55:39', 1, 1, '2017-03-16 09:55:39', 1, 1, 'jpg', 1),
(9, 1, 'slide2.jpg', '601489654575', 0, 494439, 'slide2.jpg', '', '', 1, '2017-03-16 09:56:15', 1, 1, '2017-03-16 09:56:15', 1, 1, 'jpg', 1),
(10, 1, 'hero-slide1.jpg', '581489654653', 0, 877587, 'hero-slide1.jpg', '', '', 1, '2017-03-16 09:57:33', 1, 1, '2017-03-16 09:57:33', 1, 1, 'jpg', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `gaPage`
--

CREATE TABLE `gaPage` (
  `gaiID` int(10) UNSIGNED NOT NULL,
  `cID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `GatheringConfiguredDataSources`
--

CREATE TABLE `GatheringConfiguredDataSources` (
  `gcsID` int(10) UNSIGNED NOT NULL,
  `gaID` int(10) UNSIGNED DEFAULT NULL,
  `gasID` int(10) UNSIGNED DEFAULT NULL,
  `gcdObject` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `GatheringDataSources`
--

CREATE TABLE `GatheringDataSources` (
  `gasID` int(10) UNSIGNED NOT NULL,
  `gasName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gasHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gasDisplayOrder` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `GatheringDataSources`
--

INSERT INTO `GatheringDataSources` (`gasID`, `gasName`, `gasHandle`, `pkgID`, `gasDisplayOrder`) VALUES
(1, 'Site Page', 'page', 0, 0),
(2, 'RSS Feed', 'rss_feed', 0, 1),
(3, 'Flickr Feed', 'flickr_feed', 0, 2),
(4, 'Twitter', 'twitter', 0, 3);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `GatheringItemFeatureAssignments`
--

CREATE TABLE `GatheringItemFeatureAssignments` (
  `gafaID` int(10) UNSIGNED NOT NULL,
  `gaiID` int(10) UNSIGNED DEFAULT NULL,
  `faID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `GatheringItems`
--

CREATE TABLE `GatheringItems` (
  `gaiID` int(10) UNSIGNED NOT NULL,
  `gaID` int(10) UNSIGNED DEFAULT NULL,
  `gasID` int(10) UNSIGNED DEFAULT NULL,
  `gaiDateTimeCreated` datetime NOT NULL,
  `gaiPublicDateTime` datetime NOT NULL,
  `gaiTitle` text COLLATE utf8_unicode_ci,
  `gaiSlotWidth` int(10) UNSIGNED DEFAULT '1',
  `gaiSlotHeight` int(10) UNSIGNED DEFAULT '1',
  `gaiKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gaiBatchDisplayOrder` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gaiBatchTimestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gaiIsDeleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `GatheringItemSelectedTemplates`
--

CREATE TABLE `GatheringItemSelectedTemplates` (
  `gaiID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gatID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gatTypeID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `GatheringItemTemplateFeatures`
--

CREATE TABLE `GatheringItemTemplateFeatures` (
  `gfeID` int(10) UNSIGNED NOT NULL,
  `gatID` int(10) UNSIGNED DEFAULT NULL,
  `feID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `GatheringItemTemplateFeatures`
--

INSERT INTO `GatheringItemTemplateFeatures` (`gfeID`, `gatID`, `feID`) VALUES
(4, 1, 1),
(10, 2, 1),
(13, 3, 1),
(16, 4, 1),
(19, 5, 1),
(23, 7, 1),
(29, 11, 1),
(33, 12, 1),
(37, 13, 1),
(42, 14, 1),
(47, 15, 1),
(53, 17, 1),
(56, 18, 1),
(63, 21, 1),
(64, 22, 1),
(3, 1, 2),
(9, 2, 2),
(12, 3, 2),
(15, 4, 2),
(18, 5, 2),
(21, 6, 2),
(25, 8, 2),
(27, 9, 2),
(41, 13, 3),
(46, 14, 3),
(51, 16, 3),
(55, 17, 3),
(62, 20, 3),
(66, 22, 3),
(2, 1, 4),
(8, 2, 4),
(14, 4, 4),
(17, 5, 4),
(31, 11, 4),
(35, 12, 4),
(39, 13, 4),
(44, 14, 4),
(61, 20, 4),
(5, 1, 5),
(22, 6, 5),
(24, 8, 5),
(26, 9, 5),
(28, 10, 5),
(32, 11, 5),
(36, 12, 5),
(40, 13, 5),
(45, 14, 5),
(49, 15, 5),
(50, 16, 5),
(59, 19, 5),
(65, 22, 5),
(20, 5, 6),
(1, 1, 7),
(7, 2, 7),
(11, 3, 7),
(30, 11, 7),
(34, 12, 7),
(38, 13, 7),
(43, 14, 7),
(48, 15, 7),
(52, 16, 7),
(54, 17, 7),
(57, 18, 7),
(58, 19, 7),
(60, 20, 7),
(6, 1, 8);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `GatheringItemTemplates`
--

CREATE TABLE `GatheringItemTemplates` (
  `gatID` int(10) UNSIGNED NOT NULL,
  `gatHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gatName` text COLLATE utf8_unicode_ci,
  `gatHasCustomClass` tinyint(1) NOT NULL DEFAULT '0',
  `gatFixedSlotWidth` int(10) UNSIGNED DEFAULT '0',
  `gatFixedSlotHeight` int(10) UNSIGNED DEFAULT '0',
  `gatForceDefault` int(10) UNSIGNED DEFAULT '0',
  `pkgID` int(10) UNSIGNED DEFAULT NULL,
  `gatTypeID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `GatheringItemTemplates`
--

INSERT INTO `GatheringItemTemplates` (`gatID`, `gatHandle`, `gatName`, `gatHasCustomClass`, `gatFixedSlotWidth`, `gatFixedSlotHeight`, `gatForceDefault`, `pkgID`, `gatTypeID`) VALUES
(1, 'featured', 'Featured Item', 0, 6, 2, 1, 0, 1),
(2, 'title_date_description', 'Title Date & Description', 0, 0, 0, 0, 0, 1),
(3, 'title_description', 'Title & Description', 0, 0, 0, 0, 0, 1),
(4, 'title_date', 'Title & Date', 0, 0, 0, 0, 0, 1),
(5, 'title_date_comments', 'Title, Date & Comments', 1, 0, 0, 0, 0, 1),
(6, 'thumbnail', 'Thumbnail', 0, 0, 0, 0, 0, 1),
(7, 'basic', 'Basic', 0, 0, 0, 0, 0, 2),
(8, 'image_sharing_link', 'Image Sharing Link', 0, 0, 0, 0, 0, 2),
(9, 'image_conversation', 'Image Conversation', 0, 0, 0, 0, 0, 2),
(10, 'image', 'Large Image', 0, 0, 0, 0, 0, 2),
(11, 'masthead_image_left', 'Masthead Image Left', 0, 0, 0, 0, 0, 1),
(12, 'masthead_image_right', 'Masthead Image Right', 0, 0, 0, 0, 0, 1),
(13, 'masthead_image_byline_right', 'Masthead Image Byline Right', 0, 0, 0, 0, 0, 1),
(14, 'masthead_image_byline_left', 'Masthead Image Byline Left', 0, 0, 0, 0, 0, 1),
(15, 'image_masthead_description_center', 'Image Masthead Description Center', 0, 0, 0, 0, 0, 1),
(16, 'image_byline_description_center', 'Image Byline Description Center', 0, 0, 0, 0, 0, 1),
(17, 'masthead_byline_description', 'Masthead Byline Description', 0, 0, 0, 0, 0, 1),
(18, 'masthead_description', 'Masthead Description', 0, 0, 0, 0, 0, 1),
(19, 'thumbnail_description_center', 'Thumbnail & Description Center', 0, 0, 0, 0, 0, 1),
(20, 'tweet', 'Tweet', 0, 0, 0, 0, 0, 1),
(21, 'vimeo', 'Vimeo', 0, 0, 0, 0, 0, 1),
(22, 'image_overlay_headline', 'Image Overlay Headline', 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `GatheringItemTemplateTypes`
--

CREATE TABLE `GatheringItemTemplateTypes` (
  `gatTypeID` int(10) UNSIGNED NOT NULL,
  `gatTypeHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `GatheringItemTemplateTypes`
--

INSERT INTO `GatheringItemTemplateTypes` (`gatTypeID`, `gatTypeHandle`, `pkgID`) VALUES
(1, 'tile', 0),
(2, 'detail', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `GatheringPermissionAssignments`
--

CREATE TABLE `GatheringPermissionAssignments` (
  `gaID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Gatherings`
--

CREATE TABLE `Gatherings` (
  `gaID` int(10) UNSIGNED NOT NULL,
  `gaDateCreated` datetime NOT NULL,
  `gaDateLastUpdated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Groups`
--

CREATE TABLE `Groups` (
  `gID` int(10) UNSIGNED NOT NULL,
  `gName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `gDescription` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gUserExpirationIsEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `gUserExpirationMethod` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gUserExpirationSetDateTime` datetime DEFAULT NULL,
  `gUserExpirationInterval` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gUserExpirationAction` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gIsBadge` tinyint(1) NOT NULL DEFAULT '0',
  `gBadgeFID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gBadgeDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gBadgeCommunityPointValue` int(11) NOT NULL DEFAULT '0',
  `gIsAutomated` tinyint(1) NOT NULL DEFAULT '0',
  `gCheckAutomationOnRegister` tinyint(1) NOT NULL DEFAULT '0',
  `gCheckAutomationOnLogin` tinyint(1) NOT NULL DEFAULT '0',
  `gCheckAutomationOnJobRun` tinyint(1) NOT NULL DEFAULT '0',
  `gPath` text COLLATE utf8_unicode_ci,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Groups`
--

INSERT INTO `Groups` (`gID`, `gName`, `gDescription`, `gUserExpirationIsEnabled`, `gUserExpirationMethod`, `gUserExpirationSetDateTime`, `gUserExpirationInterval`, `gUserExpirationAction`, `gIsBadge`, `gBadgeFID`, `gBadgeDescription`, `gBadgeCommunityPointValue`, `gIsAutomated`, `gCheckAutomationOnRegister`, `gCheckAutomationOnLogin`, `gCheckAutomationOnJobRun`, `gPath`, `pkgID`) VALUES
(1, 'Guest', 'The guest group represents unregistered visitors to your site.', 0, NULL, NULL, 0, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, '/Guest', 0),
(2, 'Registered Users', 'The registered users group represents all user accounts.', 0, NULL, NULL, 0, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, '/Registered Users', 0),
(3, 'Administrators', '', 0, NULL, NULL, 0, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, '/Administrators', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `GroupSetGroups`
--

CREATE TABLE `GroupSetGroups` (
  `gID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gsID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `GroupSets`
--

CREATE TABLE `GroupSets` (
  `gsID` int(10) UNSIGNED NOT NULL,
  `gsName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Jobs`
--

CREATE TABLE `Jobs` (
  `jID` int(10) UNSIGNED NOT NULL,
  `jName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `jDescription` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jDateInstalled` datetime DEFAULT NULL,
  `jDateLastRun` datetime DEFAULT NULL,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `jLastStatusText` longtext COLLATE utf8_unicode_ci,
  `jLastStatusCode` smallint(6) NOT NULL DEFAULT '0',
  `jStatus` varchar(14) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ENABLED',
  `jHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jNotUninstallable` smallint(6) NOT NULL DEFAULT '0',
  `isScheduled` smallint(6) NOT NULL DEFAULT '0',
  `scheduledInterval` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'days',
  `scheduledValue` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Jobs`
--

INSERT INTO `Jobs` (`jID`, `jName`, `jDescription`, `jDateInstalled`, `jDateLastRun`, `pkgID`, `jLastStatusText`, `jLastStatusCode`, `jStatus`, `jHandle`, `jNotUninstallable`, `isScheduled`, `scheduledInterval`, `scheduledValue`) VALUES
(1, 'Index Search Engine - Updates', 'Index the site to allow searching to work quickly and accurately. Only reindexes pages that have changed since last indexing.', '2017-03-14 09:30:40', NULL, 0, NULL, 0, 'ENABLED', 'index_search', 1, 0, 'days', 0),
(2, 'Index Search Engine - All', 'Empties the page search index and reindexes all pages.', '2017-03-14 09:30:40', NULL, 0, NULL, 0, 'ENABLED', 'index_search_all', 1, 0, 'days', 0),
(3, 'Check Automated Groups', 'Automatically add users to groups and assign badges.', '2017-03-14 09:30:40', NULL, 0, NULL, 0, 'ENABLED', 'check_automated_groups', 0, 0, 'days', 0),
(4, 'Generate the sitemap.xml file', 'Generate the sitemap.xml file that search engines use to crawl your site.', '2017-03-14 09:30:40', NULL, 0, NULL, 0, 'ENABLED', 'generate_sitemap', 0, 0, 'days', 0),
(5, 'Process Email Posts', 'Polls an email account and grabs private messages/postings that are sent there..', '2017-03-14 09:30:40', NULL, 0, NULL, 0, 'ENABLED', 'process_email', 0, 0, 'days', 0),
(6, 'Remove Old Page Versions', 'Removes all except the 10 most recent page versions for each page.', '2017-03-14 09:30:40', NULL, 0, NULL, 0, 'ENABLED', 'remove_old_page_versions', 0, 0, 'days', 0),
(7, 'Update Gatherings', 'Loads new items into gatherings.', '2017-03-14 09:30:40', NULL, 0, NULL, 0, 'ENABLED', 'update_gatherings', 0, 0, 'days', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `JobSetJobs`
--

CREATE TABLE `JobSetJobs` (
  `jsID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `jID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `jRunOrder` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `JobSetJobs`
--

INSERT INTO `JobSetJobs` (`jsID`, `jID`, `jRunOrder`) VALUES
(1, 1, 0),
(1, 4, 0),
(1, 5, 0),
(1, 6, 0),
(1, 7, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `JobSets`
--

CREATE TABLE `JobSets` (
  `jsID` int(10) UNSIGNED NOT NULL,
  `jsName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `jDateLastRun` datetime DEFAULT NULL,
  `isScheduled` smallint(6) NOT NULL DEFAULT '0',
  `scheduledInterval` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'days',
  `scheduledValue` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `JobSets`
--

INSERT INTO `JobSets` (`jsID`, `jsName`, `pkgID`, `jDateLastRun`, `isScheduled`, `scheduledInterval`, `scheduledValue`) VALUES
(1, 'Default', 0, NULL, 0, 'days', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `JobsLog`
--

CREATE TABLE `JobsLog` (
  `jlID` int(10) UNSIGNED NOT NULL,
  `jID` int(10) UNSIGNED NOT NULL,
  `jlMessage` longtext COLLATE utf8_unicode_ci NOT NULL,
  `jlTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `jlError` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Logs`
--

CREATE TABLE `Logs` (
  `logID` int(10) UNSIGNED NOT NULL,
  `channel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(10) UNSIGNED NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci,
  `uID` int(10) UNSIGNED DEFAULT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Logs`
--

INSERT INTO `Logs` (`logID`, `channel`, `time`, `message`, `uID`, `level`) VALUES
(1, 'exceptions', 1489499554, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\packages\\fugu\\elements\\header.php:29 Using $this when not in object context (1)\n', 1, 600),
(2, 'exceptions', 1489500124, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\concrete\\blocks\\image\\view.php:31 syntax error, unexpected ''.'' (4)\n', 1, 600),
(3, 'exceptions', 1489500661, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\concrete\\blocks\\image\\templates\\logo\\view.php:31 syntax error, unexpected ''.'' (4)\n', 1, 600),
(4, 'exceptions', 1489500674, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\concrete\\blocks\\image\\templates\\logo\\view.php:31 syntax error, unexpected ''.'' (4)\n', 1, 600),
(5, 'exceptions', 1489500677, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\concrete\\blocks\\image\\templates\\logo\\view.php:31 syntax error, unexpected ''.'' (4)\n', 1, 600),
(6, 'exceptions', 1489500686, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\concrete\\blocks\\image\\templates\\logo\\view.php:31 syntax error, unexpected ''.'' (4)\n', 1, 600),
(7, 'exceptions', 1489500694, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\concrete\\blocks\\image\\templates\\logo\\view.php:31 syntax error, unexpected ''.'' (4)\n', 1, 600),
(8, 'exceptions', 1489500695, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\concrete\\blocks\\image\\templates\\logo\\view.php:31 syntax error, unexpected ''.'' (4)\n', 1, 600),
(9, 'exceptions', 1489500704, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\concrete\\blocks\\image\\templates\\logo\\view.php:31 syntax error, unexpected ''.'' (4)\n', 1, 600),
(10, 'exceptions', 1489576883, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\concrete\\src\\Block\\View\\BlockView.php:256 include(C:/xampp/htdocs/fugu/packages/fugu/blocks/image_slider/templates/hero-slider/view.php): failed to open stream: No such file or directory (2)\n', 1, 600),
(11, 'exceptions', 1489576907, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\concrete\\src\\Block\\View\\BlockView.php:256 include(C:/xampp/htdocs/fugu/packages/fugu/blocks/image_slider/templates/hero-slider/view.php): failed to open stream: No such file or directory (2)\n', 1, 600),
(12, 'exceptions', 1489652964, 'Exception Occurred: C:\\xampp\\htdocs\\fugu\\concrete\\src\\Block\\View\\BlockView.php:256 include(C:/xampp/htdocs/fugu/packages/fugu/blocks/image_slider/templates/updates-slider/view.php): failed to open stream: No such file or directory (2)\n', 1, 600);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `MailImporters`
--

CREATE TABLE `MailImporters` (
  `miID` int(10) UNSIGNED NOT NULL,
  `miHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `miServer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miUsername` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miPassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miEncryption` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miIsEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `miEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miPort` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED DEFAULT NULL,
  `miConnectionMethod` varchar(8) COLLATE utf8_unicode_ci DEFAULT 'POP'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `MailImporters`
--

INSERT INTO `MailImporters` (`miID`, `miHandle`, `miServer`, `miUsername`, `miPassword`, `miEncryption`, `miIsEnabled`, `miEmail`, `miPort`, `pkgID`, `miConnectionMethod`) VALUES
(1, 'private_message', '', NULL, NULL, NULL, 0, '', 0, 0, 'POP');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `MailValidationHashes`
--

CREATE TABLE `MailValidationHashes` (
  `mvhID` int(10) UNSIGNED NOT NULL,
  `miID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `mHash` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `mDateGenerated` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mDateRedeemed` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `MultilingualPageRelations`
--

CREATE TABLE `MultilingualPageRelations` (
  `mpRelationID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mpLanguage` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mpLocale` varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `MultilingualSections`
--

CREATE TABLE `MultilingualSections` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `msLanguage` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `msCountry` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `msNumPlurals` int(11) NOT NULL DEFAULT '2',
  `msPluralRule` varchar(400) COLLATE utf8_unicode_ci NOT NULL DEFAULT '(n != 1)',
  `msPluralCases` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'one@1\nother@0, 2~16, 100, 1000, 10000, 100000, 1000000, …'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `MultilingualTranslations`
--

CREATE TABLE `MultilingualTranslations` (
  `mtID` int(10) UNSIGNED NOT NULL,
  `mtSectionID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `msgid` text COLLATE utf8_unicode_ci NOT NULL,
  `msgidPlural` text COLLATE utf8_unicode_ci,
  `msgstr` text COLLATE utf8_unicode_ci,
  `msgstrPlurals` text COLLATE utf8_unicode_ci,
  `context` text COLLATE utf8_unicode_ci,
  `comments` text COLLATE utf8_unicode_ci,
  `reference` text COLLATE utf8_unicode_ci,
  `flags` text COLLATE utf8_unicode_ci,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `OauthUserMap`
--

CREATE TABLE `OauthUserMap` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `binding` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Packages`
--

CREATE TABLE `Packages` (
  `pkgID` int(10) UNSIGNED NOT NULL,
  `pkgName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `pkgDescription` text COLLATE utf8_unicode_ci,
  `pkgDateInstalled` datetime NOT NULL,
  `pkgIsInstalled` tinyint(1) NOT NULL DEFAULT '1',
  `pkgVersion` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pkgAvailableVersion` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Packages`
--

INSERT INTO `Packages` (`pkgID`, `pkgName`, `pkgHandle`, `pkgDescription`, `pkgDateInstalled`, `pkgIsInstalled`, `pkgVersion`, `pkgAvailableVersion`) VALUES
(2, 'Fugu', 'fugu', 'Adds a the Fugu theme.', '2017-03-15 14:29:57', 1, '1.0.0', NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageFeeds`
--

CREATE TABLE `PageFeeds` (
  `pfID` int(10) UNSIGNED NOT NULL,
  `cParentID` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `pfTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pfHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pfDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pfIncludeAllDescendents` tinyint(1) NOT NULL DEFAULT '0',
  `pfContentToDisplay` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `pfAreaHandleToDisplay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customTopicAttributeKeyHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customTopicTreeNodeID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pfDisplayAliases` tinyint(1) NOT NULL DEFAULT '0',
  `ptID` smallint(5) UNSIGNED DEFAULT NULL,
  `pfDisplayFeaturedOnly` tinyint(1) DEFAULT '0',
  `iconFID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PagePaths`
--

CREATE TABLE `PagePaths` (
  `ppID` int(10) UNSIGNED NOT NULL,
  `cID` int(10) UNSIGNED DEFAULT '0',
  `cPath` text COLLATE utf8_unicode_ci NOT NULL,
  `ppIsCanonical` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `ppGeneratedFromURLSlugs` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PagePaths`
--

INSERT INTO `PagePaths` (`ppID`, `cID`, `cPath`, `ppIsCanonical`, `ppGeneratedFromURLSlugs`) VALUES
(1, 2, '/dashboard', '1', 1),
(2, 3, '/dashboard/sitemap', '1', 1),
(3, 4, '/dashboard/sitemap/full', '1', 1),
(4, 5, '/dashboard/sitemap/explore', '1', 1),
(5, 6, '/dashboard/sitemap/search', '1', 1),
(6, 7, '/dashboard/files', '1', 1),
(7, 8, '/dashboard/files/search', '1', 1),
(8, 9, '/dashboard/files/attributes', '1', 1),
(9, 10, '/dashboard/files/sets', '1', 1),
(10, 11, '/dashboard/files/add_set', '1', 1),
(11, 12, '/dashboard/users', '1', 1),
(12, 13, '/dashboard/users/search', '1', 1),
(13, 14, '/dashboard/users/groups', '1', 1),
(14, 15, '/dashboard/users/attributes', '1', 1),
(15, 16, '/dashboard/users/add', '1', 1),
(16, 17, '/dashboard/users/add_group', '1', 1),
(17, 18, '/dashboard/users/groups/bulkupdate', '1', 1),
(18, 19, '/dashboard/users/group_sets', '1', 1),
(19, 20, '/dashboard/users/points', '1', 1),
(20, 21, '/dashboard/users/points/assign', '1', 1),
(21, 22, '/dashboard/users/points/actions', '1', 1),
(22, 23, '/dashboard/reports', '1', 1),
(23, 24, '/dashboard/reports/forms', '1', 1),
(24, 25, '/dashboard/reports/surveys', '1', 1),
(25, 26, '/dashboard/reports/logs', '1', 1),
(26, 27, '/dashboard/pages', '1', 1),
(27, 28, '/dashboard/pages/themes', '1', 1),
(28, 29, '/dashboard/pages/themes/inspect', '1', 1),
(29, 30, '/dashboard/pages/types', '1', 1),
(30, 31, '/dashboard/pages/types/organize', '1', 1),
(31, 32, '/dashboard/pages/types/add', '1', 1),
(32, 33, '/dashboard/pages/types/form', '1', 1),
(33, 34, '/dashboard/pages/types/output', '1', 1),
(34, 35, '/dashboard/pages/types/attributes', '1', 1),
(35, 36, '/dashboard/pages/types/permissions', '1', 1),
(36, 37, '/dashboard/pages/templates', '1', 1),
(37, 38, '/dashboard/pages/templates/add', '1', 1),
(38, 39, '/dashboard/pages/attributes', '1', 1),
(39, 40, '/dashboard/pages/single', '1', 1),
(40, 41, '/dashboard/pages/feeds', '1', 1),
(41, 42, '/dashboard/conversations', '1', 1),
(42, 43, '/dashboard/conversations/messages', '1', 1),
(43, 44, '/dashboard/workflow', '1', 1),
(44, 45, '/dashboard/workflow/me', '1', 1),
(45, 46, '/dashboard/workflow/workflows', '1', 1),
(46, 47, '/dashboard/blocks', '1', 1),
(47, 48, '/dashboard/blocks/stacks', '1', 1),
(48, 49, '/dashboard/blocks/permissions', '1', 1),
(49, 50, '/dashboard/blocks/stacks/list', '1', 1),
(50, 51, '/dashboard/blocks/types', '1', 1),
(51, 52, '/dashboard/extend', '1', 1),
(52, 53, '/dashboard/news', '1', 1),
(53, 54, '/dashboard/extend/install', '1', 1),
(54, 55, '/dashboard/extend/update', '1', 1),
(55, 56, '/dashboard/extend/connect', '1', 1),
(56, 57, '/dashboard/extend/themes', '1', 1),
(57, 58, '/dashboard/extend/addons', '1', 1),
(58, 59, '/dashboard/system', '1', 1),
(59, 60, '/dashboard/system/basics', '1', 1),
(60, 61, '/dashboard/system/basics/name', '1', 1),
(61, 62, '/dashboard/system/basics/accessibility', '1', 1),
(62, 63, '/dashboard/system/basics/social', '1', 1),
(63, 64, '/dashboard/system/basics/icons', '1', 1),
(64, 65, '/dashboard/system/basics/editor', '1', 1),
(65, 66, '/dashboard/system/basics/multilingual', '1', 1),
(66, 67, '/dashboard/system/basics/timezone', '1', 1),
(67, 68, '/dashboard/system/multilingual', '1', 1),
(68, 69, '/dashboard/system/multilingual/setup', '1', 1),
(69, 70, '/dashboard/system/multilingual/copy', '1', 1),
(70, 71, '/dashboard/system/multilingual/page_report', '1', 1),
(71, 72, '/dashboard/system/multilingual/translate_interface', '1', 1),
(72, 73, '/dashboard/system/seo', '1', 1),
(73, 74, '/dashboard/system/seo/urls', '1', 1),
(74, 75, '/dashboard/system/seo/bulk', '1', 1),
(75, 76, '/dashboard/system/seo/codes', '1', 1),
(76, 77, '/dashboard/system/seo/excluded', '1', 1),
(77, 78, '/dashboard/system/seo/searchindex', '1', 1),
(78, 79, '/dashboard/system/files', '1', 1),
(79, 80, '/dashboard/system/files/permissions', '1', 1),
(80, 81, '/dashboard/system/files/filetypes', '1', 1),
(81, 82, '/dashboard/system/files/thumbnails', '1', 1),
(82, 83, '/dashboard/system/files/image_uploading', '1', 1),
(83, 84, '/dashboard/system/files/storage', '1', 1),
(84, 85, '/dashboard/system/optimization', '1', 1),
(85, 86, '/dashboard/system/optimization/cache', '1', 1),
(86, 87, '/dashboard/system/optimization/clearcache', '1', 1),
(87, 88, '/dashboard/system/optimization/jobs', '1', 1),
(88, 89, '/dashboard/system/optimization/query_log', '1', 1),
(89, 90, '/dashboard/system/permissions', '1', 1),
(90, 91, '/dashboard/system/permissions/site', '1', 1),
(91, 92, '/dashboard/system/permissions/tasks', '1', 1),
(92, 93, '/dashboard/system/permissions/users', '1', 1),
(93, 94, '/dashboard/system/permissions/advanced', '1', 1),
(94, 95, '/dashboard/system/permissions/blacklist', '1', 1),
(95, 96, '/dashboard/system/permissions/captcha', '1', 1),
(96, 97, '/dashboard/system/permissions/antispam', '1', 1),
(97, 98, '/dashboard/system/permissions/maintenance', '1', 1),
(98, 99, '/dashboard/system/registration', '1', 1),
(99, 100, '/dashboard/system/registration/postlogin', '1', 1),
(100, 101, '/dashboard/system/registration/profiles', '1', 1),
(101, 102, '/dashboard/system/registration/open', '1', 1),
(102, 103, '/dashboard/system/registration/authentication', '1', 1),
(103, 104, '/dashboard/system/mail', '1', 1),
(104, 105, '/dashboard/system/mail/method', '1', 1),
(105, 106, '/dashboard/system/mail/method/test', '1', 1),
(106, 107, '/dashboard/system/mail/importers', '1', 1),
(107, 108, '/dashboard/system/conversations', '1', 1),
(108, 109, '/dashboard/system/conversations/settings', '1', 1),
(109, 110, '/dashboard/system/conversations/points', '1', 1),
(110, 111, '/dashboard/system/conversations/bannedwords', '1', 1),
(111, 112, '/dashboard/system/conversations/permissions', '1', 1),
(112, 113, '/dashboard/system/attributes', '1', 1),
(113, 114, '/dashboard/system/attributes/sets', '1', 1),
(114, 115, '/dashboard/system/attributes/types', '1', 1),
(115, 116, '/dashboard/system/attributes/topics', '1', 1),
(116, 117, '/dashboard/system/attributes/topics/add', '1', 1),
(117, 118, '/dashboard/system/environment', '1', 1),
(118, 119, '/dashboard/system/environment/info', '1', 1),
(119, 120, '/dashboard/system/environment/debug', '1', 1),
(120, 121, '/dashboard/system/environment/logging', '1', 1),
(121, 122, '/dashboard/system/environment/proxy', '1', 1),
(122, 123, '/dashboard/system/environment/entities', '1', 1),
(123, 124, '/dashboard/system/backup', '1', 1),
(124, 125, '/dashboard/system/backup/backup', '1', 1),
(125, 126, '/dashboard/system/backup/update', '1', 1),
(126, 127, '/dashboard/welcome', '1', 1),
(127, 128, '/dashboard/home', '1', 1),
(128, 129, '/!drafts', '1', 1),
(129, 130, '/!trash', '1', 1),
(130, 131, '/!stacks', '1', 1),
(131, 132, '/login', '1', 1),
(132, 133, '/register', '1', 1),
(133, 134, '/account', '1', 1),
(134, 135, '/account/edit_profile', '1', 1),
(135, 136, '/account/avatar', '1', 1),
(136, 137, '/account/messages', '1', 1),
(137, 138, '/account/messages/inbox', '1', 1),
(138, 139, '/members', '1', 1),
(139, 140, '/members/profile', '1', 1),
(140, 141, '/members/directory', '1', 1),
(141, 142, '/page_not_found', '1', 1),
(142, 143, '/page_forbidden', '1', 1),
(143, 144, '/download_file', '1', 1),
(144, 146, '/!stacks/header-site-title', '1', 1),
(145, 147, '/!stacks/header-navigation', '1', 1),
(146, 148, '/!stacks/footer-legal', '1', 1),
(147, 149, '/!stacks/footer-navigation', '1', 1),
(148, 150, '/!stacks/footer-contact', '1', 1),
(149, 151, '/!stacks/header-search', '1', 1),
(150, 152, '/!stacks/footer-site-title', '1', 1),
(151, 153, '/!stacks/footer-social', '1', 1),
(152, 154, '/!stacks/navigation', '1', 1),
(153, 155, '/!stacks/logo', '1', 1),
(154, 162, '/!stacks/social-media', '1', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PagePermissionAssignments`
--

CREATE TABLE `PagePermissionAssignments` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PagePermissionAssignments`
--

INSERT INTO `PagePermissionAssignments` (`cID`, `pkID`, `paID`) VALUES
(1, 1, 42),
(1, 2, 43),
(1, 3, 44),
(1, 4, 45),
(1, 5, 46),
(1, 6, 47),
(1, 7, 48),
(1, 8, 50),
(1, 9, 51),
(1, 10, 52),
(1, 11, 53),
(1, 12, 54),
(1, 13, 55),
(1, 14, 56),
(1, 15, 57),
(1, 16, 58),
(1, 17, 59),
(1, 18, 49),
(2, 1, 62),
(129, 1, 63),
(129, 2, 64),
(129, 3, 65),
(129, 4, 66),
(129, 5, 67),
(129, 6, 68),
(129, 7, 69),
(129, 8, 71),
(129, 9, 72),
(129, 10, 73),
(129, 11, 74),
(129, 12, 75),
(129, 13, 76),
(129, 14, 77),
(129, 15, 78),
(129, 16, 79),
(129, 17, 80),
(129, 18, 70),
(132, 1, 60),
(133, 1, 61);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PagePermissionPageTypeAccessList`
--

CREATE TABLE `PagePermissionPageTypeAccessList` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `externalLink` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PagePermissionPageTypeAccessListCustom`
--

CREATE TABLE `PagePermissionPageTypeAccessListCustom` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ptID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PagePermissionPropertyAccessList`
--

CREATE TABLE `PagePermissionPropertyAccessList` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` tinyint(1) DEFAULT '0',
  `publicDateTime` tinyint(1) DEFAULT '0',
  `uID` tinyint(1) DEFAULT '0',
  `description` tinyint(1) DEFAULT '0',
  `paths` tinyint(1) DEFAULT '0',
  `attributePermission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PagePermissionPropertyAttributeAccessListCustom`
--

CREATE TABLE `PagePermissionPropertyAttributeAccessListCustom` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `akID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PagePermissionThemeAccessList`
--

CREATE TABLE `PagePermissionThemeAccessList` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PagePermissionThemeAccessListCustom`
--

CREATE TABLE `PagePermissionThemeAccessListCustom` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pThemeID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Pages`
--

CREATE TABLE `Pages` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ptID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cIsTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `uID` int(10) UNSIGNED DEFAULT NULL,
  `cIsCheckedOut` tinyint(1) NOT NULL DEFAULT '0',
  `cCheckedOutUID` int(10) UNSIGNED DEFAULT NULL,
  `cCheckedOutDatetime` datetime DEFAULT NULL,
  `cCheckedOutDatetimeLastEdit` datetime DEFAULT NULL,
  `cOverrideTemplatePermissions` tinyint(1) NOT NULL DEFAULT '1',
  `cInheritPermissionsFromCID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cInheritPermissionsFrom` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PARENT',
  `cFilename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cPointerID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cPointerExternalLink` longtext COLLATE utf8_unicode_ci,
  `cPointerExternalLinkNewWindow` tinyint(1) NOT NULL DEFAULT '0',
  `cIsActive` tinyint(1) NOT NULL DEFAULT '1',
  `cChildren` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cDisplayOrder` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cParentID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cDraftTargetParentPageID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cCacheFullPageContent` smallint(6) NOT NULL DEFAULT '-1',
  `cCacheFullPageContentOverrideLifetime` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `cCacheFullPageContentLifetimeCustom` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cIsSystemPage` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Pages`
--

INSERT INTO `Pages` (`cID`, `ptID`, `cIsTemplate`, `uID`, `cIsCheckedOut`, `cCheckedOutUID`, `cCheckedOutDatetime`, `cCheckedOutDatetimeLastEdit`, `cOverrideTemplatePermissions`, `cInheritPermissionsFromCID`, `cInheritPermissionsFrom`, `cFilename`, `cPointerID`, `cPointerExternalLink`, `cPointerExternalLinkNewWindow`, `cIsActive`, `cChildren`, `cDisplayOrder`, `cParentID`, `pkgID`, `cDraftTargetParentPageID`, `cCacheFullPageContent`, `cCacheFullPageContentOverrideLifetime`, `cCacheFullPageContentLifetimeCustom`, `cIsSystemPage`) VALUES
(1, 6, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'OVERRIDE', NULL, 0, NULL, 0, 1, 16, 0, 0, 0, 0, -1, '0', 0, 0),
(2, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'OVERRIDE', '/dashboard/view.php', 0, NULL, 0, 1, 13, 0, 0, 0, 0, -1, '0', 0, 1),
(3, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/sitemap/view.php', 0, NULL, 0, 1, 3, 0, 2, 0, 0, -1, '0', 0, 1),
(4, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/sitemap/full.php', 0, NULL, 0, 1, 0, 0, 3, 0, 0, -1, '0', 0, 1),
(5, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/sitemap/explore.php', 0, NULL, 0, 1, 0, 1, 3, 0, 0, -1, '0', 0, 1),
(6, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/sitemap/search.php', 0, NULL, 0, 1, 0, 2, 3, 0, 0, -1, '0', 0, 1),
(7, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/files/view.php', 0, NULL, 0, 1, 4, 1, 2, 0, 0, -1, '0', 0, 1),
(8, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/files/search.php', 0, NULL, 0, 1, 0, 0, 7, 0, 0, -1, '0', 0, 1),
(9, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/files/attributes.php', 0, NULL, 0, 1, 0, 1, 7, 0, 0, -1, '0', 0, 1),
(10, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/files/sets.php', 0, NULL, 0, 1, 0, 2, 7, 0, 0, -1, '0', 0, 1),
(11, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/files/add_set.php', 0, NULL, 0, 1, 0, 3, 7, 0, 0, -1, '0', 0, 1),
(12, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/view.php', 0, NULL, 0, 1, 7, 2, 2, 0, 0, -1, '0', 0, 1),
(13, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/search.php', 0, NULL, 0, 1, 0, 0, 12, 0, 0, -1, '0', 0, 1),
(14, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/groups.php', 0, NULL, 0, 1, 1, 1, 12, 0, 0, -1, '0', 0, 1),
(15, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/attributes.php', 0, NULL, 0, 1, 0, 2, 12, 0, 0, -1, '0', 0, 1),
(16, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/add.php', 0, NULL, 0, 1, 0, 3, 12, 0, 0, -1, '0', 0, 1),
(17, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/add_group.php', 0, NULL, 0, 1, 0, 4, 12, 0, 0, -1, '0', 0, 1),
(18, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/groups/bulkupdate.php', 0, NULL, 0, 1, 0, 0, 14, 0, 0, -1, '0', 0, 1),
(19, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/group_sets.php', 0, NULL, 0, 1, 0, 5, 12, 0, 0, -1, '0', 0, 1),
(20, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/points/view.php', 0, NULL, 0, 1, 2, 6, 12, 0, 0, -1, '0', 0, 1),
(21, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/points/assign.php', 0, NULL, 0, 1, 0, 0, 20, 0, 0, -1, '0', 0, 1),
(22, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/points/actions.php', 0, NULL, 0, 1, 0, 1, 20, 0, 0, -1, '0', 0, 1),
(23, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/reports.php', 0, NULL, 0, 1, 3, 3, 2, 0, 0, -1, '0', 0, 1),
(24, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/reports/forms.php', 0, NULL, 0, 1, 0, 0, 23, 0, 0, -1, '0', 0, 1),
(25, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/reports/surveys.php', 0, NULL, 0, 1, 0, 1, 23, 0, 0, -1, '0', 0, 1),
(26, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/reports/logs.php', 0, NULL, 0, 1, 0, 2, 23, 0, 0, -1, '0', 0, 1),
(27, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/view.php', 0, NULL, 0, 1, 6, 4, 2, 0, 0, -1, '0', 0, 1),
(28, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/themes/view.php', 0, NULL, 0, 1, 1, 0, 27, 0, 0, -1, '0', 0, 1),
(29, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/themes/inspect.php', 0, NULL, 0, 1, 0, 0, 28, 0, 0, -1, '0', 0, 1),
(30, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/view.php', 0, NULL, 0, 1, 6, 1, 27, 0, 0, -1, '0', 0, 1),
(31, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/organize.php', 0, NULL, 0, 1, 0, 0, 30, 0, 0, -1, '0', 0, 1),
(32, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/add.php', 0, NULL, 0, 1, 0, 1, 30, 0, 0, -1, '0', 0, 1),
(33, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/form.php', 0, NULL, 0, 1, 0, 2, 30, 0, 0, -1, '0', 0, 1),
(34, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/output.php', 0, NULL, 0, 1, 0, 3, 30, 0, 0, -1, '0', 0, 1),
(35, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/attributes.php', 0, NULL, 0, 1, 0, 4, 30, 0, 0, -1, '0', 0, 1),
(36, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/permissions.php', 0, NULL, 0, 1, 0, 5, 30, 0, 0, -1, '0', 0, 1),
(37, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/templates/view.php', 0, NULL, 0, 1, 1, 2, 27, 0, 0, -1, '0', 0, 1),
(38, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/templates/add.php', 0, NULL, 0, 1, 0, 0, 37, 0, 0, -1, '0', 0, 1),
(39, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/attributes.php', 0, NULL, 0, 1, 0, 3, 27, 0, 0, -1, '0', 0, 1),
(40, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/single.php', 0, NULL, 0, 1, 0, 4, 27, 0, 0, -1, '0', 0, 1),
(41, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/feeds.php', 0, NULL, 0, 1, 0, 5, 27, 0, 0, -1, '0', 0, 1),
(42, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/conversations/view.php', 0, NULL, 0, 1, 1, 5, 2, 0, 0, -1, '0', 0, 1),
(43, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/conversations/messages.php', 0, NULL, 0, 1, 0, 0, 42, 0, 0, -1, '0', 0, 1),
(44, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/workflow/view.php', 0, NULL, 0, 1, 2, 6, 2, 0, 0, -1, '0', 0, 1),
(45, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/workflow/me.php', 0, NULL, 0, 1, 0, 0, 44, 0, 0, -1, '0', 0, 1),
(46, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/workflow/workflows.php', 0, NULL, 0, 1, 0, 1, 44, 0, 0, -1, '0', 0, 1),
(47, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/blocks/view.php', 0, NULL, 0, 1, 3, 7, 2, 0, 0, -1, '0', 0, 1),
(48, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/blocks/stacks/view.php', 0, NULL, 0, 1, 1, 0, 47, 0, 0, -1, '0', 0, 1),
(49, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/blocks/permissions.php', 0, NULL, 0, 1, 0, 1, 47, 0, 0, -1, '0', 0, 1),
(50, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/blocks/stacks/list/view.php', 0, NULL, 0, 1, 0, 0, 48, 0, 0, -1, '0', 0, 1),
(51, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/blocks/types/view.php', 0, NULL, 0, 1, 0, 2, 47, 0, 0, -1, '0', 0, 1),
(52, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/view.php', 0, NULL, 0, 1, 5, 8, 2, 0, 0, -1, '0', 0, 1),
(53, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/news.php', 0, NULL, 0, 1, 0, 9, 2, 0, 0, -1, '0', 0, 1),
(54, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/install.php', 0, NULL, 0, 1, 0, 0, 52, 0, 0, -1, '0', 0, 1),
(55, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/update.php', 0, NULL, 0, 1, 0, 1, 52, 0, 0, -1, '0', 0, 1),
(56, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/connect.php', 0, NULL, 0, 1, 0, 2, 52, 0, 0, -1, '0', 0, 1),
(57, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/themes.php', 0, NULL, 0, 1, 0, 3, 52, 0, 0, -1, '0', 0, 1),
(58, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/addons.php', 0, NULL, 0, 1, 0, 4, 52, 0, 0, -1, '0', 0, 1),
(59, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/view.php', 0, NULL, 0, 1, 12, 10, 2, 0, 0, -1, '0', 0, 1),
(60, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/view.php', 0, NULL, 0, 1, 7, 0, 59, 0, 0, -1, '0', 0, 1),
(61, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/name.php', 0, NULL, 0, 1, 0, 0, 60, 0, 0, -1, '0', 0, 1),
(62, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/accessibility.php', 0, NULL, 0, 1, 0, 1, 60, 0, 0, -1, '0', 0, 1),
(63, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/social.php', 0, NULL, 0, 1, 0, 2, 60, 0, 0, -1, '0', 0, 1),
(64, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/icons.php', 0, NULL, 0, 1, 0, 3, 60, 0, 0, -1, '0', 0, 1),
(65, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/editor.php', 0, NULL, 0, 1, 0, 4, 60, 0, 0, -1, '0', 0, 1),
(66, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/multilingual/view.php', 0, NULL, 0, 1, 0, 5, 60, 0, 0, -1, '0', 0, 1),
(67, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/timezone.php', 0, NULL, 0, 1, 0, 6, 60, 0, 0, -1, '0', 0, 1),
(68, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/multilingual/view.php', 0, NULL, 0, 1, 4, 1, 59, 0, 0, -1, '0', 0, 1),
(69, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/multilingual/setup.php', 0, NULL, 0, 1, 0, 0, 68, 0, 0, -1, '0', 0, 1),
(70, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/multilingual/copy.php', 0, NULL, 0, 1, 0, 1, 68, 0, 0, -1, '0', 0, 1),
(71, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/multilingual/page_report.php', 0, NULL, 0, 1, 0, 2, 68, 0, 0, -1, '0', 0, 1),
(72, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/multilingual/translate_interface.php', 0, NULL, 0, 1, 0, 3, 68, 0, 0, -1, '0', 0, 1),
(73, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/view.php', 0, NULL, 0, 1, 5, 2, 59, 0, 0, -1, '0', 0, 1),
(74, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/urls.php', 0, NULL, 0, 1, 0, 0, 73, 0, 0, -1, '0', 0, 1),
(75, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/bulk.php', 0, NULL, 0, 1, 0, 1, 73, 0, 0, -1, '0', 0, 1),
(76, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/codes.php', 0, NULL, 0, 1, 0, 2, 73, 0, 0, -1, '0', 0, 1),
(77, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/excluded.php', 0, NULL, 0, 1, 0, 3, 73, 0, 0, -1, '0', 0, 1),
(78, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/searchindex.php', 0, NULL, 0, 1, 0, 4, 73, 0, 0, -1, '0', 0, 1),
(79, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/files/view.php', 0, NULL, 0, 1, 5, 3, 59, 0, 0, -1, '0', 0, 1),
(80, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/files/permissions.php', 0, NULL, 0, 1, 0, 0, 79, 0, 0, -1, '0', 0, 1),
(81, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/files/filetypes.php', 0, NULL, 0, 1, 0, 1, 79, 0, 0, -1, '0', 0, 1),
(82, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/files/thumbnails.php', 0, NULL, 0, 1, 0, 2, 79, 0, 0, -1, '0', 0, 1),
(83, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/files/image_uploading.php', 0, NULL, 0, 1, 0, 3, 79, 0, 0, -1, '0', 0, 1),
(84, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/files/storage.php', 0, NULL, 0, 1, 0, 4, 79, 0, 0, -1, '0', 0, 1),
(85, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/optimization/view.php', 0, NULL, 0, 1, 4, 4, 59, 0, 0, -1, '0', 0, 1),
(86, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/optimization/cache.php', 0, NULL, 0, 1, 0, 0, 85, 0, 0, -1, '0', 0, 1),
(87, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/optimization/clearcache.php', 0, NULL, 0, 1, 0, 1, 85, 0, 0, -1, '0', 0, 1),
(88, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/optimization/jobs.php', 0, NULL, 0, 1, 0, 2, 85, 0, 0, -1, '0', 0, 1),
(89, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/optimization/query_log.php', 0, NULL, 0, 1, 0, 3, 85, 0, 0, -1, '0', 0, 1),
(90, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/view.php', 0, NULL, 0, 1, 8, 5, 59, 0, 0, -1, '0', 0, 1),
(91, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/site.php', 0, NULL, 0, 1, 0, 0, 90, 0, 0, -1, '0', 0, 1),
(92, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/tasks.php', 0, NULL, 0, 1, 0, 1, 90, 0, 0, -1, '0', 0, 1),
(93, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/users.php', 0, NULL, 0, 1, 0, 2, 90, 0, 0, -1, '0', 0, 1),
(94, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/advanced.php', 0, NULL, 0, 1, 0, 3, 90, 0, 0, -1, '0', 0, 1),
(95, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/blacklist.php', 0, NULL, 0, 1, 0, 4, 90, 0, 0, -1, '0', 0, 1),
(96, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/captcha.php', 0, NULL, 0, 1, 0, 5, 90, 0, 0, -1, '0', 0, 1),
(97, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/antispam.php', 0, NULL, 0, 1, 0, 6, 90, 0, 0, -1, '0', 0, 1),
(98, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/maintenance.php', 0, NULL, 0, 1, 0, 7, 90, 0, 0, -1, '0', 0, 1),
(99, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/registration/view.php', 0, NULL, 0, 1, 4, 6, 59, 0, 0, -1, '0', 0, 1),
(100, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/registration/postlogin.php', 0, NULL, 0, 1, 0, 0, 99, 0, 0, -1, '0', 0, 1),
(101, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/registration/profiles.php', 0, NULL, 0, 1, 0, 1, 99, 0, 0, -1, '0', 0, 1),
(102, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/registration/open.php', 0, NULL, 0, 1, 0, 2, 99, 0, 0, -1, '0', 0, 1),
(103, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/registration/authentication.php', 0, NULL, 0, 1, 0, 3, 99, 0, 0, -1, '0', 0, 1),
(104, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/mail/view.php', 0, NULL, 0, 1, 2, 7, 59, 0, 0, -1, '0', 0, 1),
(105, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/mail/method.php', 0, NULL, 0, 1, 1, 0, 104, 0, 0, -1, '0', 0, 1),
(106, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/mail/method/test.php', 0, NULL, 0, 1, 0, 0, 105, 0, 0, -1, '0', 0, 1),
(107, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/mail/importers.php', 0, NULL, 0, 1, 0, 1, 104, 0, 0, -1, '0', 0, 1),
(108, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/conversations/view.php', 0, NULL, 0, 1, 4, 8, 59, 0, 0, -1, '0', 0, 1),
(109, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/conversations/settings.php', 0, NULL, 0, 1, 0, 0, 108, 0, 0, -1, '0', 0, 1),
(110, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/conversations/points.php', 0, NULL, 0, 1, 0, 1, 108, 0, 0, -1, '0', 0, 1),
(111, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/conversations/bannedwords.php', 0, NULL, 0, 1, 0, 2, 108, 0, 0, -1, '0', 0, 1),
(112, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/conversations/permissions.php', 0, NULL, 0, 1, 0, 3, 108, 0, 0, -1, '0', 0, 1),
(113, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/attributes/view.php', 0, NULL, 0, 1, 3, 9, 59, 0, 0, -1, '0', 0, 1),
(114, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/attributes/sets.php', 0, NULL, 0, 1, 0, 0, 113, 0, 0, -1, '0', 0, 1),
(115, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/attributes/types.php', 0, NULL, 0, 1, 0, 1, 113, 0, 0, -1, '0', 0, 1),
(116, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/attributes/topics/view.php', 0, NULL, 0, 1, 1, 2, 113, 0, 0, -1, '0', 0, 1),
(117, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/attributes/topics/add.php', 0, NULL, 0, 1, 0, 0, 116, 0, 0, -1, '0', 0, 1),
(118, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/view.php', 0, NULL, 0, 1, 5, 10, 59, 0, 0, -1, '0', 0, 1),
(119, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/info.php', 0, NULL, 0, 1, 0, 0, 118, 0, 0, -1, '0', 0, 1),
(120, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/debug.php', 0, NULL, 0, 1, 0, 1, 118, 0, 0, -1, '0', 0, 1),
(121, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/logging.php', 0, NULL, 0, 1, 0, 2, 118, 0, 0, -1, '0', 0, 1),
(122, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/proxy.php', 0, NULL, 0, 1, 0, 3, 118, 0, 0, -1, '0', 0, 1),
(123, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/entities.php', 0, NULL, 0, 1, 0, 4, 118, 0, 0, -1, '0', 0, 1),
(124, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/backup/view.php', 0, NULL, 0, 1, 2, 11, 59, 0, 0, -1, '0', 0, 1),
(125, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/backup/backup.php', 0, NULL, 0, 1, 0, 0, 124, 0, 0, -1, '0', 0, 1),
(126, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/backup/update.php', 0, NULL, 0, 1, 0, 1, 124, 0, 0, -1, '0', 0, 1),
(127, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', NULL, 0, NULL, 0, 1, 0, 11, 2, 0, 0, -1, '0', 0, 1),
(128, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', NULL, 0, NULL, 0, 1, 0, 12, 2, 0, 0, -1, '0', 0, 1),
(129, 0, 0, 1, 0, NULL, NULL, NULL, 1, 129, 'OVERRIDE', '/!drafts/view.php', 0, NULL, 0, 1, 0, 0, 0, 0, 0, -1, '0', 0, 1),
(130, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/!trash/view.php', 0, NULL, 0, 1, 0, 0, 0, 0, 0, -1, '0', 0, 1),
(131, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/!stacks/view.php', 0, NULL, 0, 1, 11, 0, 0, 0, 0, -1, '0', 0, 1),
(132, 0, 0, 1, 0, NULL, NULL, NULL, 1, 132, 'OVERRIDE', '/login.php', 0, NULL, 0, 1, 0, 0, 0, 0, 0, -1, '0', 0, 1),
(133, 0, 0, 1, 0, NULL, NULL, NULL, 1, 133, 'OVERRIDE', '/register.php', 0, NULL, 0, 1, 0, 0, 0, 0, 0, -1, '0', 0, 1),
(134, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/account/view.php', 0, NULL, 0, 1, 3, 0, 0, 0, 0, -1, '0', 0, 1),
(135, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/account/edit_profile.php', 0, NULL, 0, 1, 0, 0, 134, 0, 0, -1, '0', 0, 1),
(136, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/account/avatar.php', 0, NULL, 0, 1, 0, 1, 134, 0, 0, -1, '0', 0, 1),
(137, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/account/messages/view.php', 0, NULL, 0, 1, 1, 2, 134, 0, 0, -1, '0', 0, 1),
(138, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/account/messages/inbox.php', 0, NULL, 0, 1, 0, 0, 137, 0, 0, -1, '0', 0, 1),
(139, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/members/view.php', 0, NULL, 0, 1, 2, 0, 1, 0, 0, -1, '0', 0, 1),
(140, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/members/profile.php', 0, NULL, 0, 1, 0, 0, 139, 0, 0, -1, '0', 0, 1),
(141, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/members/directory.php', 0, NULL, 0, 1, 0, 1, 139, 0, 0, -1, '0', 0, 1),
(142, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/page_not_found.php', 0, NULL, 0, 1, 0, 1, 0, 0, 0, -1, '0', 0, 1),
(143, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/page_forbidden.php', 0, NULL, 0, 1, 0, 1, 0, 0, 0, -1, '0', 0, 1),
(144, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/download_file.php', 0, NULL, 0, 1, 0, 1, 1, 0, 0, -1, '0', 0, 1),
(145, 6, 1, NULL, 0, NULL, NULL, NULL, 1, 145, 'OVERRIDE', NULL, 0, NULL, 0, 1, 0, 0, 0, 0, 0, -1, '0', 0, 0),
(146, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 0, 131, 0, 0, -1, '0', 0, 1),
(147, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 1, 131, 0, 0, -1, '0', 0, 1),
(148, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 2, 131, 0, 0, -1, '0', 0, 1),
(149, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 3, 131, 0, 0, -1, '0', 0, 1),
(150, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 4, 131, 0, 0, -1, '0', 0, 1),
(151, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 5, 131, 0, 0, -1, '0', 0, 1),
(152, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 6, 131, 0, 0, -1, '0', 0, 1),
(153, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 7, 131, 0, 0, -1, '0', 0, 1),
(154, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 8, 131, 0, 0, -1, '0', 0, 1),
(155, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 9, 131, 0, 0, -1, '0', 0, 1),
(156, 6, 1, NULL, 0, NULL, NULL, NULL, 1, 156, 'OVERRIDE', NULL, 0, NULL, 0, 1, 0, 0, 0, 0, 0, -1, '0', 0, 0),
(157, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, '#projects', 0, 1, 0, 2, 1, 0, 0, -1, '0', 0, 0),
(158, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, '#updates', 0, 1, 0, 3, 1, 0, 0, -1, '0', 0, 0),
(159, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, '#fijnproevers', 0, 1, 0, 4, 1, 0, 0, -1, '0', 0, 0),
(160, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, '#over-ons', 0, 1, 0, 5, 1, 0, 0, -1, '0', 0, 0),
(161, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, '#contact', 0, 1, 0, 6, 1, 0, 0, -1, '0', 0, 0),
(162, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 10, 131, 0, 0, -1, '0', 0, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageSearchIndex`
--

CREATE TABLE `PageSearchIndex` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `content` longtext COLLATE utf8_unicode_ci,
  `cName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cDescription` text COLLATE utf8_unicode_ci,
  `cPath` text COLLATE utf8_unicode_ci,
  `cDatePublic` datetime DEFAULT NULL,
  `cDateLastIndexed` datetime DEFAULT NULL,
  `cDateLastSitemapped` datetime DEFAULT NULL,
  `cRequiresReindex` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PageSearchIndex`
--

INSERT INTO `PageSearchIndex` (`cID`, `content`, `cName`, `cDescription`, `cPath`, `cDatePublic`, `cDateLastIndexed`, `cDateLastSitemapped`, `cRequiresReindex`) VALUES
(2, '', 'Dashboard', '', '/dashboard', '2017-03-14 09:30:41', '2017-03-14 09:30:49', NULL, 0),
(3, '', 'Sitemap', 'Whole world at a glance.', '/dashboard/sitemap', '2017-03-14 09:30:42', '2017-03-14 09:30:49', NULL, 0),
(4, '', 'Full Sitemap', '', '/dashboard/sitemap/full', '2017-03-14 09:30:42', '2017-03-14 09:30:49', NULL, 0),
(5, '', 'Flat View', '', '/dashboard/sitemap/explore', '2017-03-14 09:30:42', '2017-03-14 09:30:50', NULL, 0),
(6, '', 'Page Search', '', '/dashboard/sitemap/search', '2017-03-14 09:30:42', '2017-03-14 09:30:50', NULL, 0),
(8, '', 'File Manager', '', '/dashboard/files/search', '2017-03-14 09:30:42', '2017-03-14 09:30:50', NULL, 0),
(9, '', 'Attributes', '', '/dashboard/files/attributes', '2017-03-14 09:30:42', '2017-03-14 09:30:50', NULL, 0),
(10, '', 'File Sets', '', '/dashboard/files/sets', '2017-03-14 09:30:42', '2017-03-14 09:30:50', NULL, 0),
(11, '', 'Add File Set', '', '/dashboard/files/add_set', '2017-03-14 09:30:42', '2017-03-14 09:30:50', NULL, 0),
(12, '', 'Members', 'Add and manage the user accounts and groups on your website.', '/dashboard/users', '2017-03-14 09:30:42', '2017-03-14 09:30:50', NULL, 0),
(13, '', 'Search Users', '', '/dashboard/users/search', '2017-03-14 09:30:42', '2017-03-14 09:30:50', NULL, 0),
(14, '', 'User Groups', '', '/dashboard/users/groups', '2017-03-14 09:30:42', '2017-03-14 09:30:50', NULL, 0),
(15, '', 'Attributes', '', '/dashboard/users/attributes', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(16, '', 'Add User', '', '/dashboard/users/add', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(17, '', 'Add Group', '', '/dashboard/users/add_group', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(19, '', 'Group Sets', '', '/dashboard/users/group_sets', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(20, '', 'Community Points', NULL, '/dashboard/users/points', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(22, '', 'Actions', NULL, '/dashboard/users/points/actions', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(23, '', 'Reports', 'Get data from forms and logs.', '/dashboard/reports', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(24, '', 'Form Results', 'Get submission data.', '/dashboard/reports/forms', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(25, '', 'Surveys', '', '/dashboard/reports/surveys', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(26, '', 'Logs', '', '/dashboard/reports/logs', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(28, '', 'Themes', 'Reskin your site.', '/dashboard/pages/themes', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(29, '', 'Inspect', '', '/dashboard/pages/themes/inspect', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(31, '', 'Organize Page Type Order', '', '/dashboard/pages/types/organize', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(32, '', 'Add Page Type', '', '/dashboard/pages/types/add', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(33, '', 'Compose Form', '', '/dashboard/pages/types/form', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(34, '', 'Defaults and Output', '', '/dashboard/pages/types/output', '2017-03-14 09:30:43', '2017-03-14 09:30:50', NULL, 0),
(35, '', 'Page Type Attributes', '', '/dashboard/pages/types/attributes', '2017-03-14 09:30:44', '2017-03-14 09:30:50', NULL, 0),
(36, '', 'Page Type Permissions', '', '/dashboard/pages/types/permissions', '2017-03-14 09:30:44', '2017-03-14 09:30:50', NULL, 0),
(38, '', 'Add Page Template', 'Add page templates to your site.', '/dashboard/pages/templates/add', '2017-03-14 09:30:44', '2017-03-14 09:30:50', NULL, 0),
(39, '', 'Attributes', '', '/dashboard/pages/attributes', '2017-03-14 09:30:44', '2017-03-14 09:30:50', NULL, 0),
(40, '', 'Single Pages', '', '/dashboard/pages/single', '2017-03-14 09:30:44', '2017-03-14 09:30:50', NULL, 0),
(41, '', 'RSS Feeds', '', '/dashboard/pages/feeds', '2017-03-14 09:30:44', '2017-03-14 09:30:50', NULL, 0),
(43, '', 'Messages', '', '/dashboard/conversations/messages', '2017-03-14 09:30:44', '2017-03-14 09:30:50', NULL, 0),
(44, '', 'Workflow', '', '/dashboard/workflow', '2017-03-14 09:30:44', '2017-03-14 09:30:50', NULL, 0),
(48, '', 'Stacks', 'Share content across your site.', '/dashboard/blocks/stacks', '2017-03-14 09:30:44', '2017-03-14 09:30:50', NULL, 0),
(50, '', 'Stack List', '', '/dashboard/blocks/stacks/list', '2017-03-14 09:30:44', '2017-03-14 09:30:50', NULL, 0),
(51, '', 'Block Types', 'Manage the installed block types in your site.', '/dashboard/blocks/types', '2017-03-14 09:30:44', '2017-03-14 09:30:51', NULL, 0),
(52, '', 'Extend concrete5', '', '/dashboard/extend', '2017-03-14 09:30:44', '2017-03-14 09:30:51', NULL, 0),
(53, '', 'Dashboard', '', '/dashboard/news', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(54, '', 'Add Functionality', 'Install add-ons & themes.', '/dashboard/extend/install', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(55, '', 'Update Add-Ons', 'Update your installed packages.', '/dashboard/extend/update', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(56, '', 'Connect to the Community', 'Connect to the concrete5 community.', '/dashboard/extend/connect', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(57, '', 'Get More Themes', 'Download themes from concrete5.org.', '/dashboard/extend/themes', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(58, '', 'Get More Add-Ons', 'Download add-ons from concrete5.org.', '/dashboard/extend/addons', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(59, '', 'System & Settings', 'Secure and setup your site.', '/dashboard/system', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(61, '', 'Site Name', '', '/dashboard/system/basics/name', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(62, '', 'Accessibility', '', '/dashboard/system/basics/accessibility', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(63, '', 'Social Links', '', '/dashboard/system/basics/social', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(64, '', 'Bookmark Icons', 'Bookmark icon and mobile home screen icon setup.', '/dashboard/system/basics/icons', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(65, '', 'Rich Text Editor', '', '/dashboard/system/basics/editor', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(66, '', 'Languages', '', '/dashboard/system/basics/multilingual', '2017-03-14 09:30:45', '2017-03-14 09:30:51', NULL, 0),
(67, '', 'Time Zone', '', '/dashboard/system/basics/timezone', '2017-03-14 09:30:46', '2017-03-14 09:30:51', NULL, 0),
(68, '', 'Multilingual', 'Run your site in multiple languages.', '/dashboard/system/multilingual', '2017-03-14 09:30:46', '2017-03-14 09:30:51', NULL, 0),
(74, '', 'URLs and Redirection', '', '/dashboard/system/seo/urls', '2017-03-14 09:30:46', '2017-03-14 09:30:51', NULL, 0),
(75, '', 'Bulk SEO Updater', '', '/dashboard/system/seo/bulk', '2017-03-14 09:30:46', '2017-03-14 09:30:51', NULL, 0),
(76, '', 'Tracking Codes', '', '/dashboard/system/seo/codes', '2017-03-14 09:30:46', '2017-03-14 09:30:51', NULL, 0),
(77, '', 'Excluded URL Word List', '', '/dashboard/system/seo/excluded', '2017-03-14 09:30:46', '2017-03-14 09:30:51', NULL, 0),
(78, '', 'Search Index', '', '/dashboard/system/seo/searchindex', '2017-03-14 09:30:46', '2017-03-14 09:30:51', NULL, 0),
(80, '', 'File Manager Permissions', '', '/dashboard/system/files/permissions', '2017-03-14 09:30:46', '2017-03-14 09:30:51', NULL, 0),
(81, '', 'Allowed File Types', '', '/dashboard/system/files/filetypes', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(82, '', 'Thumbnails', '', '/dashboard/system/files/thumbnails', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(83, '', 'Image Uploading', '', '/dashboard/system/files/image_uploading', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(84, '', 'File Storage Locations', '', '/dashboard/system/files/storage', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(86, '', 'Cache & Speed Settings', '', '/dashboard/system/optimization/cache', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(87, '', 'Clear Cache', '', '/dashboard/system/optimization/clearcache', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(88, '', 'Automated Jobs', '', '/dashboard/system/optimization/jobs', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(89, '', 'Database Query Log', '', '/dashboard/system/optimization/query_log', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(91, '', 'Site Access', '', '/dashboard/system/permissions/site', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(92, '', 'Task Permissions', '', '/dashboard/system/permissions/tasks', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(95, '', 'IP Blacklist', '', '/dashboard/system/permissions/blacklist', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(96, '', 'Captcha Setup', '', '/dashboard/system/permissions/captcha', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(97, '', 'Spam Control', '', '/dashboard/system/permissions/antispam', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(98, '', 'Maintenance Mode', '', '/dashboard/system/permissions/maintenance', '2017-03-14 09:30:47', '2017-03-14 09:30:51', NULL, 0),
(100, '', 'Login Destination', '', '/dashboard/system/registration/postlogin', '2017-03-14 09:30:48', '2017-03-14 09:30:51', NULL, 0),
(101, '', 'Public Profiles', '', '/dashboard/system/registration/profiles', '2017-03-14 09:30:48', '2017-03-14 09:30:51', NULL, 0),
(102, '', 'Public Registration', '', '/dashboard/system/registration/open', '2017-03-14 09:30:48', '2017-03-14 09:30:51', NULL, 0),
(103, '', 'Authentication Types', '', '/dashboard/system/registration/authentication', '2017-03-14 09:30:48', '2017-03-14 09:30:51', NULL, 0),
(104, '', 'Email', 'Control how your site send and processes mail.', '/dashboard/system/mail', '2017-03-14 09:30:48', '2017-03-14 09:30:51', NULL, 0),
(105, '', 'SMTP Method', '', '/dashboard/system/mail/method', '2017-03-14 09:30:48', '2017-03-14 09:30:51', NULL, 0),
(106, '', 'Test Mail Settings', '', '/dashboard/system/mail/method/test', '2017-03-14 09:30:48', '2017-03-14 09:30:52', NULL, 0),
(107, '', 'Email Importers', '', '/dashboard/system/mail/importers', '2017-03-14 09:30:48', '2017-03-14 09:30:52', NULL, 0),
(108, '', 'Conversations', 'Manage your conversations settings', '/dashboard/system/conversations', '2017-03-14 09:30:48', '2017-03-14 09:30:52', NULL, 0),
(109, '', 'Settings', '', '/dashboard/system/conversations/settings', '2017-03-14 09:30:48', '2017-03-14 09:30:52', NULL, 0),
(110, '', 'Community Points', '', '/dashboard/system/conversations/points', '2017-03-14 09:30:48', '2017-03-14 09:30:52', NULL, 0),
(111, '', 'Banned Words', '', '/dashboard/system/conversations/bannedwords', '2017-03-14 09:30:48', '2017-03-14 09:30:52', NULL, 0),
(113, '', 'Attributes', 'Setup attributes for pages, users, files and more.', '/dashboard/system/attributes', '2017-03-14 09:30:48', '2017-03-14 09:30:52', NULL, 0),
(114, '', 'Sets', 'Group attributes into sets for easier organization', '/dashboard/system/attributes/sets', '2017-03-14 09:30:48', '2017-03-14 09:30:52', NULL, 0),
(115, '', 'Types', 'Choose which attribute types are available for different items.', '/dashboard/system/attributes/types', '2017-03-14 09:30:48', '2017-03-14 09:30:52', NULL, 0),
(116, '', 'Topics', '', '/dashboard/system/attributes/topics', '2017-03-14 09:30:48', '2017-03-14 09:30:52', NULL, 0),
(118, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(119, '', 'Environment Information', '', '/dashboard/system/environment/info', '2017-03-14 09:30:48', '2017-03-14 09:30:52', NULL, 0),
(120, '', 'Debug Settings', '', '/dashboard/system/environment/debug', '2017-03-14 09:30:49', '2017-03-14 09:30:52', NULL, 0),
(121, '', 'Logging Settings', '', '/dashboard/system/environment/logging', '2017-03-14 09:30:49', '2017-03-14 09:30:52', NULL, 0),
(122, '', 'Proxy Server', '', '/dashboard/system/environment/proxy', '2017-03-14 09:30:49', '2017-03-14 09:30:52', NULL, 0),
(123, '', 'Database Entities', '', '/dashboard/system/environment/entities', '2017-03-14 09:30:49', '2017-03-14 09:30:52', NULL, 0),
(124, '', 'Backup & Restore', 'Backup or restore your website.', '/dashboard/system/backup', '2017-03-14 09:30:49', '2017-03-14 09:30:52', NULL, 0),
(126, '', 'Update concrete5', '', '/dashboard/system/backup/update', '2017-03-14 09:30:49', '2017-03-14 09:30:52', NULL, 0),
(127, '\n\n                                      ', 'Welcome to concrete5', 'Learn about how to use concrete5, how to develop for concrete5, and get general help.', '/dashboard/welcome', '2017-03-14 09:30:49', '2017-03-14 09:30:52', NULL, 0),
(128, '', 'Customize Dashboard Home', '', '/dashboard/home', '2017-03-14 09:30:49', '2017-03-14 09:30:52', NULL, 0),
(129, '', 'Drafts', '', '/!drafts', '2017-03-14 09:30:53', '2017-03-14 09:30:53', NULL, 0),
(130, '', 'Trash', '', '/!trash', '2017-03-14 09:30:53', '2017-03-14 09:30:53', NULL, 0),
(131, '', 'Stacks', '', '/!stacks', '2017-03-14 09:30:53', '2017-03-14 09:30:53', NULL, 0),
(134, '', 'My Account', '', '/account', '2017-03-14 09:30:53', '2017-03-14 09:30:53', NULL, 0),
(1, 'Well-prepared  events & productionsEen evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu. Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet. Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.\r\n Lees meer\r\n  If you fail to prepare, then prepare to fail.  Slide 2   Well-prepared  events & productions Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu. Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet. Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.\r\n  Productions\r\nPROJECTS\r\n Net zoals bij onze events hanteren wij dezelfde werkwijze voor het produceren van commercials, bedrijfsfilms en andere audiovisuele producties. Onze jarenlange ervaring bij diverse omroepen/zenders en commerciële producenten zorgen ervoor dat uw productie binnen de planning en afgesproken budget afgerond wordt. Kleine of groot Fresh Fugu zorgt dat al haar producties ‘well-prepared’ zijn.  Events\r\nPROJECTS\r\n Een event, product lancering of merk activatie Fresh Fugu hanteert altijd de regel ‘If you fail to prepare, then prepare to fail’.\r\n \r\n Voor onze opdrachtgevers verzorgen wij het hele proces voor het perfecte event. Vanaf de ontwikkeling van het creatieve concept tot en met de verzorging van een goodiebag na afloop. Voor elke stap hanteren wij slimme checklists om ervoor te zorgen dat uw evenement tot in de kleinste details geregeld is.\r\n  Onze fugu master\r\nOver ons\r\n Een evenement of productie is wat ons betreft net zoals het zeer mythische gerecht Fugu. Fantastisch als het gemaakt wordt door iemand die er verstand van heeft maar fataal in de handen van iemand die niet weet wat hij doet. Fresh Fugu combineert verfrissende skills, kennis en de ervaring die nodig is om evenementen of producties op een top niveau te serveren.\r\n  Lorem ipsum dolor sater\r\nde fijnproevers \r\n Lorem ipsum dolor sater in terra met compter veritas set in delio mentras.  \r\n Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod veniam, quis nostrud exercitation ullamco in volup velit esse cillum dolore eu fugiat nulla pariatur.\r\n  If you fail to prepare, then prepare to fail.  Slide 2    Omniversum introduceert omnivrsum Updates 03/03/2017 Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur.\r\n   Omniversum introduceert omnivrsum \r\nUpdates 03/03/2017\r\n Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ainim veniam, quis nostrud exercitation ulamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehederit in volup vse cillum dolore eu fugiat nulla pariatur.\r\n   Lorem ipsum dolor saterContact\r\n  Lorem ipsum dolor sit amet, consur adipisicing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua.\r\n \r\n Ut enim ainim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo conset. Duis aute irure dolor in reprehenderit in volup velit esse cillum dolore eu fugiat nulla pariatur.\r\n  ', 'Home', '', NULL, '2017-03-14 09:29:49', '2017-03-16 11:58:02', NULL, 0),
(146, '', 'Header Site Title', NULL, '/!stacks/header-site-title', '2017-03-14 09:41:33', '2017-03-14 14:42:45', NULL, 0),
(147, '', 'Header Navigation', NULL, '/!stacks/header-navigation', '2017-03-14 09:41:33', '2017-03-14 14:42:46', NULL, 0),
(154, '', 'Navigation', NULL, '/!stacks/navigation', '2017-03-14 14:47:21', '2017-03-14 14:48:25', NULL, 0),
(155, '', 'Logo', NULL, '/!stacks/logo', '2017-03-14 14:57:57', '2017-03-15 14:55:25', NULL, 0),
(162, '', 'Social media', NULL, '/!stacks/social-media', '2017-03-14 15:19:45', '2017-03-14 15:21:46', NULL, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageTemplates`
--

CREATE TABLE `PageTemplates` (
  `pTemplateID` int(10) UNSIGNED NOT NULL,
  `pTemplateHandle` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `pTemplateIcon` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pTemplateName` varchar(90) COLLATE utf8_unicode_ci NOT NULL,
  `pTemplateIsInternal` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PageTemplates`
--

INSERT INTO `PageTemplates` (`pTemplateID`, `pTemplateHandle`, `pTemplateIcon`, `pTemplateName`, `pTemplateIsInternal`, `pkgID`) VALUES
(1, 'core_stack', '', 'Stack', 1, 0),
(2, 'dashboard_primary_five', '', 'Dashboard Primary + Five', 1, 0),
(3, 'dashboard_header_four_col', '', 'Dashboard Header + Four Column', 1, 0),
(4, 'dashboard_full', '', 'Dashboard Full', 1, 0),
(5, 'full', 'full.png', 'Full', 0, 0),
(6, 'default', 'blank.png', 'Default', 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageThemeCustomStyles`
--

CREATE TABLE `PageThemeCustomStyles` (
  `pThemeID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `scvlID` int(10) UNSIGNED DEFAULT '0',
  `preset` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sccRecordID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageThemes`
--

CREATE TABLE `PageThemes` (
  `pThemeID` int(10) UNSIGNED NOT NULL,
  `pThemeHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `pThemeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pThemeDescription` text COLLATE utf8_unicode_ci,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pThemeHasCustomClass` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PageThemes`
--

INSERT INTO `PageThemes` (`pThemeID`, `pThemeHandle`, `pThemeName`, `pThemeDescription`, `pkgID`, `pThemeHasCustomClass`) VALUES
(1, 'elemental', 'Elemental', 'Elegant, spacious theme with support for blogs, portfolios, layouts and more.', 0, 1),
(3, 'fugu', 'Fugu', 'Copyright Forza Digital', 2, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageTypeComposerControlTypes`
--

CREATE TABLE `PageTypeComposerControlTypes` (
  `ptComposerControlTypeID` int(10) UNSIGNED NOT NULL,
  `ptComposerControlTypeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptComposerControlTypeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PageTypeComposerControlTypes`
--

INSERT INTO `PageTypeComposerControlTypes` (`ptComposerControlTypeID`, `ptComposerControlTypeHandle`, `ptComposerControlTypeName`, `pkgID`) VALUES
(1, 'core_page_property', 'Built-In Property', 0),
(2, 'collection_attribute', 'Custom Attribute', 0),
(3, 'block', 'Block', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageTypeComposerFormLayoutSetControls`
--

CREATE TABLE `PageTypeComposerFormLayoutSetControls` (
  `ptComposerFormLayoutSetControlID` int(10) UNSIGNED NOT NULL,
  `ptComposerFormLayoutSetID` int(10) UNSIGNED DEFAULT '0',
  `ptComposerControlTypeID` int(10) UNSIGNED DEFAULT '0',
  `ptComposerControlObject` longtext COLLATE utf8_unicode_ci,
  `ptComposerFormLayoutSetControlDisplayOrder` int(10) UNSIGNED DEFAULT '0',
  `ptComposerFormLayoutSetControlCustomLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptComposerFormLayoutSetControlCustomTemplate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptComposerFormLayoutSetControlDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptComposerFormLayoutSetControlRequired` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PageTypeComposerFormLayoutSetControls`
--

INSERT INTO `PageTypeComposerFormLayoutSetControls` (`ptComposerFormLayoutSetControlID`, `ptComposerFormLayoutSetID`, `ptComposerControlTypeID`, `ptComposerControlObject`, `ptComposerFormLayoutSetControlDisplayOrder`, `ptComposerFormLayoutSetControlCustomLabel`, `ptComposerFormLayoutSetControlCustomTemplate`, `ptComposerFormLayoutSetControlDescription`, `ptComposerFormLayoutSetControlRequired`) VALUES
(1, 1, 1, 'O:78:"Concrete\\Core\\Page\\Type\\Composer\\Control\\CorePageProperty\\NameCorePageProperty":11:{s:37:"\0*\0ptComposerControlRequiredByDefault";b:1;s:17:"\0*\0propertyHandle";s:4:"name";s:30:"\0*\0ptComposerControlTypeHandle";s:18:"core_page_property";s:30:"\0*\0ptComposerControlIdentifier";s:4:"name";s:24:"\0*\0ptComposerControlName";N;s:27:"\0*\0ptComposerControlIconSRC";s:34:"/concrete/attributes/text/icon.png";s:20:"\0*\0ptComposerControl";N;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:31:"\0*\0ptComposerControlCustomLabel";N;s:31:"\0*\0ptComposerControlDescription";N;s:5:"error";s:0:"";}', 0, 'Page Name', NULL, NULL, 1),
(2, 1, 1, 'O:85:"Concrete\\Core\\Page\\Type\\Composer\\Control\\CorePageProperty\\DescriptionCorePageProperty":11:{s:17:"\0*\0propertyHandle";s:11:"description";s:30:"\0*\0ptComposerControlTypeHandle";s:18:"core_page_property";s:30:"\0*\0ptComposerControlIdentifier";s:11:"description";s:24:"\0*\0ptComposerControlName";N;s:27:"\0*\0ptComposerControlIconSRC";s:38:"/concrete/attributes/textarea/icon.png";s:20:"\0*\0ptComposerControl";N;s:37:"\0*\0ptComposerControlRequiredByDefault";b:0;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:31:"\0*\0ptComposerControlCustomLabel";N;s:31:"\0*\0ptComposerControlDescription";N;s:5:"error";s:0:"";}', 1, NULL, NULL, NULL, 0),
(3, 1, 1, 'O:81:"Concrete\\Core\\Page\\Type\\Composer\\Control\\CorePageProperty\\UrlSlugCorePageProperty":11:{s:17:"\0*\0propertyHandle";s:8:"url_slug";s:30:"\0*\0ptComposerControlTypeHandle";s:18:"core_page_property";s:30:"\0*\0ptComposerControlIdentifier";s:8:"url_slug";s:24:"\0*\0ptComposerControlName";N;s:27:"\0*\0ptComposerControlIconSRC";s:34:"/concrete/attributes/text/icon.png";s:20:"\0*\0ptComposerControl";N;s:37:"\0*\0ptComposerControlRequiredByDefault";b:0;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:31:"\0*\0ptComposerControlCustomLabel";N;s:31:"\0*\0ptComposerControlDescription";N;s:5:"error";s:0:"";}', 2, NULL, NULL, NULL, 0),
(4, 1, 1, 'O:86:"Concrete\\Core\\Page\\Type\\Composer\\Control\\CorePageProperty\\PageTemplateCorePageProperty":11:{s:17:"\0*\0propertyHandle";s:13:"page_template";s:30:"\0*\0ptComposerControlTypeHandle";s:18:"core_page_property";s:30:"\0*\0ptComposerControlIdentifier";s:13:"page_template";s:24:"\0*\0ptComposerControlName";N;s:27:"\0*\0ptComposerControlIconSRC";s:36:"/concrete/attributes/select/icon.png";s:20:"\0*\0ptComposerControl";N;s:37:"\0*\0ptComposerControlRequiredByDefault";b:0;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:31:"\0*\0ptComposerControlCustomLabel";N;s:31:"\0*\0ptComposerControlDescription";N;s:5:"error";s:0:"";}', 3, NULL, NULL, NULL, 0),
(5, 1, 1, 'O:87:"Concrete\\Core\\Page\\Type\\Composer\\Control\\CorePageProperty\\PublishTargetCorePageProperty":11:{s:17:"\0*\0propertyHandle";s:14:"publish_target";s:30:"\0*\0ptComposerControlTypeHandle";s:18:"core_page_property";s:30:"\0*\0ptComposerControlIdentifier";s:14:"publish_target";s:24:"\0*\0ptComposerControlName";N;s:27:"\0*\0ptComposerControlIconSRC";s:40:"/concrete/attributes/image_file/icon.png";s:20:"\0*\0ptComposerControl";N;s:37:"\0*\0ptComposerControlRequiredByDefault";b:0;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:31:"\0*\0ptComposerControlCustomLabel";N;s:31:"\0*\0ptComposerControlDescription";N;s:5:"error";s:0:"";}', 4, NULL, NULL, NULL, 0),
(6, 2, 3, 'O:53:"Concrete\\Core\\Page\\Type\\Composer\\Control\\BlockControl":14:{s:7:"\0*\0btID";i:12;s:30:"\0*\0ptComposerControlTypeHandle";s:5:"block";s:5:"\0*\0bt";b:0;s:4:"\0*\0b";b:0;s:13:"\0*\0controller";N;s:30:"\0*\0ptComposerControlIdentifier";i:12;s:24:"\0*\0ptComposerControlName";s:7:"Content";s:27:"\0*\0ptComposerControlIconSRC";s:33:"/concrete/blocks/content/icon.png";s:20:"\0*\0ptComposerControl";N;s:37:"\0*\0ptComposerControlRequiredByDefault";b:0;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:31:"\0*\0ptComposerControlCustomLabel";N;s:31:"\0*\0ptComposerControlDescription";N;s:5:"error";s:0:"";}', 0, 'Body', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageTypeComposerFormLayoutSets`
--

CREATE TABLE `PageTypeComposerFormLayoutSets` (
  `ptComposerFormLayoutSetID` int(10) UNSIGNED NOT NULL,
  `ptID` int(10) UNSIGNED DEFAULT '0',
  `ptComposerFormLayoutSetName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptComposerFormLayoutSetDescription` text COLLATE utf8_unicode_ci,
  `ptComposerFormLayoutSetDisplayOrder` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PageTypeComposerFormLayoutSets`
--

INSERT INTO `PageTypeComposerFormLayoutSets` (`ptComposerFormLayoutSetID`, `ptID`, `ptComposerFormLayoutSetName`, `ptComposerFormLayoutSetDescription`, `ptComposerFormLayoutSetDisplayOrder`) VALUES
(1, 6, 'Basics', '', 0),
(2, 6, 'Content', '', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageTypeComposerOutputBlocks`
--

CREATE TABLE `PageTypeComposerOutputBlocks` (
  `ptComposerOutputBlockID` int(10) UNSIGNED NOT NULL,
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cbDisplayOrder` int(10) UNSIGNED DEFAULT '0',
  `ptComposerFormLayoutSetControlID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `bID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageTypeComposerOutputControls`
--

CREATE TABLE `PageTypeComposerOutputControls` (
  `ptComposerOutputControlID` int(10) UNSIGNED NOT NULL,
  `pTemplateID` int(10) UNSIGNED DEFAULT '0',
  `ptID` int(10) UNSIGNED DEFAULT '0',
  `ptComposerFormLayoutSetControlID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PageTypeComposerOutputControls`
--

INSERT INTO `PageTypeComposerOutputControls` (`ptComposerOutputControlID`, `pTemplateID`, `ptID`, `ptComposerFormLayoutSetControlID`) VALUES
(1, 5, 6, 6),
(2, 6, 6, 6);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageTypePageTemplateDefaultPages`
--

CREATE TABLE `PageTypePageTemplateDefaultPages` (
  `pTemplateID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ptID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PageTypePageTemplateDefaultPages`
--

INSERT INTO `PageTypePageTemplateDefaultPages` (`pTemplateID`, `ptID`, `cID`) VALUES
(5, 6, 145),
(6, 6, 156);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageTypePageTemplates`
--

CREATE TABLE `PageTypePageTemplates` (
  `ptID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pTemplateID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageTypePermissionAssignments`
--

CREATE TABLE `PageTypePermissionAssignments` (
  `ptID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PageTypePermissionAssignments`
--

INSERT INTO `PageTypePermissionAssignments` (`ptID`, `pkID`, `paID`) VALUES
(1, 59, 9),
(1, 60, 9),
(1, 61, 9),
(1, 62, 9),
(1, 63, 26),
(2, 59, 9),
(2, 60, 9),
(2, 61, 9),
(2, 62, 9),
(2, 63, 27),
(3, 59, 9),
(3, 60, 9),
(3, 61, 9),
(3, 62, 9),
(3, 63, 28),
(4, 59, 9),
(4, 60, 9),
(4, 61, 9),
(4, 62, 9),
(4, 63, 29),
(5, 59, 9),
(5, 60, 9),
(5, 61, 9),
(5, 62, 9),
(5, 63, 30),
(6, 59, 9),
(6, 60, 9),
(6, 61, 9),
(6, 62, 9),
(6, 63, 31);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageTypePublishTargetTypes`
--

CREATE TABLE `PageTypePublishTargetTypes` (
  `ptPublishTargetTypeID` int(10) UNSIGNED NOT NULL,
  `ptPublishTargetTypeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptPublishTargetTypeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PageTypePublishTargetTypes`
--

INSERT INTO `PageTypePublishTargetTypes` (`ptPublishTargetTypeID`, `ptPublishTargetTypeHandle`, `ptPublishTargetTypeName`, `pkgID`) VALUES
(1, 'parent_page', 'Always publish below a certain page', 0),
(2, 'page_type', 'Choose from pages of a certain type', 0),
(3, 'all', 'Choose from all pages when publishing', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageTypes`
--

CREATE TABLE `PageTypes` (
  `ptID` int(10) UNSIGNED NOT NULL,
  `ptName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ptHandle` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ptPublishTargetTypeID` int(10) UNSIGNED DEFAULT NULL,
  `ptDefaultPageTemplateID` int(10) UNSIGNED DEFAULT NULL,
  `ptAllowedPageTemplates` varchar(1) COLLATE utf8_unicode_ci DEFAULT 'A',
  `ptIsInternal` tinyint(1) NOT NULL DEFAULT '0',
  `ptIsFrequentlyAdded` tinyint(1) NOT NULL DEFAULT '1',
  `ptDisplayOrder` int(10) UNSIGNED DEFAULT NULL,
  `ptLaunchInComposer` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ptPublishTargetObject` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PageTypes`
--

INSERT INTO `PageTypes` (`ptID`, `ptName`, `ptHandle`, `ptPublishTargetTypeID`, `ptDefaultPageTemplateID`, `ptAllowedPageTemplates`, `ptIsInternal`, `ptIsFrequentlyAdded`, `ptDisplayOrder`, `ptLaunchInComposer`, `pkgID`, `ptPublishTargetObject`) VALUES
(1, 'Stack', 'core_stack', 3, 0, 'A', 1, 0, 0, 0, 0, 'O:68:"Concrete\\Core\\Page\\Type\\PublishTarget\\Configuration\\AllConfiguration":7:{s:21:"\0*\0selectorFormFactor";N;s:22:"\0*\0startingPointPageID";N;s:5:"error";s:0:"";s:21:"ptPublishTargetTypeID";s:1:"3";s:25:"ptPublishTargetTypeHandle";s:3:"all";s:9:"pkgHandle";b:0;s:17:"startingPointPage";N;}'),
(2, 'Stack Category', 'core_stack_category', 3, 0, 'A', 1, 0, 1, 0, 0, 'O:68:"Concrete\\Core\\Page\\Type\\PublishTarget\\Configuration\\AllConfiguration":7:{s:21:"\0*\0selectorFormFactor";N;s:22:"\0*\0startingPointPageID";N;s:5:"error";s:0:"";s:21:"ptPublishTargetTypeID";s:1:"3";s:25:"ptPublishTargetTypeHandle";s:3:"all";s:9:"pkgHandle";b:0;s:17:"startingPointPage";N;}'),
(3, 'Dashboard Primary + Five', 'dashboard_primary_five', 3, 0, 'A', 1, 0, 2, 0, 0, 'O:68:"Concrete\\Core\\Page\\Type\\PublishTarget\\Configuration\\AllConfiguration":7:{s:21:"\0*\0selectorFormFactor";N;s:22:"\0*\0startingPointPageID";N;s:5:"error";s:0:"";s:21:"ptPublishTargetTypeID";s:1:"3";s:25:"ptPublishTargetTypeHandle";s:3:"all";s:9:"pkgHandle";b:0;s:17:"startingPointPage";N;}'),
(4, 'Dashboard Header + Four Column', 'dashboard_header_four_col', 3, 0, 'A', 1, 0, 3, 0, 0, 'O:68:"Concrete\\Core\\Page\\Type\\PublishTarget\\Configuration\\AllConfiguration":7:{s:21:"\0*\0selectorFormFactor";N;s:22:"\0*\0startingPointPageID";N;s:5:"error";s:0:"";s:21:"ptPublishTargetTypeID";s:1:"3";s:25:"ptPublishTargetTypeHandle";s:3:"all";s:9:"pkgHandle";b:0;s:17:"startingPointPage";N;}'),
(5, 'Dashboard Full', 'dashboard_full', 3, 0, 'A', 1, 0, 4, 0, 0, 'O:68:"Concrete\\Core\\Page\\Type\\PublishTarget\\Configuration\\AllConfiguration":7:{s:21:"\0*\0selectorFormFactor";N;s:22:"\0*\0startingPointPageID";N;s:5:"error";s:0:"";s:21:"ptPublishTargetTypeID";s:1:"3";s:25:"ptPublishTargetTypeHandle";s:3:"all";s:9:"pkgHandle";b:0;s:17:"startingPointPage";N;}'),
(6, 'Page', 'page', 3, 5, 'A', 0, 1, 0, 0, 0, 'O:68:"Concrete\\Core\\Page\\Type\\PublishTarget\\Configuration\\AllConfiguration":6:{s:21:"\0*\0selectorFormFactor";N;s:22:"\0*\0startingPointPageID";N;s:5:"error";s:0:"";s:21:"ptPublishTargetTypeID";s:1:"3";s:25:"ptPublishTargetTypeHandle";s:3:"all";s:9:"pkgHandle";b:0;}');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PageWorkflowProgress`
--

CREATE TABLE `PageWorkflowProgress` (
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `wpID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionAccess`
--

CREATE TABLE `PermissionAccess` (
  `paID` int(10) UNSIGNED NOT NULL,
  `paIsInUse` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PermissionAccess`
--

INSERT INTO `PermissionAccess` (`paID`, `paIsInUse`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionAccessEntities`
--

CREATE TABLE `PermissionAccessEntities` (
  `peID` int(10) UNSIGNED NOT NULL,
  `petID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PermissionAccessEntities`
--

INSERT INTO `PermissionAccessEntities` (`peID`, `petID`) VALUES
(1, 1),
(5, 1),
(6, 1),
(2, 5),
(3, 6),
(4, 7);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionAccessEntityGroups`
--

CREATE TABLE `PermissionAccessEntityGroups` (
  `pegID` int(10) UNSIGNED NOT NULL,
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PermissionAccessEntityGroups`
--

INSERT INTO `PermissionAccessEntityGroups` (`pegID`, `peID`, `gID`) VALUES
(1, 1, 3),
(2, 5, 1),
(3, 6, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionAccessEntityGroupSets`
--

CREATE TABLE `PermissionAccessEntityGroupSets` (
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gsID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionAccessEntityTypeCategories`
--

CREATE TABLE `PermissionAccessEntityTypeCategories` (
  `petID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkCategoryID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PermissionAccessEntityTypeCategories`
--

INSERT INTO `PermissionAccessEntityTypeCategories` (`petID`, `pkCategoryID`) VALUES
(1, 1),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(2, 1),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(3, 1),
(3, 4),
(3, 5),
(3, 6),
(3, 7),
(3, 8),
(3, 9),
(3, 10),
(3, 11),
(3, 12),
(3, 13),
(3, 14),
(3, 15),
(3, 16),
(3, 17),
(3, 18),
(3, 19),
(3, 20),
(4, 1),
(4, 4),
(4, 5),
(4, 6),
(4, 7),
(4, 8),
(4, 9),
(4, 10),
(4, 11),
(4, 12),
(4, 13),
(4, 14),
(4, 15),
(4, 16),
(4, 17),
(4, 18),
(4, 19),
(4, 20),
(4, 21),
(5, 1),
(5, 14),
(6, 5),
(6, 6),
(7, 19),
(7, 20);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionAccessEntityTypes`
--

CREATE TABLE `PermissionAccessEntityTypes` (
  `petID` int(10) UNSIGNED NOT NULL,
  `petHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `petName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PermissionAccessEntityTypes`
--

INSERT INTO `PermissionAccessEntityTypes` (`petID`, `petHandle`, `petName`, `pkgID`) VALUES
(1, 'group', 'Group', 0),
(2, 'user', 'User', 0),
(3, 'group_set', 'Group Set', 0),
(4, 'group_combination', 'Group Combination', 0),
(5, 'page_owner', 'Page Owner', 0),
(6, 'file_uploader', 'File Uploader', 0),
(7, 'conversation_message_author', 'Message Author', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionAccessEntityUsers`
--

CREATE TABLE `PermissionAccessEntityUsers` (
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionAccessList`
--

CREATE TABLE `PermissionAccessList` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pdID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `accessType` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PermissionAccessList`
--

INSERT INTO `PermissionAccessList` (`paID`, `peID`, `pdID`, `accessType`) VALUES
(1, 1, 0, 10),
(2, 1, 0, 10),
(3, 1, 0, 10),
(4, 1, 0, 10),
(5, 1, 0, 10),
(6, 1, 0, 10),
(7, 1, 0, 10),
(8, 1, 0, 10),
(9, 1, 0, 10),
(10, 1, 0, 10),
(11, 1, 0, 10),
(12, 1, 0, 10),
(13, 1, 0, 10),
(14, 1, 0, 10),
(15, 1, 0, 10),
(16, 1, 0, 10),
(17, 1, 0, 10),
(18, 1, 0, 10),
(19, 1, 0, 10),
(20, 1, 0, 10),
(21, 1, 0, 10),
(22, 1, 0, 10),
(23, 1, 0, 10),
(24, 1, 0, 10),
(25, 1, 0, 10),
(26, 2, 0, 10),
(27, 2, 0, 10),
(28, 2, 0, 10),
(29, 2, 0, 10),
(30, 2, 0, 10),
(31, 2, 0, 10),
(32, 5, 0, 10),
(33, 1, 0, 10),
(33, 5, 0, 10),
(34, 1, 0, 10),
(35, 1, 0, 10),
(36, 1, 0, 10),
(37, 1, 0, 10),
(38, 1, 0, 10),
(39, 1, 0, 10),
(40, 1, 0, 10),
(41, 1, 0, 10),
(42, 5, 0, 10),
(43, 1, 0, 10),
(44, 1, 0, 10),
(45, 1, 0, 10),
(46, 1, 0, 10),
(47, 1, 0, 10),
(48, 1, 0, 10),
(49, 1, 0, 10),
(50, 1, 0, 10),
(51, 1, 0, 10),
(52, 1, 0, 10),
(53, 1, 0, 10),
(54, 1, 0, 10),
(55, 1, 0, 10),
(56, 1, 0, 10),
(57, 1, 0, 10),
(58, 1, 0, 10),
(59, 1, 0, 10),
(60, 5, 0, 10),
(61, 5, 0, 10),
(62, 1, 0, 10),
(63, 1, 0, 10),
(64, 1, 0, 10),
(65, 1, 0, 10),
(66, 1, 0, 10),
(67, 1, 0, 10),
(68, 1, 0, 10),
(69, 1, 0, 10),
(70, 1, 0, 10),
(71, 1, 0, 10),
(72, 1, 0, 10),
(73, 1, 0, 10),
(74, 1, 0, 10),
(75, 1, 0, 10),
(76, 1, 0, 10),
(77, 1, 0, 10),
(78, 1, 0, 10),
(79, 1, 0, 10),
(80, 1, 0, 10),
(81, 1, 0, 10),
(82, 1, 0, 10),
(83, 1, 0, 10),
(84, 1, 0, 10),
(85, 1, 0, 10),
(86, 5, 0, 10),
(87, 5, 0, 10),
(88, 1, 0, 10),
(88, 4, 0, 10),
(89, 1, 0, 10),
(89, 4, 0, 10),
(90, 1, 0, 10),
(90, 6, 0, 10),
(91, 1, 0, 10),
(92, 1, 0, 10),
(93, 1, 0, 10);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionAccessWorkflows`
--

CREATE TABLE `PermissionAccessWorkflows` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `wfID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionAssignments`
--

CREATE TABLE `PermissionAssignments` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PermissionAssignments`
--

INSERT INTO `PermissionAssignments` (`paID`, `pkID`) VALUES
(1, 19),
(2, 20),
(3, 74),
(4, 75),
(5, 76),
(6, 78),
(7, 79),
(8, 80),
(9, 86),
(10, 87),
(11, 89),
(12, 90),
(13, 91),
(14, 92),
(15, 93),
(16, 94),
(17, 95),
(18, 96),
(19, 97),
(20, 98),
(21, 99),
(22, 100),
(23, 101),
(24, 102),
(25, 103);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionDurationObjects`
--

CREATE TABLE `PermissionDurationObjects` (
  `pdID` int(10) UNSIGNED NOT NULL,
  `pdObject` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionKeyCategories`
--

CREATE TABLE `PermissionKeyCategories` (
  `pkCategoryID` int(10) UNSIGNED NOT NULL,
  `pkCategoryHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PermissionKeyCategories`
--

INSERT INTO `PermissionKeyCategories` (`pkCategoryID`, `pkCategoryHandle`, `pkgID`) VALUES
(1, 'page', NULL),
(2, 'single_page', NULL),
(3, 'stack', NULL),
(4, 'user', NULL),
(5, 'file_set', NULL),
(6, 'file', NULL),
(7, 'area', NULL),
(8, 'block_type', NULL),
(9, 'block', NULL),
(10, 'admin', NULL),
(11, 'sitemap', NULL),
(12, 'marketplace_newsflow', NULL),
(13, 'basic_workflow', NULL),
(14, 'page_type', NULL),
(15, 'gathering', NULL),
(16, 'group_tree_node', NULL),
(17, 'topic_category_tree_node', NULL),
(18, 'topic_tree_node', NULL),
(19, 'conversation', NULL),
(20, 'conversation_message', NULL),
(21, 'multilingual_section', NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PermissionKeys`
--

CREATE TABLE `PermissionKeys` (
  `pkID` int(10) UNSIGNED NOT NULL,
  `pkHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkCanTriggerWorkflow` tinyint(1) NOT NULL DEFAULT '0',
  `pkHasCustomClass` tinyint(1) NOT NULL DEFAULT '0',
  `pkDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pkCategoryID` int(10) UNSIGNED DEFAULT NULL,
  `pkgID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `PermissionKeys`
--

INSERT INTO `PermissionKeys` (`pkID`, `pkHandle`, `pkName`, `pkCanTriggerWorkflow`, `pkHasCustomClass`, `pkDescription`, `pkCategoryID`, `pkgID`) VALUES
(1, 'view_page', 'View', 0, 0, 'Can see a page exists and read its content.', 1, 0),
(2, 'view_page_versions', 'View Versions', 0, 0, 'Can view the page versions dialog and read past versions of a page.', 1, 0),
(3, 'view_page_in_sitemap', 'View Page in Sitemap', 0, 0, 'Controls whether a user can see a page in the sitemap or intelligent search.', 1, 0),
(4, 'preview_page_as_user', 'Preview Page As User', 0, 0, 'Ability to see what this page will look like at a specific time in the future as a specific user.', 1, 0),
(5, 'edit_page_properties', 'Edit Properties', 0, 1, 'Ability to change anything in the Page Properties menu.', 1, 0),
(6, 'edit_page_contents', 'Edit Contents', 0, 0, 'Ability to make edits to at least some of the content in the page. You can lock down different block areas and specific blocks by clicking Permissions on them as well. ', 1, 0),
(7, 'edit_page_speed_settings', 'Edit Speed Settings', 0, 0, 'Ability to change caching settings.', 1, 0),
(8, 'edit_page_theme', 'Change Theme', 0, 1, 'Ability to change just the theme for this page.', 1, 0),
(9, 'edit_page_template', 'Change Page Template', 0, 0, 'Ability to change just the page template for this page.', 1, 0),
(10, 'edit_page_page_type', 'Edit Page Type', 0, 0, 'Change the type of an existing page.', 1, 0),
(11, 'edit_page_permissions', 'Edit Permissions', 1, 0, 'Ability to change permissions for this page. Warning: by granting this a user could give themselves more access.', 1, 0),
(12, 'delete_page', 'Delete', 1, 0, 'Ability to move this page to the site''s Trash.', 1, 0),
(13, 'delete_page_versions', 'Delete Versions', 1, 0, 'Ability to remove old versions of this page.', 1, 0),
(14, 'approve_page_versions', 'Approve Changes', 1, 0, 'Can publish an unapproved version of the page.', 1, 0),
(15, 'add_subpage', 'Add Sub-Page', 0, 1, 'Can add a page beneath the current page.', 1, 0),
(16, 'move_or_copy_page', 'Move or Copy Page', 1, 0, 'Can move or copy this page to another location.', 1, 0),
(17, 'schedule_page_contents_guest_access', 'Schedule Guest Access', 0, 0, 'Can control scheduled guest access to this page.', 1, 0),
(18, 'edit_page_multilingual_settings', 'Edit Multilingual Settings', 0, 0, 'Controls whether a user can see the multilingual settings menu, re-map a page or set a page as ignored in multilingual settings.', 1, 0),
(19, 'add_block', 'Add Block', 0, 1, 'Can add a block to any area on the site. If someone is added here they can add blocks to any area (unless that area has permissions that override these global permissions.)', 8, 0),
(20, 'add_stack', 'Add Stack', 0, 0, 'Can add a stack or block from a stack to any area on the site. If someone is added here they can add stacks to any area (unless that area has permissions that override these global permissions.)', 8, 0),
(21, 'view_area', 'View Area', 0, 0, 'Can view the area and its contents.', 7, 0),
(22, 'edit_area_contents', 'Edit Area Contents', 0, 0, 'Can edit blocks within this area.', 7, 0),
(23, 'add_block_to_area', 'Add Block to Area', 0, 1, 'Can add blocks to this area. This setting overrides the global Add Block permission for this area.', 7, 0),
(24, 'add_stack_to_area', 'Add Stack to Area', 0, 0, 'Can add stacks to this area. This setting overrides the global Add Stack permission for this area.', 7, 0),
(25, 'add_layout_to_area', 'Add Layouts to Area', 0, 0, 'Controls whether users get the ability to add layouts to a particular area.', 7, 0),
(26, 'edit_area_design', 'Edit Area Design', 0, 0, 'Controls whether users see design controls and can modify an area''s custom CSS.', 7, 0),
(27, 'edit_area_permissions', 'Edit Area Permissions', 0, 0, 'Controls whether users can access the permissions on an area. Custom area permissions could override those of the page.', 7, 0),
(28, 'delete_area_contents', 'Delete Area Contents', 0, 0, 'Controls whether users can delete blocks from this area.', 7, 0),
(29, 'schedule_area_contents_guest_access', 'Schedule Guest Access', 0, 0, 'Controls whether users can schedule guest access permissions on blocks in this area. Guest Access is a shortcut for granting permissions just to the Guest Group.', 7, 0),
(30, 'view_block', 'View Block', 0, 0, 'Controls whether users can view this block in the page.', 9, 0),
(31, 'edit_block', 'Edit Block', 0, 0, 'Controls whether users can edit this block. This overrides any area or page permissions.', 9, 0),
(32, 'edit_block_custom_template', 'Change Custom Template', 0, 0, 'Controls whether users can change the custom template on this block. This overrides any area or page permissions.', 9, 0),
(33, 'edit_block_cache_settings', 'Edit Cache Settings', 0, 0, 'Controls whether users can change the block cache settings for this block instance.', 9, 0),
(34, 'edit_block_name', 'Edit Name', 0, 0, 'Controls whether users can change the block''s name (rarely used.)', 9, 0),
(35, 'delete_block', 'Delete Block', 0, 0, 'Controls whether users can delete this block. This overrides any area or page permissions.', 9, 0),
(36, 'edit_block_design', 'Edit Design', 0, 0, 'Controls whether users can set custom design properties or CSS on this block.', 9, 0),
(37, 'edit_block_permissions', 'Edit Permissions', 0, 0, 'Controls whether users can change permissions on this block, potentially granting themselves or others greater access.', 9, 0),
(38, 'schedule_guest_access', 'Schedule Guest Access', 0, 0, 'Controls whether users can schedule guest access permissions on this block. Guest Access is a shortcut for granting permissions just to the Guest Group.', 9, 0),
(39, 'view_file_set_file', 'View Files', 0, 0, 'Can view and download files in the site.', 5, 0),
(40, 'search_file_set', 'Search Files in File Manager', 0, 0, 'Can access the file manager', 5, 0),
(41, 'edit_file_set_file_properties', 'Edit File Properties', 0, 0, 'Can edit a file''s properties.', 5, 0),
(42, 'edit_file_set_file_contents', 'Edit File Contents', 0, 0, 'Can edit or replace files in set.', 5, 0),
(43, 'copy_file_set_files', 'Copy File', 0, 0, 'Can copy files in file set.', 5, 0),
(44, 'edit_file_set_permissions', 'Edit File Access', 0, 0, 'Can edit access to file sets.', 5, 0),
(45, 'delete_file_set', 'Delete File Set', 0, 0, 'Can delete file set.', 5, 0),
(46, 'delete_file_set_files', 'Delete File', 0, 0, 'Can delete files in set.', 5, 0),
(47, 'add_file', 'Add File', 0, 1, 'Can add files to set.', 5, 0),
(48, 'view_file', 'View Files', 0, 0, 'Can view and download files.', 6, 0),
(49, 'view_file_in_file_manager', 'View File in File Manager', 0, 0, 'Can access the File Manager.', 6, 0),
(50, 'edit_file_properties', 'Edit File Properties', 0, 0, 'Can edit a file''s properties.', 6, 0),
(51, 'edit_file_contents', 'Edit File Contents', 0, 0, 'Can edit or replace files.', 6, 0),
(52, 'copy_file', 'Copy File', 0, 0, 'Can copy file.', 6, 0),
(53, 'edit_file_permissions', 'Edit File Access', 0, 0, 'Can edit access to file.', 6, 0),
(54, 'delete_file', 'Delete File', 0, 0, 'Can delete file.', 6, 0),
(55, 'approve_basic_workflow_action', 'Approve or Deny', 0, 0, 'Grant ability to approve workflow.', 13, 0),
(56, 'notify_on_basic_workflow_entry', 'Notify on Entry', 0, 0, 'Notify approvers that a change has entered the workflow.', 13, 0),
(57, 'notify_on_basic_workflow_approve', 'Notify on Approve', 0, 0, 'Notify approvers that a change has been approved.', 13, 0),
(58, 'notify_on_basic_workflow_deny', 'Notify on Deny', 0, 0, 'Notify approvers that a change has been denied.', 13, 0),
(59, 'add_page_type', 'Add Pages of This Type', 0, 0, '', 14, 0),
(60, 'edit_page_type', 'Edit Page Type', 0, 0, '', 14, 0),
(61, 'delete_page_type', 'Delete Page Type', 0, 0, '', 14, 0),
(62, 'edit_page_type_permissions', 'Edit Page Type Permissions', 0, 0, '', 14, 0),
(63, 'edit_page_type_drafts', 'Edit Page Type Drafts', 0, 0, '', 14, 0),
(64, 'view_topic_tree_node', 'View Topic Tree Node', 0, 0, '', 18, 0),
(65, 'view_topic_category_tree_node', 'View Topic Category Tree Node', 0, 0, '', 17, 0),
(66, 'add_conversation_message', 'Add Message to Conversation', 0, 1, '', 19, 0),
(67, 'add_conversation_message_attachments', 'Add Message Attachments', 0, 0, '', 19, 0),
(68, 'edit_conversation_permissions', 'Edit Conversation Permissions', 0, 0, '', 19, 0),
(69, 'delete_conversation_message', 'Delete Message', 0, 0, '', 19, 0),
(70, 'edit_conversation_message', 'Edit Message', 0, 0, '', 19, 0),
(71, 'rate_conversation_message', 'Rate Message', 0, 0, '', 19, 0),
(72, 'flag_conversation_message', 'Flag Message', 0, 0, '', 19, 0),
(73, 'approve_conversation_message', 'Approve Message', 0, 0, '', 19, 0),
(74, 'edit_user_properties', 'Edit User Details', 0, 1, NULL, 4, 0),
(75, 'view_user_attributes', 'View User Attributes', 0, 1, NULL, 4, 0),
(76, 'activate_user', 'Activate/Deactivate User', 0, 0, NULL, 4, 0),
(77, 'sudo', 'Sign in as User', 0, 0, NULL, 4, 0),
(78, 'upgrade', 'Upgrade concrete5', 0, 0, NULL, 10, 0),
(79, 'access_group_search', 'Access Group Search', 0, 0, NULL, 4, 0),
(80, 'delete_user', 'Delete User', 0, 0, NULL, 4, 0),
(81, 'search_users_in_group', 'Search User Group', 0, 0, NULL, 16, 0),
(82, 'edit_group', 'Edit Group', 0, 0, NULL, 16, 0),
(83, 'assign_group', 'Assign Group', 0, 0, NULL, 16, 0),
(84, 'add_sub_group', 'Add Child Group', 0, 0, NULL, 16, 0),
(85, 'edit_group_permissions', 'Edit Group Permissions', 0, 0, NULL, 16, 0),
(86, 'access_page_type_permissions', 'Access Page Type Permissions', 0, 0, NULL, 10, 0),
(87, 'backup', 'Perform Backups', 0, 0, NULL, 10, 0),
(88, 'access_task_permissions', 'Access Task Permissions', 0, 0, NULL, 10, 0),
(89, 'access_sitemap', 'Access Sitemap', 0, 0, NULL, 11, 0),
(90, 'access_page_defaults', 'Access Page Type Defaults', 0, 0, NULL, 10, 0),
(91, 'customize_themes', 'Customize Themes', 0, 0, NULL, 10, 0),
(92, 'manage_layout_presets', 'Manage Layout Presets', 0, 0, NULL, 10, 0),
(93, 'empty_trash', 'Empty Trash', 0, 0, NULL, 10, 0),
(94, 'add_topic_tree', 'Add Topic Tree', 0, 0, NULL, 10, 0),
(95, 'remove_topic_tree', 'Remove Topic Tree', 0, 0, NULL, 10, 0),
(96, 'view_in_maintenance_mode', 'View Site in Maintenance Mode', 0, 0, 'Ability to see and use the website when concrete5 is in maintenance mode.', 10, 0),
(97, 'uninstall_packages', 'Uninstall Packages', 0, 0, NULL, 12, 0),
(98, 'install_packages', 'Install Packages', 0, 0, NULL, 12, 0),
(99, 'view_newsflow', 'View Newsflow', 0, 0, NULL, 12, 0),
(100, 'access_user_search_export', 'Export Site Users', 0, 0, 'Controls whether a user can export site users or not', 4, 0),
(101, 'access_user_search', 'Access User Search', 0, 0, 'Controls whether a user can view the search user interface.', 4, 0),
(102, 'edit_gatherings', 'Edit Gatherings', 0, 0, 'Can edit the footprint and items in all gatherings.', 10, 0),
(103, 'edit_gathering_items', 'Edit Gathering Items', 0, 0, '', 15, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `PileContents`
--

CREATE TABLE `PileContents` (
  `pcID` int(10) UNSIGNED NOT NULL,
  `pID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `itemID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `itemType` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `displayOrder` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Piles`
--

CREATE TABLE `Piles` (
  `pID` int(10) UNSIGNED NOT NULL,
  `uID` int(10) UNSIGNED DEFAULT NULL,
  `isDefault` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Piles`
--

INSERT INTO `Piles` (`pID`, `uID`, `isDefault`, `timestamp`, `name`, `state`) VALUES
(1, 1, 1, '2017-03-14 13:42:05', NULL, 'READY');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `QueueMessages`
--

CREATE TABLE `QueueMessages` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `queue_id` int(10) UNSIGNED NOT NULL,
  `handle` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `md5` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `timeout` decimal(14,0) DEFAULT NULL,
  `created` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `QueuePageDuplicationRelations`
--

CREATE TABLE `QueuePageDuplicationRelations` (
  `queue_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `originalCID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Queues`
--

CREATE TABLE `Queues` (
  `queue_id` int(10) UNSIGNED NOT NULL,
  `queue_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `timeout` int(10) UNSIGNED NOT NULL DEFAULT '30'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Sessions`
--

CREATE TABLE `Sessions` (
  `sessionID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sessionValue` text COLLATE utf8_unicode_ci NOT NULL,
  `sessionTime` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `SignupRequests`
--

CREATE TABLE `SignupRequests` (
  `id` int(11) NOT NULL,
  `ipFrom` tinyblob,
  `date_access` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `SocialLinks`
--

CREATE TABLE `SocialLinks` (
  `slID` int(10) UNSIGNED NOT NULL,
  `ssHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `SocialLinks`
--

INSERT INTO `SocialLinks` (`slID`, `ssHandle`, `url`) VALUES
(1, 'facebook', 'www.facebook.com'),
(2, 'instagram', 'www.instagram.com'),
(3, 'linkedin', 'www.linkedin.com');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Stacks`
--

CREATE TABLE `Stacks` (
  `stID` int(10) UNSIGNED NOT NULL,
  `stName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stType` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `stMultilingualSection` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Stacks`
--

INSERT INTO `Stacks` (`stID`, `stName`, `stType`, `cID`, `stMultilingualSection`) VALUES
(1, 'Header Site Title', 20, 146, 0),
(2, 'Header Navigation', 20, 147, 0),
(3, 'Footer Legal', 20, 148, 0),
(4, 'Footer Navigation', 20, 149, 0),
(5, 'Footer Contact', 20, 150, 0),
(6, 'Header Search', 20, 151, 0),
(7, 'Footer Site Title', 20, 152, 0),
(8, 'Footer Social', 20, 153, 0),
(9, 'Navigation', 20, 154, 0),
(10, 'Logo', 20, 155, 0),
(11, 'Social media', 20, 162, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `StyleCustomizerCustomCssRecords`
--

CREATE TABLE `StyleCustomizerCustomCssRecords` (
  `sccRecordID` int(10) UNSIGNED NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `StyleCustomizerInlineStylePresets`
--

CREATE TABLE `StyleCustomizerInlineStylePresets` (
  `pssPresetID` int(10) UNSIGNED NOT NULL,
  `pssPresetName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `issID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `StyleCustomizerInlineStyleSets`
--

CREATE TABLE `StyleCustomizerInlineStyleSets` (
  `issID` int(10) UNSIGNED NOT NULL,
  `backgroundColor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `backgroundImageFileID` int(11) DEFAULT NULL,
  `backgroundRepeat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `backgroundSize` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `backgroundPosition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borderWidth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borderColor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borderStyle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borderRadius` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `baseFontSize` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alignment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `textColor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkColor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paddingTop` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paddingBottom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paddingLeft` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paddingRight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marginTop` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marginBottom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marginLeft` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marginRight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rotate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boxShadowHorizontal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boxShadowVertical` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boxShadowBlur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boxShadowSpread` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boxShadowColor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customClass` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hideOnExtraSmallDevice` tinyint(1) DEFAULT NULL,
  `hideOnSmallDevice` tinyint(1) DEFAULT NULL,
  `hideOnMediumDevice` tinyint(1) DEFAULT NULL,
  `hideOnLargeDevice` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `StyleCustomizerValueLists`
--

CREATE TABLE `StyleCustomizerValueLists` (
  `scvlID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `StyleCustomizerValues`
--

CREATE TABLE `StyleCustomizerValues` (
  `scvID` int(10) UNSIGNED NOT NULL,
  `scvlID` int(10) UNSIGNED DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `SystemAntispamLibraries`
--

CREATE TABLE `SystemAntispamLibraries` (
  `saslHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `saslName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `saslIsActive` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `SystemCaptchaLibraries`
--

CREATE TABLE `SystemCaptchaLibraries` (
  `sclHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sclName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sclIsActive` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `SystemCaptchaLibraries`
--

INSERT INTO `SystemCaptchaLibraries` (`sclHandle`, `sclName`, `sclIsActive`, `pkgID`) VALUES
('securimage', 'SecurImage (Default)', 1, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `SystemContentEditorSnippets`
--

CREATE TABLE `SystemContentEditorSnippets` (
  `scsHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `scsName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scsIsActive` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `SystemContentEditorSnippets`
--

INSERT INTO `SystemContentEditorSnippets` (`scsHandle`, `scsName`, `scsIsActive`, `pkgID`) VALUES
('page_name', 'Page Name', 1, 0),
('user_name', 'User Name', 1, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `SystemDatabaseMigrations`
--

CREATE TABLE `SystemDatabaseMigrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `SystemDatabaseMigrations`
--

INSERT INTO `SystemDatabaseMigrations` (`version`) VALUES
('20160615000000');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `SystemDatabaseQueryLog`
--

CREATE TABLE `SystemDatabaseQueryLog` (
  `ID` int(10) UNSIGNED NOT NULL,
  `query` text COLLATE utf8_unicode_ci,
  `params` text COLLATE utf8_unicode_ci,
  `executionMS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `TopicTrees`
--

CREATE TABLE `TopicTrees` (
  `treeID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `topicTreeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `TreeCategoryNodes`
--

CREATE TABLE `TreeCategoryNodes` (
  `treeNodeID` int(10) UNSIGNED NOT NULL,
  `treeNodeCategoryName` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `TreeGroupNodes`
--

CREATE TABLE `TreeGroupNodes` (
  `treeNodeID` int(10) UNSIGNED NOT NULL,
  `gID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `TreeGroupNodes`
--

INSERT INTO `TreeGroupNodes` (`treeNodeID`, `gID`) VALUES
(2, 1),
(3, 2),
(4, 3);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `TreeNodePermissionAssignments`
--

CREATE TABLE `TreeNodePermissionAssignments` (
  `treeNodeID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pkID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `TreeNodePermissionAssignments`
--

INSERT INTO `TreeNodePermissionAssignments` (`treeNodeID`, `pkID`, `paID`) VALUES
(1, 81, 81),
(1, 82, 82),
(1, 83, 83),
(1, 84, 84),
(1, 85, 85);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `TreeNodes`
--

CREATE TABLE `TreeNodes` (
  `treeNodeID` int(10) UNSIGNED NOT NULL,
  `treeNodeTypeID` int(10) UNSIGNED DEFAULT '0',
  `treeID` int(10) UNSIGNED DEFAULT '0',
  `treeNodeParentID` int(10) UNSIGNED DEFAULT '0',
  `treeNodeDisplayOrder` int(10) UNSIGNED DEFAULT '0',
  `treeNodeOverridePermissions` tinyint(1) DEFAULT '0',
  `inheritPermissionsFromTreeNodeID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `TreeNodes`
--

INSERT INTO `TreeNodes` (`treeNodeID`, `treeNodeTypeID`, `treeID`, `treeNodeParentID`, `treeNodeDisplayOrder`, `treeNodeOverridePermissions`, `inheritPermissionsFromTreeNodeID`) VALUES
(1, 1, 1, 0, 0, 1, 1),
(2, 1, 1, 1, 0, 0, 1),
(3, 1, 1, 1, 1, 0, 1),
(4, 1, 1, 1, 2, 0, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `TreeNodeTypes`
--

CREATE TABLE `TreeNodeTypes` (
  `treeNodeTypeID` int(10) UNSIGNED NOT NULL,
  `treeNodeTypeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `pkgID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `TreeNodeTypes`
--

INSERT INTO `TreeNodeTypes` (`treeNodeTypeID`, `treeNodeTypeHandle`, `pkgID`) VALUES
(1, 'group', 0),
(2, 'topic_category', 0),
(3, 'topic', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Trees`
--

CREATE TABLE `Trees` (
  `treeID` int(10) UNSIGNED NOT NULL,
  `treeTypeID` int(10) UNSIGNED DEFAULT '0',
  `treeDateAdded` datetime DEFAULT NULL,
  `rootTreeNodeID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Trees`
--

INSERT INTO `Trees` (`treeID`, `treeTypeID`, `treeDateAdded`, `rootTreeNodeID`) VALUES
(1, 1, '2017-03-14 09:29:47', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `TreeTopicNodes`
--

CREATE TABLE `TreeTopicNodes` (
  `treeNodeID` int(10) UNSIGNED NOT NULL,
  `treeNodeTopicName` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `TreeTypes`
--

CREATE TABLE `TreeTypes` (
  `treeTypeID` int(10) UNSIGNED NOT NULL,
  `treeTypeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `pkgID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `TreeTypes`
--

INSERT INTO `TreeTypes` (`treeTypeID`, `treeTypeHandle`, `pkgID`) VALUES
(1, 'group', 0),
(2, 'topic', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserAttributeKeys`
--

CREATE TABLE `UserAttributeKeys` (
  `akID` int(10) UNSIGNED NOT NULL,
  `uakProfileDisplay` tinyint(1) NOT NULL DEFAULT '0',
  `uakMemberListDisplay` tinyint(1) NOT NULL DEFAULT '0',
  `uakProfileEdit` tinyint(1) NOT NULL DEFAULT '1',
  `uakProfileEditRequired` tinyint(1) NOT NULL DEFAULT '0',
  `uakRegisterEdit` tinyint(1) NOT NULL DEFAULT '0',
  `uakRegisterEditRequired` tinyint(1) NOT NULL DEFAULT '0',
  `displayOrder` int(10) UNSIGNED DEFAULT '0',
  `uakIsActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `UserAttributeKeys`
--

INSERT INTO `UserAttributeKeys` (`akID`, `uakProfileDisplay`, `uakMemberListDisplay`, `uakProfileEdit`, `uakProfileEditRequired`, `uakRegisterEdit`, `uakRegisterEditRequired`, `displayOrder`, `uakIsActive`) VALUES
(12, 0, 0, 1, 0, 1, 0, 1, 1),
(13, 0, 0, 1, 0, 1, 0, 2, 1),
(16, 0, 0, 0, 0, 0, 0, 3, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserAttributeValues`
--

CREATE TABLE `UserAttributeValues` (
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `akID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `avID` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserBannedIPs`
--

CREATE TABLE `UserBannedIPs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipFrom` tinyblob,
  `ipTo` tinyblob,
  `banCode` tinyint(1) NOT NULL DEFAULT '1',
  `expires` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `isManual` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserGroups`
--

CREATE TABLE `UserGroups` (
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ugEntered` datetime NOT NULL DEFAULT '1000-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserPermissionEditPropertyAccessList`
--

CREATE TABLE `UserPermissionEditPropertyAccessList` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uName` tinyint(1) DEFAULT '0',
  `uEmail` tinyint(1) DEFAULT '0',
  `uPassword` tinyint(1) DEFAULT '0',
  `uAvatar` tinyint(1) DEFAULT '0',
  `uTimezone` tinyint(1) DEFAULT '0',
  `uDefaultLanguage` tinyint(1) DEFAULT '0',
  `attributePermission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserPermissionEditPropertyAttributeAccessListCustom`
--

CREATE TABLE `UserPermissionEditPropertyAttributeAccessListCustom` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `akID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserPermissionViewAttributeAccessList`
--

CREATE TABLE `UserPermissionViewAttributeAccessList` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserPermissionViewAttributeAccessListCustom`
--

CREATE TABLE `UserPermissionViewAttributeAccessListCustom` (
  `paID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `peID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `akID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserPointActions`
--

CREATE TABLE `UserPointActions` (
  `upaID` int(11) NOT NULL,
  `upaHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upaDefaultPoints` int(11) NOT NULL DEFAULT '0',
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `upaHasCustomClass` tinyint(1) NOT NULL DEFAULT '0',
  `upaIsActive` tinyint(1) NOT NULL DEFAULT '1',
  `gBadgeID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `UserPointActions`
--

INSERT INTO `UserPointActions` (`upaID`, `upaHandle`, `upaName`, `upaDefaultPoints`, `pkgID`, `upaHasCustomClass`, `upaIsActive`, `gBadgeID`) VALUES
(1, 'won_badge', 'Won a Badge', 5, 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserPointHistory`
--

CREATE TABLE `UserPointHistory` (
  `upID` int(11) NOT NULL,
  `upuID` int(11) NOT NULL DEFAULT '0',
  `upaID` int(11) DEFAULT '0',
  `upPoints` int(11) DEFAULT '0',
  `object` longtext COLLATE utf8_unicode_ci,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserPrivateMessages`
--

CREATE TABLE `UserPrivateMessages` (
  `msgID` int(10) UNSIGNED NOT NULL,
  `uAuthorID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `msgDateCreated` datetime NOT NULL,
  `msgSubject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `msgBody` text COLLATE utf8_unicode_ci,
  `uToID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserPrivateMessagesTo`
--

CREATE TABLE `UserPrivateMessagesTo` (
  `msgID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uAuthorID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `msgMailboxID` int(11) NOT NULL,
  `msgIsNew` tinyint(1) NOT NULL DEFAULT '0',
  `msgIsUnread` tinyint(1) NOT NULL DEFAULT '0',
  `msgIsReplied` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Users`
--

CREATE TABLE `Users` (
  `uID` int(10) UNSIGNED NOT NULL,
  `uName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `uEmail` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `uPassword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uIsActive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `uIsValidated` tinyint(1) NOT NULL DEFAULT '-1',
  `uIsFullRecord` tinyint(1) NOT NULL DEFAULT '1',
  `uDateAdded` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `uLastPasswordChange` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `uHasAvatar` tinyint(1) NOT NULL DEFAULT '0',
  `uLastOnline` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uLastLogin` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uLastIP` tinyblob,
  `uPreviousLogin` int(10) UNSIGNED DEFAULT '0',
  `uNumLogins` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uLastAuthTypeID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uTimezone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uDefaultLanguage` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `Users`
--

INSERT INTO `Users` (`uID`, `uName`, `uEmail`, `uPassword`, `uIsActive`, `uIsValidated`, `uIsFullRecord`, `uDateAdded`, `uLastPasswordChange`, `uHasAvatar`, `uLastOnline`, `uLastLogin`, `uLastIP`, `uPreviousLogin`, `uNumLogins`, `uLastAuthTypeID`, `uTimezone`, `uDefaultLanguage`) VALUES
(1, 'admin', 'caitlin.koopman@forza-digital.com', '$2a$12$7os6mCmRAAGDAYYx1SEHGuRBOhm6ZBL.Zuiyl7oy5gx7u7JQJF78a', '1', -1, 1, '2017-03-14 09:29:47', '2017-03-14 09:29:47', 0, 1489661770, 1489657268, 0x3766303030303031, 1489650062, 10, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserSearchIndexAttributes`
--

CREATE TABLE `UserSearchIndexAttributes` (
  `uID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ak_profile_private_messages_enabled` tinyint(1) DEFAULT '0',
  `ak_profile_private_messages_notification_enabled` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `UserValidationHashes`
--

CREATE TABLE `UserValidationHashes` (
  `uvhID` int(10) UNSIGNED NOT NULL,
  `uID` int(10) UNSIGNED DEFAULT NULL,
  `uHash` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `uDateGenerated` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `uDateRedeemed` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `WorkflowProgress`
--

CREATE TABLE `WorkflowProgress` (
  `wpID` int(10) UNSIGNED NOT NULL,
  `wpCategoryID` int(10) UNSIGNED DEFAULT NULL,
  `wfID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `wpApproved` tinyint(1) NOT NULL DEFAULT '0',
  `wpDateAdded` datetime DEFAULT NULL,
  `wpDateLastAction` datetime DEFAULT NULL,
  `wpCurrentStatus` int(11) NOT NULL DEFAULT '0',
  `wrID` int(11) NOT NULL DEFAULT '0',
  `wpIsCompleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `WorkflowProgressCategories`
--

CREATE TABLE `WorkflowProgressCategories` (
  `wpCategoryID` int(10) UNSIGNED NOT NULL,
  `wpCategoryHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `WorkflowProgressCategories`
--

INSERT INTO `WorkflowProgressCategories` (`wpCategoryID`, `wpCategoryHandle`, `pkgID`) VALUES
(1, 'page', NULL),
(2, 'file', NULL),
(3, 'user', NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `WorkflowProgressHistory`
--

CREATE TABLE `WorkflowProgressHistory` (
  `wphID` int(10) UNSIGNED NOT NULL,
  `wpID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `object` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `WorkflowProgressHistory`
--

INSERT INTO `WorkflowProgressHistory` (`wphID`, `wpID`, `timestamp`, `object`) VALUES
(1, 1, '2017-03-14 13:42:45', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"2";s:4:"wrID";s:1:"1";}'),
(2, 2, '2017-03-14 13:44:57', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"3";s:4:"wrID";s:1:"2";}'),
(3, 3, '2017-03-14 13:45:56', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"4";s:4:"wrID";s:1:"3";}'),
(4, 4, '2017-03-14 13:48:25', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"5";s:4:"wrID";s:1:"4";}'),
(5, 5, '2017-03-14 13:56:16', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"6";s:4:"wrID";s:1:"5";}'),
(6, 6, '2017-03-14 13:58:19', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"7";s:4:"wrID";s:1:"6";}'),
(7, 7, '2017-03-14 14:08:47', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"8";s:4:"wrID";s:1:"7";}'),
(8, 8, '2017-03-14 14:12:07', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"9";s:4:"wrID";s:1:"8";}'),
(9, 9, '2017-03-14 14:21:46', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"10";s:4:"wrID";s:1:"9";}'),
(10, 10, '2017-03-14 16:21:37', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"11";s:4:"wrID";s:2:"10";}'),
(11, 11, '2017-03-14 16:22:08', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"12";s:4:"wrID";s:2:"11";}'),
(12, 12, '2017-03-15 08:10:47', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"13";s:4:"wrID";s:2:"12";}'),
(13, 13, '2017-03-15 08:11:37', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"14";s:4:"wrID";s:2:"13";}'),
(14, 14, '2017-03-15 08:11:55', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"15";s:4:"wrID";s:2:"14";}'),
(15, 15, '2017-03-15 08:13:30', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"16";s:4:"wrID";s:2:"15";}'),
(16, 16, '2017-03-15 08:14:28', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"17";s:4:"wrID";s:2:"16";}'),
(17, 17, '2017-03-15 08:14:47', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"18";s:4:"wrID";s:2:"17";}'),
(18, 18, '2017-03-15 08:15:46', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"19";s:4:"wrID";s:2:"18";}'),
(19, 19, '2017-03-15 08:57:18', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"20";s:4:"wrID";s:2:"19";}'),
(20, 20, '2017-03-15 09:03:27', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"21";s:4:"wrID";s:2:"20";}'),
(21, 21, '2017-03-15 09:10:41', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"22";s:4:"wrID";s:2:"21";}'),
(22, 22, '2017-03-15 09:24:28', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"23";s:4:"wrID";s:2:"22";}'),
(23, 23, '2017-03-15 09:32:41', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"24";s:4:"wrID";s:2:"23";}'),
(24, 24, '2017-03-15 09:36:22', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"25";s:4:"wrID";s:2:"24";}'),
(25, 25, '2017-03-15 09:38:04', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"26";s:4:"wrID";s:2:"25";}'),
(26, 26, '2017-03-15 09:43:23', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"27";s:4:"wrID";s:2:"26";}'),
(27, 27, '2017-03-15 10:27:35', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"28";s:4:"wrID";s:2:"27";}'),
(28, 28, '2017-03-15 10:59:07', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"29";s:4:"wrID";s:2:"28";}'),
(29, 29, '2017-03-15 11:05:10', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"30";s:4:"wrID";s:2:"29";}'),
(30, 30, '2017-03-15 11:09:51', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"31";s:4:"wrID";s:2:"30";}'),
(31, 31, '2017-03-15 11:12:56', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"32";s:4:"wrID";s:2:"31";}'),
(32, 32, '2017-03-15 11:15:07', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"33";s:4:"wrID";s:2:"32";}'),
(33, 33, '2017-03-15 11:15:52', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"34";s:4:"wrID";s:2:"33";}'),
(34, 34, '2017-03-15 11:17:18', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"35";s:4:"wrID";s:2:"34";}'),
(35, 35, '2017-03-15 11:19:34', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"36";s:4:"wrID";s:2:"35";}'),
(36, 36, '2017-03-15 11:22:12', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"37";s:4:"wrID";s:2:"36";}'),
(37, 37, '2017-03-15 11:27:05', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"38";s:4:"wrID";s:2:"37";}'),
(38, 38, '2017-03-15 11:30:27', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"39";s:4:"wrID";s:2:"38";}'),
(39, 39, '2017-03-15 11:31:21', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"40";s:4:"wrID";s:2:"39";}'),
(40, 40, '2017-03-15 11:33:15', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"41";s:4:"wrID";s:2:"40";}'),
(41, 41, '2017-03-15 11:46:25', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"42";s:4:"wrID";s:2:"41";}'),
(42, 42, '2017-03-15 11:47:06', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"43";s:4:"wrID";s:2:"42";}'),
(43, 43, '2017-03-15 11:49:01', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"44";s:4:"wrID";s:2:"43";}'),
(44, 44, '2017-03-15 11:49:20', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"45";s:4:"wrID";s:2:"44";}'),
(45, 45, '2017-03-15 12:55:04', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"46";s:4:"wrID";s:2:"45";}'),
(46, 46, '2017-03-15 13:39:55', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"47";s:4:"wrID";s:2:"46";}'),
(47, 47, '2017-03-15 13:41:54', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"48";s:4:"wrID";s:2:"47";}'),
(48, 48, '2017-03-15 13:51:50', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"49";s:4:"wrID";s:2:"48";}'),
(49, 49, '2017-03-15 13:55:25', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"50";s:4:"wrID";s:2:"49";}'),
(50, 50, '2017-03-15 14:16:20', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"51";s:4:"wrID";s:2:"50";}'),
(51, 51, '2017-03-15 14:17:25', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"52";s:4:"wrID";s:2:"51";}'),
(52, 52, '2017-03-15 14:18:56', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"53";s:4:"wrID";s:2:"52";}'),
(53, 53, '2017-03-15 14:36:23', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"54";s:4:"wrID";s:2:"53";}'),
(54, 54, '2017-03-15 14:36:46', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"55";s:4:"wrID";s:2:"54";}'),
(55, 55, '2017-03-15 14:37:50', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"56";s:4:"wrID";s:2:"55";}'),
(56, 56, '2017-03-15 14:43:08', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"57";s:4:"wrID";s:2:"56";}'),
(57, 57, '2017-03-15 14:46:21', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"58";s:4:"wrID";s:2:"57";}'),
(58, 58, '2017-03-15 14:51:30', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"59";s:4:"wrID";s:2:"58";}'),
(59, 59, '2017-03-15 15:57:53', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"60";s:4:"wrID";s:2:"59";}'),
(60, 60, '2017-03-16 08:02:54', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"61";s:4:"wrID";s:2:"60";}'),
(61, 61, '2017-03-16 08:24:19', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"62";s:4:"wrID";s:2:"61";}'),
(62, 62, '2017-03-16 08:29:46', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"63";s:4:"wrID";s:2:"62";}'),
(63, 63, '2017-03-16 08:41:41', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"64";s:4:"wrID";s:2:"63";}'),
(64, 64, '2017-03-16 08:42:54', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"65";s:4:"wrID";s:2:"64";}'),
(65, 65, '2017-03-16 08:46:37', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"66";s:4:"wrID";s:2:"65";}'),
(66, 66, '2017-03-16 08:57:47', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"67";s:4:"wrID";s:2:"66";}'),
(67, 67, '2017-03-16 08:58:10', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"68";s:4:"wrID";s:2:"67";}'),
(68, 68, '2017-03-16 08:59:42', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"69";s:4:"wrID";s:2:"68";}'),
(69, 69, '2017-03-16 09:08:42', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"70";s:4:"wrID";s:2:"69";}'),
(70, 70, '2017-03-16 09:22:36', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"71";s:4:"wrID";s:2:"70";}'),
(71, 71, '2017-03-16 09:32:55', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"72";s:4:"wrID";s:2:"71";}'),
(72, 72, '2017-03-16 10:57:30', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"73";s:4:"wrID";s:2:"72";}'),
(73, 73, '2017-03-16 10:58:00', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"74";s:4:"wrID";s:2:"73";}');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `WorkflowRequestObjects`
--

CREATE TABLE `WorkflowRequestObjects` (
  `wrID` int(10) UNSIGNED NOT NULL,
  `wrObject` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Workflows`
--

CREATE TABLE `Workflows` (
  `wfID` int(10) UNSIGNED NOT NULL,
  `wfName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wftID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `WorkflowTypes`
--

CREATE TABLE `WorkflowTypes` (
  `wftID` int(10) UNSIGNED NOT NULL,
  `wftHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `wftName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `WorkflowTypes`
--

INSERT INTO `WorkflowTypes` (`wftID`, `wftHandle`, `wftName`, `pkgID`) VALUES
(1, 'basic', 'Basic Workflow', 0);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `AreaLayoutColumns`
--
ALTER TABLE `AreaLayoutColumns`
  ADD PRIMARY KEY (`arLayoutColumnID`),
  ADD KEY `arLayoutID` (`arLayoutID`,`arLayoutColumnIndex`),
  ADD KEY `arID` (`arID`),
  ADD KEY `arLayoutColumnDisplayID` (`arLayoutColumnDisplayID`);

--
-- Indexen voor tabel `AreaLayoutCustomColumns`
--
ALTER TABLE `AreaLayoutCustomColumns`
  ADD PRIMARY KEY (`arLayoutColumnID`);

--
-- Indexen voor tabel `AreaLayoutPresets`
--
ALTER TABLE `AreaLayoutPresets`
  ADD PRIMARY KEY (`arLayoutPresetID`),
  ADD KEY `arLayoutID` (`arLayoutID`),
  ADD KEY `arLayoutPresetName` (`arLayoutPresetName`);

--
-- Indexen voor tabel `AreaLayouts`
--
ALTER TABLE `AreaLayouts`
  ADD PRIMARY KEY (`arLayoutID`);

--
-- Indexen voor tabel `AreaLayoutsUsingPresets`
--
ALTER TABLE `AreaLayoutsUsingPresets`
  ADD PRIMARY KEY (`arLayoutID`);

--
-- Indexen voor tabel `AreaLayoutThemeGridColumns`
--
ALTER TABLE `AreaLayoutThemeGridColumns`
  ADD PRIMARY KEY (`arLayoutColumnID`);

--
-- Indexen voor tabel `AreaPermissionAssignments`
--
ALTER TABLE `AreaPermissionAssignments`
  ADD PRIMARY KEY (`cID`,`arHandle`,`pkID`,`paID`),
  ADD KEY `paID` (`paID`),
  ADD KEY `pkID` (`pkID`);

--
-- Indexen voor tabel `AreaPermissionBlockTypeAccessList`
--
ALTER TABLE `AreaPermissionBlockTypeAccessList`
  ADD PRIMARY KEY (`paID`,`peID`),
  ADD KEY `peID` (`peID`);

--
-- Indexen voor tabel `AreaPermissionBlockTypeAccessListCustom`
--
ALTER TABLE `AreaPermissionBlockTypeAccessListCustom`
  ADD PRIMARY KEY (`paID`,`peID`,`btID`),
  ADD KEY `peID` (`peID`),
  ADD KEY `btID` (`btID`);

--
-- Indexen voor tabel `Areas`
--
ALTER TABLE `Areas`
  ADD PRIMARY KEY (`arID`),
  ADD KEY `arIsGlobal` (`arIsGlobal`),
  ADD KEY `cID` (`cID`),
  ADD KEY `arHandle` (`arHandle`),
  ADD KEY `arParentID` (`arParentID`);

--
-- Indexen voor tabel `atAddress`
--
ALTER TABLE `atAddress`
  ADD PRIMARY KEY (`avID`);

--
-- Indexen voor tabel `atAddressCustomCountries`
--
ALTER TABLE `atAddressCustomCountries`
  ADD PRIMARY KEY (`atAddressCustomCountryID`),
  ADD KEY `akID` (`akID`);

--
-- Indexen voor tabel `atAddressSettings`
--
ALTER TABLE `atAddressSettings`
  ADD PRIMARY KEY (`akID`);

--
-- Indexen voor tabel `atBoolean`
--
ALTER TABLE `atBoolean`
  ADD PRIMARY KEY (`avID`);

--
-- Indexen voor tabel `atBooleanSettings`
--
ALTER TABLE `atBooleanSettings`
  ADD PRIMARY KEY (`akID`);

--
-- Indexen voor tabel `atDateTime`
--
ALTER TABLE `atDateTime`
  ADD PRIMARY KEY (`avID`);

--
-- Indexen voor tabel `atDateTimeSettings`
--
ALTER TABLE `atDateTimeSettings`
  ADD PRIMARY KEY (`akID`);

--
-- Indexen voor tabel `atDefault`
--
ALTER TABLE `atDefault`
  ADD PRIMARY KEY (`avID`);

--
-- Indexen voor tabel `atFile`
--
ALTER TABLE `atFile`
  ADD PRIMARY KEY (`avID`),
  ADD KEY `fID` (`fID`);

--
-- Indexen voor tabel `atNumber`
--
ALTER TABLE `atNumber`
  ADD PRIMARY KEY (`avID`);

--
-- Indexen voor tabel `atSelectedTopics`
--
ALTER TABLE `atSelectedTopics`
  ADD PRIMARY KEY (`avID`,`TopicNodeID`),
  ADD KEY `TopicNodeID` (`TopicNodeID`);

--
-- Indexen voor tabel `atSelectOptions`
--
ALTER TABLE `atSelectOptions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `akID` (`akID`,`displayOrder`),
  ADD KEY `value` (`value`);

--
-- Indexen voor tabel `atSelectOptionsSelected`
--
ALTER TABLE `atSelectOptionsSelected`
  ADD PRIMARY KEY (`avID`,`atSelectOptionID`),
  ADD KEY `atSelectOptionID` (`atSelectOptionID`);

--
-- Indexen voor tabel `atSelectSettings`
--
ALTER TABLE `atSelectSettings`
  ADD PRIMARY KEY (`akID`);

--
-- Indexen voor tabel `atSocialLinks`
--
ALTER TABLE `atSocialLinks`
  ADD PRIMARY KEY (`avsID`),
  ADD KEY `avID` (`avID`,`avsID`);

--
-- Indexen voor tabel `atTextareaSettings`
--
ALTER TABLE `atTextareaSettings`
  ADD PRIMARY KEY (`akID`);

--
-- Indexen voor tabel `atTopicSettings`
--
ALTER TABLE `atTopicSettings`
  ADD PRIMARY KEY (`akID`),
  ADD KEY `akTopicTreeID` (`akTopicTreeID`);

--
-- Indexen voor tabel `AttributeKeyCategories`
--
ALTER TABLE `AttributeKeyCategories`
  ADD PRIMARY KEY (`akCategoryID`),
  ADD KEY `akCategoryHandle` (`akCategoryHandle`),
  ADD KEY `pkgID` (`pkgID`,`akCategoryID`);

--
-- Indexen voor tabel `AttributeKeys`
--
ALTER TABLE `AttributeKeys`
  ADD PRIMARY KEY (`akID`),
  ADD UNIQUE KEY `akHandle` (`akHandle`,`akCategoryID`),
  ADD KEY `akCategoryID` (`akCategoryID`),
  ADD KEY `atID` (`atID`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `AttributeSetKeys`
--
ALTER TABLE `AttributeSetKeys`
  ADD PRIMARY KEY (`akID`,`asID`),
  ADD KEY `asID` (`asID`,`displayOrder`);

--
-- Indexen voor tabel `AttributeSets`
--
ALTER TABLE `AttributeSets`
  ADD PRIMARY KEY (`asID`),
  ADD KEY `asHandle` (`asHandle`),
  ADD KEY `akCategoryID` (`akCategoryID`,`asDisplayOrder`),
  ADD KEY `pkgID` (`pkgID`,`asID`);

--
-- Indexen voor tabel `AttributeTypeCategories`
--
ALTER TABLE `AttributeTypeCategories`
  ADD PRIMARY KEY (`atID`,`akCategoryID`),
  ADD KEY `akCategoryID` (`akCategoryID`);

--
-- Indexen voor tabel `AttributeTypes`
--
ALTER TABLE `AttributeTypes`
  ADD PRIMARY KEY (`atID`),
  ADD UNIQUE KEY `atHandle` (`atHandle`),
  ADD KEY `pkgID` (`pkgID`,`atID`);

--
-- Indexen voor tabel `AttributeValues`
--
ALTER TABLE `AttributeValues`
  ADD PRIMARY KEY (`avID`),
  ADD KEY `akID` (`akID`),
  ADD KEY `uID` (`uID`),
  ADD KEY `atID` (`atID`);

--
-- Indexen voor tabel `AuthenticationTypes`
--
ALTER TABLE `AuthenticationTypes`
  ADD PRIMARY KEY (`authTypeID`),
  ADD UNIQUE KEY `authTypeHandle` (`authTypeHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `authTypeConcreteCookieMap`
--
ALTER TABLE `authTypeConcreteCookieMap`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `token` (`token`),
  ADD KEY `uID` (`uID`);

--
-- Indexen voor tabel `BannedWords`
--
ALTER TABLE `BannedWords`
  ADD PRIMARY KEY (`bwID`);

--
-- Indexen voor tabel `BasicWorkflowPermissionAssignments`
--
ALTER TABLE `BasicWorkflowPermissionAssignments`
  ADD PRIMARY KEY (`wfID`,`pkID`,`paID`),
  ADD KEY `pkID` (`pkID`),
  ADD KEY `paID` (`paID`);

--
-- Indexen voor tabel `BasicWorkflowProgressData`
--
ALTER TABLE `BasicWorkflowProgressData`
  ADD PRIMARY KEY (`wpID`),
  ADD KEY `uIDStarted` (`uIDStarted`),
  ADD KEY `uIDCompleted` (`uIDCompleted`);

--
-- Indexen voor tabel `BlockFeatureAssignments`
--
ALTER TABLE `BlockFeatureAssignments`
  ADD PRIMARY KEY (`cID`,`cvID`,`bID`,`faID`),
  ADD KEY `faID` (`faID`,`cID`,`cvID`),
  ADD KEY `bID` (`bID`);

--
-- Indexen voor tabel `BlockPermissionAssignments`
--
ALTER TABLE `BlockPermissionAssignments`
  ADD PRIMARY KEY (`cID`,`cvID`,`bID`,`pkID`,`paID`),
  ADD KEY `bID` (`bID`),
  ADD KEY `pkID` (`pkID`),
  ADD KEY `paID` (`paID`);

--
-- Indexen voor tabel `BlockRelations`
--
ALTER TABLE `BlockRelations`
  ADD PRIMARY KEY (`brID`),
  ADD KEY `bID` (`bID`),
  ADD KEY `originalBID` (`originalBID`);

--
-- Indexen voor tabel `Blocks`
--
ALTER TABLE `Blocks`
  ADD PRIMARY KEY (`bID`),
  ADD KEY `btID` (`btID`),
  ADD KEY `uID` (`uID`);

--
-- Indexen voor tabel `BlockTypePermissionBlockTypeAccessList`
--
ALTER TABLE `BlockTypePermissionBlockTypeAccessList`
  ADD PRIMARY KEY (`paID`,`peID`),
  ADD KEY `peID` (`peID`);

--
-- Indexen voor tabel `BlockTypePermissionBlockTypeAccessListCustom`
--
ALTER TABLE `BlockTypePermissionBlockTypeAccessListCustom`
  ADD PRIMARY KEY (`paID`,`peID`,`btID`),
  ADD KEY `peID` (`peID`),
  ADD KEY `btID` (`btID`);

--
-- Indexen voor tabel `BlockTypes`
--
ALTER TABLE `BlockTypes`
  ADD PRIMARY KEY (`btID`),
  ADD UNIQUE KEY `btHandle` (`btHandle`),
  ADD KEY `btDisplayOrder` (`btDisplayOrder`,`btName`,`btID`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `BlockTypeSetBlockTypes`
--
ALTER TABLE `BlockTypeSetBlockTypes`
  ADD PRIMARY KEY (`btID`,`btsID`),
  ADD KEY `btsID` (`btsID`,`displayOrder`);

--
-- Indexen voor tabel `BlockTypeSets`
--
ALTER TABLE `BlockTypeSets`
  ADD PRIMARY KEY (`btsID`),
  ADD UNIQUE KEY `btsHandle` (`btsHandle`),
  ADD KEY `btsDisplayOrder` (`btsDisplayOrder`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `btContentFile`
--
ALTER TABLE `btContentFile`
  ADD PRIMARY KEY (`bID`),
  ADD KEY `fID` (`fID`);

--
-- Indexen voor tabel `btContentImage`
--
ALTER TABLE `btContentImage`
  ADD PRIMARY KEY (`bID`),
  ADD KEY `fID` (`fID`);

--
-- Indexen voor tabel `btContentLocal`
--
ALTER TABLE `btContentLocal`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btCoreAreaLayout`
--
ALTER TABLE `btCoreAreaLayout`
  ADD PRIMARY KEY (`bID`),
  ADD KEY `arLayoutID` (`arLayoutID`);

--
-- Indexen voor tabel `btCoreConversation`
--
ALTER TABLE `btCoreConversation`
  ADD PRIMARY KEY (`bID`),
  ADD KEY `cnvID` (`cnvID`);

--
-- Indexen voor tabel `btCorePageTypeComposerControlOutput`
--
ALTER TABLE `btCorePageTypeComposerControlOutput`
  ADD PRIMARY KEY (`bID`),
  ADD KEY `ptComposerOutputControlID` (`ptComposerOutputControlID`);

--
-- Indexen voor tabel `btCoreScrapbookDisplay`
--
ALTER TABLE `btCoreScrapbookDisplay`
  ADD PRIMARY KEY (`bID`),
  ADD KEY `bOriginalID` (`bOriginalID`);

--
-- Indexen voor tabel `btCoreStackDisplay`
--
ALTER TABLE `btCoreStackDisplay`
  ADD PRIMARY KEY (`bID`),
  ADD KEY `stID` (`stID`);

--
-- Indexen voor tabel `btDashboardNewsflowLatest`
--
ALTER TABLE `btDashboardNewsflowLatest`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btDateNavigation`
--
ALTER TABLE `btDateNavigation`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btExternalForm`
--
ALTER TABLE `btExternalForm`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btFaq`
--
ALTER TABLE `btFaq`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btFaqEntries`
--
ALTER TABLE `btFaqEntries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bID` (`bID`,`sortOrder`);

--
-- Indexen voor tabel `btFeature`
--
ALTER TABLE `btFeature`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btForm`
--
ALTER TABLE `btForm`
  ADD PRIMARY KEY (`bID`),
  ADD KEY `questionSetIdForeign` (`questionSetId`);

--
-- Indexen voor tabel `btFormAnswers`
--
ALTER TABLE `btFormAnswers`
  ADD PRIMARY KEY (`aID`),
  ADD KEY `asID` (`asID`),
  ADD KEY `msqID` (`msqID`);

--
-- Indexen voor tabel `btFormAnswerSet`
--
ALTER TABLE `btFormAnswerSet`
  ADD PRIMARY KEY (`asID`),
  ADD KEY `questionSetId` (`questionSetId`),
  ADD KEY `uID` (`uID`);

--
-- Indexen voor tabel `btFormQuestions`
--
ALTER TABLE `btFormQuestions`
  ADD PRIMARY KEY (`qID`),
  ADD KEY `questionSetId` (`questionSetId`),
  ADD KEY `msqID` (`msqID`),
  ADD KEY `bID` (`bID`,`questionSetId`);

--
-- Indexen voor tabel `btGoogleMap`
--
ALTER TABLE `btGoogleMap`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btImageSlider`
--
ALTER TABLE `btImageSlider`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btImageSliderEntries`
--
ALTER TABLE `btImageSliderEntries`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `btNavigation`
--
ALTER TABLE `btNavigation`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btNextPrevious`
--
ALTER TABLE `btNextPrevious`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btPageAttributeDisplay`
--
ALTER TABLE `btPageAttributeDisplay`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btPageList`
--
ALTER TABLE `btPageList`
  ADD PRIMARY KEY (`bID`),
  ADD KEY `ptID` (`ptID`);

--
-- Indexen voor tabel `btPageTitle`
--
ALTER TABLE `btPageTitle`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btRssDisplay`
--
ALTER TABLE `btRssDisplay`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btSearch`
--
ALTER TABLE `btSearch`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btShareThisPage`
--
ALTER TABLE `btShareThisPage`
  ADD PRIMARY KEY (`btShareThisPageID`);

--
-- Indexen voor tabel `btSocialLinks`
--
ALTER TABLE `btSocialLinks`
  ADD PRIMARY KEY (`btSocialLinkID`),
  ADD KEY `bID` (`bID`,`displayOrder`),
  ADD KEY `slID` (`slID`);

--
-- Indexen voor tabel `btSurvey`
--
ALTER TABLE `btSurvey`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btSurveyOptions`
--
ALTER TABLE `btSurveyOptions`
  ADD PRIMARY KEY (`optionID`),
  ADD KEY `bID` (`bID`,`displayOrder`);

--
-- Indexen voor tabel `btSurveyResults`
--
ALTER TABLE `btSurveyResults`
  ADD PRIMARY KEY (`resultID`),
  ADD KEY `optionID` (`optionID`),
  ADD KEY `cID` (`cID`,`optionID`,`bID`),
  ADD KEY `bID` (`bID`,`cID`,`uID`),
  ADD KEY `uID` (`uID`);

--
-- Indexen voor tabel `btSwitchLanguage`
--
ALTER TABLE `btSwitchLanguage`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btTags`
--
ALTER TABLE `btTags`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btTestimonial`
--
ALTER TABLE `btTestimonial`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btTopicList`
--
ALTER TABLE `btTopicList`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btVideo`
--
ALTER TABLE `btVideo`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `btYouTube`
--
ALTER TABLE `btYouTube`
  ADD PRIMARY KEY (`bID`);

--
-- Indexen voor tabel `CollectionAttributeValues`
--
ALTER TABLE `CollectionAttributeValues`
  ADD PRIMARY KEY (`cID`,`cvID`,`akID`),
  ADD KEY `akID` (`akID`),
  ADD KEY `avID` (`avID`);

--
-- Indexen voor tabel `Collections`
--
ALTER TABLE `Collections`
  ADD PRIMARY KEY (`cID`),
  ADD KEY `cIDDateModified` (`cID`,`cDateModified`),
  ADD KEY `cDateModified` (`cDateModified`),
  ADD KEY `cDateAdded` (`cDateAdded`);

--
-- Indexen voor tabel `CollectionSearchIndexAttributes`
--
ALTER TABLE `CollectionSearchIndexAttributes`
  ADD PRIMARY KEY (`cID`);

--
-- Indexen voor tabel `CollectionVersionAreaStyles`
--
ALTER TABLE `CollectionVersionAreaStyles`
  ADD PRIMARY KEY (`cID`,`cvID`,`arHandle`),
  ADD KEY `issID` (`issID`);

--
-- Indexen voor tabel `CollectionVersionBlocks`
--
ALTER TABLE `CollectionVersionBlocks`
  ADD PRIMARY KEY (`cID`,`cvID`,`bID`,`arHandle`),
  ADD KEY `bID` (`bID`,`cID`);

--
-- Indexen voor tabel `CollectionVersionBlocksCacheSettings`
--
ALTER TABLE `CollectionVersionBlocksCacheSettings`
  ADD PRIMARY KEY (`cID`,`cvID`,`bID`,`arHandle`),
  ADD KEY `bID` (`bID`,`cID`,`cvID`,`arHandle`);

--
-- Indexen voor tabel `CollectionVersionBlocksOutputCache`
--
ALTER TABLE `CollectionVersionBlocksOutputCache`
  ADD PRIMARY KEY (`cID`,`cvID`,`bID`,`arHandle`),
  ADD KEY `bID` (`bID`,`cID`,`cvID`,`arHandle`);

--
-- Indexen voor tabel `CollectionVersionBlockStyles`
--
ALTER TABLE `CollectionVersionBlockStyles`
  ADD PRIMARY KEY (`cID`,`cvID`,`bID`,`arHandle`),
  ADD KEY `bID` (`bID`,`issID`),
  ADD KEY `issID` (`issID`);

--
-- Indexen voor tabel `CollectionVersionFeatureAssignments`
--
ALTER TABLE `CollectionVersionFeatureAssignments`
  ADD PRIMARY KEY (`cID`,`cvID`,`faID`),
  ADD KEY `faID` (`faID`);

--
-- Indexen voor tabel `CollectionVersionRelatedEdits`
--
ALTER TABLE `CollectionVersionRelatedEdits`
  ADD PRIMARY KEY (`cID`,`cvID`,`cRelationID`,`cvRelationID`);

--
-- Indexen voor tabel `CollectionVersions`
--
ALTER TABLE `CollectionVersions`
  ADD PRIMARY KEY (`cID`,`cvID`),
  ADD KEY `cvIsApproved` (`cvIsApproved`),
  ADD KEY `cvAuthorUID` (`cvAuthorUID`),
  ADD KEY `cvApproverUID` (`cvApproverUID`),
  ADD KEY `pThemeID` (`pThemeID`),
  ADD KEY `pTemplateID` (`pTemplateID`);

--
-- Indexen voor tabel `CollectionVersionThemeCustomStyles`
--
ALTER TABLE `CollectionVersionThemeCustomStyles`
  ADD PRIMARY KEY (`cID`,`cvID`,`pThemeID`),
  ADD KEY `pThemeID` (`pThemeID`),
  ADD KEY `scvlID` (`scvlID`),
  ADD KEY `sccRecordID` (`sccRecordID`);

--
-- Indexen voor tabel `Config`
--
ALTER TABLE `Config`
  ADD PRIMARY KEY (`configNamespace`,`configGroup`,`configItem`),
  ADD KEY `configGroup` (`configGroup`);

--
-- Indexen voor tabel `ConfigStore`
--
ALTER TABLE `ConfigStore`
  ADD PRIMARY KEY (`cfKey`,`uID`),
  ADD KEY `uID` (`uID`,`cfKey`),
  ADD KEY `pkgID` (`pkgID`,`cfKey`);

--
-- Indexen voor tabel `ConversationDiscussions`
--
ALTER TABLE `ConversationDiscussions`
  ADD PRIMARY KEY (`cnvDiscussionID`),
  ADD KEY `cID` (`cID`);

--
-- Indexen voor tabel `ConversationEditors`
--
ALTER TABLE `ConversationEditors`
  ADD PRIMARY KEY (`cnvEditorID`),
  ADD KEY `pkgID` (`pkgID`,`cnvEditorHandle`);

--
-- Indexen voor tabel `ConversationFeatureDetailAssignments`
--
ALTER TABLE `ConversationFeatureDetailAssignments`
  ADD PRIMARY KEY (`faID`),
  ADD KEY `cnvID` (`cnvID`);

--
-- Indexen voor tabel `ConversationFlaggedMessages`
--
ALTER TABLE `ConversationFlaggedMessages`
  ADD PRIMARY KEY (`cnvMessageID`),
  ADD KEY `cnvMessageFlagTypeID` (`cnvMessageFlagTypeID`);

--
-- Indexen voor tabel `ConversationFlaggedMessageTypes`
--
ALTER TABLE `ConversationFlaggedMessageTypes`
  ADD PRIMARY KEY (`cnvMessageFlagTypeID`),
  ADD UNIQUE KEY `cnvMessageFlagTypeHandle` (`cnvMessageFlagTypeHandle`);

--
-- Indexen voor tabel `ConversationMessageAttachments`
--
ALTER TABLE `ConversationMessageAttachments`
  ADD PRIMARY KEY (`cnvMessageAttachmentID`),
  ADD KEY `cnvMessageID` (`cnvMessageID`),
  ADD KEY `fID` (`fID`);

--
-- Indexen voor tabel `ConversationMessageRatings`
--
ALTER TABLE `ConversationMessageRatings`
  ADD PRIMARY KEY (`cnvMessageRatingID`),
  ADD KEY `cnvMessageID` (`cnvMessageID`,`cnvRatingTypeID`),
  ADD KEY `cnvRatingTypeID` (`cnvRatingTypeID`),
  ADD KEY `uID` (`uID`);

--
-- Indexen voor tabel `ConversationMessages`
--
ALTER TABLE `ConversationMessages`
  ADD PRIMARY KEY (`cnvMessageID`),
  ADD KEY `cnvID` (`cnvID`),
  ADD KEY `cnvMessageParentID` (`cnvMessageParentID`),
  ADD KEY `uID` (`uID`);

--
-- Indexen voor tabel `ConversationPermissionAddMessageAccessList`
--
ALTER TABLE `ConversationPermissionAddMessageAccessList`
  ADD PRIMARY KEY (`paID`,`peID`),
  ADD KEY `peID` (`peID`);

--
-- Indexen voor tabel `ConversationPermissionAssignments`
--
ALTER TABLE `ConversationPermissionAssignments`
  ADD PRIMARY KEY (`cnvID`,`pkID`,`paID`),
  ADD KEY `paID` (`paID`),
  ADD KEY `pkID` (`pkID`);

--
-- Indexen voor tabel `ConversationRatingTypes`
--
ALTER TABLE `ConversationRatingTypes`
  ADD PRIMARY KEY (`cnvRatingTypeID`),
  ADD UNIQUE KEY `cnvRatingTypeHandle` (`cnvRatingTypeHandle`),
  ADD KEY `pkgID` (`pkgID`,`cnvRatingTypeHandle`);

--
-- Indexen voor tabel `Conversations`
--
ALTER TABLE `Conversations`
  ADD PRIMARY KEY (`cnvID`),
  ADD KEY `cID` (`cID`),
  ADD KEY `cnvParentMessageID` (`cnvParentMessageID`);

--
-- Indexen voor tabel `ConversationSubscriptions`
--
ALTER TABLE `ConversationSubscriptions`
  ADD PRIMARY KEY (`cnvID`,`uID`),
  ADD KEY `cnvID` (`cnvID`),
  ADD KEY `uID` (`uID`);

--
-- Indexen voor tabel `DownloadStatistics`
--
ALTER TABLE `DownloadStatistics`
  ADD PRIMARY KEY (`dsID`),
  ADD KEY `fID` (`fID`,`timestamp`),
  ADD KEY `fvID` (`fID`,`fvID`),
  ADD KEY `uID` (`uID`),
  ADD KEY `rcID` (`rcID`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexen voor tabel `FeatureAssignments`
--
ALTER TABLE `FeatureAssignments`
  ADD PRIMARY KEY (`faID`),
  ADD KEY `feID` (`feID`),
  ADD KEY `fcID` (`fcID`);

--
-- Indexen voor tabel `FeatureCategories`
--
ALTER TABLE `FeatureCategories`
  ADD PRIMARY KEY (`fcID`),
  ADD UNIQUE KEY `fcHandle` (`fcHandle`),
  ADD KEY `pkgID` (`pkgID`,`fcID`);

--
-- Indexen voor tabel `Features`
--
ALTER TABLE `Features`
  ADD PRIMARY KEY (`feID`),
  ADD UNIQUE KEY `feHandle` (`feHandle`),
  ADD KEY `pkgID` (`pkgID`,`feID`);

--
-- Indexen voor tabel `FileAttributeValues`
--
ALTER TABLE `FileAttributeValues`
  ADD PRIMARY KEY (`fID`,`fvID`,`akID`),
  ADD KEY `akID` (`akID`),
  ADD KEY `avID` (`avID`);

--
-- Indexen voor tabel `FileImageThumbnailPaths`
--
ALTER TABLE `FileImageThumbnailPaths`
  ADD UNIQUE KEY `thumbnailPathID` (`fileID`,`fileVersionID`,`thumbnailTypeHandle`,`storageLocationID`);

--
-- Indexen voor tabel `FileImageThumbnailTypes`
--
ALTER TABLE `FileImageThumbnailTypes`
  ADD PRIMARY KEY (`ftTypeID`);

--
-- Indexen voor tabel `FilePermissionAssignments`
--
ALTER TABLE `FilePermissionAssignments`
  ADD PRIMARY KEY (`fID`,`pkID`,`paID`),
  ADD KEY `pkID` (`pkID`),
  ADD KEY `paID` (`paID`);

--
-- Indexen voor tabel `FilePermissionFileTypes`
--
ALTER TABLE `FilePermissionFileTypes`
  ADD PRIMARY KEY (`fsID`,`gID`,`uID`,`extension`),
  ADD KEY `gID` (`gID`),
  ADD KEY `uID` (`uID`);

--
-- Indexen voor tabel `Files`
--
ALTER TABLE `Files`
  ADD PRIMARY KEY (`fID`,`uID`,`fslID`),
  ADD KEY `uID` (`uID`),
  ADD KEY `fslID` (`fslID`),
  ADD KEY `ocID` (`ocID`),
  ADD KEY `fOverrideSetPermissions` (`fOverrideSetPermissions`);

--
-- Indexen voor tabel `FileSearchIndexAttributes`
--
ALTER TABLE `FileSearchIndexAttributes`
  ADD PRIMARY KEY (`fID`);

--
-- Indexen voor tabel `FileSetFiles`
--
ALTER TABLE `FileSetFiles`
  ADD PRIMARY KEY (`fsfID`),
  ADD KEY `fID` (`fID`),
  ADD KEY `fsID` (`fsID`);

--
-- Indexen voor tabel `FileSetPermissionAssignments`
--
ALTER TABLE `FileSetPermissionAssignments`
  ADD PRIMARY KEY (`fsID`,`pkID`,`paID`),
  ADD KEY `paID` (`paID`),
  ADD KEY `pkID` (`pkID`);

--
-- Indexen voor tabel `FileSetPermissionFileTypeAccessList`
--
ALTER TABLE `FileSetPermissionFileTypeAccessList`
  ADD PRIMARY KEY (`paID`,`peID`),
  ADD KEY `peID` (`peID`);

--
-- Indexen voor tabel `FileSetPermissionFileTypeAccessListCustom`
--
ALTER TABLE `FileSetPermissionFileTypeAccessListCustom`
  ADD PRIMARY KEY (`paID`,`peID`,`extension`),
  ADD KEY `peID` (`peID`);

--
-- Indexen voor tabel `FileSets`
--
ALTER TABLE `FileSets`
  ADD PRIMARY KEY (`fsID`),
  ADD KEY `uID` (`uID`,`fsType`,`fsName`),
  ADD KEY `fsName` (`fsName`),
  ADD KEY `fsType` (`fsType`);

--
-- Indexen voor tabel `FileSetSavedSearches`
--
ALTER TABLE `FileSetSavedSearches`
  ADD PRIMARY KEY (`fsID`);

--
-- Indexen voor tabel `FileStorageLocations`
--
ALTER TABLE `FileStorageLocations`
  ADD PRIMARY KEY (`fslID`);

--
-- Indexen voor tabel `FileStorageLocationTypes`
--
ALTER TABLE `FileStorageLocationTypes`
  ADD PRIMARY KEY (`fslTypeID`),
  ADD UNIQUE KEY `fslTypeHandle` (`fslTypeHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `FileVersionLog`
--
ALTER TABLE `FileVersionLog`
  ADD PRIMARY KEY (`fvlID`),
  ADD KEY `fvID` (`fID`,`fvID`,`fvlID`);

--
-- Indexen voor tabel `FileVersions`
--
ALTER TABLE `FileVersions`
  ADD PRIMARY KEY (`fID`,`fvID`),
  ADD KEY `fvExtension` (`fvExtension`),
  ADD KEY `fvType` (`fvType`),
  ADD KEY `fvFilename` (`fvFilename`);

--
-- Indexen voor tabel `gaPage`
--
ALTER TABLE `gaPage`
  ADD PRIMARY KEY (`gaiID`),
  ADD KEY `cID` (`cID`);

--
-- Indexen voor tabel `GatheringConfiguredDataSources`
--
ALTER TABLE `GatheringConfiguredDataSources`
  ADD PRIMARY KEY (`gcsID`),
  ADD KEY `gaID` (`gaID`),
  ADD KEY `gasID` (`gasID`);

--
-- Indexen voor tabel `GatheringDataSources`
--
ALTER TABLE `GatheringDataSources`
  ADD PRIMARY KEY (`gasID`),
  ADD UNIQUE KEY `gasHandle` (`gasHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `GatheringItemFeatureAssignments`
--
ALTER TABLE `GatheringItemFeatureAssignments`
  ADD PRIMARY KEY (`gafaID`),
  ADD KEY `gaiID` (`gaiID`,`faID`),
  ADD KEY `faID` (`faID`);

--
-- Indexen voor tabel `GatheringItems`
--
ALTER TABLE `GatheringItems`
  ADD PRIMARY KEY (`gaiID`),
  ADD UNIQUE KEY `gaiUniqueKey` (`gaiKey`,`gasID`,`gaID`),
  ADD KEY `gaID` (`gaID`,`gaiBatchTimestamp`),
  ADD KEY `gasID` (`gasID`);

--
-- Indexen voor tabel `GatheringItemSelectedTemplates`
--
ALTER TABLE `GatheringItemSelectedTemplates`
  ADD PRIMARY KEY (`gaiID`,`gatID`),
  ADD UNIQUE KEY `gatUniqueKey` (`gaiID`,`gatTypeID`),
  ADD KEY `gatTypeID` (`gatTypeID`),
  ADD KEY `gatID` (`gatID`);

--
-- Indexen voor tabel `GatheringItemTemplateFeatures`
--
ALTER TABLE `GatheringItemTemplateFeatures`
  ADD PRIMARY KEY (`gfeID`),
  ADD KEY `gatID` (`gatID`),
  ADD KEY `feID` (`feID`,`gatID`);

--
-- Indexen voor tabel `GatheringItemTemplates`
--
ALTER TABLE `GatheringItemTemplates`
  ADD PRIMARY KEY (`gatID`),
  ADD UNIQUE KEY `gatHandle` (`gatHandle`,`gatTypeID`),
  ADD KEY `gatTypeID` (`gatTypeID`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `GatheringItemTemplateTypes`
--
ALTER TABLE `GatheringItemTemplateTypes`
  ADD PRIMARY KEY (`gatTypeID`),
  ADD UNIQUE KEY `gatTypeHandle` (`gatTypeHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `GatheringPermissionAssignments`
--
ALTER TABLE `GatheringPermissionAssignments`
  ADD PRIMARY KEY (`gaID`,`pkID`,`paID`),
  ADD KEY `pkID` (`pkID`),
  ADD KEY `paID` (`paID`);

--
-- Indexen voor tabel `Gatherings`
--
ALTER TABLE `Gatherings`
  ADD PRIMARY KEY (`gaID`),
  ADD KEY `gaDateLastUpdated` (`gaDateLastUpdated`);

--
-- Indexen voor tabel `Groups`
--
ALTER TABLE `Groups`
  ADD PRIMARY KEY (`gID`),
  ADD KEY `gName` (`gName`),
  ADD KEY `gBadgeFID` (`gBadgeFID`),
  ADD KEY `pkgID` (`pkgID`),
  ADD KEY `gPath` (`gPath`(255));

--
-- Indexen voor tabel `GroupSetGroups`
--
ALTER TABLE `GroupSetGroups`
  ADD PRIMARY KEY (`gID`,`gsID`),
  ADD KEY `gsID` (`gsID`);

--
-- Indexen voor tabel `GroupSets`
--
ALTER TABLE `GroupSets`
  ADD PRIMARY KEY (`gsID`),
  ADD KEY `gsName` (`gsName`),
  ADD KEY `pkgID` (`pkgID`,`gsID`);

--
-- Indexen voor tabel `Jobs`
--
ALTER TABLE `Jobs`
  ADD PRIMARY KEY (`jID`),
  ADD UNIQUE KEY `jHandle` (`jHandle`),
  ADD KEY `pkgID` (`pkgID`),
  ADD KEY `isScheduled` (`isScheduled`,`jDateLastRun`,`jID`),
  ADD KEY `jDateLastRun` (`jDateLastRun`,`jID`);

--
-- Indexen voor tabel `JobSetJobs`
--
ALTER TABLE `JobSetJobs`
  ADD PRIMARY KEY (`jsID`,`jID`),
  ADD KEY `jID` (`jID`);

--
-- Indexen voor tabel `JobSets`
--
ALTER TABLE `JobSets`
  ADD PRIMARY KEY (`jsID`),
  ADD KEY `pkgID` (`pkgID`),
  ADD KEY `jsName` (`jsName`);

--
-- Indexen voor tabel `JobsLog`
--
ALTER TABLE `JobsLog`
  ADD PRIMARY KEY (`jlID`),
  ADD KEY `jID` (`jID`);

--
-- Indexen voor tabel `Logs`
--
ALTER TABLE `Logs`
  ADD PRIMARY KEY (`logID`),
  ADD KEY `channel` (`channel`),
  ADD KEY `uID` (`uID`);

--
-- Indexen voor tabel `MailImporters`
--
ALTER TABLE `MailImporters`
  ADD PRIMARY KEY (`miID`),
  ADD UNIQUE KEY `miHandle` (`miHandle`),
  ADD KEY `pkgID` (`pkgID`,`miID`);

--
-- Indexen voor tabel `MailValidationHashes`
--
ALTER TABLE `MailValidationHashes`
  ADD PRIMARY KEY (`mvhID`),
  ADD UNIQUE KEY `mHash` (`mHash`),
  ADD KEY `miID` (`miID`);

--
-- Indexen voor tabel `MultilingualPageRelations`
--
ALTER TABLE `MultilingualPageRelations`
  ADD PRIMARY KEY (`mpRelationID`,`cID`,`mpLocale`);

--
-- Indexen voor tabel `MultilingualSections`
--
ALTER TABLE `MultilingualSections`
  ADD PRIMARY KEY (`cID`);

--
-- Indexen voor tabel `MultilingualTranslations`
--
ALTER TABLE `MultilingualTranslations`
  ADD PRIMARY KEY (`mtID`);

--
-- Indexen voor tabel `OauthUserMap`
--
ALTER TABLE `OauthUserMap`
  ADD PRIMARY KEY (`user_id`,`namespace`),
  ADD UNIQUE KEY `oauth_binding` (`binding`,`namespace`);

--
-- Indexen voor tabel `Packages`
--
ALTER TABLE `Packages`
  ADD PRIMARY KEY (`pkgID`),
  ADD UNIQUE KEY `pkgHandle` (`pkgHandle`);

--
-- Indexen voor tabel `PageFeeds`
--
ALTER TABLE `PageFeeds`
  ADD PRIMARY KEY (`pfID`),
  ADD UNIQUE KEY `pfHandle` (`pfHandle`);

--
-- Indexen voor tabel `PagePaths`
--
ALTER TABLE `PagePaths`
  ADD PRIMARY KEY (`ppID`),
  ADD KEY `cID` (`cID`),
  ADD KEY `ppIsCanonical` (`ppIsCanonical`),
  ADD KEY `cPath` (`cPath`(255));

--
-- Indexen voor tabel `PagePermissionAssignments`
--
ALTER TABLE `PagePermissionAssignments`
  ADD PRIMARY KEY (`cID`,`pkID`,`paID`),
  ADD KEY `paID` (`paID`,`pkID`),
  ADD KEY `pkID` (`pkID`);

--
-- Indexen voor tabel `PagePermissionPageTypeAccessList`
--
ALTER TABLE `PagePermissionPageTypeAccessList`
  ADD PRIMARY KEY (`paID`,`peID`),
  ADD KEY `peID` (`peID`);

--
-- Indexen voor tabel `PagePermissionPageTypeAccessListCustom`
--
ALTER TABLE `PagePermissionPageTypeAccessListCustom`
  ADD PRIMARY KEY (`paID`,`peID`,`ptID`),
  ADD KEY `peID` (`peID`),
  ADD KEY `ptID` (`ptID`);

--
-- Indexen voor tabel `PagePermissionPropertyAccessList`
--
ALTER TABLE `PagePermissionPropertyAccessList`
  ADD PRIMARY KEY (`paID`,`peID`),
  ADD KEY `peID` (`peID`),
  ADD KEY `uID` (`uID`);

--
-- Indexen voor tabel `PagePermissionPropertyAttributeAccessListCustom`
--
ALTER TABLE `PagePermissionPropertyAttributeAccessListCustom`
  ADD PRIMARY KEY (`paID`,`peID`,`akID`),
  ADD KEY `peID` (`peID`),
  ADD KEY `akID` (`akID`);

--
-- Indexen voor tabel `PagePermissionThemeAccessList`
--
ALTER TABLE `PagePermissionThemeAccessList`
  ADD PRIMARY KEY (`paID`,`peID`),
  ADD KEY `peID` (`peID`);

--
-- Indexen voor tabel `PagePermissionThemeAccessListCustom`
--
ALTER TABLE `PagePermissionThemeAccessListCustom`
  ADD PRIMARY KEY (`paID`,`peID`,`pThemeID`),
  ADD KEY `peID` (`peID`),
  ADD KEY `pThemeID` (`pThemeID`);

--
-- Indexen voor tabel `Pages`
--
ALTER TABLE `Pages`
  ADD PRIMARY KEY (`cID`,`ptID`),
  ADD KEY `cParentID` (`cParentID`),
  ADD KEY `cIsActive` (`cID`,`cIsActive`),
  ADD KEY `cCheckedOutUID` (`cCheckedOutUID`),
  ADD KEY `uID` (`uID`,`cPointerID`),
  ADD KEY `cPointerID` (`cPointerID`,`cDisplayOrder`),
  ADD KEY `cIsTemplate` (`cID`,`cIsTemplate`),
  ADD KEY `cIsSystemPage` (`cID`,`cIsSystemPage`),
  ADD KEY `pkgID` (`pkgID`),
  ADD KEY `cParentMaxDisplay` (`cParentID`,`cDisplayOrder`);

--
-- Indexen voor tabel `PageSearchIndex`
--
ALTER TABLE `PageSearchIndex`
  ADD PRIMARY KEY (`cID`),
  ADD KEY `cDateLastIndexed` (`cDateLastIndexed`),
  ADD KEY `cDateLastSitemapped` (`cDateLastSitemapped`),
  ADD KEY `cRequiresReindex` (`cRequiresReindex`);
ALTER TABLE `PageSearchIndex` ADD FULLTEXT KEY `cName` (`cName`);
ALTER TABLE `PageSearchIndex` ADD FULLTEXT KEY `cDescription` (`cDescription`);
ALTER TABLE `PageSearchIndex` ADD FULLTEXT KEY `content` (`content`);
ALTER TABLE `PageSearchIndex` ADD FULLTEXT KEY `content2` (`cName`,`cDescription`,`content`);

--
-- Indexen voor tabel `PageTemplates`
--
ALTER TABLE `PageTemplates`
  ADD PRIMARY KEY (`pTemplateID`),
  ADD UNIQUE KEY `pTemplateHandle` (`pTemplateHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `PageThemeCustomStyles`
--
ALTER TABLE `PageThemeCustomStyles`
  ADD PRIMARY KEY (`pThemeID`),
  ADD KEY `scvlID` (`scvlID`),
  ADD KEY `sccRecordID` (`sccRecordID`);

--
-- Indexen voor tabel `PageThemes`
--
ALTER TABLE `PageThemes`
  ADD PRIMARY KEY (`pThemeID`),
  ADD UNIQUE KEY `ptHandle` (`pThemeHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `PageTypeComposerControlTypes`
--
ALTER TABLE `PageTypeComposerControlTypes`
  ADD PRIMARY KEY (`ptComposerControlTypeID`),
  ADD UNIQUE KEY `ptComposerControlTypeHandle` (`ptComposerControlTypeHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `PageTypeComposerFormLayoutSetControls`
--
ALTER TABLE `PageTypeComposerFormLayoutSetControls`
  ADD PRIMARY KEY (`ptComposerFormLayoutSetControlID`),
  ADD KEY `ptComposerControlTypeID` (`ptComposerControlTypeID`);

--
-- Indexen voor tabel `PageTypeComposerFormLayoutSets`
--
ALTER TABLE `PageTypeComposerFormLayoutSets`
  ADD PRIMARY KEY (`ptComposerFormLayoutSetID`),
  ADD KEY `ptID` (`ptID`);

--
-- Indexen voor tabel `PageTypeComposerOutputBlocks`
--
ALTER TABLE `PageTypeComposerOutputBlocks`
  ADD PRIMARY KEY (`ptComposerOutputBlockID`),
  ADD KEY `cID` (`cID`),
  ADD KEY `bID` (`bID`,`cID`),
  ADD KEY `ptComposerFormLayoutSetControlID` (`ptComposerFormLayoutSetControlID`);

--
-- Indexen voor tabel `PageTypeComposerOutputControls`
--
ALTER TABLE `PageTypeComposerOutputControls`
  ADD PRIMARY KEY (`ptComposerOutputControlID`),
  ADD KEY `pTemplateID` (`pTemplateID`,`ptComposerFormLayoutSetControlID`),
  ADD KEY `ptID` (`ptID`),
  ADD KEY `ptComposerFormLayoutSetControlID` (`ptComposerFormLayoutSetControlID`);

--
-- Indexen voor tabel `PageTypePageTemplateDefaultPages`
--
ALTER TABLE `PageTypePageTemplateDefaultPages`
  ADD PRIMARY KEY (`pTemplateID`,`ptID`),
  ADD KEY `ptID` (`ptID`),
  ADD KEY `cID` (`cID`);

--
-- Indexen voor tabel `PageTypePageTemplates`
--
ALTER TABLE `PageTypePageTemplates`
  ADD PRIMARY KEY (`ptID`,`pTemplateID`),
  ADD KEY `pTemplateID` (`pTemplateID`);

--
-- Indexen voor tabel `PageTypePermissionAssignments`
--
ALTER TABLE `PageTypePermissionAssignments`
  ADD PRIMARY KEY (`ptID`,`pkID`,`paID`),
  ADD KEY `pkID` (`pkID`),
  ADD KEY `ptID` (`ptID`);

--
-- Indexen voor tabel `PageTypePublishTargetTypes`
--
ALTER TABLE `PageTypePublishTargetTypes`
  ADD PRIMARY KEY (`ptPublishTargetTypeID`),
  ADD KEY `ptPublishTargetTypeHandle` (`ptPublishTargetTypeHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `PageTypes`
--
ALTER TABLE `PageTypes`
  ADD PRIMARY KEY (`ptID`),
  ADD UNIQUE KEY `ptHandle` (`ptHandle`),
  ADD KEY `pkgID` (`pkgID`,`ptID`);

--
-- Indexen voor tabel `PageWorkflowProgress`
--
ALTER TABLE `PageWorkflowProgress`
  ADD PRIMARY KEY (`cID`,`wpID`),
  ADD KEY `wpID` (`wpID`);

--
-- Indexen voor tabel `PermissionAccess`
--
ALTER TABLE `PermissionAccess`
  ADD PRIMARY KEY (`paID`);

--
-- Indexen voor tabel `PermissionAccessEntities`
--
ALTER TABLE `PermissionAccessEntities`
  ADD PRIMARY KEY (`peID`),
  ADD KEY `petID` (`petID`);

--
-- Indexen voor tabel `PermissionAccessEntityGroups`
--
ALTER TABLE `PermissionAccessEntityGroups`
  ADD PRIMARY KEY (`pegID`),
  ADD KEY `peID` (`peID`),
  ADD KEY `gID` (`gID`);

--
-- Indexen voor tabel `PermissionAccessEntityGroupSets`
--
ALTER TABLE `PermissionAccessEntityGroupSets`
  ADD PRIMARY KEY (`peID`,`gsID`),
  ADD KEY `gsID` (`gsID`);

--
-- Indexen voor tabel `PermissionAccessEntityTypeCategories`
--
ALTER TABLE `PermissionAccessEntityTypeCategories`
  ADD PRIMARY KEY (`petID`,`pkCategoryID`),
  ADD KEY `pkCategoryID` (`pkCategoryID`);

--
-- Indexen voor tabel `PermissionAccessEntityTypes`
--
ALTER TABLE `PermissionAccessEntityTypes`
  ADD PRIMARY KEY (`petID`),
  ADD UNIQUE KEY `petHandle` (`petHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `PermissionAccessEntityUsers`
--
ALTER TABLE `PermissionAccessEntityUsers`
  ADD PRIMARY KEY (`peID`,`uID`),
  ADD KEY `uID` (`uID`);

--
-- Indexen voor tabel `PermissionAccessList`
--
ALTER TABLE `PermissionAccessList`
  ADD PRIMARY KEY (`paID`,`peID`),
  ADD KEY `accessType` (`accessType`),
  ADD KEY `peID` (`peID`),
  ADD KEY `peID_accessType` (`peID`,`accessType`),
  ADD KEY `pdID` (`pdID`),
  ADD KEY `permissionAccessDuration` (`paID`,`pdID`);

--
-- Indexen voor tabel `PermissionAccessWorkflows`
--
ALTER TABLE `PermissionAccessWorkflows`
  ADD PRIMARY KEY (`paID`,`wfID`),
  ADD KEY `wfID` (`wfID`);

--
-- Indexen voor tabel `PermissionAssignments`
--
ALTER TABLE `PermissionAssignments`
  ADD PRIMARY KEY (`paID`,`pkID`),
  ADD KEY `pkID` (`pkID`);

--
-- Indexen voor tabel `PermissionDurationObjects`
--
ALTER TABLE `PermissionDurationObjects`
  ADD PRIMARY KEY (`pdID`);

--
-- Indexen voor tabel `PermissionKeyCategories`
--
ALTER TABLE `PermissionKeyCategories`
  ADD PRIMARY KEY (`pkCategoryID`),
  ADD UNIQUE KEY `pkCategoryHandle` (`pkCategoryHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `PermissionKeys`
--
ALTER TABLE `PermissionKeys`
  ADD PRIMARY KEY (`pkID`),
  ADD UNIQUE KEY `akHandle` (`pkHandle`),
  ADD KEY `pkCategoryID` (`pkCategoryID`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `PileContents`
--
ALTER TABLE `PileContents`
  ADD PRIMARY KEY (`pcID`),
  ADD KEY `pID` (`pID`,`displayOrder`),
  ADD KEY `itemID` (`itemID`),
  ADD KEY `itemType` (`itemType`,`itemID`,`pID`);

--
-- Indexen voor tabel `Piles`
--
ALTER TABLE `Piles`
  ADD PRIMARY KEY (`pID`),
  ADD KEY `uID` (`uID`,`name`);

--
-- Indexen voor tabel `QueueMessages`
--
ALTER TABLE `QueueMessages`
  ADD PRIMARY KEY (`message_id`),
  ADD UNIQUE KEY `message_handle` (`handle`),
  ADD KEY `message_queueid` (`queue_id`);

--
-- Indexen voor tabel `QueuePageDuplicationRelations`
--
ALTER TABLE `QueuePageDuplicationRelations`
  ADD PRIMARY KEY (`cID`,`originalCID`),
  ADD KEY `originalCID` (`originalCID`,`queue_name`);

--
-- Indexen voor tabel `Queues`
--
ALTER TABLE `Queues`
  ADD PRIMARY KEY (`queue_id`),
  ADD KEY `queue_name` (`queue_name`);

--
-- Indexen voor tabel `Sessions`
--
ALTER TABLE `Sessions`
  ADD PRIMARY KEY (`sessionID`);

--
-- Indexen voor tabel `SignupRequests`
--
ALTER TABLE `SignupRequests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ipFrom` (`ipFrom`(32));

--
-- Indexen voor tabel `SocialLinks`
--
ALTER TABLE `SocialLinks`
  ADD PRIMARY KEY (`slID`),
  ADD UNIQUE KEY `ssHandle` (`ssHandle`);

--
-- Indexen voor tabel `Stacks`
--
ALTER TABLE `Stacks`
  ADD PRIMARY KEY (`stID`),
  ADD KEY `stType` (`stType`),
  ADD KEY `stName` (`stName`),
  ADD KEY `cID` (`cID`);

--
-- Indexen voor tabel `StyleCustomizerCustomCssRecords`
--
ALTER TABLE `StyleCustomizerCustomCssRecords`
  ADD PRIMARY KEY (`sccRecordID`);

--
-- Indexen voor tabel `StyleCustomizerInlineStylePresets`
--
ALTER TABLE `StyleCustomizerInlineStylePresets`
  ADD PRIMARY KEY (`pssPresetID`),
  ADD KEY `issID` (`issID`);

--
-- Indexen voor tabel `StyleCustomizerInlineStyleSets`
--
ALTER TABLE `StyleCustomizerInlineStyleSets`
  ADD PRIMARY KEY (`issID`);

--
-- Indexen voor tabel `StyleCustomizerValueLists`
--
ALTER TABLE `StyleCustomizerValueLists`
  ADD PRIMARY KEY (`scvlID`);

--
-- Indexen voor tabel `StyleCustomizerValues`
--
ALTER TABLE `StyleCustomizerValues`
  ADD PRIMARY KEY (`scvID`),
  ADD KEY `scvlID` (`scvlID`);

--
-- Indexen voor tabel `SystemAntispamLibraries`
--
ALTER TABLE `SystemAntispamLibraries`
  ADD PRIMARY KEY (`saslHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `SystemCaptchaLibraries`
--
ALTER TABLE `SystemCaptchaLibraries`
  ADD PRIMARY KEY (`sclHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `SystemContentEditorSnippets`
--
ALTER TABLE `SystemContentEditorSnippets`
  ADD PRIMARY KEY (`scsHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `SystemDatabaseMigrations`
--
ALTER TABLE `SystemDatabaseMigrations`
  ADD PRIMARY KEY (`version`);

--
-- Indexen voor tabel `SystemDatabaseQueryLog`
--
ALTER TABLE `SystemDatabaseQueryLog`
  ADD PRIMARY KEY (`ID`);

--
-- Indexen voor tabel `TopicTrees`
--
ALTER TABLE `TopicTrees`
  ADD PRIMARY KEY (`treeID`);

--
-- Indexen voor tabel `TreeCategoryNodes`
--
ALTER TABLE `TreeCategoryNodes`
  ADD PRIMARY KEY (`treeNodeID`);

--
-- Indexen voor tabel `TreeGroupNodes`
--
ALTER TABLE `TreeGroupNodes`
  ADD PRIMARY KEY (`treeNodeID`),
  ADD KEY `gID` (`gID`);

--
-- Indexen voor tabel `TreeNodePermissionAssignments`
--
ALTER TABLE `TreeNodePermissionAssignments`
  ADD PRIMARY KEY (`treeNodeID`,`pkID`,`paID`),
  ADD KEY `pkID` (`pkID`),
  ADD KEY `paID` (`paID`);

--
-- Indexen voor tabel `TreeNodes`
--
ALTER TABLE `TreeNodes`
  ADD PRIMARY KEY (`treeNodeID`),
  ADD KEY `treeNodeParentID` (`treeNodeParentID`),
  ADD KEY `treeNodeTypeID` (`treeNodeTypeID`),
  ADD KEY `treeID` (`treeID`),
  ADD KEY `inheritPermissionsFromTreeNodeID` (`inheritPermissionsFromTreeNodeID`,`treeNodeID`);

--
-- Indexen voor tabel `TreeNodeTypes`
--
ALTER TABLE `TreeNodeTypes`
  ADD PRIMARY KEY (`treeNodeTypeID`),
  ADD UNIQUE KEY `treeNodeTypeHandle` (`treeNodeTypeHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `Trees`
--
ALTER TABLE `Trees`
  ADD PRIMARY KEY (`treeID`),
  ADD KEY `treeTypeID` (`treeTypeID`);

--
-- Indexen voor tabel `TreeTopicNodes`
--
ALTER TABLE `TreeTopicNodes`
  ADD PRIMARY KEY (`treeNodeID`);

--
-- Indexen voor tabel `TreeTypes`
--
ALTER TABLE `TreeTypes`
  ADD PRIMARY KEY (`treeTypeID`),
  ADD UNIQUE KEY `treeTypeHandle` (`treeTypeHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `UserAttributeKeys`
--
ALTER TABLE `UserAttributeKeys`
  ADD PRIMARY KEY (`akID`);

--
-- Indexen voor tabel `UserAttributeValues`
--
ALTER TABLE `UserAttributeValues`
  ADD PRIMARY KEY (`uID`,`akID`),
  ADD KEY `akID` (`akID`),
  ADD KEY `avID` (`avID`);

--
-- Indexen voor tabel `UserBannedIPs`
--
ALTER TABLE `UserBannedIPs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ipFrom` (`ipFrom`(32),`ipTo`(32));

--
-- Indexen voor tabel `UserGroups`
--
ALTER TABLE `UserGroups`
  ADD PRIMARY KEY (`uID`,`gID`),
  ADD KEY `uID` (`uID`),
  ADD KEY `gID` (`gID`);

--
-- Indexen voor tabel `UserPermissionEditPropertyAccessList`
--
ALTER TABLE `UserPermissionEditPropertyAccessList`
  ADD PRIMARY KEY (`paID`,`peID`);

--
-- Indexen voor tabel `UserPermissionEditPropertyAttributeAccessListCustom`
--
ALTER TABLE `UserPermissionEditPropertyAttributeAccessListCustom`
  ADD PRIMARY KEY (`paID`,`peID`,`akID`),
  ADD KEY `peID` (`peID`),
  ADD KEY `akID` (`akID`);

--
-- Indexen voor tabel `UserPermissionViewAttributeAccessList`
--
ALTER TABLE `UserPermissionViewAttributeAccessList`
  ADD PRIMARY KEY (`paID`,`peID`),
  ADD KEY `peID` (`peID`);

--
-- Indexen voor tabel `UserPermissionViewAttributeAccessListCustom`
--
ALTER TABLE `UserPermissionViewAttributeAccessListCustom`
  ADD PRIMARY KEY (`paID`,`peID`,`akID`),
  ADD KEY `peID` (`peID`),
  ADD KEY `akID` (`akID`);

--
-- Indexen voor tabel `UserPointActions`
--
ALTER TABLE `UserPointActions`
  ADD PRIMARY KEY (`upaID`),
  ADD UNIQUE KEY `upaHandle` (`upaHandle`),
  ADD KEY `pkgID` (`pkgID`),
  ADD KEY `gBBadgeID` (`gBadgeID`);

--
-- Indexen voor tabel `UserPointHistory`
--
ALTER TABLE `UserPointHistory`
  ADD PRIMARY KEY (`upID`),
  ADD KEY `upuID` (`upuID`),
  ADD KEY `upaID` (`upaID`);

--
-- Indexen voor tabel `UserPrivateMessages`
--
ALTER TABLE `UserPrivateMessages`
  ADD PRIMARY KEY (`msgID`),
  ADD KEY `uAuthorID` (`uAuthorID`,`msgDateCreated`);

--
-- Indexen voor tabel `UserPrivateMessagesTo`
--
ALTER TABLE `UserPrivateMessagesTo`
  ADD PRIMARY KEY (`msgID`,`uID`,`uAuthorID`,`msgMailboxID`),
  ADD KEY `uID` (`uID`),
  ADD KEY `uAuthorID` (`uAuthorID`),
  ADD KEY `msgFolderID` (`msgMailboxID`),
  ADD KEY `msgIsNew` (`msgIsNew`);

--
-- Indexen voor tabel `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`uID`),
  ADD UNIQUE KEY `uName` (`uName`),
  ADD KEY `uEmail` (`uEmail`);

--
-- Indexen voor tabel `UserSearchIndexAttributes`
--
ALTER TABLE `UserSearchIndexAttributes`
  ADD PRIMARY KEY (`uID`);

--
-- Indexen voor tabel `UserValidationHashes`
--
ALTER TABLE `UserValidationHashes`
  ADD PRIMARY KEY (`uvhID`),
  ADD KEY `uID` (`uID`,`type`),
  ADD KEY `uHash` (`uHash`,`type`),
  ADD KEY `uDateGenerated` (`uDateGenerated`,`type`);

--
-- Indexen voor tabel `WorkflowProgress`
--
ALTER TABLE `WorkflowProgress`
  ADD PRIMARY KEY (`wpID`),
  ADD KEY `wpCategoryID` (`wpCategoryID`),
  ADD KEY `wfID` (`wfID`),
  ADD KEY `wrID` (`wrID`,`wpID`,`wpIsCompleted`);

--
-- Indexen voor tabel `WorkflowProgressCategories`
--
ALTER TABLE `WorkflowProgressCategories`
  ADD PRIMARY KEY (`wpCategoryID`),
  ADD UNIQUE KEY `wpCategoryHandle` (`wpCategoryHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- Indexen voor tabel `WorkflowProgressHistory`
--
ALTER TABLE `WorkflowProgressHistory`
  ADD PRIMARY KEY (`wphID`),
  ADD KEY `wpID` (`wpID`,`timestamp`);

--
-- Indexen voor tabel `WorkflowRequestObjects`
--
ALTER TABLE `WorkflowRequestObjects`
  ADD PRIMARY KEY (`wrID`);

--
-- Indexen voor tabel `Workflows`
--
ALTER TABLE `Workflows`
  ADD PRIMARY KEY (`wfID`),
  ADD UNIQUE KEY `wfName` (`wfName`),
  ADD KEY `wftID` (`wftID`,`wfID`);

--
-- Indexen voor tabel `WorkflowTypes`
--
ALTER TABLE `WorkflowTypes`
  ADD PRIMARY KEY (`wftID`),
  ADD UNIQUE KEY `wftHandle` (`wftHandle`),
  ADD KEY `pkgID` (`pkgID`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `AreaLayoutColumns`
--
ALTER TABLE `AreaLayoutColumns`
  MODIFY `arLayoutColumnID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `AreaLayoutCustomColumns`
--
ALTER TABLE `AreaLayoutCustomColumns`
  MODIFY `arLayoutColumnID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `AreaLayoutPresets`
--
ALTER TABLE `AreaLayoutPresets`
  MODIFY `arLayoutPresetID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `AreaLayouts`
--
ALTER TABLE `AreaLayouts`
  MODIFY `arLayoutID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `AreaLayoutThemeGridColumns`
--
ALTER TABLE `AreaLayoutThemeGridColumns`
  MODIFY `arLayoutColumnID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `Areas`
--
ALTER TABLE `Areas`
  MODIFY `arID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT voor een tabel `atAddressCustomCountries`
--
ALTER TABLE `atAddressCustomCountries`
  MODIFY `atAddressCustomCountryID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `atSelectOptions`
--
ALTER TABLE `atSelectOptions`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `atSocialLinks`
--
ALTER TABLE `atSocialLinks`
  MODIFY `avsID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `AttributeKeyCategories`
--
ALTER TABLE `AttributeKeyCategories`
  MODIFY `akCategoryID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `AttributeKeys`
--
ALTER TABLE `AttributeKeys`
  MODIFY `akID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT voor een tabel `AttributeSets`
--
ALTER TABLE `AttributeSets`
  MODIFY `asID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT voor een tabel `AttributeTypes`
--
ALTER TABLE `AttributeTypes`
  MODIFY `atID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT voor een tabel `AttributeValues`
--
ALTER TABLE `AttributeValues`
  MODIFY `avID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT voor een tabel `AuthenticationTypes`
--
ALTER TABLE `AuthenticationTypes`
  MODIFY `authTypeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT voor een tabel `authTypeConcreteCookieMap`
--
ALTER TABLE `authTypeConcreteCookieMap`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `BannedWords`
--
ALTER TABLE `BannedWords`
  MODIFY `bwID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `BlockRelations`
--
ALTER TABLE `BlockRelations`
  MODIFY `brID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT voor een tabel `Blocks`
--
ALTER TABLE `Blocks`
  MODIFY `bID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT voor een tabel `BlockTypes`
--
ALTER TABLE `BlockTypes`
  MODIFY `btID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT voor een tabel `BlockTypeSets`
--
ALTER TABLE `BlockTypeSets`
  MODIFY `btsID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT voor een tabel `btFaqEntries`
--
ALTER TABLE `btFaqEntries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `btFormAnswers`
--
ALTER TABLE `btFormAnswers`
  MODIFY `aID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `btFormAnswerSet`
--
ALTER TABLE `btFormAnswerSet`
  MODIFY `asID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `btFormQuestions`
--
ALTER TABLE `btFormQuestions`
  MODIFY `qID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT voor een tabel `btImageSliderEntries`
--
ALTER TABLE `btImageSliderEntries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT voor een tabel `btShareThisPage`
--
ALTER TABLE `btShareThisPage`
  MODIFY `btShareThisPageID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `btSocialLinks`
--
ALTER TABLE `btSocialLinks`
  MODIFY `btSocialLinkID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `btSurveyOptions`
--
ALTER TABLE `btSurveyOptions`
  MODIFY `optionID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `btSurveyResults`
--
ALTER TABLE `btSurveyResults`
  MODIFY `resultID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `Collections`
--
ALTER TABLE `Collections`
  MODIFY `cID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;
--
-- AUTO_INCREMENT voor een tabel `ConversationDiscussions`
--
ALTER TABLE `ConversationDiscussions`
  MODIFY `cnvDiscussionID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `ConversationEditors`
--
ALTER TABLE `ConversationEditors`
  MODIFY `cnvEditorID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `ConversationFlaggedMessageTypes`
--
ALTER TABLE `ConversationFlaggedMessageTypes`
  MODIFY `cnvMessageFlagTypeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `ConversationMessageAttachments`
--
ALTER TABLE `ConversationMessageAttachments`
  MODIFY `cnvMessageAttachmentID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `ConversationMessageRatings`
--
ALTER TABLE `ConversationMessageRatings`
  MODIFY `cnvMessageRatingID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `ConversationMessages`
--
ALTER TABLE `ConversationMessages`
  MODIFY `cnvMessageID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `ConversationRatingTypes`
--
ALTER TABLE `ConversationRatingTypes`
  MODIFY `cnvRatingTypeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT voor een tabel `Conversations`
--
ALTER TABLE `Conversations`
  MODIFY `cnvID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `DownloadStatistics`
--
ALTER TABLE `DownloadStatistics`
  MODIFY `dsID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `FeatureAssignments`
--
ALTER TABLE `FeatureAssignments`
  MODIFY `faID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT voor een tabel `FeatureCategories`
--
ALTER TABLE `FeatureCategories`
  MODIFY `fcID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT voor een tabel `Features`
--
ALTER TABLE `Features`
  MODIFY `feID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT voor een tabel `FileImageThumbnailTypes`
--
ALTER TABLE `FileImageThumbnailTypes`
  MODIFY `ftTypeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT voor een tabel `Files`
--
ALTER TABLE `Files`
  MODIFY `fID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT voor een tabel `FileSetFiles`
--
ALTER TABLE `FileSetFiles`
  MODIFY `fsfID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `FileSets`
--
ALTER TABLE `FileSets`
  MODIFY `fsID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `FileStorageLocations`
--
ALTER TABLE `FileStorageLocations`
  MODIFY `fslID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `FileStorageLocationTypes`
--
ALTER TABLE `FileStorageLocationTypes`
  MODIFY `fslTypeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT voor een tabel `FileVersionLog`
--
ALTER TABLE `FileVersionLog`
  MODIFY `fvlID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT voor een tabel `GatheringConfiguredDataSources`
--
ALTER TABLE `GatheringConfiguredDataSources`
  MODIFY `gcsID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `GatheringDataSources`
--
ALTER TABLE `GatheringDataSources`
  MODIFY `gasID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `GatheringItemFeatureAssignments`
--
ALTER TABLE `GatheringItemFeatureAssignments`
  MODIFY `gafaID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `GatheringItems`
--
ALTER TABLE `GatheringItems`
  MODIFY `gaiID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `GatheringItemTemplateFeatures`
--
ALTER TABLE `GatheringItemTemplateFeatures`
  MODIFY `gfeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT voor een tabel `GatheringItemTemplates`
--
ALTER TABLE `GatheringItemTemplates`
  MODIFY `gatID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT voor een tabel `GatheringItemTemplateTypes`
--
ALTER TABLE `GatheringItemTemplateTypes`
  MODIFY `gatTypeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT voor een tabel `Gatherings`
--
ALTER TABLE `Gatherings`
  MODIFY `gaID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `Groups`
--
ALTER TABLE `Groups`
  MODIFY `gID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `GroupSets`
--
ALTER TABLE `GroupSets`
  MODIFY `gsID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `Jobs`
--
ALTER TABLE `Jobs`
  MODIFY `jID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT voor een tabel `JobSets`
--
ALTER TABLE `JobSets`
  MODIFY `jsID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `JobsLog`
--
ALTER TABLE `JobsLog`
  MODIFY `jlID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `Logs`
--
ALTER TABLE `Logs`
  MODIFY `logID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT voor een tabel `MailImporters`
--
ALTER TABLE `MailImporters`
  MODIFY `miID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `MailValidationHashes`
--
ALTER TABLE `MailValidationHashes`
  MODIFY `mvhID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `MultilingualTranslations`
--
ALTER TABLE `MultilingualTranslations`
  MODIFY `mtID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `Packages`
--
ALTER TABLE `Packages`
  MODIFY `pkgID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT voor een tabel `PageFeeds`
--
ALTER TABLE `PageFeeds`
  MODIFY `pfID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `PagePaths`
--
ALTER TABLE `PagePaths`
  MODIFY `ppID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT voor een tabel `PageTemplates`
--
ALTER TABLE `PageTemplates`
  MODIFY `pTemplateID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT voor een tabel `PageThemes`
--
ALTER TABLE `PageThemes`
  MODIFY `pThemeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `PageTypeComposerControlTypes`
--
ALTER TABLE `PageTypeComposerControlTypes`
  MODIFY `ptComposerControlTypeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `PageTypeComposerFormLayoutSetControls`
--
ALTER TABLE `PageTypeComposerFormLayoutSetControls`
  MODIFY `ptComposerFormLayoutSetControlID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT voor een tabel `PageTypeComposerFormLayoutSets`
--
ALTER TABLE `PageTypeComposerFormLayoutSets`
  MODIFY `ptComposerFormLayoutSetID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT voor een tabel `PageTypeComposerOutputBlocks`
--
ALTER TABLE `PageTypeComposerOutputBlocks`
  MODIFY `ptComposerOutputBlockID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `PageTypeComposerOutputControls`
--
ALTER TABLE `PageTypeComposerOutputControls`
  MODIFY `ptComposerOutputControlID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT voor een tabel `PageTypePublishTargetTypes`
--
ALTER TABLE `PageTypePublishTargetTypes`
  MODIFY `ptPublishTargetTypeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `PageTypes`
--
ALTER TABLE `PageTypes`
  MODIFY `ptID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT voor een tabel `PermissionAccess`
--
ALTER TABLE `PermissionAccess`
  MODIFY `paID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT voor een tabel `PermissionAccessEntities`
--
ALTER TABLE `PermissionAccessEntities`
  MODIFY `peID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT voor een tabel `PermissionAccessEntityGroups`
--
ALTER TABLE `PermissionAccessEntityGroups`
  MODIFY `pegID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `PermissionAccessEntityTypes`
--
ALTER TABLE `PermissionAccessEntityTypes`
  MODIFY `petID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT voor een tabel `PermissionDurationObjects`
--
ALTER TABLE `PermissionDurationObjects`
  MODIFY `pdID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `PermissionKeyCategories`
--
ALTER TABLE `PermissionKeyCategories`
  MODIFY `pkCategoryID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT voor een tabel `PermissionKeys`
--
ALTER TABLE `PermissionKeys`
  MODIFY `pkID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT voor een tabel `PileContents`
--
ALTER TABLE `PileContents`
  MODIFY `pcID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `Piles`
--
ALTER TABLE `Piles`
  MODIFY `pID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `QueueMessages`
--
ALTER TABLE `QueueMessages`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `Queues`
--
ALTER TABLE `Queues`
  MODIFY `queue_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `SignupRequests`
--
ALTER TABLE `SignupRequests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `SocialLinks`
--
ALTER TABLE `SocialLinks`
  MODIFY `slID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `Stacks`
--
ALTER TABLE `Stacks`
  MODIFY `stID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT voor een tabel `StyleCustomizerCustomCssRecords`
--
ALTER TABLE `StyleCustomizerCustomCssRecords`
  MODIFY `sccRecordID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `StyleCustomizerInlineStylePresets`
--
ALTER TABLE `StyleCustomizerInlineStylePresets`
  MODIFY `pssPresetID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `StyleCustomizerInlineStyleSets`
--
ALTER TABLE `StyleCustomizerInlineStyleSets`
  MODIFY `issID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `StyleCustomizerValueLists`
--
ALTER TABLE `StyleCustomizerValueLists`
  MODIFY `scvlID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `StyleCustomizerValues`
--
ALTER TABLE `StyleCustomizerValues`
  MODIFY `scvID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `SystemDatabaseQueryLog`
--
ALTER TABLE `SystemDatabaseQueryLog`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `TreeCategoryNodes`
--
ALTER TABLE `TreeCategoryNodes`
  MODIFY `treeNodeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `TreeGroupNodes`
--
ALTER TABLE `TreeGroupNodes`
  MODIFY `treeNodeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `TreeNodes`
--
ALTER TABLE `TreeNodes`
  MODIFY `treeNodeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `TreeNodeTypes`
--
ALTER TABLE `TreeNodeTypes`
  MODIFY `treeNodeTypeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `Trees`
--
ALTER TABLE `Trees`
  MODIFY `treeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `TreeTopicNodes`
--
ALTER TABLE `TreeTopicNodes`
  MODIFY `treeNodeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `TreeTypes`
--
ALTER TABLE `TreeTypes`
  MODIFY `treeTypeID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT voor een tabel `UserBannedIPs`
--
ALTER TABLE `UserBannedIPs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `UserPointActions`
--
ALTER TABLE `UserPointActions`
  MODIFY `upaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `UserPointHistory`
--
ALTER TABLE `UserPointHistory`
  MODIFY `upID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `UserPrivateMessages`
--
ALTER TABLE `UserPrivateMessages`
  MODIFY `msgID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `Users`
--
ALTER TABLE `Users`
  MODIFY `uID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `UserValidationHashes`
--
ALTER TABLE `UserValidationHashes`
  MODIFY `uvhID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `WorkflowProgress`
--
ALTER TABLE `WorkflowProgress`
  MODIFY `wpID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT voor een tabel `WorkflowProgressCategories`
--
ALTER TABLE `WorkflowProgressCategories`
  MODIFY `wpCategoryID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `WorkflowProgressHistory`
--
ALTER TABLE `WorkflowProgressHistory`
  MODIFY `wphID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT voor een tabel `WorkflowRequestObjects`
--
ALTER TABLE `WorkflowRequestObjects`
  MODIFY `wrID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT voor een tabel `Workflows`
--
ALTER TABLE `Workflows`
  MODIFY `wfID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `WorkflowTypes`
--
ALTER TABLE `WorkflowTypes`
  MODIFY `wftID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `QueueMessages`
--
ALTER TABLE `QueueMessages`
  ADD CONSTRAINT `queuemessages_ibfk_1` FOREIGN KEY (`queue_id`) REFERENCES `Queues` (`queue_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
