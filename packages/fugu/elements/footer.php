<!--    </div> End Page Wrap -->

    <footer class="footer" id="contact">
      <div class="grid__container">
        <div class="grid__row">
          <div class="grid__col--8 grid__centered text--center grid__col--sm--12">
            <?php
              $a = new Area('Footer header');
              $a->display($c);
            ?>
          </div>
          <div class="grid__col--8 grid__centered grid__col--sm--12">
            <div class="grid__container">
              <div class="grid__row">
                <div class="grid__col--6 grid__col--sm--12 footer__txt">
                  <?php
                    $a = new Area('Footer text');
                    $a->display($c);
                  ?>
                </div>
                <div class="grid__col--6 grid__col--sm--12 footer__form">
                  <?php
                    $a = new Area('Footer form');
                    $a->display($c);
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

  </div><!-- End Page -->

  <?php Loader::element('footer_required') ?>
<!--  <script src="<?php echo $view->getThemePath() ?>/dist/js/app.js"></script>-->
</body>
</html>
