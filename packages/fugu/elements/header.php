<?php
  $u = new User();
  $c = Page::getCurrentPage();
?>
<!doctype html>
<html lang="nl">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  <?php Loader::element('header_required') ?>
<!--  <link href="<?php echo $view->getThemePath() ?>/dist/css/app.css" type="text/css" rel="stylesheet" />-->
  <link href="<?php echo $view->getThemePath() ?>/assets/css/style.css" type="text/css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,700" rel="stylesheet">
  <script src="<?php echo $view->getThemePath() ?>/assets/js/ofi.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="<?php echo $view->getThemePath() ?>/assets/js/app.js" type="text/javascript"></script>
</head>
<body<?php if ($u->isLoggedIn()):?> style="margin-top: 48px;"<?php endif; ?>>

  <div class="page"><!-- Start Page -->
    <div class="page__wrap"><!-- Start Page Wrap -->

      <!-- Header -->
      
      <header class="header header--fixed background__black--transparent">
        <div class="grid__container">
          
          <div class="grid__row">
            <div class="grid__col--12 header__logo">
              <?php
                  $a = new GlobalArea('Logo');
                  $a->display($c);
                ?>
            </div>
            <div class="grid__col--2 header__navicon">
              <a class="navicon x" href="#">
                <div class="navicon__bars"></div>
              </a>
            </div>
            <div class="grid__col--2 float--right header__social">
              <?php
                  $a = new GlobalArea('Social media');
                  $a->display($c);
                ?>
            </div>
            <div class="grid__col--12">
              <nav class="header__nav" <?php if ($c->isEditMode()): ?>style="width: 100%;"<?php endif; ?>>
                <?php
                  $a = new GlobalArea('Navigation');
                  $a->display($c);
                ?>
              </nav>
            </div>
            
          </div>
        </div>
      </header>
      
      <section class="hero">
        <img class="background__image" src="<?php echo $image_src; ?>" alt="<?php echo $image_title; ?>" />
            <?php
              $a = new Area('Hero');
              $a->display($c);
            ?>
        </div>
      </section>