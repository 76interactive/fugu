<?php
  Loader::packageElement('header', 'fugu');
  $image = $c->getAttribute('page_image');
  if ($image) {
    $image_src = $image->getRelativePath();
    $image_title = $image->getTitle();
  } else {
    $image_src = $view->getThemePath() . '/assets/images/placeholder.png';
  }
?>

  <section class="section__introduction">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--8 grid__centered grid__col--sm--12">
        <?php
          $a = new Area('Introduction');
          $a->display($c);
        ?>
        </div>
      </div>
    </div>
  </section>

  <div class="section__content">
    <section id="projects">
      <div class="grid__container">
        <div class="grid__row row-eq-height">
          <div class="grid__col--6 grid__col--sm--12 section section__event--slider">
            <?php
              $a = new Area('Events slider');
              $a->display($c);
            ?>
          </div>
          <div class="grid__col--4 grid__col--sm--12 grid__shift--1 section section__event--txt">
            <div>
              <?php
                $a = new Area('Events text');
                $a->display($c);
              ?>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="grid__container">
        <div class="grid__row row-eq-height">
          <div class="grid__col--6 grid__col--sm--12 grid__shift--1 section section__producties--txt">
            <div>
              <?php
                $a = new Area('Producties text');
                $a->display($c);
              ?>
            </div>
          </div>
          <div class="grid__col--4 grid__col--sm--12 grid__shift--1 section section__producties--img">
            <div>
              <?php
                $a = new Area('Producties img');
                $a->display($c);
              ?>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="updates">
      <div class="grid__container">
        <div class="grid__row">
          <div class="grid__col--12 section section__updates--slider">
            <?php
              $a = new Area('Updates slider');
              $a->display($c);
            ?>
          </div>
        </div>
      </div>
    </section>

    <section id="fijnproevers">
      <div class="grid__container">
        <div class="grid__row row-eq-height">
          <div class="grid__col--6 grid__col--sm--12 section section__friends--img">
            <?php
              $a = new Area('Friends img');
              $a->display($c);
            ?>
          </div>
          <div class="grid__col--3 grid__shift--1 grid__col--sm--12 section section__friends--txt">
            <div>
              <?php
                $a = new Area('Friends text');
                $a->display($c);
              ?>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="over-ons">
      <div class="grid__container">
        <div class="grid__row row-eq-height">
          <div class="grid__col--6 grid__col--sm--12 section section__about--img">
            <?php
              $a = new Area('About img');
              $a->display($c);
            ?>
          </div>
          <div class="grid__col--3 grid__shift--1 grid__col--sm--12 section section__about-txt">
            <div>
              <?php
                $a = new Area('About text');
                $a->display($c);
              ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

<?php Loader::packageElement('footer', 'fugu'); ?>
